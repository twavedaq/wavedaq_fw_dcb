WaveDAQ Data Concentrator Board (DCB) - Device Tree
===================================================

This folder contains the device tree files for the Data Concentrator Board.
This device tree shows the resources available in the ZYNQ 7000 FPGA.
The file system-top.dts contains the toplevel device tree.
While pcw.dtsi and zynq-7000.dtsi contain standard devices of the ZYNQ PS, the pl.dtsi contains the devices implemented in the FPGAs PL.
The PL may contain custom IPs.
Most of the device tree was automatically generated using Xilinx Vivado XSDK (see https://forums.xilinx.com/t5/Embedded-Development-Tools/Generate-Device-tree-with-xilinx-sdk/td-p/959729)

Kernel Device Tree
------------------

The device tree of the kernel is located in a yocto layer repository.
If you are using the Main WaveDAQ repository with submodules, the submodule is located in

    /linux/yocto_layer

If you want to clone the repository independently, use

    git clone git@bitbucket.org:twavedaq/wavedaq_yocto_layer.git
    git clone https://elmarschmid@bitbucket.org/twavedaq/wavedaq_yocto_layer.git

Within the yocto layer repository, the device tree is located in

    /meta-wavedaq-dcb-bsp/recipes-kernel/linux/files/git/arch/arm/boot/dts

For instructions on how to build the kernel, see the documentation in the corresponding repositories.