--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Thu Oct 29 14:15:49 2020
--Host        : ems-rh7-lc.psi.ch running 64-bit Red Hat Enterprise Linux Server release 7.9 (Maipo)
--Command     : generate_target dcb_wrapper.bd
--Design      : dcb_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dcb_wrapper is
  port (
    AUX_OUT_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    AUX_OUT_P_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    BOARD_SELECT_N_I : in STD_LOGIC;
    BUSY_BPL_N_I : in STD_LOGIC;
    BUSY_BPL_N_O : out STD_LOGIC;
    BUSY_FCI_N_N_O : out STD_LOGIC;
    BUSY_FCI_N_P_O : out STD_LOGIC;
    BUS_CLK_SEL_O : out STD_LOGIC;
    CE_DIS_1_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CE_DIS_2_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_DIS_1_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_DIS_2_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_FPGA_N_I : in STD_LOGIC;
    CLK_FPGA_P_I : in STD_LOGIC;
    CLK_SEL_EXT_O : out STD_LOGIC;
    CLK_SEL_O : out STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DIR_EXT_TRG_IN_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    DIR_EXT_TRG_OUT_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    ETH0_SFP_rxn : in STD_LOGIC;
    ETH0_SFP_rxp : in STD_LOGIC;
    ETH0_SFP_txn : out STD_LOGIC;
    ETH0_SFP_txp : out STD_LOGIC;
    ETH1_SFP_rxn : in STD_LOGIC;
    ETH1_SFP_rxp : in STD_LOGIC;
    ETH1_SFP_txn : out STD_LOGIC;
    ETH1_SFP_txp : out STD_LOGIC;
    EXT_TRG_IN_I : in STD_LOGIC;
    EXT_TRG_OUT_O : out STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FLASH_SELECT_INIT_N_O : out STD_LOGIC;
    FS_INIT_N_I : in STD_LOGIC;
    GTREFCLK0_N_I : in STD_LOGIC;
    GTREFCLK0_P_I : in STD_LOGIC;
    INFO_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    LD_I : in STD_LOGIC;
    LED_B_N_O : out STD_LOGIC;
    LED_G_N_O : out STD_LOGIC;
    LED_R_N_O : out STD_LOGIC;
    MASTER_SPI_CLK_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_DE_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_MISO_I : in STD_LOGIC;
    MASTER_SPI_MOSI_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_TCB_SS_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_WDB_SS_N_O : out STD_LOGIC_VECTOR ( 15 downto 0 );
    MOSI_DIS_1_O : out STD_LOGIC;
    MOSI_DIS_2_O : out STD_LOGIC;
    MSCB_DATA_I : in STD_LOGIC;
    MSCB_DATA_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MSCB_DRV_EN_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_FPGA_BUS_N_O : out STD_LOGIC;
    SD_DATA_N_I : in STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_DATA_P_I : in STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_READY_N_O : out STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_READY_P_O : out STD_LOGIC_VECTOR ( 16 downto 0 );
    SFP_1_LOS_I : in STD_LOGIC;
    SFP_1_MOD_I : in STD_LOGIC;
    SFP_1_RS_O : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SFP_1_TX_DISABLE_O : out STD_LOGIC;
    SFP_1_TX_FAULT_I : in STD_LOGIC;
    SFP_2_LOS_I : in STD_LOGIC;
    SFP_2_MOD_I : in STD_LOGIC;
    SFP_2_RS_O : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SFP_2_TX_DISABLE_O : out STD_LOGIC;
    SFP_2_TX_FAULT_I : in STD_LOGIC;
    SLAVE_SPI_CLK_I : in STD_LOGIC;
    SLAVE_SPI_MISO_EN_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SLAVE_SPI_MISO_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SLAVE_SPI_MOSI_I : in STD_LOGIC;
    SPI_CS_FPGA_N_I : in STD_LOGIC;
    SPI_CS_N_O : out STD_LOGIC;
    SYNC_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SYNC_FCI_N_I : in STD_LOGIC;
    SYNC_FCI_P_I : in STD_LOGIC;
    TRG_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    TRIGGER_BPL_N_I : in STD_LOGIC;
    TRIGGER_BPL_P_I : in STD_LOGIC;
    TRIGGER_FCI_N_I : in STD_LOGIC;
    TRIGGER_FCI_P_I : in STD_LOGIC;
    TRIGGER_O : out STD_LOGIC;
    TR_INFO_BPL_N_I : in STD_LOGIC;
    TR_INFO_BPL_P_I : in STD_LOGIC;
    TR_INFO_FCI_N_I : in STD_LOGIC;
    TR_INFO_FCI_P_I : in STD_LOGIC;
    TR_INFO_O : out STD_LOGIC;
    TR_SYNC_O : out STD_LOGIC;
    WDB_CLK_N_I : in STD_LOGIC;
    WDB_CLK_P_I : in STD_LOGIC
  );
end dcb_wrapper;

architecture STRUCTURE of dcb_wrapper is
  component dcb is
  port (
    AUX_OUT_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    AUX_OUT_P_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    BOARD_SELECT_N_I : in STD_LOGIC;
    BUSY_BPL_N_I : in STD_LOGIC;
    BUSY_FCI_N_N_O : out STD_LOGIC;
    BUSY_FCI_N_P_O : out STD_LOGIC;
    BUSY_BPL_N_O : out STD_LOGIC;
    BUS_CLK_SEL_O : out STD_LOGIC;
    CE_DIS_1_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CE_DIS_2_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_DIS_1_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_DIS_2_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_FPGA_N_I : in STD_LOGIC;
    CLK_FPGA_P_I : in STD_LOGIC;
    CLK_SEL_EXT_O : out STD_LOGIC;
    CLK_SEL_O : out STD_LOGIC;
    SD_DATA_N_I : in STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_DATA_P_I : in STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_READY_N_O : out STD_LOGIC_VECTOR ( 16 downto 0 );
    SD_READY_P_O : out STD_LOGIC_VECTOR ( 16 downto 0 );
    DIR_EXT_TRG_IN_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    DIR_EXT_TRG_OUT_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    EXT_TRG_IN_I : in STD_LOGIC;
    EXT_TRG_OUT_O : out STD_LOGIC;
    FLASH_SELECT_INIT_N_O : out STD_LOGIC;
    FS_INIT_N_I : in STD_LOGIC;
    GTREFCLK0_N_I : in STD_LOGIC;
    GTREFCLK0_P_I : in STD_LOGIC;
    INFO_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    LD_I : in STD_LOGIC;
    LED_B_N_O : out STD_LOGIC;
    LED_G_N_O : out STD_LOGIC;
    LED_R_N_O : out STD_LOGIC;
    TRIGGER_BPL_N_I : in STD_LOGIC;
    TRIGGER_BPL_P_I : in STD_LOGIC;
    TR_INFO_BPL_N_I : in STD_LOGIC;
    TR_INFO_BPL_P_I : in STD_LOGIC;
    MASTER_SPI_CLK_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_DE_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_MISO_I : in STD_LOGIC;
    MASTER_SPI_MOSI_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_TCB_SS_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MASTER_SPI_WDB_SS_N_O : out STD_LOGIC_VECTOR ( 15 downto 0 );
    MOSI_DIS_1_O : out STD_LOGIC;
    MOSI_DIS_2_O : out STD_LOGIC;
    MSCB_DATA_I : in STD_LOGIC;
    MSCB_DATA_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    MSCB_DRV_EN_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    RESET_FPGA_BUS_N_O : out STD_LOGIC;
    SFP_1_LOS_I : in STD_LOGIC;
    SFP_1_MOD_I : in STD_LOGIC;
    SFP_1_RS_O : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SFP_1_TX_DISABLE_O : out STD_LOGIC;
    SFP_1_TX_FAULT_I : in STD_LOGIC;
    SFP_2_LOS_I : in STD_LOGIC;
    SFP_2_MOD_I : in STD_LOGIC;
    SFP_2_RS_O : out STD_LOGIC_VECTOR ( 1 downto 0 );
    SFP_2_TX_DISABLE_O : out STD_LOGIC;
    SFP_2_TX_FAULT_I : in STD_LOGIC;
    SLAVE_SPI_CLK_I : in STD_LOGIC;
    SLAVE_SPI_MISO_EN_N_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SLAVE_SPI_MISO_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SLAVE_SPI_MOSI_I : in STD_LOGIC;
    SPI_CS_FPGA_N_I : in STD_LOGIC;
    SPI_CS_N_O : out STD_LOGIC;
    TR_INFO_FCI_N_I : in STD_LOGIC;
    TR_INFO_FCI_P_I : in STD_LOGIC;
    SYNC_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    SYNC_FCI_N_I : in STD_LOGIC;
    SYNC_FCI_P_I : in STD_LOGIC;
    TRG_DIR_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    TRIGGER_FCI_N_I : in STD_LOGIC;
    TRIGGER_FCI_P_I : in STD_LOGIC;
    TRIGGER_O : out STD_LOGIC;
    TR_INFO_O : out STD_LOGIC;
    TR_SYNC_O : out STD_LOGIC;
    WDB_CLK_N_I : in STD_LOGIC;
    WDB_CLK_P_I : in STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    ETH1_SFP_rxn : in STD_LOGIC;
    ETH1_SFP_rxp : in STD_LOGIC;
    ETH1_SFP_txn : out STD_LOGIC;
    ETH1_SFP_txp : out STD_LOGIC;
    ETH0_SFP_rxn : in STD_LOGIC;
    ETH0_SFP_rxp : in STD_LOGIC;
    ETH0_SFP_txn : out STD_LOGIC;
    ETH0_SFP_txp : out STD_LOGIC
  );
  end component dcb;
begin
dcb_i: component dcb
     port map (
      AUX_OUT_N_O(0) => AUX_OUT_N_O(0),
      AUX_OUT_P_O(0) => AUX_OUT_P_O(0),
      BOARD_SELECT_N_I => BOARD_SELECT_N_I,
      BUSY_BPL_N_I => BUSY_BPL_N_I,
      BUSY_BPL_N_O => BUSY_BPL_N_O,
      BUSY_FCI_N_N_O => BUSY_FCI_N_N_O,
      BUSY_FCI_N_P_O => BUSY_FCI_N_P_O,
      BUS_CLK_SEL_O => BUS_CLK_SEL_O,
      CE_DIS_1_O(0) => CE_DIS_1_O(0),
      CE_DIS_2_O(0) => CE_DIS_2_O(0),
      CLK_DIS_1_O(0) => CLK_DIS_1_O(0),
      CLK_DIS_2_O(0) => CLK_DIS_2_O(0),
      CLK_FPGA_N_I => CLK_FPGA_N_I,
      CLK_FPGA_P_I => CLK_FPGA_P_I,
      CLK_SEL_EXT_O => CLK_SEL_EXT_O,
      CLK_SEL_O => CLK_SEL_O,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      DIR_EXT_TRG_IN_O(0) => DIR_EXT_TRG_IN_O(0),
      DIR_EXT_TRG_OUT_O(0) => DIR_EXT_TRG_OUT_O(0),
      ETH0_SFP_rxn => ETH0_SFP_rxn,
      ETH0_SFP_rxp => ETH0_SFP_rxp,
      ETH0_SFP_txn => ETH0_SFP_txn,
      ETH0_SFP_txp => ETH0_SFP_txp,
      ETH1_SFP_rxn => ETH1_SFP_rxn,
      ETH1_SFP_rxp => ETH1_SFP_rxp,
      ETH1_SFP_txn => ETH1_SFP_txn,
      ETH1_SFP_txp => ETH1_SFP_txp,
      EXT_TRG_IN_I => EXT_TRG_IN_I,
      EXT_TRG_OUT_O => EXT_TRG_OUT_O,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      FLASH_SELECT_INIT_N_O => FLASH_SELECT_INIT_N_O,
      FS_INIT_N_I => FS_INIT_N_I,
      GTREFCLK0_N_I => GTREFCLK0_N_I,
      GTREFCLK0_P_I => GTREFCLK0_P_I,
      INFO_DIR_O(0) => INFO_DIR_O(0),
      LD_I => LD_I,
      LED_B_N_O => LED_B_N_O,
      LED_G_N_O => LED_G_N_O,
      LED_R_N_O => LED_R_N_O,
      MASTER_SPI_CLK_O(0) => MASTER_SPI_CLK_O(0),
      MASTER_SPI_DE_N_O(0) => MASTER_SPI_DE_N_O(0),
      MASTER_SPI_MISO_I => MASTER_SPI_MISO_I,
      MASTER_SPI_MOSI_O(0) => MASTER_SPI_MOSI_O(0),
      MASTER_SPI_TCB_SS_N_O(0) => MASTER_SPI_TCB_SS_N_O(0),
      MASTER_SPI_WDB_SS_N_O(15 downto 0) => MASTER_SPI_WDB_SS_N_O(15 downto 0),
      MOSI_DIS_1_O => MOSI_DIS_1_O,
      MOSI_DIS_2_O => MOSI_DIS_2_O,
      MSCB_DATA_I => MSCB_DATA_I,
      MSCB_DATA_O(0) => MSCB_DATA_O(0),
      MSCB_DRV_EN_O(0) => MSCB_DRV_EN_O(0),
      RESET_FPGA_BUS_N_O => RESET_FPGA_BUS_N_O,
      SD_DATA_N_I(16 downto 0) => SD_DATA_N_I(16 downto 0),
      SD_DATA_P_I(16 downto 0) => SD_DATA_P_I(16 downto 0),
      SD_READY_N_O(16 downto 0) => SD_READY_N_O(16 downto 0),
      SD_READY_P_O(16 downto 0) => SD_READY_P_O(16 downto 0),
      SFP_1_LOS_I => SFP_1_LOS_I,
      SFP_1_MOD_I => SFP_1_MOD_I,
      SFP_1_RS_O(1 downto 0) => SFP_1_RS_O(1 downto 0),
      SFP_1_TX_DISABLE_O => SFP_1_TX_DISABLE_O,
      SFP_1_TX_FAULT_I => SFP_1_TX_FAULT_I,
      SFP_2_LOS_I => SFP_2_LOS_I,
      SFP_2_MOD_I => SFP_2_MOD_I,
      SFP_2_RS_O(1 downto 0) => SFP_2_RS_O(1 downto 0),
      SFP_2_TX_DISABLE_O => SFP_2_TX_DISABLE_O,
      SFP_2_TX_FAULT_I => SFP_2_TX_FAULT_I,
      SLAVE_SPI_CLK_I => SLAVE_SPI_CLK_I,
      SLAVE_SPI_MISO_EN_N_O(0) => SLAVE_SPI_MISO_EN_N_O(0),
      SLAVE_SPI_MISO_O(0) => SLAVE_SPI_MISO_O(0),
      SLAVE_SPI_MOSI_I => SLAVE_SPI_MOSI_I,
      SPI_CS_FPGA_N_I => SPI_CS_FPGA_N_I,
      SPI_CS_N_O => SPI_CS_N_O,
      SYNC_DIR_O(0) => SYNC_DIR_O(0),
      SYNC_FCI_N_I => SYNC_FCI_N_I,
      SYNC_FCI_P_I => SYNC_FCI_P_I,
      TRG_DIR_O(0) => TRG_DIR_O(0),
      TRIGGER_BPL_N_I => TRIGGER_BPL_N_I,
      TRIGGER_BPL_P_I => TRIGGER_BPL_P_I,
      TRIGGER_FCI_N_I => TRIGGER_FCI_N_I,
      TRIGGER_FCI_P_I => TRIGGER_FCI_P_I,
      TRIGGER_O => TRIGGER_O,
      TR_INFO_BPL_N_I => TR_INFO_BPL_N_I,
      TR_INFO_BPL_P_I => TR_INFO_BPL_P_I,
      TR_INFO_FCI_N_I => TR_INFO_FCI_N_I,
      TR_INFO_FCI_P_I => TR_INFO_FCI_P_I,
      TR_INFO_O => TR_INFO_O,
      TR_SYNC_O => TR_SYNC_O,
      WDB_CLK_N_I => WDB_CLK_N_I,
      WDB_CLK_P_I => WDB_CLK_P_I
    );
end STRUCTURE;
