create_clock -name clk_fpga_ext -period 12.5 [get_ports CLK_FPGA_P_I]
create_clock -name wdb_clk      -period 12.5 [get_ports WDB_CLK_P_I]
#create_clock -period 12.5 [get_ports EXT_CLK_P_I]
create_clock -name gtp_refclk   -period 8.0 [get_ports GTREFCLK0_P_I]

set_clock_groups -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2]
set_clock_groups -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks clk_out_240MHz_dcb_clk_wiz_0_0]
set_clock_groups -asynchronous -group [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2] -group [get_clocks clk_out_240MHz_dcb_clk_wiz_0_0]
set_clock_groups -asynchronous -group [get_clocks clk_fpga_ext] -group [get_clocks clk_out_240MHz_dcb_clk_wiz_0_0]

#set_false_path -from [get_pins */register_bank_0/axi_dcb_register_bank_0/U0/ipif_axi_inst/ipif_logic_inst/ipif_register_inst/reg_wr_reg[*][*]/C] -to [get_pins */hier_data_gen/axis_data_gen_0/U0/g_generators[0].i_data_gen_single/*/*]
set_false_path -from [get_pins {*/register_bank_0/axi_dcb_register_bank_0/U0/ipif_axi_inst/ipif_logic_inst/ipif_register_inst/*/C}] -to [get_pins {*/tr_sync_manager_0/U0/TR_SYNC_DELAY_INST/ODDR_inst/D*}]

# Inter Clock Constraints
# PS-PL clk 0 / PS-PL clk 2
set_max_delay -datapath_only -from clk_fpga_0 -to clk_fpga_2 8.0
set_max_delay -datapath_only -from clk_fpga_2 -to clk_fpga_0 20.0
# Ext FPGA clk / PS-PL clk 0
set_max_delay -datapath_only -from clk_fpga_0 -to [get_clocks clk_fpga_ext] 12.5
set_max_delay -datapath_only -from [get_clocks clk_fpga_ext] -to clk_fpga_0 20.0
## Ext FPGA clk / PS-PL clk 2
#set_max_delay -datapath_only -from clk_fpga_2 -to [get_clocks clk_fpga_ext] 12.5
#set_max_delay -datapath_only -from [get_clocks clk_fpga_ext] -to clk_fpga_2  8.0
# Ext FPGA clk / clkout0 (MMCM)
set_max_delay -datapath_only -from [get_clocks clk_fpga_ext] -to [get_clocks clkout0]  8.0
set_max_delay -datapath_only -from [get_clocks clkout0] -to [get_clocks clk_fpga_ext] 12.5
# Ext FPGA clk / clkout1 (MMCM)
set_max_delay -datapath_only -from [get_clocks clk_fpga_ext] -to [get_clocks clkout1] 16.0
set_max_delay -datapath_only -from [get_clocks clkout1] -to [get_clocks clk_fpga_ext] 12.5
# clkout0 (MMCM) / clkout1 (MMCM)
set_max_delay -datapath_only -from [get_clocks clkout0] -to [get_clocks clkout1] 16.0
set_max_delay -datapath_only -from [get_clocks clkout1] -to [get_clocks clkout0]  8.0
# PS-PL clk 0 / DivClk_Bufg_dcb_clk_wiz_0_2 (PLL)
set_max_delay -datapath_only -from clk_fpga_0 -to [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2] 12.5
set_max_delay -datapath_only -from [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2] -to clk_fpga_0 12.5
# PS-PL clk 2 / DivClk_Bufg_dcb_clk_wiz_0_2 (PLL)
set_max_delay -datapath_only -from clk_fpga_2 -to [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2] 12.5
set_max_delay -datapath_only -from [get_clocks DivClk_Bufg_dcb_clk_wiz_0_2] -to clk_fpga_2  8.0
# Ext FPGA clk / GTP Refclk
set_max_delay -datapath_only -from [get_clocks clk_fpga_ext] -to [get_clocks gtp_refclk]  8.0
set_max_delay -datapath_only -from [get_clocks gtp_refclk] -to [get_clocks clk_fpga_ext] 12.5

set_multicycle_path 4 -setup -from [get_cells {dcb_i/trigger_manager_0/U0/i_auto_trigger/count_msbs_reg[*]}] -to [get_cells {dcb_i/trigger_manager_0/U0/i_auto_trigger/count_msbs_reg[*]}]
set_multicycle_path 3 -hold  -from [get_cells {dcb_i/trigger_manager_0/U0/i_auto_trigger/count_msbs_reg[*]}] -to [get_cells {dcb_i/trigger_manager_0/U0/i_auto_trigger/count_msbs_reg[*]}]

set_property PACKAGE_PIN L5       [get_ports CLK_FPGA_P_I]; # PCB Net: CLK_FPGA_P
set_property IOSTANDARD  LVDS     [get_ports CLK_FPGA_P_I];
set_property PACKAGE_PIN L4       [get_ports CLK_FPGA_N_I]; # PCB Net: CLK_FPGA_N
set_property IOSTANDARD  LVDS     [get_ports CLK_FPGA_N_I];
set_property DIFF_TERM TRUE       [get_ports CLK_FPGA_P_I];

set_property PACKAGE_PIN AC13      [get_ports WDB_CLK_P_I]; # PCB Net: FPGA_CLK_P
set_property IOSTANDARD  LVDS_25   [get_ports WDB_CLK_P_I];
set_property PACKAGE_PIN AD13      [get_ports WDB_CLK_N_I]; # PCB Net: FPGA_CLK_N
set_property IOSTANDARD  LVDS_25   [get_ports WDB_CLK_N_I];
set_property DIFF_TERM TRUE        [get_ports WDB_CLK_P_I];

#set_property PACKAGE_PIN G7       [get_ports EXT_CLK_P_I]; # PCB Net: IN_EXT_CLK_P
#set_property IOSTANDARD  LVDS     [get_ports EXT_CLK_P_I];
#set_property PACKAGE_PIN F7       [get_ports EXT_CLK_N_I]; # PCB Net: IN_EXT_CLK_N
#set_property IOSTANDARD  LVDS     [get_ports EXT_CLK_N_I];
#set_property DIFF_TERM TRUE       [get_ports EXT_CLK_P_I];

#set_property PACKAGE_PIN K5        [get_ports CLK_LMK_FPGA_P_I]; # PCB Net: CLK_LMK_FPGA_P
#set_property IOSTANDARD  LVDS_25   [get_ports CLK_LMK_FPGA_P_I];
#set_property PACKAGE_PIN J5        [get_ports CLK_LMK_FPGA_N_I]; # PCB Net: CLK_LMK_FPGA_N
#set_property IOSTANDARD  LVDS_25   [get_ports CLK_LMK_FPGA_N_I];
#set_property DIFF_TERM TRUE        [get_ports CLK_LMK_FPGA_P_I];

set_property PACKAGE_PIN AC19     [get_ports LED_R_N_O]; # PCB Net: LED_RED
set_property IOSTANDARD  LVCMOS33 [get_ports LED_R_N_O];
set_property PACKAGE_PIN AA20     [get_ports LED_B_N_O]; # PCB Net: LED_BLUE
set_property IOSTANDARD  LVCMOS33 [get_ports LED_B_N_O];
set_property PACKAGE_PIN AB20     [get_ports LED_G_N_O]; # PCB Net: LED_GREEN
set_property IOSTANDARD  LVCMOS33 [get_ports LED_G_N_O];


# FCI Connector
set_property PACKAGE_PIN AC17     [get_ports TRIGGER_FCI_P_I]; # PCB Net: TRG_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TRIGGER_FCI_P_I];
set_property PULLDOWN true        [get_ports TRIGGER_FCI_P_I];
set_property PACKAGE_PIN AC16     [get_ports TRIGGER_FCI_N_I]; # PCB Net: TRG_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TRIGGER_FCI_N_I];
set_property PULLUP true          [get_ports TRIGGER_FCI_N_I];
set_property PACKAGE_PIN W16      [get_ports SYNC_FCI_P_I]; # PCB Net: SYNC_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SYNC_FCI_P_I];
set_property PULLDOWN true        [get_ports SYNC_FCI_P_I];
set_property PACKAGE_PIN W15      [get_ports SYNC_FCI_N_I]; # PCB Net: SYNC_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SYNC_FCI_N_I];
set_property PULLUP true          [get_ports SYNC_FCI_N_I];
set_property PACKAGE_PIN AB17     [get_ports BUSY_FCI_N_P_O]; # PCB Net: BUSY_P   # AD20 in DCB_A
set_property IOSTANDARD  LVDS_25  [get_ports BUSY_FCI_N_P_O];
set_property PULLDOWN true        [get_ports BUSY_FCI_N_P_O];
set_property PACKAGE_PIN AB16     [get_ports BUSY_FCI_N_N_O]; # PCB Net: BUSY_N   # AD21 in DCB_A
set_property IOSTANDARD  LVDS_25  [get_ports BUSY_FCI_N_N_O];
set_property PULLUP true          [get_ports BUSY_FCI_N_N_O];
set_property PACKAGE_PIN Y16      [get_ports TR_INFO_FCI_P_I]; # PCB Net: SPR_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TR_INFO_FCI_P_I];
set_property PULLDOWN true        [get_ports TR_INFO_FCI_P_I];
set_property PACKAGE_PIN Y15      [get_ports TR_INFO_FCI_N_I]; # PCB Net: SPR_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TR_INFO_FCI_N_I];
set_property PULLUP true          [get_ports TR_INFO_FCI_N_I];

#set_property PACKAGE_PIN Y17      [get_ports SPR1_FCI_P_I]; # PCB Net: SPR1_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR1_FCI_P_I];
#set_property PULLDOWN true        [get_ports SPR1_FCI_P_I];
#set_property PACKAGE_PIN AA17     [get_ports SPR1_FCI_N_I]; # PCB Net: SPR1_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR1_FCI_N_I];
#set_property PULLUP true          [get_ports SPR1_FCI_N_I];
#set_property PACKAGE_PIN AA15     [get_ports SPR2_FCI_P_I]; # PCB Net: SPR2_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR2_FCI_P_I];
#set_property PULLDOWN true        [get_ports SPR2_FCI_P_I];
#set_property PACKAGE_PIN AA14     [get_ports SPR2_FCI_N_I]; # PCB Net: SPR2_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR2_FCI_N_I];
#set_property PULLUP true          [get_ports SPR2_FCI_N_I];
#set_property PACKAGE_PIN AB15     [get_ports SPR3_FCI_P_I]; # PCB Net: SPR3_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR3_FCI_P_I];
#set_property PULLDOWN true        [get_ports SPR3_FCI_P_I];
#set_property PACKAGE_PIN AB14     [get_ports SPR3_FCI_N_I]; # PCB Net: SPR3_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR3_FCI_N_I];
#set_property PULLUP true          [get_ports SPR3_FCI_N_I];

# Trigger interface
# Bank 12 (2.5V)
set_property PACKAGE_PIN Y12      [get_ports TR_SYNC_O]; # PCB Net: TR_SYNC
set_property IOSTANDARD LVCMOS25  [get_ports TR_SYNC_O];
set_property PACKAGE_PIN AD25     [get_ports SYNC_DIR_O]; # PCB Net: SYNC_DIR
set_property IOSTANDARD LVCMOS33  [get_ports SYNC_DIR_O];
set_property PACKAGE_PIN AB12     [get_ports TRIGGER_O]; # PCB Net: TRIGGER
set_property IOSTANDARD LVCMOS25  [get_ports TRIGGER_O];
set_property PACKAGE_PIN AC24     [get_ports TRG_DIR_O]; # PCB Net: TRG_DIR
set_property IOSTANDARD LVCMOS33  [get_ports TRG_DIR_O];
set_property PACKAGE_PIN AA12     [get_ports TR_INFO_O]; # PCB Net: TR_INFO
set_property IOSTANDARD LVCMOS25  [get_ports TR_INFO_O];
set_property PACKAGE_PIN AD26     [get_ports INFO_DIR_O]; # PCB Net: INFO_DIR
set_property IOSTANDARD LVCMOS33  [get_ports INFO_DIR_O];


# Configuration control
# Bank 12 (2.5V)
set_property PACKAGE_PIN AB10     [get_ports RESET_FPGA_BUS_N_O]; # PCB Net: RESET_FPGA_BUS
set_property IOSTANDARD  LVCMOS25 [get_ports RESET_FPGA_BUS_N_O];
set_property PULLUP true          [get_ports RESET_FPGA_BUS_N_O];
#set_property IOB true             [get_ports RESET_FPGA_BUS_N_O]; # Not from FF
set_property PACKAGE_PIN AD15     [get_ports FLASH_SELECT_INIT_N_O]; # PCB Net: INIT_BUS
set_property IOSTANDARD LVCMOS25  [get_ports FLASH_SELECT_INIT_N_O];
set_property PULLUP true          [get_ports FLASH_SELECT_INIT_N_O];
#set_property IOB true             [get_ports FLASH_SELECT_INIT_N_O]; # Not from FF
set_property PACKAGE_PIN AA10     [get_ports SPI_CS_N_O]; # PCB Net: FS_BUS
set_property IOSTANDARD LVCMOS25  [get_ports SPI_CS_N_O];
set_property PULLUP true          [get_ports SPI_CS_N_O];
#set_property IOB true             [get_ports SPI_CS_N_O]; # Not from FF
# Bank 13 (3.3V)
set_property PACKAGE_PIN AB25     [get_ports FS_INIT_N_I]; # PCB Net: INIT_FPGA
set_property IOSTANDARD LVCMOS33  [get_ports FS_INIT_N_I];
set_property PACKAGE_PIN AB26     [get_ports BOARD_SELECT_N_I]; # PCB Net: BS_FPGA
set_property IOSTANDARD LVCMOS33  [get_ports BOARD_SELECT_N_I];


#set_property PACKAGE_PIN AF25     [get_ports SYNC_LMK_N_O]; # PCB Net: SYNC_LMK
#set_property IOSTANDARD LVCMOS33  [get_ports SYNC_LMK_N_O];
set_property PACKAGE_PIN AF24     [get_ports LD_I]; # PCB Net: LD
set_property IOSTANDARD LVCMOS33  [get_ports LD_I];
set_property PACKAGE_PIN AF18     [get_ports BUSY_BPL_N_I]; # PCB Net: BUSY_IN
set_property IOSTANDARD LVCMOS33  [get_ports BUSY_BPL_N_I];
set_property PACKAGE_PIN AE18     [get_ports BUSY_BPL_N_O]; # PCB Net: BUSY_OUT
set_property IOSTANDARD LVCMOS33  [get_ports BUSY_BPL_N_O];
set_property PULLUP true          [get_ports BUSY_BPL_N_O];
#set_property PACKAGE_PIN AD18     [get_ports ATTENTION_BPL_N_I]; # PCB Net: ATTENTION_IN
#set_property IOSTANDARD LVCMOS33  [get_ports ATTENTION_BPL_N_I];
#set_property PACKAGE_PIN AC18     [get_ports ATTENTION_BPL_N_O]; # PCB Net: ATTENTION_OUT
#set_property IOSTANDARD LVCMOS33  [get_ports ATTENTION_BPL_N_O];
set_property PACKAGE_PIN AD24     [get_ports CLK_SEL_O]; # PCB Net: CLK_SEL
set_property IOSTANDARD LVCMOS33  [get_ports CLK_SEL_O];
set_property PACKAGE_PIN AA23     [get_ports CLK_SEL_EXT_O]; # PCB Net: CLK_SEL_EXT
set_property IOSTANDARD LVCMOS33  [get_ports CLK_SEL_EXT_O];


# SFP Control
# SFP 2
set_property PACKAGE_PIN AD20     [get_ports SFP_2_RS_O[1]]; # PCB Net: SFP_2_RS1
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_RS_O[1]];
set_property PACKAGE_PIN AD21     [get_ports SFP_2_RS_O[0]]; # PCB Net: SFP_2_RS0
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_RS_O[0]];
set_property PACKAGE_PIN W18      [get_ports SFP_2_TX_DISABLE_O]; # PCB Net: SFP_2_TX_DISABLE
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_TX_DISABLE_O];
set_property PACKAGE_PIN V18      [get_ports SFP_2_TX_FAULT_I]; # PCB Net: SFP_2_TX_FAULT
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_TX_FAULT_I];
set_property PACKAGE_PIN Y18      [get_ports SFP_2_MOD_I]; # PCB Net: SFP_2_MOD
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_MOD_I];
set_property PACKAGE_PIN AA18     [get_ports SFP_2_LOS_I]; # PCB Net: SFP_2_LOS
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_LOS_I];
# SFP 1
set_property PACKAGE_PIN AC21     [get_ports SFP_1_RS_O[1]]; # PCB Net: SFP_1_RS1
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_RS_O[1]];
set_property PACKAGE_PIN AC22     [get_ports SFP_1_RS_O[0]]; # PCB Net: SFP_1_RS0
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_RS_O[0]];
set_property PACKAGE_PIN W19      [get_ports SFP_1_TX_DISABLE_O]; # PCB Net: SFP_1_TX_DISABLE
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_TX_DISABLE_O];
set_property PACKAGE_PIN V19      [get_ports SFP_1_TX_FAULT_I]; # PCB Net: SFP_1_TX_FAULT
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_TX_FAULT_I];
set_property PACKAGE_PIN W20      [get_ports SFP_1_MOD_I]; # PCB Net: SFP_1_MOD
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_MOD_I];
set_property PACKAGE_PIN Y20      [get_ports SFP_1_LOS_I]; # PCB Net: SFP_1_LOS
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_LOS_I];

# Ethernet
# Refclocks
set_property PACKAGE_PIN R6       [get_ports GTREFCLK0_P_I]; # PCB Net: -
set_property PACKAGE_PIN R5       [get_ports GTREFCLK0_N_I]; # PCB Net: -
#set_property PACKAGE_PIN U6       [get_ports GTREFCLK1_P_I]; # PCB Net: -
#set_property PACKAGE_PIN U5       [get_ports GTREFCLK1_N_I]; # PCB Net: -
# ETH0
set_property PACKAGE_PIN AB4      [get_ports ETH0_SFP_rxp];  # PCB Net: SFP_1_RX_P
set_property PACKAGE_PIN AB3      [get_ports ETH0_SFP_rxn];  # PCB Net: SFP_1_RX_N
set_property PACKAGE_PIN AA2      [get_ports ETH0_SFP_txp];  # PCB Net: SFP_1_TX_P
set_property PACKAGE_PIN AA1      [get_ports ETH0_SFP_txn];  # PCB Net: SFP_1_TX_N
# ETH1
set_property PACKAGE_PIN Y4       [get_ports ETH1_SFP_rxp];  # PCB Net: SFP_2_RX_P
set_property PACKAGE_PIN Y3       [get_ports ETH1_SFP_rxn];  # PCB Net: SFP_2_RX_N
set_property PACKAGE_PIN W2       [get_ports ETH1_SFP_txp];  # PCB Net: SFP_2_TX_P
set_property PACKAGE_PIN W1       [get_ports ETH1_SFP_txn];  # PCB Net: SFP_2_TX_N


# Clock Distributor Interface (Bank 34, 1.8V)
set_property PACKAGE_PIN J10      [get_ports CLK_DIS_1_O]; # PCB Net: CLK_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports CLK_DIS_1_O];
set_property PACKAGE_PIN H7       [get_ports CLK_DIS_2_O]; # PCB Net: CLK_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports CLK_DIS_2_O];
set_property PACKAGE_PIN H6       [get_ports CE_DIS_1_O]; # PCB Net: CE_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports CE_DIS_1_O];
set_property PACKAGE_PIN J9       [get_ports CE_DIS_2_O]; # PCB Net: CE_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports CE_DIS_2_O];
set_property PACKAGE_PIN J8       [get_ports MOSI_DIS_1_O]; # PCB Net: MOSI_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports MOSI_DIS_1_O];
set_property PACKAGE_PIN H8       [get_ports MOSI_DIS_2_O]; # PCB Net: MOSI_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports MOSI_DIS_2_O];


# MCX Connectors
set_property PACKAGE_PIN AF20     [get_ports EXT_TRG_IN_I];      # PCB Net: EXT_TRG_IN
set_property IOSTANDARD  LVCMOS33 [get_ports EXT_TRG_IN_I];
set_property PULLDOWN true        [get_ports EXT_TRG_IN_I];
set_property PACKAGE_PIN AE20     [get_ports DIR_EXT_TRG_IN_O];  # PCB Net: DIR_EXT_TRG_IN
set_property IOSTANDARD  LVCMOS33 [get_ports DIR_EXT_TRG_IN_O];
set_property PACKAGE_PIN AF19     [get_ports EXT_TRG_OUT_O];     # PCB Net: EXT_TRG_OUT
set_property IOSTANDARD  LVCMOS33 [get_ports EXT_TRG_OUT_O];
set_property PACKAGE_PIN AD19     [get_ports DIR_EXT_TRG_OUT_O]; # PCB Net: DIR_EXT_TRG_OUT
set_property IOSTANDARD  LVCMOS33 [get_ports DIR_EXT_TRG_OUT_O];

#MSCB TO BACKPLANE
set_property PACKAGE_PIN AF22    [get_ports MSCB_DATA_I]; # MSCB_RO
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DATA_I];
set_property PACKAGE_PIN AB21    [get_ports MSCB_DATA_O]; # MSCB_DI
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DATA_O];
set_property PACKAGE_PIN AE21    [get_ports MSCB_DRV_EN_O]; # MSCB_DE
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DRV_EN_O];

#SPI TO BACKPLANE (IF MASTER)
set_property PACKAGE_PIN AE22    [get_ports MASTER_SPI_MOSI_O]; # MASTER_SPI_MOSI
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_MOSI_O];
set_property PACKAGE_PIN AB22    [get_ports MASTER_SPI_MISO_I]; # MASTER_SPI_MISO
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_MISO_I];
set_property PACKAGE_PIN AF23    [get_ports MASTER_SPI_CLK_O]; # MASTER_SPI_CLK
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_CLK_O];
set_property PACKAGE_PIN AE23     [get_ports MASTER_SPI_DE_N_O]; # MASTER_SPI_DE
set_property IOSTANDARD LVCMOS33  [get_ports MASTER_SPI_DE_N_O];
set_property PULLDOWN true        [get_ports MASTER_SPI_DE_N_O];

#SPI FROM BACKPLANE (IF SALVE)
set_property PACKAGE_PIN AD23    [get_ports SLAVE_SPI_MOSI_I]; # MOSI_FPGA
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MOSI_I];
set_property PACKAGE_PIN AE25    [get_ports SPI_CS_FPGA_N_I]; # SPI_CS_FPGA
set_property IOSTANDARD LVCMOS33 [get_ports SPI_CS_FPGA_N_I];
set_property PACKAGE_PIN AE26    [get_ports SLAVE_SPI_CLK_I];
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_CLK_I];
set_property PACKAGE_PIN AB24    [get_ports SLAVE_SPI_MISO_O]; # SPI_MISO
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MISO_O];
set_property PACKAGE_PIN AC26    [get_ports SLAVE_SPI_MISO_EN_N_O]; # MISO_EN
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MISO_EN_N_O];
set_property PULLDOWN true       [get_ports SLAVE_SPI_MISO_EN_N_O];

#Board selects WDB and TCB
set_property PACKAGE_PIN AF15    [get_ports MASTER_SPI_WDB_SS_N_O[0]];  # BS_0
set_property PACKAGE_PIN AE17    [get_ports MASTER_SPI_WDB_SS_N_O[1]];  # BS_1
set_property PACKAGE_PIN AE15    [get_ports MASTER_SPI_WDB_SS_N_O[2]];  # BS_2
set_property PACKAGE_PIN AF17    [get_ports MASTER_SPI_WDB_SS_N_O[3]];  # BS_3
set_property PACKAGE_PIN AF14    [get_ports MASTER_SPI_WDB_SS_N_O[4]];  # BS_4
set_property PACKAGE_PIN AF13    [get_ports MASTER_SPI_WDB_SS_N_O[5]];  # BS_5
set_property PACKAGE_PIN AE10    [get_ports MASTER_SPI_WDB_SS_N_O[6]];  # BS_6
set_property PACKAGE_PIN AF10    [get_ports MASTER_SPI_WDB_SS_N_O[7]];  # BS_7
set_property PACKAGE_PIN AE11    [get_ports MASTER_SPI_WDB_SS_N_O[8]];  # BS_8
set_property PACKAGE_PIN AE13    [get_ports MASTER_SPI_WDB_SS_N_O[9]];  # BS_9
set_property PACKAGE_PIN AE12    [get_ports MASTER_SPI_WDB_SS_N_O[10]]; # BS_10
set_property PACKAGE_PIN AF12    [get_ports MASTER_SPI_WDB_SS_N_O[11]]; # BS_11
set_property PACKAGE_PIN AD10    [get_ports MASTER_SPI_WDB_SS_N_O[12]]; # BS_12
set_property PACKAGE_PIN AC12    [get_ports MASTER_SPI_WDB_SS_N_O[13]]; # BS_13
set_property PACKAGE_PIN AC11    [get_ports MASTER_SPI_WDB_SS_N_O[14]]; # BS_14
set_property PACKAGE_PIN AD11    [get_ports MASTER_SPI_WDB_SS_N_O[15]]; # BS_15
set_property PACKAGE_PIN AE16    [get_ports MASTER_SPI_TCB_SS_N_O];     # BS_TCB
set_property IOSTANDARD LVCMOS25 [get_ports MASTER_SPI_WDB_SS_N_O[*]];
#set_property IOB true            [get_ports MASTER_SPI_WDB_SS_N_O[*]]; # Not from FF
set_property PULLUP true         [get_ports MASTER_SPI_WDB_SS_N_O[*]];
set_property IOSTANDARD LVCMOS25 [get_ports MASTER_SPI_TCB_SS_N_O];
#set_property IOB true            [get_ports MASTER_SPI_TCB_SS_N_O]; # Not from FF
set_property PULLUP true         [get_ports MASTER_SPI_TCB_SS_N_O];



# CPLD interface
#set_property PACKAGE_PIN AC23    [get_ports CPLD_FPGA_D_I]; # CPLD_DIO
#set_property IOSTANDARD LVCMOS25 [get_ports CPLD_FPGA_D_I];
#set_property PACKAGE_PIN AB19    [get_ports FPGA_CPLD_CE_O]; # CPLD_CE
#set_property IOSTANDARD LVCMOS25 [get_ports FPGA_CPLD_CE_O];
#set_property PACKAGE_PIN AA19    [get_ports FPGA_CPLD_CLK_O]; # CPLD_CLK
#set_property IOSTANDARD LVCMOS25 [get_ports FPGA_CPLD_CLK_O];



# Miscellaneous
#BUS clock - Bank12 (2.5V)
set_property PACKAGE_PIN AA13    [get_ports BUS_CLK_SEL_O]; # BUS_CLK_SEL
set_property IOSTANDARD LVCMOS25 [get_ports BUS_CLK_SEL_O];
set_property PACKAGE_PIN W13     [get_ports AUX_OUT_P_O]; # AUX_OUT_P
set_property IOSTANDARD LVCMOS25 [get_ports AUX_OUT_P_O];
set_property PACKAGE_PIN Y13     [get_ports AUX_OUT_N_O]; # AUX_OUT_N
set_property IOSTANDARD LVCMOS25 [get_ports AUX_OUT_N_O];
#Bank 13 (3.3V)
#set_property PACKAGE_PIN AA22    [get_ports SIG_TP68_O]; # SIG_TP68
#set_property IOSTANDARD LVCMOS33 [get_ports SIG_TP68_O];



# SERDES Connections

# DCB
set_property PACKAGE_PIN B17    [get_ports SD_DATA_P_I[0]]; # WDB Slot0 : SD0_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[0]];
set_property PACKAGE_PIN A17    [get_ports SD_DATA_N_I[0]]; # WDB Slot0 : SD0_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[0]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[0]];
set_property PACKAGE_PIN A15    [get_ports SD_DATA_P_I[1]]; # WDB Slot1 : SD1_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[1]];
set_property PACKAGE_PIN A14    [get_ports SD_DATA_N_I[1]]; # WDB Slot1 : SD1_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[1]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[1]];
set_property PACKAGE_PIN A13    [get_ports SD_DATA_P_I[2]]; # WDB Slot2 : SD2_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[2]];
set_property PACKAGE_PIN A12    [get_ports SD_DATA_N_I[2]]; # WDB Slot2 : SD2_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[2]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[2]];
set_property PACKAGE_PIN E10    [get_ports SD_DATA_P_I[3]]; # WDB Slot3 : SD3_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[3]];
set_property PACKAGE_PIN D10    [get_ports SD_DATA_N_I[3]]; # WDB Slot3 : SD3_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[3]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[3]];
set_property PACKAGE_PIN B10    [get_ports SD_DATA_P_I[4]]; # WDB Slot4 : SD4_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[4]];
set_property PACKAGE_PIN A10    [get_ports SD_DATA_N_I[4]]; # WDB Slot4 : SD4_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[4]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[4]];
set_property PACKAGE_PIN A4     [get_ports SD_DATA_P_I[5]]; # WDB Slot5 : SD5_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[5]];
set_property PACKAGE_PIN A3     [get_ports SD_DATA_N_I[5]]; # WDB Slot5 : SD5_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[5]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[5]];
set_property PACKAGE_PIN B2     [get_ports SD_DATA_P_I[6]]; # WDB Slot6 : SD6_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[6]];
set_property PACKAGE_PIN A2     [get_ports SD_DATA_N_I[6]]; # WDB Slot6 : SD6_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[6]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[6]];
set_property PACKAGE_PIN C8     [get_ports SD_DATA_P_I[7]]; # WDB Slot7 : SD7_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[7]];
set_property PACKAGE_PIN C7     [get_ports SD_DATA_N_I[7]]; # WDB Slot7 : SD7_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[7]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[7]];
set_property PACKAGE_PIN C2     [get_ports SD_DATA_P_I[8]]; # WDB Slot8 : SD8_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[8]];
set_property PACKAGE_PIN B1     [get_ports SD_DATA_N_I[8]]; # WDB Slot8 : SD8_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[8]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[8]];
set_property PACKAGE_PIN F8     [get_ports SD_DATA_P_I[9]]; # WDB Slot9 : SD9_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[9]];
set_property PACKAGE_PIN E7     [get_ports SD_DATA_N_I[9]]; # WDB Slot9 : SD9_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[9]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[9]];
set_property PACKAGE_PIN B5     [get_ports SD_DATA_P_I[10]]; # WDB Slot10 : SD10_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[10]];
set_property PACKAGE_PIN B4     [get_ports SD_DATA_N_I[10]]; # WDB Slot10 : SD10_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[10]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[10]];
set_property PACKAGE_PIN C9     [get_ports SD_DATA_P_I[11]]; # WDB Slot11 : SD11_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[11]];
set_property PACKAGE_PIN B9     [get_ports SD_DATA_N_I[11]]; # WDB Slot11 : SD11_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[11]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[11]];
set_property PACKAGE_PIN F12    [get_ports SD_DATA_P_I[12]]; # WDB Slot12 : SD12_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[12]];
set_property PACKAGE_PIN E12    [get_ports SD_DATA_N_I[12]]; # WDB Slot12 : SD12_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[12]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[12]];
set_property PACKAGE_PIN C12    [get_ports SD_DATA_P_I[13]]; # WDB Slot13 : SD13_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[13]];
set_property PACKAGE_PIN B12    [get_ports SD_DATA_N_I[13]]; # WDB Slot13 : SD13_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[13]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[13]];
set_property PACKAGE_PIN D15    [get_ports SD_DATA_P_I[14]]; # WDB Slot14 : SD14_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[14]];
set_property PACKAGE_PIN D14    [get_ports SD_DATA_N_I[14]]; # WDB Slot14 : SD14_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[14]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[14]];
set_property PACKAGE_PIN E16    [get_ports SD_DATA_P_I[15]]; # WDB Slot15 : SD15_D0_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[15]];
set_property PACKAGE_PIN D16    [get_ports SD_DATA_N_I[15]]; # WDB Slot15 : SD15_D0_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[15]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[15]];
set_property PACKAGE_PIN N3     [get_ports SD_READY_P_O[0]]; # WDB Slot0 : SD0_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[0]];
set_property PACKAGE_PIN N2     [get_ports SD_READY_N_O[0]]; # WDB Slot0 : SD0_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[0]];
set_property PACKAGE_PIN M2     [get_ports SD_READY_P_O[1]]; # WDB Slot1 : SD1_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[1]];
set_property PACKAGE_PIN L2     [get_ports SD_READY_N_O[1]]; # WDB Slot1 : SD1_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[1]];
set_property PACKAGE_PIN K2     [get_ports SD_READY_P_O[2]]; # WDB Slot2 : SD2_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[2]];
set_property PACKAGE_PIN K1     [get_ports SD_READY_N_O[2]]; # WDB Slot2 : SD2_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[2]];
set_property PACKAGE_PIN J1     [get_ports SD_READY_P_O[3]]; # WDB Slot3 : SD3_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[3]];
set_property PACKAGE_PIN H1     [get_ports SD_READY_N_O[3]]; # WDB Slot3 : SD3_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[3]];
set_property PACKAGE_PIN H2     [get_ports SD_READY_P_O[4]]; # WDB Slot4 : SD4_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[4]];
set_property PACKAGE_PIN G1     [get_ports SD_READY_N_O[4]]; # WDB Slot4 : SD4_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[4]];
set_property PACKAGE_PIN G2     [get_ports SD_READY_P_O[5]]; # WDB Slot5 : SD5_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[5]];
set_property PACKAGE_PIN F2     [get_ports SD_READY_N_O[5]]; # WDB Slot5 : SD5_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[5]];
set_property PACKAGE_PIN D4     [get_ports SD_READY_P_O[6]]; # WDB Slot6 : SD6_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[6]];
set_property PACKAGE_PIN D3     [get_ports SD_READY_N_O[6]]; # WDB Slot6 : SD6_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[6]];
set_property PACKAGE_PIN D1     [get_ports SD_READY_P_O[7]]; # WDB Slot7 : SD7_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[7]];
set_property PACKAGE_PIN C1     [get_ports SD_READY_N_O[7]]; # WDB Slot7 : SD7_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[7]];
set_property PACKAGE_PIN E2     [get_ports SD_READY_P_O[8]]; # WDB Slot8 : SD8_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[8]];
set_property PACKAGE_PIN E1     [get_ports SD_READY_N_O[8]]; # WDB Slot8 : SD8_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[8]];
set_property PACKAGE_PIN F3     [get_ports SD_READY_P_O[9]]; # WDB Slot9 : SD9_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[9]];
set_property PACKAGE_PIN E3     [get_ports SD_READY_N_O[9]]; # WDB Slot9 : SD9_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[9]];
set_property PACKAGE_PIN G4     [get_ports SD_READY_P_O[10]]; # WDB Slot10 : SD10_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[10]];
set_property PACKAGE_PIN F4     [get_ports SD_READY_N_O[10]]; # WDB Slot10 : SD10_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[10]];
set_property PACKAGE_PIN H4     [get_ports SD_READY_P_O[11]]; # WDB Slot11 : SD11_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[11]];
set_property PACKAGE_PIN H3     [get_ports SD_READY_N_O[11]]; # WDB Slot11 : SD11_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[11]];
set_property PACKAGE_PIN J4     [get_ports SD_READY_P_O[12]]; # WDB Slot12 : SD12_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[12]];
set_property PACKAGE_PIN J3     [get_ports SD_READY_N_O[12]]; # WDB Slot12 : SD12_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[12]];
set_property PACKAGE_PIN L3     [get_ports SD_READY_P_O[13]]; # WDB Slot13 : SD13_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[13]];
set_property PACKAGE_PIN K3     [get_ports SD_READY_N_O[13]]; # WDB Slot13 : SD13_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[13]];
set_property PACKAGE_PIN N1     [get_ports SD_READY_P_O[14]]; # WDB Slot14 : SD14_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[14]];
set_property PACKAGE_PIN M1     [get_ports SD_READY_N_O[14]]; # WDB Slot14 : SD14_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[14]];
set_property PACKAGE_PIN N4     [get_ports SD_READY_P_O[15]]; # WDB Slot15 : SD15_D1_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[15]];
set_property PACKAGE_PIN M4     [get_ports SD_READY_N_O[15]]; # WDB Slot15 : SD15_D1_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[15]];

# TCB
set_property PACKAGE_PIN G16    [get_ports SD_DATA_P_I[16]]; # TCB (Slot 17): SD_TX_D_P
set_property IOSTANDARD LVDS    [get_ports SD_DATA_P_I[16]];
set_property PACKAGE_PIN G15    [get_ports SD_DATA_N_I[16]]; # TCB (Slot 17): SD_TX_D_N
set_property IOSTANDARD LVDS    [get_ports SD_DATA_N_I[16]];
set_property DIFF_TERM TRUE     [get_ports SD_DATA_N_I[16]];
set_property PACKAGE_PIN G14    [get_ports SD_READY_P_O[16]]; # TCB (Slot 17): SD_TX_CLK_P
set_property IOSTANDARD LVDS    [get_ports SD_READY_P_O[16]];
set_property PACKAGE_PIN F14    [get_ports SD_READY_N_O[16]]; # TCB (Slot 17): SD_TX_CLK_N
set_property IOSTANDARD LVDS    [get_ports SD_READY_N_O[16]];



## TCB direct trigger connection
set_property PACKAGE_PIN G10    [get_ports TRIGGER_BPL_P_I]; # SD_RX_D_P
set_property IOSTANDARD LVDS    [get_ports TRIGGER_BPL_P_I];
set_property PULLDOWN true      [get_ports TRIGGER_BPL_P_I];
set_property PACKAGE_PIN F10    [get_ports TRIGGER_BPL_N_I]; # SD_RX_D_N
set_property IOSTANDARD LVDS    [get_ports TRIGGER_BPL_N_I];
set_property PULLUP true        [get_ports TRIGGER_BPL_N_I];
set_property DIFF_TERM TRUE     [get_ports TRIGGER_BPL_N_I];
set_property PACKAGE_PIN G12    [get_ports TR_INFO_BPL_P_I]; # SD_RX_CLK_P
set_property IOSTANDARD LVDS    [get_ports TR_INFO_BPL_P_I];
set_property PULLDOWN true      [get_ports TR_INFO_BPL_P_I];
set_property PACKAGE_PIN G11    [get_ports TR_INFO_BPL_N_I]; # SD_RX_CLK_N
set_property IOSTANDARD LVDS    [get_ports TR_INFO_BPL_N_I];
set_property PULLUP true        [get_ports TR_INFO_BPL_N_I];
set_property DIFF_TERM TRUE     [get_ports TR_INFO_BPL_N_I];
