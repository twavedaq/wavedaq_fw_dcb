
################################################################
# This is a generated script based on design: dcb
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   common::send_msg_id "BD_TCL-1002" "WARNING" "This script was generated using Vivado <$scripts_vivado_version> without IP versions in the create_bd_cell commands, but is now being run in <$current_vivado_version> of Vivado. There may have been major IP version changes between Vivado <$scripts_vivado_version> and <$current_vivado_version>, which could impact the parameter settings of the IPs."

}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source dcb_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z030fbg676-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name dcb

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir ./bd

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:axi_quad_spi:*\
PSI:psi_3205:busy_manager:*\
xilinx.com:ip:clk_wiz:*\
PSI:PSI:util_led_ctrl:*\
xilinx.com:ip:processing_system7:*\
PSI:psi_3205:reset_generator:*\
PSI:psi_3205:sc_io:*\
xilinx.com:ip:system_ila:*\
PSI:psi_3205:tr_sync_manager:*\
PSI:psi_3205:trigger_manager:*\
PSI:psi_3205:util_build_info:*\
xilinx.com:ip:util_ds_buf:*\
xilinx.com:ip:xlconcat:*\
xilinx.com:ip:xlslice:*\
xilinx.com:ip:xlconstant:*\
psi.ch:PSI:clock_measure:*\
xilinx.com:ip:gig_ethernet_pcs_pma:*\
psi.ch:PSI:dma_pkt_sched_axi:*\
psi.ch:PSI:serdes_pkt_rcvr:*\
PSI:psi_3205:axi_dcb_register_bank:*\
PSI:psi_3205:util_mod_flag_reg:*\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: eth_constants_0
proc create_hier_cell_eth_constants_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_eth_constants_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 dout0
  create_bd_pin -dir O -from 0 -to 0 dout1
  create_bd_pin -dir O -from 0 -to 0 dout2
  create_bd_pin -dir O -from 4 -to 0 dout3
  create_bd_pin -dir O -from 15 -to 0 dout4
  create_bd_pin -dir O -from 0 -to 0 dout5
  create_bd_pin -dir O -from 0 -to 0 dout6

  # Create instance: const_eth_an_adv_cfg_vec, and set properties
  set const_eth_an_adv_cfg_vec [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_an_adv_cfg_vec ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0x86A1} \
   CONFIG.CONST_WIDTH {16} \
 ] $const_eth_an_adv_cfg_vec

  # Create instance: const_eth_basex_or_sgmii, and set properties
  set const_eth_basex_or_sgmii [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_basex_or_sgmii ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_basex_or_sgmii

  # Create instance: const_eth_cfg_vec, and set properties
  set const_eth_cfg_vec [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_cfg_vec ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth_cfg_vec

  # Create instance: const_eth_col, and set properties
  set const_eth_col [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_col ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_col

  # Create instance: const_eth_crs, and set properties
  set const_eth_crs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_crs ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_crs

  # Create instance: const_eth_gp_0, and set properties
  set const_eth_gp_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_gp_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_gp_0

  # Create instance: const_eth_gp_1, and set properties
  set const_eth_gp_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_gp_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_gp_1

  # Create port connections
  connect_bd_net -net const_eth_an_adv_cfg_vec_dout [get_bd_pins dout4] [get_bd_pins const_eth_an_adv_cfg_vec/dout]
  connect_bd_net -net const_eth_cfg_vec_dout [get_bd_pins dout3] [get_bd_pins const_eth_cfg_vec/dout]
  connect_bd_net -net const_eth_col_dout [get_bd_pins dout2] [get_bd_pins const_eth_col/dout]
  connect_bd_net -net const_eth_crs_dout [get_bd_pins dout5] [get_bd_pins const_eth_crs/dout]
  connect_bd_net -net const_eth_gp_0_dout [get_bd_pins dout0] [get_bd_pins const_eth_gp_0/dout]
  connect_bd_net -net const_eth_gp_1_dout [get_bd_pins dout1] [get_bd_pins const_eth_gp_1/dout]
  connect_bd_net -net const_eth_gp_1_dout1 [get_bd_pins dout6] [get_bd_pins const_eth_basex_or_sgmii/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: register_bank_0
proc create_hier_cell_register_bank_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_register_bank_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI


  # Create pins
  create_bd_pin -dir O -from 31 -to 0 AUTO_TRIGGER_PERIOD_O
  create_bd_pin -dir I BITSLIP_OK_00_I
  create_bd_pin -dir I BITSLIP_OK_01_I
  create_bd_pin -dir I BITSLIP_OK_02_I
  create_bd_pin -dir I BITSLIP_OK_03_I
  create_bd_pin -dir I BITSLIP_OK_04_I
  create_bd_pin -dir I BITSLIP_OK_05_I
  create_bd_pin -dir I BITSLIP_OK_06_I
  create_bd_pin -dir I BITSLIP_OK_07_I
  create_bd_pin -dir I BITSLIP_OK_08_I
  create_bd_pin -dir I BITSLIP_OK_09_I
  create_bd_pin -dir I BITSLIP_OK_10_I
  create_bd_pin -dir I BITSLIP_OK_11_I
  create_bd_pin -dir I BITSLIP_OK_12_I
  create_bd_pin -dir I BITSLIP_OK_13_I
  create_bd_pin -dir I BITSLIP_OK_14_I
  create_bd_pin -dir I BITSLIP_OK_15_I
  create_bd_pin -dir I BITSLIP_OK_17_I
  create_bd_pin -dir I BOARD_SELECT_N_I
  create_bd_pin -dir I BUSY_IN_N_I
  create_bd_pin -dir O BUS_CLK_SEL_O
  create_bd_pin -dir O CLK_SEL_EXT_O
  create_bd_pin -dir O CLK_SEL_O
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_00_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_01_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_02_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_03_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_04_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_05_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_06_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_07_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_08_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_09_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_10_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_11_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_12_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_13_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_14_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_15_I
  create_bd_pin -dir I -from 7 -to 0 CRC_ERRORS_17_I
  create_bd_pin -dir O DAQ_AUTO_O
  create_bd_pin -dir O DAQ_SOFT_TRIGGER_O
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_00_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_01_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_02_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_03_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_04_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_05_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_06_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_07_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_08_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_09_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_10_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_11_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_12_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_13_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_14_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_15_I
  create_bd_pin -dir I -from 7 -to 0 DATAGRAM_ERRORS_17_I
  create_bd_pin -dir I DELAY_OK_00_I
  create_bd_pin -dir I DELAY_OK_01_I
  create_bd_pin -dir I DELAY_OK_02_I
  create_bd_pin -dir I DELAY_OK_03_I
  create_bd_pin -dir I DELAY_OK_04_I
  create_bd_pin -dir I DELAY_OK_05_I
  create_bd_pin -dir I DELAY_OK_06_I
  create_bd_pin -dir I DELAY_OK_07_I
  create_bd_pin -dir I DELAY_OK_08_I
  create_bd_pin -dir I DELAY_OK_09_I
  create_bd_pin -dir I DELAY_OK_10_I
  create_bd_pin -dir I DELAY_OK_11_I
  create_bd_pin -dir I DELAY_OK_12_I
  create_bd_pin -dir I DELAY_OK_13_I
  create_bd_pin -dir I DELAY_OK_14_I
  create_bd_pin -dir I DELAY_OK_15_I
  create_bd_pin -dir I DELAY_OK_17_I
  create_bd_pin -dir I -from 47 -to 0 Din1
  create_bd_pin -dir O EXT_TRIGGER_OUT_ENABLE_O
  create_bd_pin -dir I FLASH_SELECT_N_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_00_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_01_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_02_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_03_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_04_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_05_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_06_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_07_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_08_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_09_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_10_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_11_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_12_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_13_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_14_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_15_I
  create_bd_pin -dir I -from 7 -to 0 FRAME_ERRORS_17_I
  create_bd_pin -dir I -from 7 -to 0 FW_BUILD_DAY_I
  create_bd_pin -dir I -from 7 -to 0 FW_BUILD_HOUR_I
  create_bd_pin -dir I -from 7 -to 0 FW_BUILD_MINUTE_I
  create_bd_pin -dir I -from 7 -to 0 FW_BUILD_MONTH_I
  create_bd_pin -dir I -from 7 -to 0 FW_BUILD_SECOND_I
  create_bd_pin -dir I -from 15 -to 0 FW_BUILD_YEAR_I
  create_bd_pin -dir I -from 31 -to 0 FW_GIT_HASH_TAG_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_00_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_01_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_02_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_03_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_04_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_05_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_06_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_07_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_08_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_09_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_10_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_11_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_12_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_13_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_14_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_15_I
  create_bd_pin -dir I IDLE_PATTERN_DETECT_17_I
  create_bd_pin -dir O ISERDES_RCVR_ERROR_COUNT_RST_O
  create_bd_pin -dir O ISERDES_RCVR_PACKET_COUNT_RST_O
  create_bd_pin -dir O ISERDES_RECEIVER_RESYNC_O
  create_bd_pin -dir O ISERDES_RECEIVER_RST_O
  create_bd_pin -dir I LD_I
  create_bd_pin -dir O LMK_SYNC_DCB_O
  create_bd_pin -dir O MOSI_DIS_2_O
  create_bd_pin -dir O RECONFIGURE_FPGA_O
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_0_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_10_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_11_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_12_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_13_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_14_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_15_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_17_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_1_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_2_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_3_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_4_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_5_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_6_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_7_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_8_I
  create_bd_pin -dir I -from 19 -to 0 SD_EYE_9_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_0_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_10_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_11_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_12_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_13_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_14_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_15_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_17_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_1_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_2_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_3_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_4_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_5_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_6_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_7_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_8_I
  create_bd_pin -dir I -from 31 -to 0 SD_PKT_CNT_9_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_0_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_10_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_11_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_12_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_13_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_14_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_15_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_17_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_1_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_2_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_3_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_4_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_5_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_6_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_7_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_8_I
  create_bd_pin -dir I -from 4 -to 0 SD_TAP_9_I
  create_bd_pin -dir I SERDES_CLK_MGR_LOCK_I
  create_bd_pin -dir O -from 4 -to 0 SYNC_DELAY_O
  create_bd_pin -dir I SYNC_DONE_00_I
  create_bd_pin -dir I SYNC_DONE_01_I
  create_bd_pin -dir I SYNC_DONE_02_I
  create_bd_pin -dir I SYNC_DONE_03_I
  create_bd_pin -dir I SYNC_DONE_04_I
  create_bd_pin -dir I SYNC_DONE_05_I
  create_bd_pin -dir I SYNC_DONE_06_I
  create_bd_pin -dir I SYNC_DONE_07_I
  create_bd_pin -dir I SYNC_DONE_08_I
  create_bd_pin -dir I SYNC_DONE_09_I
  create_bd_pin -dir I SYNC_DONE_10_I
  create_bd_pin -dir I SYNC_DONE_11_I
  create_bd_pin -dir I SYNC_DONE_12_I
  create_bd_pin -dir I SYNC_DONE_13_I
  create_bd_pin -dir I SYNC_DONE_14_I
  create_bd_pin -dir I SYNC_DONE_15_I
  create_bd_pin -dir I SYNC_DONE_17_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_00_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_01_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_02_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_03_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_04_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_05_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_06_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_07_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_08_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_09_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_10_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_11_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_12_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_13_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_14_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_15_I
  create_bd_pin -dir I -from 7 -to 0 SYNC_ERRORS_17_I
  create_bd_pin -dir I TRB_FLAG_NEW_I
  create_bd_pin -dir I TRB_FLAG_PARITY_ERROR_I
  create_bd_pin -dir I -from 15 -to 0 TRB_PARITY_ERROR_COUNT_I
  create_bd_pin -dir O TRIGGER_MGR_RST_O
  create_bd_pin -dir O TR_SYNC_BPL_O
  create_bd_pin -dir I WDB_CLK_MGR_LOCK_I
  create_bd_pin -dir O WDB_REFCLK_MGR_RST_O
  create_bd_pin -dir O WDB_SERDES_CLK_MGR_RST_O
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: axi_dcb_register_bank_0, and set properties
  set axi_dcb_register_bank_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:axi_dcb_register_bank axi_dcb_register_bank_0 ]

  # Create instance: util_mod_flag_lmk_ch, and set properties
  set util_mod_flag_lmk_ch [ create_bd_cell -type ip -vlnv PSI:psi_3205:util_mod_flag_reg util_mod_flag_lmk_ch ]
  set_property -dict [ list \
   CONFIG.CGN_REGISTER_WIDTH {8} \
 ] $util_mod_flag_lmk_ch

  # Create instance: xlconcat_lmk_ch_mod, and set properties
  set xlconcat_lmk_ch_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_lmk_ch_mod ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {8} \
 ] $xlconcat_lmk_ch_mod

  # Create instance: xlslice_lmk0_mod, and set properties
  set xlslice_lmk0_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk0_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk0_mod

  # Create instance: xlslice_lmk1_mod, and set properties
  set xlslice_lmk1_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk1_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk1_mod

  # Create instance: xlslice_lmk2_mod, and set properties
  set xlslice_lmk2_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk2_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk2_mod

  # Create instance: xlslice_lmk3_mod, and set properties
  set xlslice_lmk3_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk3_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk3_mod

  # Create instance: xlslice_lmk4_mod, and set properties
  set xlslice_lmk4_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk4_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {4} \
   CONFIG.DIN_TO {4} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk4_mod

  # Create instance: xlslice_lmk5_mod, and set properties
  set xlslice_lmk5_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk5_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {5} \
   CONFIG.DIN_TO {5} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk5_mod

  # Create instance: xlslice_lmk6_mod, and set properties
  set xlslice_lmk6_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk6_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {6} \
   CONFIG.DIN_TO {6} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk6_mod

  # Create instance: xlslice_lmk7_mod, and set properties
  set xlslice_lmk7_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk7_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {7} \
   CONFIG.DIN_TO {7} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk7_mod

  # Create instance: xlslice_trb_info_lsbs, and set properties
  set xlslice_trb_info_lsbs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_trb_info_lsbs ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {31} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {48} \
   CONFIG.DOUT_WIDTH {32} \
 ] $xlslice_trb_info_lsbs

  # Create instance: xlslice_trb_info_msbs, and set properties
  set xlslice_trb_info_msbs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_trb_info_msbs ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {47} \
   CONFIG.DIN_TO {32} \
   CONFIG.DIN_WIDTH {48} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_trb_info_msbs

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins axi_dcb_register_bank_0/S00_AXI]

  # Create port connections
  connect_bd_net -net BITSLIP_OK_00_I_1 [get_bd_pins BITSLIP_OK_00_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_00_I]
  connect_bd_net -net BITSLIP_OK_01_I_1 [get_bd_pins BITSLIP_OK_01_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_01_I]
  connect_bd_net -net BITSLIP_OK_02_I_1 [get_bd_pins BITSLIP_OK_02_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_02_I]
  connect_bd_net -net BITSLIP_OK_03_I_1 [get_bd_pins BITSLIP_OK_03_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_03_I]
  connect_bd_net -net BITSLIP_OK_04_I_1 [get_bd_pins BITSLIP_OK_04_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_04_I]
  connect_bd_net -net BITSLIP_OK_05_I_1 [get_bd_pins BITSLIP_OK_05_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_05_I]
  connect_bd_net -net BITSLIP_OK_06_I_1 [get_bd_pins BITSLIP_OK_06_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_06_I]
  connect_bd_net -net BITSLIP_OK_07_I_1 [get_bd_pins BITSLIP_OK_07_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_07_I]
  connect_bd_net -net BITSLIP_OK_08_I_1 [get_bd_pins BITSLIP_OK_08_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_08_I]
  connect_bd_net -net BITSLIP_OK_09_I_1 [get_bd_pins BITSLIP_OK_09_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_09_I]
  connect_bd_net -net BITSLIP_OK_10_I_1 [get_bd_pins BITSLIP_OK_10_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_10_I]
  connect_bd_net -net BITSLIP_OK_11_I_1 [get_bd_pins BITSLIP_OK_11_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_11_I]
  connect_bd_net -net BITSLIP_OK_12_I_1 [get_bd_pins BITSLIP_OK_12_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_12_I]
  connect_bd_net -net BITSLIP_OK_13_I_1 [get_bd_pins BITSLIP_OK_13_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_13_I]
  connect_bd_net -net BITSLIP_OK_14_I_1 [get_bd_pins BITSLIP_OK_14_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_14_I]
  connect_bd_net -net BITSLIP_OK_15_I_1 [get_bd_pins BITSLIP_OK_15_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_15_I]
  connect_bd_net -net BITSLIP_OK_17_I_1 [get_bd_pins BITSLIP_OK_17_I] [get_bd_pins axi_dcb_register_bank_0/BITSLIP_OK_17_I]
  connect_bd_net -net BOARD_SELECT_N_I_1 [get_bd_pins BOARD_SELECT_N_I] [get_bd_pins axi_dcb_register_bank_0/BOARD_SEL_I]
  connect_bd_net -net BUSY_IN_N_I_1 [get_bd_pins BUSY_IN_N_I] [get_bd_pins axi_dcb_register_bank_0/SYS_BUSY_I]
  connect_bd_net -net CRC_ERRORS_00_I_1 [get_bd_pins CRC_ERRORS_00_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_00_I]
  connect_bd_net -net CRC_ERRORS_01_I_1 [get_bd_pins CRC_ERRORS_01_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_01_I]
  connect_bd_net -net CRC_ERRORS_02_I_1 [get_bd_pins CRC_ERRORS_02_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_02_I]
  connect_bd_net -net CRC_ERRORS_03_I_1 [get_bd_pins CRC_ERRORS_03_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_03_I]
  connect_bd_net -net CRC_ERRORS_04_I_1 [get_bd_pins CRC_ERRORS_04_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_04_I]
  connect_bd_net -net CRC_ERRORS_05_I_1 [get_bd_pins CRC_ERRORS_05_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_05_I]
  connect_bd_net -net CRC_ERRORS_06_I_1 [get_bd_pins CRC_ERRORS_06_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_06_I]
  connect_bd_net -net CRC_ERRORS_07_I_1 [get_bd_pins CRC_ERRORS_07_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_07_I]
  connect_bd_net -net CRC_ERRORS_08_I_1 [get_bd_pins CRC_ERRORS_08_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_08_I]
  connect_bd_net -net CRC_ERRORS_09_I_1 [get_bd_pins CRC_ERRORS_09_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_09_I]
  connect_bd_net -net CRC_ERRORS_10_I_1 [get_bd_pins CRC_ERRORS_10_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_10_I]
  connect_bd_net -net CRC_ERRORS_11_I_1 [get_bd_pins CRC_ERRORS_11_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_11_I]
  connect_bd_net -net CRC_ERRORS_12_I_1 [get_bd_pins CRC_ERRORS_12_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_12_I]
  connect_bd_net -net CRC_ERRORS_13_I_1 [get_bd_pins CRC_ERRORS_13_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_13_I]
  connect_bd_net -net CRC_ERRORS_14_I_1 [get_bd_pins CRC_ERRORS_14_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_14_I]
  connect_bd_net -net CRC_ERRORS_15_I_1 [get_bd_pins CRC_ERRORS_15_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_15_I]
  connect_bd_net -net CRC_ERRORS_17_I_1 [get_bd_pins CRC_ERRORS_17_I] [get_bd_pins axi_dcb_register_bank_0/CRC_ERRORS_17_I]
  connect_bd_net -net DATAGRAM_ERRORS_00_I_1 [get_bd_pins DATAGRAM_ERRORS_00_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_00_I]
  connect_bd_net -net DATAGRAM_ERRORS_01_I_1 [get_bd_pins DATAGRAM_ERRORS_01_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_01_I]
  connect_bd_net -net DATAGRAM_ERRORS_02_I_1 [get_bd_pins DATAGRAM_ERRORS_02_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_02_I]
  connect_bd_net -net DATAGRAM_ERRORS_03_I_1 [get_bd_pins DATAGRAM_ERRORS_03_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_03_I]
  connect_bd_net -net DATAGRAM_ERRORS_04_I_1 [get_bd_pins DATAGRAM_ERRORS_04_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_04_I]
  connect_bd_net -net DATAGRAM_ERRORS_05_I_1 [get_bd_pins DATAGRAM_ERRORS_05_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_05_I]
  connect_bd_net -net DATAGRAM_ERRORS_06_I_1 [get_bd_pins DATAGRAM_ERRORS_06_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_06_I]
  connect_bd_net -net DATAGRAM_ERRORS_07_I_1 [get_bd_pins DATAGRAM_ERRORS_07_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_07_I]
  connect_bd_net -net DATAGRAM_ERRORS_08_I_1 [get_bd_pins DATAGRAM_ERRORS_08_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_08_I]
  connect_bd_net -net DATAGRAM_ERRORS_09_I_1 [get_bd_pins DATAGRAM_ERRORS_09_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_09_I]
  connect_bd_net -net DATAGRAM_ERRORS_10_I_1 [get_bd_pins DATAGRAM_ERRORS_10_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_10_I]
  connect_bd_net -net DATAGRAM_ERRORS_11_I_1 [get_bd_pins DATAGRAM_ERRORS_11_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_11_I]
  connect_bd_net -net DATAGRAM_ERRORS_12_I_1 [get_bd_pins DATAGRAM_ERRORS_12_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_12_I]
  connect_bd_net -net DATAGRAM_ERRORS_13_I_1 [get_bd_pins DATAGRAM_ERRORS_13_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_13_I]
  connect_bd_net -net DATAGRAM_ERRORS_14_I_1 [get_bd_pins DATAGRAM_ERRORS_14_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_14_I]
  connect_bd_net -net DATAGRAM_ERRORS_15_I_1 [get_bd_pins DATAGRAM_ERRORS_15_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_15_I]
  connect_bd_net -net DATAGRAM_ERRORS_17_I_1 [get_bd_pins DATAGRAM_ERRORS_17_I] [get_bd_pins axi_dcb_register_bank_0/DATAGRAM_ERRORS_17_I]
  connect_bd_net -net DELAY_OK_00_I_1 [get_bd_pins DELAY_OK_00_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_00_I]
  connect_bd_net -net DELAY_OK_01_I_1 [get_bd_pins DELAY_OK_01_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_01_I]
  connect_bd_net -net DELAY_OK_02_I_1 [get_bd_pins DELAY_OK_02_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_02_I]
  connect_bd_net -net DELAY_OK_03_I_1 [get_bd_pins DELAY_OK_03_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_03_I]
  connect_bd_net -net DELAY_OK_04_I_1 [get_bd_pins DELAY_OK_04_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_04_I]
  connect_bd_net -net DELAY_OK_05_I_1 [get_bd_pins DELAY_OK_05_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_05_I]
  connect_bd_net -net DELAY_OK_06_I_1 [get_bd_pins DELAY_OK_06_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_06_I]
  connect_bd_net -net DELAY_OK_07_I_1 [get_bd_pins DELAY_OK_07_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_07_I]
  connect_bd_net -net DELAY_OK_08_I_1 [get_bd_pins DELAY_OK_08_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_08_I]
  connect_bd_net -net DELAY_OK_09_I_1 [get_bd_pins DELAY_OK_09_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_09_I]
  connect_bd_net -net DELAY_OK_10_I_1 [get_bd_pins DELAY_OK_10_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_10_I]
  connect_bd_net -net DELAY_OK_11_I_1 [get_bd_pins DELAY_OK_11_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_11_I]
  connect_bd_net -net DELAY_OK_12_I_1 [get_bd_pins DELAY_OK_12_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_12_I]
  connect_bd_net -net DELAY_OK_13_I_1 [get_bd_pins DELAY_OK_13_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_13_I]
  connect_bd_net -net DELAY_OK_14_I_1 [get_bd_pins DELAY_OK_14_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_14_I]
  connect_bd_net -net DELAY_OK_15_I_1 [get_bd_pins DELAY_OK_15_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_15_I]
  connect_bd_net -net DELAY_OK_17_I_1 [get_bd_pins DELAY_OK_17_I] [get_bd_pins axi_dcb_register_bank_0/DELAY_OK_17_I]
  connect_bd_net -net FLASH_SELECT_N_I_1 [get_bd_pins FLASH_SELECT_N_I] [get_bd_pins axi_dcb_register_bank_0/FLASH_SEL_I]
  connect_bd_net -net FRAME_ERRORS_00_I_1 [get_bd_pins FRAME_ERRORS_00_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_00_I]
  connect_bd_net -net FRAME_ERRORS_01_I_1 [get_bd_pins FRAME_ERRORS_01_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_01_I]
  connect_bd_net -net FRAME_ERRORS_02_I_1 [get_bd_pins FRAME_ERRORS_02_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_02_I]
  connect_bd_net -net FRAME_ERRORS_03_I_1 [get_bd_pins FRAME_ERRORS_03_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_03_I]
  connect_bd_net -net FRAME_ERRORS_04_I_1 [get_bd_pins FRAME_ERRORS_04_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_04_I]
  connect_bd_net -net FRAME_ERRORS_05_I_1 [get_bd_pins FRAME_ERRORS_05_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_05_I]
  connect_bd_net -net FRAME_ERRORS_06_I_1 [get_bd_pins FRAME_ERRORS_06_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_06_I]
  connect_bd_net -net FRAME_ERRORS_07_I_1 [get_bd_pins FRAME_ERRORS_07_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_07_I]
  connect_bd_net -net FRAME_ERRORS_08_I_1 [get_bd_pins FRAME_ERRORS_08_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_08_I]
  connect_bd_net -net FRAME_ERRORS_09_I_1 [get_bd_pins FRAME_ERRORS_09_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_09_I]
  connect_bd_net -net FRAME_ERRORS_10_I_1 [get_bd_pins FRAME_ERRORS_10_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_10_I]
  connect_bd_net -net FRAME_ERRORS_11_I_1 [get_bd_pins FRAME_ERRORS_11_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_11_I]
  connect_bd_net -net FRAME_ERRORS_12_I_1 [get_bd_pins FRAME_ERRORS_12_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_12_I]
  connect_bd_net -net FRAME_ERRORS_13_I_1 [get_bd_pins FRAME_ERRORS_13_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_13_I]
  connect_bd_net -net FRAME_ERRORS_14_I_1 [get_bd_pins FRAME_ERRORS_14_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_14_I]
  connect_bd_net -net FRAME_ERRORS_15_I_1 [get_bd_pins FRAME_ERRORS_15_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_15_I]
  connect_bd_net -net FRAME_ERRORS_17_I_1 [get_bd_pins FRAME_ERRORS_17_I] [get_bd_pins axi_dcb_register_bank_0/FRAME_ERRORS_17_I]
  connect_bd_net -net FW_BUILD_DAY_I_1 [get_bd_pins FW_BUILD_DAY_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_DAY_I]
  connect_bd_net -net FW_BUILD_HOUR_I_1 [get_bd_pins FW_BUILD_HOUR_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_HOUR_I]
  connect_bd_net -net FW_BUILD_MINUTE_I_1 [get_bd_pins FW_BUILD_MINUTE_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_MINUTE_I]
  connect_bd_net -net FW_BUILD_MONTH_I_1 [get_bd_pins FW_BUILD_MONTH_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_MONTH_I]
  connect_bd_net -net FW_BUILD_SECOND_I_1 [get_bd_pins FW_BUILD_SECOND_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_SECOND_I]
  connect_bd_net -net FW_BUILD_YEAR_I_1 [get_bd_pins FW_BUILD_YEAR_I] [get_bd_pins axi_dcb_register_bank_0/FW_BUILD_YEAR_I]
  connect_bd_net -net FW_GIT_HASH_TAG_I_1 [get_bd_pins FW_GIT_HASH_TAG_I] [get_bd_pins axi_dcb_register_bank_0/FW_GIT_HASH_TAG_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_00_I_1 [get_bd_pins IDLE_PATTERN_DETECT_00_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_00_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_01_I_1 [get_bd_pins IDLE_PATTERN_DETECT_01_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_01_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_02_I_1 [get_bd_pins IDLE_PATTERN_DETECT_02_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_02_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_03_I_1 [get_bd_pins IDLE_PATTERN_DETECT_03_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_03_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_04_I_1 [get_bd_pins IDLE_PATTERN_DETECT_04_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_04_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_05_I_1 [get_bd_pins IDLE_PATTERN_DETECT_05_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_05_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_06_I_1 [get_bd_pins IDLE_PATTERN_DETECT_06_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_06_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_07_I_1 [get_bd_pins IDLE_PATTERN_DETECT_07_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_07_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_08_I_1 [get_bd_pins IDLE_PATTERN_DETECT_08_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_08_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_09_I_1 [get_bd_pins IDLE_PATTERN_DETECT_09_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_09_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_10_I_1 [get_bd_pins IDLE_PATTERN_DETECT_10_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_10_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_11_I_1 [get_bd_pins IDLE_PATTERN_DETECT_11_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_11_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_12_I_1 [get_bd_pins IDLE_PATTERN_DETECT_12_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_12_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_13_I_1 [get_bd_pins IDLE_PATTERN_DETECT_13_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_13_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_14_I_1 [get_bd_pins IDLE_PATTERN_DETECT_14_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_14_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_15_I_1 [get_bd_pins IDLE_PATTERN_DETECT_15_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_15_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_17_I_1 [get_bd_pins IDLE_PATTERN_DETECT_17_I] [get_bd_pins axi_dcb_register_bank_0/IDLE_PATTERN_DETECT_17_I]
  connect_bd_net -net LD_I_1 [get_bd_pins LD_I] [get_bd_pins axi_dcb_register_bank_0/LMK_PLL_LOCK_I]
  connect_bd_net -net SD_EYE_0_I_1 [get_bd_pins SD_EYE_0_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_0_I]
  connect_bd_net -net SD_EYE_10_I_1 [get_bd_pins SD_EYE_10_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_10_I]
  connect_bd_net -net SD_EYE_11_I_1 [get_bd_pins SD_EYE_11_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_11_I]
  connect_bd_net -net SD_EYE_12_I_1 [get_bd_pins SD_EYE_12_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_12_I]
  connect_bd_net -net SD_EYE_13_I_1 [get_bd_pins SD_EYE_13_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_13_I]
  connect_bd_net -net SD_EYE_14_I_1 [get_bd_pins SD_EYE_14_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_14_I]
  connect_bd_net -net SD_EYE_15_I_1 [get_bd_pins SD_EYE_15_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_15_I]
  connect_bd_net -net SD_EYE_17_I_1 [get_bd_pins SD_EYE_17_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_17_I]
  connect_bd_net -net SD_EYE_1_I_1 [get_bd_pins SD_EYE_1_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_1_I]
  connect_bd_net -net SD_EYE_2_I_1 [get_bd_pins SD_EYE_2_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_2_I]
  connect_bd_net -net SD_EYE_3_I_1 [get_bd_pins SD_EYE_3_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_3_I]
  connect_bd_net -net SD_EYE_4_I_1 [get_bd_pins SD_EYE_4_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_4_I]
  connect_bd_net -net SD_EYE_5_I_1 [get_bd_pins SD_EYE_5_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_5_I]
  connect_bd_net -net SD_EYE_6_I_1 [get_bd_pins SD_EYE_6_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_6_I]
  connect_bd_net -net SD_EYE_7_I_1 [get_bd_pins SD_EYE_7_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_7_I]
  connect_bd_net -net SD_EYE_8_I_1 [get_bd_pins SD_EYE_8_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_8_I]
  connect_bd_net -net SD_EYE_9_I_1 [get_bd_pins SD_EYE_9_I] [get_bd_pins axi_dcb_register_bank_0/SD_EYE_9_I]
  connect_bd_net -net SD_PKT_CNT_0_I_1 [get_bd_pins SD_PKT_CNT_0_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_0_I]
  connect_bd_net -net SD_PKT_CNT_10_I_1 [get_bd_pins SD_PKT_CNT_10_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_10_I]
  connect_bd_net -net SD_PKT_CNT_11_I_1 [get_bd_pins SD_PKT_CNT_11_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_11_I]
  connect_bd_net -net SD_PKT_CNT_12_I_1 [get_bd_pins SD_PKT_CNT_12_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_12_I]
  connect_bd_net -net SD_PKT_CNT_13_I_1 [get_bd_pins SD_PKT_CNT_13_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_13_I]
  connect_bd_net -net SD_PKT_CNT_14_I_1 [get_bd_pins SD_PKT_CNT_14_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_14_I]
  connect_bd_net -net SD_PKT_CNT_15_I_1 [get_bd_pins SD_PKT_CNT_15_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_15_I]
  connect_bd_net -net SD_PKT_CNT_17_I_1 [get_bd_pins SD_PKT_CNT_17_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_17_I]
  connect_bd_net -net SD_PKT_CNT_1_I_1 [get_bd_pins SD_PKT_CNT_1_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_1_I]
  connect_bd_net -net SD_PKT_CNT_2_I_1 [get_bd_pins SD_PKT_CNT_2_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_2_I]
  connect_bd_net -net SD_PKT_CNT_3_I_1 [get_bd_pins SD_PKT_CNT_3_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_3_I]
  connect_bd_net -net SD_PKT_CNT_4_I_1 [get_bd_pins SD_PKT_CNT_4_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_4_I]
  connect_bd_net -net SD_PKT_CNT_5_I_1 [get_bd_pins SD_PKT_CNT_5_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_5_I]
  connect_bd_net -net SD_PKT_CNT_6_I_1 [get_bd_pins SD_PKT_CNT_6_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_6_I]
  connect_bd_net -net SD_PKT_CNT_7_I_1 [get_bd_pins SD_PKT_CNT_7_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_7_I]
  connect_bd_net -net SD_PKT_CNT_8_I_1 [get_bd_pins SD_PKT_CNT_8_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_8_I]
  connect_bd_net -net SD_PKT_CNT_9_I_1 [get_bd_pins SD_PKT_CNT_9_I] [get_bd_pins axi_dcb_register_bank_0/SD_PKT_CNT_9_I]
  connect_bd_net -net SD_TAP_0_I_1 [get_bd_pins SD_TAP_0_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_0_I]
  connect_bd_net -net SD_TAP_10_I_1 [get_bd_pins SD_TAP_10_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_10_I]
  connect_bd_net -net SD_TAP_11_I_1 [get_bd_pins SD_TAP_11_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_11_I]
  connect_bd_net -net SD_TAP_12_I_1 [get_bd_pins SD_TAP_12_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_12_I]
  connect_bd_net -net SD_TAP_13_I_1 [get_bd_pins SD_TAP_13_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_13_I]
  connect_bd_net -net SD_TAP_14_I_1 [get_bd_pins SD_TAP_14_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_14_I]
  connect_bd_net -net SD_TAP_15_I_1 [get_bd_pins SD_TAP_15_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_15_I]
  connect_bd_net -net SD_TAP_17_I_1 [get_bd_pins SD_TAP_17_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_17_I]
  connect_bd_net -net SD_TAP_1_I_1 [get_bd_pins SD_TAP_1_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_1_I]
  connect_bd_net -net SD_TAP_2_I_1 [get_bd_pins SD_TAP_2_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_2_I]
  connect_bd_net -net SD_TAP_3_I_1 [get_bd_pins SD_TAP_3_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_3_I]
  connect_bd_net -net SD_TAP_4_I_1 [get_bd_pins SD_TAP_4_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_4_I]
  connect_bd_net -net SD_TAP_5_I_1 [get_bd_pins SD_TAP_5_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_5_I]
  connect_bd_net -net SD_TAP_6_I_1 [get_bd_pins SD_TAP_6_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_6_I]
  connect_bd_net -net SD_TAP_7_I_1 [get_bd_pins SD_TAP_7_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_7_I]
  connect_bd_net -net SD_TAP_8_I_1 [get_bd_pins SD_TAP_8_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_8_I]
  connect_bd_net -net SD_TAP_9_I_1 [get_bd_pins SD_TAP_9_I] [get_bd_pins axi_dcb_register_bank_0/SD_TAP_9_I]
  connect_bd_net -net SERDES_CLK_MGR_LOCK_I_1 [get_bd_pins SERDES_CLK_MGR_LOCK_I] [get_bd_pins axi_dcb_register_bank_0/SERDES_CLK_MGR_LOCK_I]
  connect_bd_net -net SYNC_DONE_00_I_1 [get_bd_pins SYNC_DONE_00_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_00_I]
  connect_bd_net -net SYNC_DONE_01_I_1 [get_bd_pins SYNC_DONE_01_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_01_I]
  connect_bd_net -net SYNC_DONE_02_I_1 [get_bd_pins SYNC_DONE_02_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_02_I]
  connect_bd_net -net SYNC_DONE_03_I_1 [get_bd_pins SYNC_DONE_03_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_03_I]
  connect_bd_net -net SYNC_DONE_04_I_1 [get_bd_pins SYNC_DONE_04_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_04_I]
  connect_bd_net -net SYNC_DONE_05_I_1 [get_bd_pins SYNC_DONE_05_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_05_I]
  connect_bd_net -net SYNC_DONE_06_I_1 [get_bd_pins SYNC_DONE_06_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_06_I]
  connect_bd_net -net SYNC_DONE_07_I_1 [get_bd_pins SYNC_DONE_07_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_07_I]
  connect_bd_net -net SYNC_DONE_08_I_1 [get_bd_pins SYNC_DONE_08_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_08_I]
  connect_bd_net -net SYNC_DONE_09_I_1 [get_bd_pins SYNC_DONE_09_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_09_I]
  connect_bd_net -net SYNC_DONE_10_I_1 [get_bd_pins SYNC_DONE_10_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_10_I]
  connect_bd_net -net SYNC_DONE_11_I_1 [get_bd_pins SYNC_DONE_11_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_11_I]
  connect_bd_net -net SYNC_DONE_12_I_1 [get_bd_pins SYNC_DONE_12_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_12_I]
  connect_bd_net -net SYNC_DONE_13_I_1 [get_bd_pins SYNC_DONE_13_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_13_I]
  connect_bd_net -net SYNC_DONE_14_I_1 [get_bd_pins SYNC_DONE_14_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_14_I]
  connect_bd_net -net SYNC_DONE_15_I_1 [get_bd_pins SYNC_DONE_15_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_15_I]
  connect_bd_net -net SYNC_DONE_17_I_1 [get_bd_pins SYNC_DONE_17_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_DONE_17_I]
  connect_bd_net -net SYNC_ERRORS_00_I_1 [get_bd_pins SYNC_ERRORS_00_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_00_I]
  connect_bd_net -net SYNC_ERRORS_01_I_1 [get_bd_pins SYNC_ERRORS_01_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_01_I]
  connect_bd_net -net SYNC_ERRORS_02_I_1 [get_bd_pins SYNC_ERRORS_02_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_02_I]
  connect_bd_net -net SYNC_ERRORS_03_I_1 [get_bd_pins SYNC_ERRORS_03_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_03_I]
  connect_bd_net -net SYNC_ERRORS_04_I_1 [get_bd_pins SYNC_ERRORS_04_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_04_I]
  connect_bd_net -net SYNC_ERRORS_05_I_1 [get_bd_pins SYNC_ERRORS_05_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_05_I]
  connect_bd_net -net SYNC_ERRORS_06_I_1 [get_bd_pins SYNC_ERRORS_06_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_06_I]
  connect_bd_net -net SYNC_ERRORS_07_I_1 [get_bd_pins SYNC_ERRORS_07_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_07_I]
  connect_bd_net -net SYNC_ERRORS_08_I_1 [get_bd_pins SYNC_ERRORS_08_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_08_I]
  connect_bd_net -net SYNC_ERRORS_09_I_1 [get_bd_pins SYNC_ERRORS_09_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_09_I]
  connect_bd_net -net SYNC_ERRORS_10_I_1 [get_bd_pins SYNC_ERRORS_10_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_10_I]
  connect_bd_net -net SYNC_ERRORS_11_I_1 [get_bd_pins SYNC_ERRORS_11_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_11_I]
  connect_bd_net -net SYNC_ERRORS_12_I_1 [get_bd_pins SYNC_ERRORS_12_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_12_I]
  connect_bd_net -net SYNC_ERRORS_13_I_1 [get_bd_pins SYNC_ERRORS_13_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_13_I]
  connect_bd_net -net SYNC_ERRORS_14_I_1 [get_bd_pins SYNC_ERRORS_14_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_14_I]
  connect_bd_net -net SYNC_ERRORS_15_I_1 [get_bd_pins SYNC_ERRORS_15_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_15_I]
  connect_bd_net -net SYNC_ERRORS_17_I_1 [get_bd_pins SYNC_ERRORS_17_I] [get_bd_pins axi_dcb_register_bank_0/SYNC_ERRORS_17_I]
  connect_bd_net -net axi_dcb_register_bank_0_AUTO_TRIGGER_PERIOD_O [get_bd_pins AUTO_TRIGGER_PERIOD_O] [get_bd_pins axi_dcb_register_bank_0/AUTO_TRIGGER_PERIOD_O]
  connect_bd_net -net axi_dcb_register_bank_0_BUS_CLK_SRC_SEL_O [get_bd_pins BUS_CLK_SEL_O] [get_bd_pins axi_dcb_register_bank_0/BUS_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_DAQ_AUTO_O [get_bd_pins DAQ_AUTO_O] [get_bd_pins axi_dcb_register_bank_0/DAQ_AUTO_O]
  connect_bd_net -net axi_dcb_register_bank_0_DAQ_SOFT_TRIGGER_O [get_bd_pins DAQ_SOFT_TRIGGER_O] [get_bd_pins axi_dcb_register_bank_0/DAQ_SOFT_TRIGGER_O]
  connect_bd_net -net axi_dcb_register_bank_0_DISTRIBUTOR_CLK_SRC_SEL_O [get_bd_pins MOSI_DIS_2_O] [get_bd_pins axi_dcb_register_bank_0/DISTRIBUTOR_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_EXT_CLK_IN_SEL_O [get_bd_pins CLK_SEL_EXT_O] [get_bd_pins axi_dcb_register_bank_0/EXT_CLK_IN_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_EXT_TRIGGER_OUT_ENABLE_O [get_bd_pins EXT_TRIGGER_OUT_ENABLE_O] [get_bd_pins axi_dcb_register_bank_0/EXT_TRIGGER_OUT_ENABLE_O]
  connect_bd_net -net axi_dcb_register_bank_0_ISERDES_RCVR_ERROR_COUNT_RST_O [get_bd_pins ISERDES_RCVR_ERROR_COUNT_RST_O] [get_bd_pins axi_dcb_register_bank_0/ISERDES_RCVR_ERROR_COUNT_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_ISERDES_RCVR_PACKET_COUNT_RST_O [get_bd_pins ISERDES_RCVR_PACKET_COUNT_RST_O] [get_bd_pins axi_dcb_register_bank_0/ISERDES_RCVR_PACKET_COUNT_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_ISERDES_RECEIVER_RESYNC_O [get_bd_pins ISERDES_RECEIVER_RESYNC_O] [get_bd_pins axi_dcb_register_bank_0/ISERDES_RECEIVER_RESYNC_O]
  connect_bd_net -net axi_dcb_register_bank_0_ISERDES_RECEIVER_RST_O [get_bd_pins ISERDES_RECEIVER_RST_O] [get_bd_pins axi_dcb_register_bank_0/ISERDES_RECEIVER_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_0_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_0_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In0]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_1_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_1_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In1]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_2_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_2_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In2]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_3_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_3_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In3]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_4_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_4_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In4]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_5_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_5_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In5]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_6_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_6_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In6]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_7_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_7_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In7]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_CLK_SRC_SEL_O [get_bd_pins CLK_SEL_O] [get_bd_pins axi_dcb_register_bank_0/LMK_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_MOD_FLAG_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_MOD_FLAG_REG_WR_STROBE_O] [get_bd_pins util_mod_flag_lmk_ch/RESET_I]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_SYNC_DCB_O [get_bd_pins LMK_SYNC_DCB_O] [get_bd_pins axi_dcb_register_bank_0/LMK_SYNC_DCB_O]
  connect_bd_net -net axi_dcb_register_bank_0_RECONFIGURE_FPGA_O [get_bd_pins RECONFIGURE_FPGA_O] [get_bd_pins axi_dcb_register_bank_0/RECONFIGURE_FPGA_O]
  connect_bd_net -net axi_dcb_register_bank_0_SYNC_DELAY_O [get_bd_pins SYNC_DELAY_O] [get_bd_pins axi_dcb_register_bank_0/SYNC_DELAY_O]
  connect_bd_net -net axi_dcb_register_bank_0_TRIGGER_MGR_RST_O [get_bd_pins TRIGGER_MGR_RST_O] [get_bd_pins axi_dcb_register_bank_0/TRIGGER_MGR_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_TR_SYNC_BPL_O [get_bd_pins TR_SYNC_BPL_O] [get_bd_pins axi_dcb_register_bank_0/TR_SYNC_BPL_O]
  connect_bd_net -net axi_dcb_register_bank_0_WDB_REFCLK_MGR_RST_O [get_bd_pins WDB_REFCLK_MGR_RST_O] [get_bd_pins axi_dcb_register_bank_0/WDB_REFCLK_MGR_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_WDB_SERDES_CLK_MGR_RST_O [get_bd_pins WDB_SERDES_CLK_MGR_RST_O] [get_bd_pins axi_dcb_register_bank_0/WDB_SERDES_CLK_MGR_RST_O]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins WDB_CLK_MGR_LOCK_I] [get_bd_pins axi_dcb_register_bank_0/WDB_CLK_MGR_LOCK_I]
  connect_bd_net -net trigger_manager_0_PERR_COUNT_O [get_bd_pins TRB_PARITY_ERROR_COUNT_I] [get_bd_pins axi_dcb_register_bank_0/TRB_PARITY_ERROR_COUNT_I]
  connect_bd_net -net trigger_manager_0_PERR_O [get_bd_pins TRB_FLAG_PARITY_ERROR_I] [get_bd_pins axi_dcb_register_bank_0/TRB_FLAG_PARITY_ERROR_I]
  connect_bd_net -net trigger_manager_0_TRIGGER_PDATA_O [get_bd_pins Din1] [get_bd_pins xlslice_trb_info_lsbs/Din] [get_bd_pins xlslice_trb_info_msbs/Din]
  connect_bd_net -net trigger_manager_0_VALID_O [get_bd_pins TRB_FLAG_NEW_I] [get_bd_pins axi_dcb_register_bank_0/TRB_FLAG_NEW_I]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins s00_axi_aclk] [get_bd_pins axi_dcb_register_bank_0/s00_axi_aclk] [get_bd_pins util_mod_flag_lmk_ch/CLK_I]
  connect_bd_net -net util_mod_flag_reg_0_MODIFIED_FLAG_O [get_bd_pins util_mod_flag_lmk_ch/MODIFIED_FLAG_O] [get_bd_pins xlslice_lmk0_mod/Din] [get_bd_pins xlslice_lmk1_mod/Din] [get_bd_pins xlslice_lmk2_mod/Din] [get_bd_pins xlslice_lmk3_mod/Din] [get_bd_pins xlslice_lmk4_mod/Din] [get_bd_pins xlslice_lmk5_mod/Din] [get_bd_pins xlslice_lmk6_mod/Din] [get_bd_pins xlslice_lmk7_mod/Din]
  connect_bd_net -net xlconcat_lmk_ch_mod_dout [get_bd_pins util_mod_flag_lmk_ch/MODIFIED_I] [get_bd_pins xlconcat_lmk_ch_mod/dout]
  connect_bd_net -net xlslice_lmk0_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_0_MOD_I] [get_bd_pins xlslice_lmk0_mod/Dout]
  connect_bd_net -net xlslice_lmk1_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_1_MOD_I] [get_bd_pins xlslice_lmk1_mod/Dout]
  connect_bd_net -net xlslice_lmk2_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_2_MOD_I] [get_bd_pins xlslice_lmk2_mod/Dout]
  connect_bd_net -net xlslice_lmk3_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_3_MOD_I] [get_bd_pins xlslice_lmk3_mod/Dout]
  connect_bd_net -net xlslice_lmk4_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_4_MOD_I] [get_bd_pins xlslice_lmk4_mod/Dout]
  connect_bd_net -net xlslice_lmk5_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_5_MOD_I] [get_bd_pins xlslice_lmk5_mod/Dout]
  connect_bd_net -net xlslice_lmk6_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_6_MOD_I] [get_bd_pins xlslice_lmk6_mod/Dout]
  connect_bd_net -net xlslice_lmk7_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_7_MOD_I] [get_bd_pins xlslice_lmk7_mod/Dout]
  connect_bd_net -net xlslice_peripheral_rst_n0_Dout [get_bd_pins s00_axi_aresetn] [get_bd_pins axi_dcb_register_bank_0/s00_axi_aresetn]
  connect_bd_net -net xlslice_trb_info_lsbs_Dout [get_bd_pins axi_dcb_register_bank_0/TRB_INFO_LSB_I] [get_bd_pins xlslice_trb_info_lsbs/Dout]
  connect_bd_net -net xlslice_trb_info_msbs_Dout [get_bd_pins axi_dcb_register_bank_0/TRB_INFO_MSB_I] [get_bd_pins xlslice_trb_info_msbs/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_serdes_dma_if
proc create_hier_cell_hier_serdes_dma_if { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_serdes_dma_if() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 M_Axi

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_Axi


  # Create pins
  create_bd_pin -dir I BitClk_Serdes_i
  create_bd_pin -dir O -from 16 -to 0 CrcOk_Dbg_o
  create_bd_pin -dir I -type clk DivClk_Serdes_i
  create_bd_pin -dir I IdelayCtrl_Refclk_i
  create_bd_pin -dir I Idelay_Refclk_Lock_i
  create_bd_pin -dir O -type intr Irq
  create_bd_pin -dir I -type rst M_Axi_Aresetn
  create_bd_pin -dir O -type rst M_Axi_o_Aresetn
  create_bd_pin -dir O -from 135 -to 0 ParallelData_Dbg_o
  create_bd_pin -dir I Resync_i
  create_bd_pin -dir I Rst_Errors_i
  create_bd_pin -dir I Rst_RcvPktCount_i
  create_bd_pin -dir I Rst_i
  create_bd_pin -dir I -from 16 -to 0 SD_DATA_N_I
  create_bd_pin -dir I -from 16 -to 0 SD_DATA_P_I
  create_bd_pin -dir O -from 16 -to 0 -type clk SD_READY_N_O
  create_bd_pin -dir O -from 16 -to 0 -type clk SD_READY_P_O
  create_bd_pin -dir I -type clk S_Axi_Aclk
  create_bd_pin -dir I -type rst S_Axi_Aresetn
  create_bd_pin -dir I SerdesClk_Lock_i
  create_bd_pin -dir O Slot00_BitslipOk_o
  create_bd_pin -dir O -from 3 -to 0 Slot00_Bytes_o
  create_bd_pin -dir O Slot00_CrcError_o
  create_bd_pin -dir O -from 7 -to 0 Slot00_CrcErrors_o
  create_bd_pin -dir O Slot00_DatagramError_o
  create_bd_pin -dir O -from 7 -to 0 Slot00_DatagramErrors_o
  create_bd_pin -dir O Slot00_DelayOk_o
  create_bd_pin -dir O Slot00_EOE_o
  create_bd_pin -dir O Slot00_FrameError_o
  create_bd_pin -dir O -from 7 -to 0 Slot00_FrameErrors_o
  create_bd_pin -dir O Slot00_LinkIdle_o
  create_bd_pin -dir O -from 15 -to 0 Slot00_PktBytes_o
  create_bd_pin -dir O Slot00_PktValid_o
  create_bd_pin -dir O -from 31 -to 0 Slot00_RcvPktCount_o
  create_bd_pin -dir O Slot00_SyncDone_o
  create_bd_pin -dir O Slot00_SyncError_o
  create_bd_pin -dir O -from 7 -to 0 Slot00_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot00_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot00_TestTap_o
  create_bd_pin -dir O Slot01_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot01_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot01_DatagramErrors_o
  create_bd_pin -dir O Slot01_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot01_FrameErrors_o
  create_bd_pin -dir O Slot01_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot01_RcvPktCount_o
  create_bd_pin -dir O Slot01_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot01_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot01_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot01_TestTap_o
  create_bd_pin -dir O Slot02_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot02_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot02_DatagramErrors_o
  create_bd_pin -dir O Slot02_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot02_FrameErrors_o
  create_bd_pin -dir O Slot02_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot02_RcvPktCount_o
  create_bd_pin -dir O Slot02_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot02_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot02_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot02_TestTap_o
  create_bd_pin -dir O Slot03_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot03_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot03_DatagramErrors_o
  create_bd_pin -dir O Slot03_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot03_FrameErrors_o
  create_bd_pin -dir O Slot03_LinkIdle_o
  create_bd_pin -dir O -from 15 -to 0 Slot03_PktBytes_o
  create_bd_pin -dir O -from 31 -to 0 Slot03_RcvPktCount_o
  create_bd_pin -dir O Slot03_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot03_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot03_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot03_TestTap_o
  create_bd_pin -dir O Slot04_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot04_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot04_DatagramErrors_o
  create_bd_pin -dir O Slot04_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot04_FrameErrors_o
  create_bd_pin -dir O Slot04_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot04_RcvPktCount_o
  create_bd_pin -dir O Slot04_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot04_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot04_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot04_TestTap_o
  create_bd_pin -dir O Slot05_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot05_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot05_DatagramErrors_o
  create_bd_pin -dir O Slot05_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot05_FrameErrors_o
  create_bd_pin -dir O Slot05_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot05_RcvPktCount_o
  create_bd_pin -dir O Slot05_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot05_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot05_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot05_TestTap_o
  create_bd_pin -dir O Slot06_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot06_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot06_DatagramErrors_o
  create_bd_pin -dir O Slot06_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot06_FrameErrors_o
  create_bd_pin -dir O Slot06_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot06_RcvPktCount_o
  create_bd_pin -dir O Slot06_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot06_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot06_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot06_TestTap_o
  create_bd_pin -dir O Slot07_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot07_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot07_DatagramErrors_o
  create_bd_pin -dir O Slot07_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot07_FrameErrors_o
  create_bd_pin -dir O Slot07_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot07_RcvPktCount_o
  create_bd_pin -dir O Slot07_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot07_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot07_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot07_TestTap_o
  create_bd_pin -dir O Slot08_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot08_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot08_DatagramErrors_o
  create_bd_pin -dir O Slot08_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot08_FrameErrors_o
  create_bd_pin -dir O Slot08_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot08_RcvPktCount_o
  create_bd_pin -dir O Slot08_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot08_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot08_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot08_TestTap_o
  create_bd_pin -dir O Slot09_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot09_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot09_DatagramErrors_o
  create_bd_pin -dir O Slot09_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot09_FrameErrors_o
  create_bd_pin -dir O Slot09_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot09_RcvPktCount_o
  create_bd_pin -dir O Slot09_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot09_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot09_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot09_TestTap_o
  create_bd_pin -dir O Slot10_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot10_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot10_DatagramErrors_o
  create_bd_pin -dir O Slot10_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot10_FrameErrors_o
  create_bd_pin -dir O Slot10_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot10_RcvPktCount_o
  create_bd_pin -dir O Slot10_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot10_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot10_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot10_TestTap_o
  create_bd_pin -dir O Slot11_BitslipOk_o
  create_bd_pin -dir O Slot11_CrcError_o
  create_bd_pin -dir O -from 7 -to 0 Slot11_CrcErrors_o
  create_bd_pin -dir O Slot11_DatagramError_o
  create_bd_pin -dir O -from 7 -to 0 Slot11_DatagramErrors_o
  create_bd_pin -dir O Slot11_DelayOk_o
  create_bd_pin -dir O Slot11_EOE_o
  create_bd_pin -dir O Slot11_FrameError_o
  create_bd_pin -dir O -from 7 -to 0 Slot11_FrameErrors_o
  create_bd_pin -dir O Slot11_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot11_RcvPktCount_o
  create_bd_pin -dir O Slot11_SyncDone_o
  create_bd_pin -dir O Slot11_SyncError_o
  create_bd_pin -dir O -from 7 -to 0 Slot11_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot11_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot11_TestTap_o
  create_bd_pin -dir O Slot12_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot12_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot12_DatagramErrors_o
  create_bd_pin -dir O Slot12_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot12_FrameErrors_o
  create_bd_pin -dir O Slot12_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot12_RcvPktCount_o
  create_bd_pin -dir O Slot12_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot12_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot12_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot12_TestTap_o
  create_bd_pin -dir O Slot13_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot13_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot13_DatagramErrors_o
  create_bd_pin -dir O Slot13_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot13_FrameErrors_o
  create_bd_pin -dir O Slot13_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot13_RcvPktCount_o
  create_bd_pin -dir O Slot13_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot13_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot13_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot13_TestTap_o
  create_bd_pin -dir O Slot14_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot14_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot14_DatagramErrors_o
  create_bd_pin -dir O Slot14_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot14_FrameErrors_o
  create_bd_pin -dir O Slot14_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot14_RcvPktCount_o
  create_bd_pin -dir O Slot14_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot14_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot14_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot14_TestTap_o
  create_bd_pin -dir O Slot15_BitslipOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot15_CrcErrors_o
  create_bd_pin -dir O -from 7 -to 0 Slot15_DatagramErrors_o
  create_bd_pin -dir O Slot15_DelayOk_o
  create_bd_pin -dir O -from 7 -to 0 Slot15_FrameErrors_o
  create_bd_pin -dir O Slot15_LinkIdle_o
  create_bd_pin -dir O -from 31 -to 0 Slot15_RcvPktCount_o
  create_bd_pin -dir O Slot15_SyncDone_o
  create_bd_pin -dir O -from 7 -to 0 Slot15_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot15_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot15_TestTap_o
  create_bd_pin -dir O Slot16_BitslipOk_o
  create_bd_pin -dir O -from 3 -to 0 Slot16_Bytes_o
  create_bd_pin -dir O Slot16_CrcError_o
  create_bd_pin -dir O -from 7 -to 0 Slot16_CrcErrors_o
  create_bd_pin -dir O Slot16_DatagramError_o
  create_bd_pin -dir O -from 7 -to 0 Slot16_DatagramErrors_o
  create_bd_pin -dir O Slot16_DelayOk_o
  create_bd_pin -dir O Slot16_EOE_o
  create_bd_pin -dir O Slot16_FrameError_o
  create_bd_pin -dir O -from 7 -to 0 Slot16_FrameErrors_o
  create_bd_pin -dir O Slot16_LinkIdle_o
  create_bd_pin -dir O -from 15 -to 0 Slot16_PktBytes_o
  create_bd_pin -dir O Slot16_PktValid_o
  create_bd_pin -dir O -from 31 -to 0 Slot16_RcvPktCount_o
  create_bd_pin -dir O Slot16_SyncDone_o
  create_bd_pin -dir O Slot16_SyncError_o
  create_bd_pin -dir O -from 7 -to 0 Slot16_SyncErrors_o
  create_bd_pin -dir O -from 19 -to 0 Slot16_TestEye_o
  create_bd_pin -dir O -from 4 -to 0 Slot16_TestTap_o

  # Create instance: dma_pkt_sched_axi_0, and set properties
  set dma_pkt_sched_axi_0 [ create_bd_cell -type ip -vlnv psi.ch:PSI:dma_pkt_sched_axi dma_pkt_sched_axi_0 ]
  set_property -dict [ list \
   CONFIG.C_S_Axi_ID_WIDTH {12} \
   CONFIG.MaxWindows_g {2} \
   CONFIG.Streams_g {17} \
 ] $dma_pkt_sched_axi_0

  # Create instance: serdes_pkt_rcvr_0, and set properties
  set serdes_pkt_rcvr_0 [ create_bd_cell -type ip -vlnv psi.ch:PSI:serdes_pkt_rcvr serdes_pkt_rcvr_0 ]
  set_property -dict [ list \
   CONFIG.BitOrder_cgn {LSB first} \
   CONFIG.IdelayCtrls_cgn {2} \
   CONFIG.IdlePattern_cgn {0x5A} \
   CONFIG.SDataRate_cgn {640} \
   CONFIG.Streams_cgn {17} \
 ] $serdes_pkt_rcvr_0

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_0_M03_AXI [get_bd_intf_pins S_Axi] [get_bd_intf_pins dma_pkt_sched_axi_0/S_Axi]
  connect_bd_intf_net -intf_net dma_pkt_sched_axi_0_M_Axi [get_bd_intf_pins M_Axi] [get_bd_intf_pins dma_pkt_sched_axi_0/M_Axi]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot00 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot00] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot00]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot01 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot01] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot01]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot02 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot02] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot02]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot03 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot03] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot03]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot04 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot04] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot04]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot05 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot05] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot05]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot06 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot06] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot06]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot07 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot07] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot07]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot08 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot08] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot08]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot09 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot09] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot09]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot10 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot10] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot10]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot11 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot11] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot11]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot12 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot12] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot12]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot13 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot13] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot13]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot14 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot14] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot14]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot15 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot15] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot15]
  connect_bd_intf_net -intf_net serdes_pkt_rcvr_0_Slot16 [get_bd_intf_pins dma_pkt_sched_axi_0/Slot16] [get_bd_intf_pins serdes_pkt_rcvr_0/Slot16]

  # Create port connections
  connect_bd_net -net DCB_DATA_N_I_1 [get_bd_pins SD_DATA_N_I] [get_bd_pins serdes_pkt_rcvr_0/SerialData_i_n]
  connect_bd_net -net DCB_DATA_P_I_1 [get_bd_pins SD_DATA_P_I] [get_bd_pins serdes_pkt_rcvr_0/SerialData_i_p]
  connect_bd_net -net FRAME_ERRORS_01_I_1 [get_bd_pins Slot01_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_FrameErrors_o]
  connect_bd_net -net In1_1 [get_bd_pins SD_READY_N_O] [get_bd_pins serdes_pkt_rcvr_0/ReadyToRcv_o_n]
  connect_bd_net -net M_Axi_Aresetn_1 [get_bd_pins M_Axi_Aresetn] [get_bd_pins dma_pkt_sched_axi_0/M_Axi_Aresetn]
  connect_bd_net -net Rst_RcvPktCount_i_1 [get_bd_pins Rst_RcvPktCount_i] [get_bd_pins serdes_pkt_rcvr_0/Rst_RcvPktCount_i]
  connect_bd_net -net Slot00_Resync_i_1 [get_bd_pins Resync_i] [get_bd_pins serdes_pkt_rcvr_0/Resync_i]
  connect_bd_net -net axis_dps_data_gen_0_Slot00_Bytes [get_bd_pins Slot00_Bytes_o] [get_bd_pins dma_pkt_sched_axi_0/Slot00_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot00_Bytes_o]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets axis_dps_data_gen_0_Slot00_Bytes]
  connect_bd_net -net axis_dps_data_gen_0_Slot00_EOE [get_bd_pins Slot00_EOE_o] [get_bd_pins dma_pkt_sched_axi_0/Slot00_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot00_EOE_o]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets axis_dps_data_gen_0_Slot00_EOE]
  connect_bd_net -net axis_dps_data_gen_0_Slot00_PktValid [get_bd_pins Slot00_PktValid_o] [get_bd_pins dma_pkt_sched_axi_0/Slot00_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot00_PktValid_o]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets axis_dps_data_gen_0_Slot00_PktValid]
  connect_bd_net -net clk_wiz_0_clk_out_200MHz [get_bd_pins IdelayCtrl_Refclk_i] [get_bd_pins serdes_pkt_rcvr_0/IdelayCtrl_Refclk_i]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins Idelay_Refclk_Lock_i] [get_bd_pins serdes_pkt_rcvr_0/Idelay_Refclk_Lock_i]
  connect_bd_net -net clk_wiz_1_clk_out_640MHz [get_bd_pins BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot00_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot01_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot02_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot03_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot04_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot05_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot06_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot07_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot08_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot09_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot10_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot11_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot12_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot13_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot14_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot15_BitClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot16_BitClk_Serdes_i]
  connect_bd_net -net clk_wiz_1_locked [get_bd_pins SerdesClk_Lock_i] [get_bd_pins serdes_pkt_rcvr_0/SerdesClk_Lock_i]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets clk_wiz_1_locked]
  connect_bd_net -net dma_pkt_sched_axi_0_Irq [get_bd_pins Irq] [get_bd_pins dma_pkt_sched_axi_0/Irq]
  connect_bd_net -net dma_pkt_sched_axi_0_M_Axi_o_Aresetn [get_bd_pins M_Axi_o_Aresetn] [get_bd_pins dma_pkt_sched_axi_0/M_Axi_o_Aresetn]
  connect_bd_net -net hier_sd_clocking_0_O [get_bd_pins DivClk_Serdes_i] [get_bd_pins dma_pkt_sched_axi_0/M_Axi_Aclk] [get_bd_pins serdes_pkt_rcvr_0/DivClk_Global_i] [get_bd_pins serdes_pkt_rcvr_0/Slot00_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot01_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot02_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot03_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot04_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot05_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot06_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot07_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot08_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot09_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot10_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot11_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot12_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot13_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot14_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot15_DivClk_Serdes_i] [get_bd_pins serdes_pkt_rcvr_0/Slot16_DivClk_Serdes_i]
  connect_bd_net -net ps_0_FCLK_CLK0 [get_bd_pins S_Axi_Aclk] [get_bd_pins dma_pkt_sched_axi_0/S_Axi_Aclk]
  connect_bd_net -net register_bank_0_ISERDES_RCVR_ERROR_COUNT_RST_O [get_bd_pins Rst_Errors_i] [get_bd_pins serdes_pkt_rcvr_0/Rst_Errors_i]
  connect_bd_net -net reset_generator_0_BUS_RST_N_O [get_bd_pins S_Axi_Aresetn] [get_bd_pins dma_pkt_sched_axi_0/S_Axi_Aresetn]
  connect_bd_net -net reset_generator_0_PERIPHERAL_RST_00_O [get_bd_pins Rst_i] [get_bd_pins serdes_pkt_rcvr_0/IdelayCtrl_Rst_i] [get_bd_pins serdes_pkt_rcvr_0/Rst_i]
  connect_bd_net -net serdes_pkt_rcvr_0_CrcOk_Dbg_o [get_bd_pins CrcOk_Dbg_o] [get_bd_pins serdes_pkt_rcvr_0/CrcOk_Dbg_o]
  connect_bd_net -net serdes_pkt_rcvr_0_ParallelData_Dbg_o [get_bd_pins ParallelData_Dbg_o] [get_bd_pins serdes_pkt_rcvr_0/ParallelData_Dbg_o]
  connect_bd_net -net serdes_pkt_rcvr_0_ReadyToRcv_o_p [get_bd_pins SD_READY_P_O] [get_bd_pins serdes_pkt_rcvr_0/ReadyToRcv_o_p]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_BitslipOk_o [get_bd_pins Slot00_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot00_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot00_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_CrcError_o [get_bd_pins Slot00_CrcError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_CrcError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_CrcErrors_o [get_bd_pins Slot00_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_DatagramError_o [get_bd_pins Slot00_DatagramError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_DatagramError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_DatagramErrors_o [get_bd_pins Slot00_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_DelayOk_o [get_bd_pins Slot00_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_FrameError_o [get_bd_pins Slot00_FrameError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_FrameError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_FrameErrors_o [get_bd_pins Slot00_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_LinkIdle_o [get_bd_pins Slot00_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_PktBytes_o [get_bd_pins Slot00_PktBytes_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_PktBytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_RcvPktCount_o [get_bd_pins Slot00_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_SyncError_o [get_bd_pins Slot00_SyncError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_SyncError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_SyncErrors_o [get_bd_pins Slot00_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_SyncStatus_o [get_bd_pins Slot00_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_TestEye_o [get_bd_pins Slot00_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_TestTap_o [get_bd_pins Slot00_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot00_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_BitslipOk_o [get_bd_pins Slot01_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot01_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot01_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot01_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot01_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_CrcErrors_o [get_bd_pins Slot01_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_DatagramErrors_o [get_bd_pins Slot01_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_DelayOk_o [get_bd_pins Slot01_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot01_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot01_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_LinkIdle_o [get_bd_pins Slot01_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot01_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot01_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_RcvPktCount_o [get_bd_pins Slot01_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_SyncDone_o [get_bd_pins Slot01_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_SyncErrors_o [get_bd_pins Slot01_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_TestEye_o [get_bd_pins Slot01_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_TestTap_o [get_bd_pins Slot01_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot01_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_BitslipOk_o [get_bd_pins Slot02_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot02_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot02_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot02_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot02_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_CrcErrors_o [get_bd_pins Slot02_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_DatagramErrors_o [get_bd_pins Slot02_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_DelayOk_o [get_bd_pins Slot02_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot02_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot02_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_FrameErrors_o [get_bd_pins Slot02_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_LinkIdle_o [get_bd_pins Slot02_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot02_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot02_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_RcvPktCount_o [get_bd_pins Slot02_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_SyncDone_o [get_bd_pins Slot02_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_SyncErrors_o [get_bd_pins Slot02_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_TestEye_o [get_bd_pins Slot02_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_TestTap_o [get_bd_pins Slot02_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot02_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_BitslipOk_o [get_bd_pins Slot03_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot03_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot03_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot03_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot03_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_CrcErrors_o [get_bd_pins Slot03_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_DatagramErrors_o [get_bd_pins Slot03_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_DelayOk_o [get_bd_pins Slot03_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot03_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot03_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_FrameErrors_o [get_bd_pins Slot03_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_LinkIdle_o [get_bd_pins Slot03_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot03_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot03_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_RcvPktCount_o [get_bd_pins Slot03_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_SyncDone_o [get_bd_pins Slot03_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_SyncErrors_o [get_bd_pins Slot03_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_TestEye_o [get_bd_pins Slot03_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_TestTap_o [get_bd_pins Slot03_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot03_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_BitslipOk_o [get_bd_pins Slot04_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot04_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot04_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot04_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot04_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_CrcErrors_o [get_bd_pins Slot04_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_DatagramErrors_o [get_bd_pins Slot04_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_DelayOk_o [get_bd_pins Slot04_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot04_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot04_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_FrameErrors_o [get_bd_pins Slot04_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_LinkIdle_o [get_bd_pins Slot04_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot04_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot04_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_RcvPktCount_o [get_bd_pins Slot04_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_SyncDone_o [get_bd_pins Slot04_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_SyncErrors_o [get_bd_pins Slot04_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_TestEye_o [get_bd_pins Slot04_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot04_TestTap_o [get_bd_pins Slot04_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot04_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_BitslipOk_o [get_bd_pins Slot05_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot05_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot05_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot05_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot05_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_CrcErrors_o [get_bd_pins Slot05_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_DatagramErrors_o [get_bd_pins Slot05_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_DelayOk_o [get_bd_pins Slot05_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot05_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot05_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_FrameErrors_o [get_bd_pins Slot05_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_LinkIdle_o [get_bd_pins Slot05_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot05_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot05_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_RcvPktCount_o [get_bd_pins Slot05_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_SyncDone_o [get_bd_pins Slot05_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_SyncErrors_o [get_bd_pins Slot05_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_TestEye_o [get_bd_pins Slot05_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot05_TestTap_o [get_bd_pins Slot05_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot05_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_BitslipOk_o [get_bd_pins Slot06_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot06_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot06_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot06_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot06_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_CrcErrors_o [get_bd_pins Slot06_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_DatagramErrors_o [get_bd_pins Slot06_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_DelayOk_o [get_bd_pins Slot06_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot06_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot06_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_FrameErrors_o [get_bd_pins Slot06_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_LinkIdle_o [get_bd_pins Slot06_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot06_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot06_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_RcvPktCount_o [get_bd_pins Slot06_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_SyncDone_o [get_bd_pins Slot06_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_SyncErrors_o [get_bd_pins Slot06_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_TestEye_o [get_bd_pins Slot06_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot06_TestTap_o [get_bd_pins Slot06_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot06_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_BitslipOk_o [get_bd_pins Slot07_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot07_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot07_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot07_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot07_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_CrcErrors_o [get_bd_pins Slot07_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_DatagramErrors_o [get_bd_pins Slot07_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_DelayOk_o [get_bd_pins Slot07_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot07_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot07_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_FrameErrors_o [get_bd_pins Slot07_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_LinkIdle_o [get_bd_pins Slot07_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot07_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot07_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_RcvPktCount_o [get_bd_pins Slot07_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_SyncDone_o [get_bd_pins Slot07_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_SyncErrors_o [get_bd_pins Slot07_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_TestEye_o [get_bd_pins Slot07_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot07_TestTap_o [get_bd_pins Slot07_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot07_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_BitslipOk_o [get_bd_pins Slot08_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot08_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot08_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot08_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot08_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_CrcErrors_o [get_bd_pins Slot08_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_DatagramErrors_o [get_bd_pins Slot08_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_DelayOk_o [get_bd_pins Slot08_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot08_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot08_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_FrameErrors_o [get_bd_pins Slot08_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_LinkIdle_o [get_bd_pins Slot08_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_PktBytes_o [get_bd_pins Slot03_PktBytes_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_PktBytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot08_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot08_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_RcvPktCount_o [get_bd_pins Slot08_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_SyncDone_o [get_bd_pins Slot08_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_SyncErrors_o [get_bd_pins Slot08_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_TestEye_o [get_bd_pins Slot08_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot08_TestTap_o [get_bd_pins Slot08_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot08_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_BitslipOk_o [get_bd_pins Slot09_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot09_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot09_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot09_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot09_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_CrcErrors_o [get_bd_pins Slot09_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_DatagramErrors_o [get_bd_pins Slot09_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_DelayOk_o [get_bd_pins Slot09_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot09_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot09_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_FrameErrors_o [get_bd_pins Slot09_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_LinkIdle_o [get_bd_pins Slot09_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot09_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot09_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_RcvPktCount_o [get_bd_pins Slot09_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_SyncDone_o [get_bd_pins Slot09_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_SyncErrors_o [get_bd_pins Slot09_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_TestEye_o [get_bd_pins Slot09_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot09_TestTap_o [get_bd_pins Slot09_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot09_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_BitslipOk_o [get_bd_pins Slot10_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot10_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot10_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot10_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot10_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_CrcErrors_o [get_bd_pins Slot10_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_DatagramErrors_o [get_bd_pins Slot10_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_DelayOk_o [get_bd_pins Slot10_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot10_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot10_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_FrameErrors_o [get_bd_pins Slot10_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_LinkIdle_o [get_bd_pins Slot10_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot10_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot10_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_RcvPktCount_o [get_bd_pins Slot10_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_SyncDone_o [get_bd_pins Slot10_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_SyncErrors_o [get_bd_pins Slot10_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_TestEye_o [get_bd_pins Slot10_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot10_TestTap_o [get_bd_pins Slot10_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot10_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_BitslipOk_o [get_bd_pins Slot11_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot11_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot11_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot11_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot11_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_CrcError_o [get_bd_pins Slot11_CrcError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_CrcError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_CrcErrors_o [get_bd_pins Slot11_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_DatagramError_o [get_bd_pins Slot11_DatagramError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_DatagramError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_DatagramErrors_o [get_bd_pins Slot11_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_DelayOk_o [get_bd_pins Slot11_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_EOE_o [get_bd_pins Slot11_EOE_o] [get_bd_pins dma_pkt_sched_axi_0/Slot11_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot11_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_FrameError_o [get_bd_pins Slot11_FrameError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_FrameError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_FrameErrors_o [get_bd_pins Slot11_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_LinkIdle_o [get_bd_pins Slot11_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot11_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot11_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_RcvPktCount_o [get_bd_pins Slot11_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_SyncDone_o [get_bd_pins Slot11_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_SyncError_o [get_bd_pins Slot11_SyncError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_SyncError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_SyncErrors_o [get_bd_pins Slot11_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_TestEye_o [get_bd_pins Slot11_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot11_TestTap_o [get_bd_pins Slot11_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot11_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_BitslipOk_o [get_bd_pins Slot12_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot12_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot12_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot12_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot12_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_CrcErrors_o [get_bd_pins Slot12_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_DatagramErrors_o [get_bd_pins Slot12_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_DelayOk_o [get_bd_pins Slot12_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot12_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot12_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_FrameErrors_o [get_bd_pins Slot12_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_LinkIdle_o [get_bd_pins Slot12_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot12_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot12_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_RcvPktCount_o [get_bd_pins Slot12_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_SyncDone_o [get_bd_pins Slot12_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_SyncErrors_o [get_bd_pins Slot12_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_TestEye_o [get_bd_pins Slot12_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot12_TestTap_o [get_bd_pins Slot12_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot12_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_BitslipOk_o [get_bd_pins Slot13_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot13_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot13_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot13_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot13_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_CrcErrors_o [get_bd_pins Slot13_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_DatagramErrors_o [get_bd_pins Slot13_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_DelayOk_o [get_bd_pins Slot13_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot13_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot13_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_FrameErrors_o [get_bd_pins Slot13_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_LinkIdle_o [get_bd_pins Slot13_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot13_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot13_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_RcvPktCount_o [get_bd_pins Slot13_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_SyncDone_o [get_bd_pins Slot13_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_SyncErrors_o [get_bd_pins Slot13_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_TestEye_o [get_bd_pins Slot13_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot13_TestTap_o [get_bd_pins Slot13_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot13_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_BitslipOk_o [get_bd_pins Slot14_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot14_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot14_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot14_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot14_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_CrcErrors_o [get_bd_pins Slot14_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_DatagramErrors_o [get_bd_pins Slot14_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_DelayOk_o [get_bd_pins Slot14_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot14_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot14_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_FrameErrors_o [get_bd_pins Slot14_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_LinkIdle_o [get_bd_pins Slot14_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot14_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot14_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_RcvPktCount_o [get_bd_pins Slot14_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_SyncDone_o [get_bd_pins Slot14_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_SyncErrors_o [get_bd_pins Slot14_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_TestEye_o [get_bd_pins Slot14_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot14_TestTap_o [get_bd_pins Slot14_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot14_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_BitslipOk_o [get_bd_pins Slot15_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_Bytes_o [get_bd_pins dma_pkt_sched_axi_0/Slot15_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot15_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot15_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot15_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_CrcErrors_o [get_bd_pins Slot15_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_DatagramErrors_o [get_bd_pins Slot15_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_DelayOk_o [get_bd_pins Slot15_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_EOE_o [get_bd_pins dma_pkt_sched_axi_0/Slot15_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot15_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_FrameErrors_o [get_bd_pins Slot15_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_LinkIdle_o [get_bd_pins Slot15_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_PktValid_o [get_bd_pins dma_pkt_sched_axi_0/Slot15_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot15_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_RcvPktCount_o [get_bd_pins Slot15_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_SyncDone_o [get_bd_pins Slot15_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_SyncErrors_o [get_bd_pins Slot15_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_TestEye_o [get_bd_pins Slot15_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot15_TestTap_o [get_bd_pins Slot15_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot15_TestTap_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_BitslipOk_o [get_bd_pins Slot16_BitslipOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_BitslipOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_Bytes_o [get_bd_pins Slot16_Bytes_o] [get_bd_pins dma_pkt_sched_axi_0/Slot16_Bytes] [get_bd_pins serdes_pkt_rcvr_0/Slot16_Bytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_Clk [get_bd_pins dma_pkt_sched_axi_0/Slot16_Clk] [get_bd_pins serdes_pkt_rcvr_0/Slot16_Clk]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_CrcError_o [get_bd_pins Slot16_CrcError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_CrcError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_CrcErrors_o [get_bd_pins Slot16_CrcErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_CrcErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_DatagramError_o [get_bd_pins Slot16_DatagramError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_DatagramError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_DatagramErrors_o [get_bd_pins Slot16_DatagramErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_DatagramErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_DelayOk_o [get_bd_pins Slot16_DelayOk_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_DelayOk_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_EOE_o [get_bd_pins Slot16_EOE_o] [get_bd_pins dma_pkt_sched_axi_0/Slot16_EOE] [get_bd_pins serdes_pkt_rcvr_0/Slot16_EOE_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_FrameError_o [get_bd_pins Slot16_FrameError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_FrameError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_FrameErrors_o [get_bd_pins Slot16_FrameErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_FrameErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_LinkIdle_o [get_bd_pins Slot16_LinkIdle_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_LinkIdle_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_PktBytes_o [get_bd_pins Slot16_PktBytes_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_PktBytes_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_PktValid_o [get_bd_pins Slot16_PktValid_o] [get_bd_pins dma_pkt_sched_axi_0/Slot16_PktValid] [get_bd_pins serdes_pkt_rcvr_0/Slot16_PktValid_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_RcvPktCount_o [get_bd_pins Slot16_RcvPktCount_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_RcvPktCount_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_SyncDone_o [get_bd_pins Slot16_SyncDone_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_SyncDone_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_SyncError_o [get_bd_pins Slot16_SyncError_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_SyncError_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_SyncErrors_o [get_bd_pins Slot16_SyncErrors_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_SyncErrors_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_TestEye_o [get_bd_pins Slot16_TestEye_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_TestEye_o]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot16_TestTap_o [get_bd_pins Slot16_TestTap_o] [get_bd_pins serdes_pkt_rcvr_0/Slot16_TestTap_o]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_ethernet_0
proc create_hier_cell_hier_ethernet_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_ethernet_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH0_SFP

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH1_SFP


  # Create pins
  create_bd_pin -dir I -type clk GTREFCLK0_N_I
  create_bd_pin -dir I -type clk GTREFCLK0_P_I
  create_bd_pin -dir O -from 0 -to 0 dout2
  create_bd_pin -dir O -from 0 -to 0 dout5
  create_bd_pin -dir O gmii_rx_dv
  create_bd_pin -dir O gmii_rx_dv1
  create_bd_pin -dir O gmii_rx_er
  create_bd_pin -dir O gmii_rx_er1
  create_bd_pin -dir O -from 7 -to 0 gmii_rxd
  create_bd_pin -dir O -from 7 -to 0 gmii_rxd1
  create_bd_pin -dir I gmii_tx_en
  create_bd_pin -dir I gmii_tx_en1
  create_bd_pin -dir I gmii_tx_er
  create_bd_pin -dir I gmii_tx_er1
  create_bd_pin -dir I -from 7 -to 0 gmii_txd
  create_bd_pin -dir I -from 7 -to 0 gmii_txd1
  create_bd_pin -dir O -type clk gtrefclk_bufg
  create_bd_pin -dir I -type clk independent_clock_bufg
  create_bd_pin -dir I -type clk mdc
  create_bd_pin -dir I -type clk mdc1
  create_bd_pin -dir I mdio_i
  create_bd_pin -dir I mdio_i1
  create_bd_pin -dir O mdio_o
  create_bd_pin -dir O mdio_o1
  create_bd_pin -dir I -type rst reset
  create_bd_pin -dir I -type rst reset1
  create_bd_pin -dir O -type clk rxuserclk
  create_bd_pin -dir O sgmii_clk_r
  create_bd_pin -dir O sgmii_clk_r1
  create_bd_pin -dir O -type clk userclk2

  # Create instance: const_eth0_phy_addr, and set properties
  set const_eth0_phy_addr [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth0_phy_addr ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth0_phy_addr

  # Create instance: const_eth1_phy_addr, and set properties
  set const_eth1_phy_addr [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth1_phy_addr ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth1_phy_addr

  # Create instance: eth_constants_0
  create_hier_cell_eth_constants_0 $hier_obj eth_constants_0

  # Create instance: gig_ethernet_pcs_pma_0, and set properties
  set gig_ethernet_pcs_pma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:gig_ethernet_pcs_pma gig_ethernet_pcs_pma_0 ]
  set_property -dict [ list \
   CONFIG.Auto_Negotiation {true} \
   CONFIG.EMAC_IF_TEMAC {GEM} \
   CONFIG.Ext_Management_Interface {false} \
   CONFIG.RxGmiiClkSrc {TXOUTCLK} \
   CONFIG.SGMII_PHY_Mode {false} \
   CONFIG.Standard {BOTH} \
   CONFIG.SupportLevel {Include_Shared_Logic_in_Core} \
   CONFIG.TransceiverControl {false} \
 ] $gig_ethernet_pcs_pma_0

  # Create instance: gig_ethernet_pcs_pma_1, and set properties
  set gig_ethernet_pcs_pma_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:gig_ethernet_pcs_pma gig_ethernet_pcs_pma_1 ]
  set_property -dict [ list \
   CONFIG.Auto_Negotiation {true} \
   CONFIG.EMAC_IF_TEMAC {GEM} \
   CONFIG.Ext_Management_Interface {false} \
   CONFIG.RxGmiiClkSrc {TXOUTCLK} \
   CONFIG.SGMII_PHY_Mode {false} \
   CONFIG.Standard {BOTH} \
   CONFIG.SupportLevel {Include_Shared_Logic_in_Example_Design} \
   CONFIG.TransceiverControl {false} \
 ] $gig_ethernet_pcs_pma_1

  # Create interface connections
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_0_sfp [get_bd_intf_pins ETH0_SFP] [get_bd_intf_pins gig_ethernet_pcs_pma_0/sfp]
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_1_sfp [get_bd_intf_pins ETH1_SFP] [get_bd_intf_pins gig_ethernet_pcs_pma_1/sfp]

  # Create port connections
  connect_bd_net -net GTREFCLK0_N_I_1 [get_bd_pins GTREFCLK0_N_I] [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_n]
  connect_bd_net -net GTREFCLK0_P_I_1 [get_bd_pins GTREFCLK0_P_I] [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_p]
  connect_bd_net -net const_eth0_phy_addr1_dout [get_bd_pins const_eth1_phy_addr/dout] [get_bd_pins gig_ethernet_pcs_pma_1/phyaddr]
  connect_bd_net -net const_eth0_phy_addr_dout [get_bd_pins const_eth0_phy_addr/dout] [get_bd_pins gig_ethernet_pcs_pma_0/phyaddr]
  connect_bd_net -net const_eth_an_adv_cfg_vec_dout [get_bd_pins eth_constants_0/dout4] [get_bd_pins gig_ethernet_pcs_pma_0/an_adv_config_vector] [get_bd_pins gig_ethernet_pcs_pma_1/an_adv_config_vector]
  connect_bd_net -net const_eth_cfg_vec_dout [get_bd_pins eth_constants_0/dout3] [get_bd_pins gig_ethernet_pcs_pma_0/configuration_vector] [get_bd_pins gig_ethernet_pcs_pma_1/configuration_vector]
  connect_bd_net -net const_eth_col_dout [get_bd_pins dout2] [get_bd_pins eth_constants_0/dout2]
  connect_bd_net -net const_eth_crs_dout [get_bd_pins dout5] [get_bd_pins eth_constants_0/dout5]
  connect_bd_net -net const_eth_gp_0_dout [get_bd_pins eth_constants_0/dout0] [get_bd_pins gig_ethernet_pcs_pma_0/an_adv_config_val] [get_bd_pins gig_ethernet_pcs_pma_0/an_restart_config] [get_bd_pins gig_ethernet_pcs_pma_0/configuration_valid] [get_bd_pins gig_ethernet_pcs_pma_1/an_adv_config_val] [get_bd_pins gig_ethernet_pcs_pma_1/an_restart_config] [get_bd_pins gig_ethernet_pcs_pma_1/configuration_valid]
  connect_bd_net -net const_eth_signal_detect_dout [get_bd_pins eth_constants_0/dout1] [get_bd_pins gig_ethernet_pcs_pma_0/signal_detect] [get_bd_pins gig_ethernet_pcs_pma_1/signal_detect]
  connect_bd_net -net eth_constants_0_dout6 [get_bd_pins eth_constants_0/dout6] [get_bd_pins gig_ethernet_pcs_pma_0/basex_or_sgmii] [get_bd_pins gig_ethernet_pcs_pma_1/basex_or_sgmii]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_dv [get_bd_pins gmii_rx_dv] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rx_dv]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_er [get_bd_pins gmii_rx_er] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rx_er]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rxd [get_bd_pins gmii_rxd] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rxd]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gt0_qplloutclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gt0_qplloutclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gt0_qplloutclk_in]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gt0_qplloutrefclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gt0_qplloutrefclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gt0_qplloutrefclk_in]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_bufg_out [get_bd_pins gtrefclk_bufg] [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_bufg_out] [get_bd_pins gig_ethernet_pcs_pma_1/gtrefclk_bufg]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gtrefclk]
  connect_bd_net -net gig_ethernet_pcs_pma_0_mdio_o [get_bd_pins mdio_o] [get_bd_pins gig_ethernet_pcs_pma_0/mdio_o]
  connect_bd_net -net gig_ethernet_pcs_pma_0_mmcm_locked_out [get_bd_pins gig_ethernet_pcs_pma_0/mmcm_locked_out] [get_bd_pins gig_ethernet_pcs_pma_1/mmcm_locked]
  connect_bd_net -net gig_ethernet_pcs_pma_0_pma_reset_out [get_bd_pins gig_ethernet_pcs_pma_0/pma_reset_out] [get_bd_pins gig_ethernet_pcs_pma_1/pma_reset]
  connect_bd_net -net gig_ethernet_pcs_pma_0_sgmii_clk_r [get_bd_pins sgmii_clk_r] [get_bd_pins gig_ethernet_pcs_pma_0/sgmii_clk_r]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk2_out [get_bd_pins userclk2] [get_bd_pins gig_ethernet_pcs_pma_0/userclk2_out] [get_bd_pins gig_ethernet_pcs_pma_1/userclk2]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk_out [get_bd_pins rxuserclk] [get_bd_pins gig_ethernet_pcs_pma_0/userclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/rxuserclk] [get_bd_pins gig_ethernet_pcs_pma_1/rxuserclk2] [get_bd_pins gig_ethernet_pcs_pma_1/userclk]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_dv [get_bd_pins gmii_rx_dv1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rx_dv]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_er [get_bd_pins gmii_rx_er1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rx_er]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rxd [get_bd_pins gmii_rxd1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rxd]
  connect_bd_net -net gig_ethernet_pcs_pma_1_mdio_o [get_bd_pins mdio_o1] [get_bd_pins gig_ethernet_pcs_pma_1/mdio_o]
  connect_bd_net -net gig_ethernet_pcs_pma_1_sgmii_clk_r [get_bd_pins sgmii_clk_r1] [get_bd_pins gig_ethernet_pcs_pma_1/sgmii_clk_r]
  connect_bd_net -net ps_0_ENET0_GMII_TXD [get_bd_pins gmii_txd] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_txd]
  connect_bd_net -net ps_0_ENET0_GMII_TX_EN [get_bd_pins gmii_tx_en] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_tx_en]
  connect_bd_net -net ps_0_ENET0_GMII_TX_ER [get_bd_pins gmii_tx_er] [get_bd_pins gig_ethernet_pcs_pma_0/gmii_tx_er]
  connect_bd_net -net ps_0_ENET0_MDIO_MDC [get_bd_pins mdc] [get_bd_pins gig_ethernet_pcs_pma_0/mdc]
  connect_bd_net -net ps_0_ENET0_MDIO_O [get_bd_pins mdio_i] [get_bd_pins gig_ethernet_pcs_pma_0/mdio_i]
  connect_bd_net -net ps_0_ENET1_GMII_TXD [get_bd_pins gmii_txd1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_txd]
  connect_bd_net -net ps_0_ENET1_GMII_TX_EN [get_bd_pins gmii_tx_en1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_tx_en]
  connect_bd_net -net ps_0_ENET1_GMII_TX_ER [get_bd_pins gmii_tx_er1] [get_bd_pins gig_ethernet_pcs_pma_1/gmii_tx_er]
  connect_bd_net -net ps_0_ENET1_MDIO_MDC [get_bd_pins mdc1] [get_bd_pins gig_ethernet_pcs_pma_1/mdc]
  connect_bd_net -net ps_0_ENET1_MDIO_O [get_bd_pins mdio_i1] [get_bd_pins gig_ethernet_pcs_pma_1/mdio_i]
  connect_bd_net -net ps_0_FCLK_CLK1 [get_bd_pins independent_clock_bufg] [get_bd_pins gig_ethernet_pcs_pma_0/independent_clock_bufg] [get_bd_pins gig_ethernet_pcs_pma_1/independent_clock_bufg]
  connect_bd_net -net sc_io_0_ETH0_RESET_O [get_bd_pins reset] [get_bd_pins gig_ethernet_pcs_pma_0/reset]
  connect_bd_net -net sc_io_0_ETH1_RESET_O [get_bd_pins reset1] [get_bd_pins gig_ethernet_pcs_pma_1/reset]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_clk_distr_ctrl
proc create_hier_cell_hier_const_clk_distr_ctrl { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_clk_distr_ctrl() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 CE_DIS_1_O
  create_bd_pin -dir O -from 0 -to 0 CE_DIS_2_O
  create_bd_pin -dir O -from 0 -to 0 CLK_DIS_1_O
  create_bd_pin -dir O -from 0 -to 0 CLK_DIS_2_O

  # Create instance: const_ce_dis_1, and set properties
  set const_ce_dis_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_ce_dis_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_ce_dis_1

  # Create instance: const_ce_dis_2, and set properties
  set const_ce_dis_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_ce_dis_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_ce_dis_2

  # Create instance: const_clk_dis_1, and set properties
  set const_clk_dis_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_clk_dis_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_clk_dis_1

  # Create instance: const_clk_dis_2, and set properties
  set const_clk_dis_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_clk_dis_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_clk_dis_2

  # Create port connections
  connect_bd_net -net const_ce_dis_1_dout [get_bd_pins CE_DIS_1_O] [get_bd_pins const_ce_dis_1/dout]
  connect_bd_net -net const_ce_dis_2_dout [get_bd_pins CE_DIS_2_O] [get_bd_pins const_ce_dis_2/dout]
  connect_bd_net -net const_clk_dis_1_dout [get_bd_pins CLK_DIS_1_O] [get_bd_pins const_clk_dis_1/dout]
  connect_bd_net -net const_clk_dis_2_dout [get_bd_pins CLK_DIS_2_O] [get_bd_pins const_clk_dis_2/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_bpl_dir
proc create_hier_cell_hier_const_bpl_dir { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_bpl_dir() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 INFO_DIR_O
  create_bd_pin -dir O -from 0 -to 0 SYNC_DIR_O
  create_bd_pin -dir O -from 0 -to 0 TRG_DIR_O

  # Create instance: const_info_dir, and set properties
  set const_info_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_info_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_info_dir

  # Create instance: const_sync_dir, and set properties
  set const_sync_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_sync_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_sync_dir

  # Create instance: const_trg_dir, and set properties
  set const_trg_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_trg_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_trg_dir

  # Create port connections
  connect_bd_net -net const_info_dir_dout [get_bd_pins INFO_DIR_O] [get_bd_pins const_info_dir/dout]
  connect_bd_net -net const_sync_dir_dout [get_bd_pins SYNC_DIR_O] [get_bd_pins const_sync_dir/dout]
  connect_bd_net -net const_trg_dir_dout [get_bd_pins TRG_DIR_O] [get_bd_pins const_trg_dir/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_aux_clk
proc create_hier_cell_hier_const_aux_clk { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_aux_clk() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 AUX_OUT_N_O
  create_bd_pin -dir O -from 0 -to 0 AUX_OUT_P_O

  # Create instance: const_aux_clk_n, and set properties
  set const_aux_clk_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_aux_clk_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_aux_clk_n

  # Create instance: const_aux_clk_p, and set properties
  set const_aux_clk_p [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_aux_clk_p ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_aux_clk_p

  # Create port connections
  connect_bd_net -net const_aux_clk_n_dout [get_bd_pins AUX_OUT_N_O] [get_bd_pins const_aux_clk_n/dout]
  connect_bd_net -net const_aux_clk_p_dout [get_bd_pins AUX_OUT_P_O] [get_bd_pins const_aux_clk_p/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_clock_measure_0
proc create_hier_cell_hier_clock_measure_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_clock_measure_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s00_axi


  # Create pins
  create_bd_pin -dir I -from 0 -to 0 In0
  create_bd_pin -dir I -from 0 -to 0 In1
  create_bd_pin -dir I -from 0 -to 0 In2
  create_bd_pin -dir I -from 0 -to 0 In3
  create_bd_pin -dir I -from 0 -to 0 In4
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: clock_measure_0, and set properties
  set clock_measure_0 [ create_bd_cell -type ip -vlnv psi.ch:PSI:clock_measure clock_measure_0 ]
  set_property -dict [ list \
   CONFIG.AxiClkFreq_g {80000000} \
   CONFIG.C_S00_AXI_ID_WIDTH {12} \
   CONFIG.NumOfClocks_g {6} \
 ] $clock_measure_0

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_1 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {6} \
 ] $xlconcat_1

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins s00_axi] [get_bd_intf_pins clock_measure_0/s00_axi]

  # Create port connections
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_bufg_out [get_bd_pins In0] [get_bd_pins xlconcat_1/In0]
  connect_bd_net -net gig_ethernet_pcs_pma_0_sgmii_clk_r [get_bd_pins In3] [get_bd_pins xlconcat_1/In3]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk2_out [get_bd_pins In2] [get_bd_pins xlconcat_1/In2]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk_out [get_bd_pins In1] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net gig_ethernet_pcs_pma_1_sgmii_clk_r [get_bd_pins In4] [get_bd_pins xlconcat_1/In4]
  connect_bd_net -net s00_axi_aresetn_1 [get_bd_pins s00_axi_aresetn] [get_bd_pins clock_measure_0/s00_axi_aresetn]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins s00_axi_aclk] [get_bd_pins clock_measure_0/s00_axi_aclk] [get_bd_pins xlconcat_1/In5]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins clock_measure_0/Clocks] [get_bd_pins xlconcat_1/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_led_0
proc create_hier_cell_const_led_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_led_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 8 -to 0 dout
  create_bd_pin -dir O -from 0 -to 0 dout1
  create_bd_pin -dir O -from 0 -to 0 dout2

  # Create instance: const_led_hw_error, and set properties
  set const_led_hw_error [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_error ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $const_led_hw_error

  # Create instance: const_led_hw_error_n, and set properties
  set const_led_hw_error_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_error_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0b111111111} \
   CONFIG.CONST_WIDTH {9} \
 ] $const_led_hw_error_n

  # Create instance: const_led_hw_status, and set properties
  set const_led_hw_status [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_status ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
 ] $const_led_hw_status

  # Create port connections
  connect_bd_net -net const_led_hw_error_dout [get_bd_pins dout2] [get_bd_pins const_led_hw_error/dout]
  connect_bd_net -net const_led_hw_error_n_dout [get_bd_pins dout] [get_bd_pins const_led_hw_error_n/dout]
  connect_bd_net -net const_led_hw_status_dout [get_bd_pins dout1] [get_bd_pins const_led_hw_status/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_dir_ext_trig_0
proc create_hier_cell_const_dir_ext_trig_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_dir_ext_trig_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 DIR_EXT_TRG_IN_O
  create_bd_pin -dir O -from 0 -to 0 DIR_EXT_TRG_OUT_O

  # Create instance: const_dir_ext_trig_in, and set properties
  set const_dir_ext_trig_in [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_dir_ext_trig_in ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_dir_ext_trig_in

  # Create instance: const_dir_ext_trig_out, and set properties
  set const_dir_ext_trig_out [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_dir_ext_trig_out ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_dir_ext_trig_out

  # Create port connections
  connect_bd_net -net const_dir_ext_trig_in_dout [get_bd_pins DIR_EXT_TRG_IN_O] [get_bd_pins const_dir_ext_trig_in/dout]
  connect_bd_net -net const_dir_ext_trig_out_dout [get_bd_pins DIR_EXT_TRG_OUT_O] [get_bd_pins const_dir_ext_trig_out/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_bpl_signals_0
proc create_hier_cell_const_bpl_signals_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_bpl_signals_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 MSCB_DATA_O
  create_bd_pin -dir O -from 0 -to 0 MSCB_DRV_EN_O
  create_bd_pin -dir O -from 0 -to 0 SLAVE_SPI_MISO_EN_N_O
  create_bd_pin -dir O -from 0 -to 0 SLAVE_SPI_MISO_O

  # Create instance: const_mscb_data_out, and set properties
  set const_mscb_data_out [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_mscb_data_out ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_mscb_data_out

  # Create instance: const_mscb_drv_en, and set properties
  set const_mscb_drv_en [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_mscb_drv_en ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_mscb_drv_en

  # Create instance: const_slave_spi_miso, and set properties
  set const_slave_spi_miso [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_slave_spi_miso ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_slave_spi_miso

  # Create instance: const_slave_spi_miso_en_n, and set properties
  set const_slave_spi_miso_en_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_slave_spi_miso_en_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_slave_spi_miso_en_n

  # Create port connections
  connect_bd_net -net const_mscb_data_out_dout [get_bd_pins MSCB_DATA_O] [get_bd_pins const_mscb_data_out/dout]
  connect_bd_net -net const_mscb_drv_en_dout [get_bd_pins MSCB_DRV_EN_O] [get_bd_pins const_mscb_drv_en/dout]
  connect_bd_net -net const_slave_spi_miso_dout [get_bd_pins SLAVE_SPI_MISO_O] [get_bd_pins const_slave_spi_miso/dout]
  connect_bd_net -net const_slave_spi_miso_en_n_dout [get_bd_pins SLAVE_SPI_MISO_EN_N_O] [get_bd_pins const_slave_spi_miso_en_n/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: bus_split_spi_ss_0
proc create_hier_cell_bus_split_spi_ss_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_bus_split_spi_ss_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 16 -to 0 Din
  create_bd_pin -dir O -from 0 -to 0 MASTER_SPI_TCB_SS_N_O
  create_bd_pin -dir O -from 15 -to 0 MASTER_SPI_WDB_SS_N_O

  # Create instance: xlslice_spi_ss_tcb_0, and set properties
  set xlslice_spi_ss_tcb_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_spi_ss_tcb_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {16} \
   CONFIG.DIN_TO {16} \
   CONFIG.DIN_WIDTH {17} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_spi_ss_tcb_0

  # Create instance: xlslice_spi_ss_wdb_0, and set properties
  set xlslice_spi_ss_wdb_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_spi_ss_wdb_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {15} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {17} \
   CONFIG.DOUT_WIDTH {16} \
 ] $xlslice_spi_ss_wdb_0

  # Create port connections
  connect_bd_net -net axi_quad_spi_to_bpl_0_ss_o [get_bd_pins Din] [get_bd_pins xlslice_spi_ss_tcb_0/Din] [get_bd_pins xlslice_spi_ss_wdb_0/Din]
  connect_bd_net -net xlslice_spi_ss_tcb_0_Dout [get_bd_pins MASTER_SPI_TCB_SS_N_O] [get_bd_pins xlslice_spi_ss_tcb_0/Dout]
  connect_bd_net -net xlslice_spi_ss_wdb_0_Dout [get_bd_pins MASTER_SPI_WDB_SS_N_O] [get_bd_pins xlslice_spi_ss_wdb_0/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]

  set ETH0_SFP [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH0_SFP ]

  set ETH1_SFP [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH1_SFP ]

  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]


  # Create ports
  set AUX_OUT_N_O [ create_bd_port -dir O -from 0 -to 0 AUX_OUT_N_O ]
  set AUX_OUT_P_O [ create_bd_port -dir O -from 0 -to 0 AUX_OUT_P_O ]
  set BOARD_SELECT_N_I [ create_bd_port -dir I BOARD_SELECT_N_I ]
  set BUSY_BPL_N_I [ create_bd_port -dir I BUSY_BPL_N_I ]
  set BUSY_BPL_N_O [ create_bd_port -dir O BUSY_BPL_N_O ]
  set BUSY_FCI_N_N_O [ create_bd_port -dir O BUSY_FCI_N_N_O ]
  set BUSY_FCI_N_P_O [ create_bd_port -dir O BUSY_FCI_N_P_O ]
  set BUS_CLK_SEL_O [ create_bd_port -dir O BUS_CLK_SEL_O ]
  set CE_DIS_1_O [ create_bd_port -dir O -from 0 -to 0 CE_DIS_1_O ]
  set CE_DIS_2_O [ create_bd_port -dir O -from 0 -to 0 CE_DIS_2_O ]
  set CLK_DIS_1_O [ create_bd_port -dir O -from 0 -to 0 CLK_DIS_1_O ]
  set CLK_DIS_2_O [ create_bd_port -dir O -from 0 -to 0 CLK_DIS_2_O ]
  set CLK_FPGA_N_I [ create_bd_port -dir I -type clk CLK_FPGA_N_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $CLK_FPGA_N_I
  set CLK_FPGA_P_I [ create_bd_port -dir I -type clk CLK_FPGA_P_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $CLK_FPGA_P_I
  set CLK_SEL_EXT_O [ create_bd_port -dir O CLK_SEL_EXT_O ]
  set CLK_SEL_O [ create_bd_port -dir O CLK_SEL_O ]
  set DIR_EXT_TRG_IN_O [ create_bd_port -dir O -from 0 -to 0 DIR_EXT_TRG_IN_O ]
  set DIR_EXT_TRG_OUT_O [ create_bd_port -dir O -from 0 -to 0 DIR_EXT_TRG_OUT_O ]
  set EXT_TRG_IN_I [ create_bd_port -dir I EXT_TRG_IN_I ]
  set EXT_TRG_OUT_O [ create_bd_port -dir O EXT_TRG_OUT_O ]
  set FLASH_SELECT_INIT_N_O [ create_bd_port -dir O FLASH_SELECT_INIT_N_O ]
  set FS_INIT_N_I [ create_bd_port -dir I FS_INIT_N_I ]
  set GTREFCLK0_N_I [ create_bd_port -dir I GTREFCLK0_N_I ]
  set GTREFCLK0_P_I [ create_bd_port -dir I GTREFCLK0_P_I ]
  set INFO_DIR_O [ create_bd_port -dir O -from 0 -to 0 INFO_DIR_O ]
  set LD_I [ create_bd_port -dir I LD_I ]
  set LED_B_N_O [ create_bd_port -dir O LED_B_N_O ]
  set LED_G_N_O [ create_bd_port -dir O LED_G_N_O ]
  set LED_R_N_O [ create_bd_port -dir O LED_R_N_O ]
  set MASTER_SPI_CLK_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_CLK_O ]
  set MASTER_SPI_DE_N_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_DE_N_O ]
  set MASTER_SPI_MISO_I [ create_bd_port -dir I MASTER_SPI_MISO_I ]
  set MASTER_SPI_MOSI_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_MOSI_O ]
  set MASTER_SPI_TCB_SS_N_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_TCB_SS_N_O ]
  set MASTER_SPI_WDB_SS_N_O [ create_bd_port -dir O -from 15 -to 0 MASTER_SPI_WDB_SS_N_O ]
  set MOSI_DIS_1_O [ create_bd_port -dir O MOSI_DIS_1_O ]
  set MOSI_DIS_2_O [ create_bd_port -dir O MOSI_DIS_2_O ]
  set MSCB_DATA_I [ create_bd_port -dir I MSCB_DATA_I ]
  set MSCB_DATA_O [ create_bd_port -dir O -from 0 -to 0 MSCB_DATA_O ]
  set MSCB_DRV_EN_O [ create_bd_port -dir O -from 0 -to 0 MSCB_DRV_EN_O ]
  set RESET_FPGA_BUS_N_O [ create_bd_port -dir O RESET_FPGA_BUS_N_O ]
  set SD_DATA_N_I [ create_bd_port -dir I -from 16 -to 0 SD_DATA_N_I ]
  set SD_DATA_P_I [ create_bd_port -dir I -from 16 -to 0 SD_DATA_P_I ]
  set SD_READY_N_O [ create_bd_port -dir O -from 16 -to 0 SD_READY_N_O ]
  set SD_READY_P_O [ create_bd_port -dir O -from 16 -to 0 SD_READY_P_O ]
  set SFP_1_LOS_I [ create_bd_port -dir I SFP_1_LOS_I ]
  set SFP_1_MOD_I [ create_bd_port -dir I SFP_1_MOD_I ]
  set SFP_1_RS_O [ create_bd_port -dir O -from 1 -to 0 SFP_1_RS_O ]
  set SFP_1_TX_DISABLE_O [ create_bd_port -dir O SFP_1_TX_DISABLE_O ]
  set SFP_1_TX_FAULT_I [ create_bd_port -dir I SFP_1_TX_FAULT_I ]
  set SFP_2_LOS_I [ create_bd_port -dir I SFP_2_LOS_I ]
  set SFP_2_MOD_I [ create_bd_port -dir I SFP_2_MOD_I ]
  set SFP_2_RS_O [ create_bd_port -dir O -from 1 -to 0 SFP_2_RS_O ]
  set SFP_2_TX_DISABLE_O [ create_bd_port -dir O SFP_2_TX_DISABLE_O ]
  set SFP_2_TX_FAULT_I [ create_bd_port -dir I SFP_2_TX_FAULT_I ]
  set SLAVE_SPI_CLK_I [ create_bd_port -dir I SLAVE_SPI_CLK_I ]
  set SLAVE_SPI_MISO_EN_N_O [ create_bd_port -dir O -from 0 -to 0 SLAVE_SPI_MISO_EN_N_O ]
  set SLAVE_SPI_MISO_O [ create_bd_port -dir O -from 0 -to 0 SLAVE_SPI_MISO_O ]
  set SLAVE_SPI_MOSI_I [ create_bd_port -dir I SLAVE_SPI_MOSI_I ]
  set SPI_CS_FPGA_N_I [ create_bd_port -dir I SPI_CS_FPGA_N_I ]
  set SPI_CS_N_O [ create_bd_port -dir O SPI_CS_N_O ]
  set SYNC_DIR_O [ create_bd_port -dir O -from 0 -to 0 SYNC_DIR_O ]
  set SYNC_FCI_N_I [ create_bd_port -dir I SYNC_FCI_N_I ]
  set SYNC_FCI_P_I [ create_bd_port -dir I SYNC_FCI_P_I ]
  set SYNC_LMK_N_O [ create_bd_port -dir O SYNC_LMK_N_O ]
  set TRG_DIR_O [ create_bd_port -dir O -from 0 -to 0 TRG_DIR_O ]
  set TRIGGER_BPL_N_I [ create_bd_port -dir I TRIGGER_BPL_N_I ]
  set TRIGGER_BPL_P_I [ create_bd_port -dir I TRIGGER_BPL_P_I ]
  set TRIGGER_FCI_N_I [ create_bd_port -dir I TRIGGER_FCI_N_I ]
  set TRIGGER_FCI_P_I [ create_bd_port -dir I TRIGGER_FCI_P_I ]
  set TRIGGER_O [ create_bd_port -dir O TRIGGER_O ]
  set TR_INFO_BPL_N_I [ create_bd_port -dir I TR_INFO_BPL_N_I ]
  set TR_INFO_BPL_P_I [ create_bd_port -dir I TR_INFO_BPL_P_I ]
  set TR_INFO_FCI_N_I [ create_bd_port -dir I TR_INFO_FCI_N_I ]
  set TR_INFO_FCI_P_I [ create_bd_port -dir I TR_INFO_FCI_P_I ]
  set TR_INFO_O [ create_bd_port -dir O TR_INFO_O ]
  set TR_SYNC_O [ create_bd_port -dir O TR_SYNC_O ]
  set WDB_CLK_N_I [ create_bd_port -dir I -type clk WDB_CLK_N_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $WDB_CLK_N_I
  set WDB_CLK_P_I [ create_bd_port -dir I -type clk WDB_CLK_P_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $WDB_CLK_P_I

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.M03_HAS_DATA_FIFO {0} \
   CONFIG.NUM_MI {4} \
 ] $axi_interconnect_0

  # Create instance: axi_interconnect_mem, and set properties
  set axi_interconnect_mem [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect axi_interconnect_mem ]
  set_property -dict [ list \
   CONFIG.NUM_MI {1} \
   CONFIG.S00_HAS_DATA_FIFO {1} \
 ] $axi_interconnect_mem

  # Create instance: axi_quad_spi_to_bpl_0, and set properties
  set axi_quad_spi_to_bpl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi axi_quad_spi_to_bpl_0 ]
  set_property -dict [ list \
   CONFIG.C_FIFO_DEPTH {256} \
   CONFIG.C_NUM_SS_BITS {3} \
   CONFIG.C_SCK_RATIO {16} \
   CONFIG.C_TYPE_OF_AXI4_INTERFACE {0} \
   CONFIG.C_USE_STARTUP {0} \
   CONFIG.C_USE_STARTUP_INT {0} \
   CONFIG.C_XIP_MODE {0} \
   CONFIG.Master_mode {1} \
 ] $axi_quad_spi_to_bpl_0

  # Create instance: bus_split_spi_ss_0
  create_hier_cell_bus_split_spi_ss_0 [current_bd_instance .] bus_split_spi_ss_0

  # Create instance: busy_manager_0, and set properties
  set busy_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:busy_manager busy_manager_0 ]

  # Create instance: clk_wiz_refclks, and set properties
  set clk_wiz_refclks [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz clk_wiz_refclks ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {125.0} \
   CONFIG.CLKOUT1_JITTER {138.615} \
   CONFIG.CLKOUT1_PHASE_ERROR {138.018} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {240.000} \
   CONFIG.CLKOUT2_JITTER {143.377} \
   CONFIG.CLKOUT2_PHASE_ERROR {138.018} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {200.000} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLK_OUT1_PORT {clk_out_240MHz} \
   CONFIG.CLK_OUT2_PORT {clk_out_200MHz} \
   CONFIG.ENABLE_CLOCK_MONITOR {false} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {7.500} \
   CONFIG.MMCM_CLKIN1_PERIOD {12.5} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.000} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {2.500} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {3} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {2} \
   CONFIG.PRIMITIVE {MMCM} \
   CONFIG.PRIM_IN_FREQ {80.000} \
   CONFIG.PRIM_SOURCE {No_buffer} \
 ] $clk_wiz_refclks

  # Create instance: clk_wiz_serdes, and set properties
  set clk_wiz_serdes [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz clk_wiz_serdes ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {125.0} \
   CONFIG.CLKOUT1_DRIVES {BUFG} \
   CONFIG.CLKOUT1_JITTER {147.966} \
   CONFIG.CLKOUT1_PHASE_ERROR {103.963} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {80.000} \
   CONFIG.CLKOUT2_DRIVES {BUFG} \
   CONFIG.CLKOUT2_JITTER {113.729} \
   CONFIG.CLKOUT2_PHASE_ERROR {103.963} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {320.000} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_DRIVES {BUFG} \
   CONFIG.CLKOUT3_JITTER {113.393} \
   CONFIG.CLKOUT3_PHASE_ERROR {94.353} \
   CONFIG.CLKOUT3_USED {false} \
   CONFIG.CLKOUT4_DRIVES {BUFG} \
   CONFIG.CLKOUT5_DRIVES {BUFG} \
   CONFIG.CLKOUT6_DRIVES {BUFG} \
   CONFIG.CLKOUT7_DRIVES {BUFG} \
   CONFIG.CLK_OUT1_PORT {DivClk_Bufg} \
   CONFIG.CLK_OUT2_PORT {BitClk_Bufg} \
   CONFIG.ENABLE_CLOCK_MONITOR {false} \
   CONFIG.FEEDBACK_SOURCE {FDBK_ONCHIP} \
   CONFIG.JITTER_SEL {No_Jitter} \
   CONFIG.MMCM_BANDWIDTH {OPTIMIZED} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {12} \
   CONFIG.MMCM_CLKIN1_PERIOD {12.500} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.000} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {12} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {3} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {1} \
   CONFIG.MMCM_COMPENSATION {ZHOLD} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {2} \
   CONFIG.PRIMITIVE {PLL} \
   CONFIG.PRIM_IN_FREQ {80.000} \
   CONFIG.PRIM_SOURCE {No_buffer} \
 ] $clk_wiz_serdes

  # Create instance: const_bpl_signals_0
  create_hier_cell_const_bpl_signals_0 [current_bd_instance .] const_bpl_signals_0

  # Create instance: const_dir_ext_trig_0
  create_hier_cell_const_dir_ext_trig_0 [current_bd_instance .] const_dir_ext_trig_0

  # Create instance: const_led_0
  create_hier_cell_const_led_0 [current_bd_instance .] const_led_0

  # Create instance: hier_clock_measure_0
  create_hier_cell_hier_clock_measure_0 [current_bd_instance .] hier_clock_measure_0

  # Create instance: hier_const_aux_clk
  create_hier_cell_hier_const_aux_clk [current_bd_instance .] hier_const_aux_clk

  # Create instance: hier_const_bpl_dir
  create_hier_cell_hier_const_bpl_dir [current_bd_instance .] hier_const_bpl_dir

  # Create instance: hier_const_clk_distr_ctrl
  create_hier_cell_hier_const_clk_distr_ctrl [current_bd_instance .] hier_const_clk_distr_ctrl

  # Create instance: hier_ethernet_0
  create_hier_cell_hier_ethernet_0 [current_bd_instance .] hier_ethernet_0

  # Create instance: hier_serdes_dma_if
  create_hier_cell_hier_serdes_dma_if [current_bd_instance .] hier_serdes_dma_if

  # Create instance: led_ctrl_0, and set properties
  set led_ctrl_0 [ create_bd_cell -type ip -vlnv PSI:PSI:util_led_ctrl led_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.CGN_ACTIVE_HIGH_HW_ERRORS {2} \
   CONFIG.CGN_CLK_PERIOD {12500} \
 ] $led_ctrl_0

  # Create instance: ps_0, and set properties
  set ps_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7 ps_0 ]
  set_property -dict [ list \
   CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {666.666687} \
   CONFIG.PCW_ACT_CAN0_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN1_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
   CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {125.000000} \
   CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {125.000000} \
   CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {123.076920} \
   CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_I2C_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {145.454544} \
   CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
   CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {666.666666} \
   CONFIG.PCW_ARMPLL_CTRL_FBDIV {60} \
   CONFIG.PCW_CAN0_BASEADDR {0xE0008000} \
   CONFIG.PCW_CAN0_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN0_HIGHADDR {0xE0008FFF} \
   CONFIG.PCW_CAN0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN0_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN1_BASEADDR {0xE0009000} \
   CONFIG.PCW_CAN1_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN1_HIGHADDR {0xE0009FFF} \
   CONFIG.PCW_CAN1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN1_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_CAN_PERIPHERAL_VALID {0} \
   CONFIG.PCW_CLK0_FREQ {50000000} \
   CONFIG.PCW_CLK1_FREQ {200000000} \
   CONFIG.PCW_CLK2_FREQ {123076920} \
   CONFIG.PCW_CLK3_FREQ {10000000} \
   CONFIG.PCW_CORE0_FIQ_INTR {0} \
   CONFIG.PCW_CORE0_IRQ_INTR {0} \
   CONFIG.PCW_CORE1_FIQ_INTR {0} \
   CONFIG.PCW_CORE1_IRQ_INTR {0} \
   CONFIG.PCW_CPU_CPU_6X4X_MAX_RANGE {667} \
   CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {2000.000} \
   CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {3} \
   CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
   CONFIG.PCW_DCI_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
   CONFIG.PCW_DCI_PERIPHERAL_FREQMHZ {10.159} \
   CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
   CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
   CONFIG.PCW_DDR_HPRLPR_QUEUE_PARTITION {HPR(0)/LPR(32)} \
   CONFIG.PCW_DDR_HPR_TO_CRITICAL_PRIORITY_LEVEL {15} \
   CONFIG.PCW_DDR_LPR_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_DDR_PORT0_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT1_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT2_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT3_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_RAM_BASEADDR {0x00100000} \
   CONFIG.PCW_DDR_RAM_HIGHADDR {0x3FFFFFFF} \
   CONFIG.PCW_DDR_WRITE_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DM_WIDTH {4} \
   CONFIG.PCW_DQS_WIDTH {4} \
   CONFIG.PCW_DQ_WIDTH {32} \
   CONFIG.PCW_ENET0_BASEADDR {0xE000B000} \
   CONFIG.PCW_ENET0_ENET0_IO {EMIO} \
   CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET0_GRP_MDIO_IO {EMIO} \
   CONFIG.PCW_ENET0_HIGHADDR {0xE000BFFF} \
   CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET0_RESET_ENABLE {0} \
   CONFIG.PCW_ENET1_BASEADDR {0xE000C000} \
   CONFIG.PCW_ENET1_ENET1_IO {EMIO} \
   CONFIG.PCW_ENET1_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET1_GRP_MDIO_IO {EMIO} \
   CONFIG.PCW_ENET1_HIGHADDR {0xE000CFFF} \
   CONFIG.PCW_ENET1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET1_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_POLARITY {Active Low} \
   CONFIG.PCW_EN_4K_TIMER {0} \
   CONFIG.PCW_EN_CAN0 {0} \
   CONFIG.PCW_EN_CAN1 {0} \
   CONFIG.PCW_EN_CLK0_PORT {1} \
   CONFIG.PCW_EN_CLK1_PORT {1} \
   CONFIG.PCW_EN_CLK2_PORT {1} \
   CONFIG.PCW_EN_CLK3_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG0_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG1_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG2_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG3_PORT {0} \
   CONFIG.PCW_EN_DDR {1} \
   CONFIG.PCW_EN_EMIO_CAN0 {0} \
   CONFIG.PCW_EN_EMIO_CAN1 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_ENET0 {1} \
   CONFIG.PCW_EN_EMIO_ENET1 {1} \
   CONFIG.PCW_EN_EMIO_GPIO {1} \
   CONFIG.PCW_EN_EMIO_I2C0 {0} \
   CONFIG.PCW_EN_EMIO_I2C1 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART0 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART1 {0} \
   CONFIG.PCW_EN_EMIO_PJTAG {0} \
   CONFIG.PCW_EN_EMIO_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_SPI0 {0} \
   CONFIG.PCW_EN_EMIO_SPI1 {0} \
   CONFIG.PCW_EN_EMIO_SRAM_INT {0} \
   CONFIG.PCW_EN_EMIO_TRACE {0} \
   CONFIG.PCW_EN_EMIO_TTC0 {1} \
   CONFIG.PCW_EN_EMIO_TTC1 {0} \
   CONFIG.PCW_EN_EMIO_UART0 {0} \
   CONFIG.PCW_EN_EMIO_UART1 {0} \
   CONFIG.PCW_EN_EMIO_WDT {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
   CONFIG.PCW_EN_ENET0 {1} \
   CONFIG.PCW_EN_ENET1 {1} \
   CONFIG.PCW_EN_GPIO {1} \
   CONFIG.PCW_EN_I2C0 {1} \
   CONFIG.PCW_EN_I2C1 {1} \
   CONFIG.PCW_EN_MODEM_UART0 {0} \
   CONFIG.PCW_EN_MODEM_UART1 {0} \
   CONFIG.PCW_EN_PJTAG {0} \
   CONFIG.PCW_EN_PTP_ENET0 {0} \
   CONFIG.PCW_EN_PTP_ENET1 {0} \
   CONFIG.PCW_EN_QSPI {1} \
   CONFIG.PCW_EN_RST0_PORT {1} \
   CONFIG.PCW_EN_RST1_PORT {0} \
   CONFIG.PCW_EN_RST2_PORT {0} \
   CONFIG.PCW_EN_RST3_PORT {0} \
   CONFIG.PCW_EN_SDIO0 {1} \
   CONFIG.PCW_EN_SDIO1 {0} \
   CONFIG.PCW_EN_SMC {0} \
   CONFIG.PCW_EN_SPI0 {0} \
   CONFIG.PCW_EN_SPI1 {1} \
   CONFIG.PCW_EN_TRACE {0} \
   CONFIG.PCW_EN_TTC0 {1} \
   CONFIG.PCW_EN_TTC1 {0} \
   CONFIG.PCW_EN_UART0 {0} \
   CONFIG.PCW_EN_UART1 {1} \
   CONFIG.PCW_EN_USB0 {1} \
   CONFIG.PCW_EN_USB1 {0} \
   CONFIG.PCW_EN_WDT {0} \
   CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {8} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {5} \
   CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {2} \
   CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {4} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {4} \
   CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK_CLK0_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK2_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK3_BUF {FALSE} \
   CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {125} \
   CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK2_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
   CONFIG.PCW_FTM_CTI_IN0 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN1 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN2 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN3 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT0 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT1 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT2 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT3 {<Select>} \
   CONFIG.PCW_GP0_EN_MODIFIABLE_TXN {1} \
   CONFIG.PCW_GP0_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP0_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GP1_EN_MODIFIABLE_TXN {1} \
   CONFIG.PCW_GP1_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP1_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GPIO_BASEADDR {0xE000A000} \
   CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_EMIO_GPIO_IO {64} \
   CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {64} \
   CONFIG.PCW_GPIO_HIGHADDR {0xE000AFFF} \
   CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
   CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_BASEADDR {0xE0004000} \
   CONFIG.PCW_I2C0_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C0_HIGHADDR {0xE0004FFF} \
   CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51} \
   CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C0_RESET_ENABLE {0} \
   CONFIG.PCW_I2C1_BASEADDR {0xE0005000} \
   CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C1_HIGHADDR {0xE0005FFF} \
   CONFIG.PCW_I2C1_I2C1_IO {MIO 48 .. 49} \
   CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C1_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_I2C_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_RESET_POLARITY {Active Low} \
   CONFIG.PCW_IMPORT_BOARD_PRESET {None} \
   CONFIG.PCW_INCLUDE_ACP_TRANS_CHECK {0} \
   CONFIG.PCW_INCLUDE_TRACE_BUFFER {0} \
   CONFIG.PCW_IOPLL_CTRL_FBDIV {60} \
   CONFIG.PCW_IO_IO_PLL_FREQMHZ {2000.000} \
   CONFIG.PCW_IRQ_F2P_INTR {1} \
   CONFIG.PCW_IRQ_F2P_MODE {DIRECT} \
   CONFIG.PCW_MIO_0_DIRECTION {out} \
   CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_0_PULLUP {enabled} \
   CONFIG.PCW_MIO_0_SLEW {slow} \
   CONFIG.PCW_MIO_10_DIRECTION {inout} \
   CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_10_PULLUP {enabled} \
   CONFIG.PCW_MIO_10_SLEW {slow} \
   CONFIG.PCW_MIO_11_DIRECTION {inout} \
   CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_11_PULLUP {enabled} \
   CONFIG.PCW_MIO_11_SLEW {slow} \
   CONFIG.PCW_MIO_12_DIRECTION {inout} \
   CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_12_PULLUP {enabled} \
   CONFIG.PCW_MIO_12_SLEW {slow} \
   CONFIG.PCW_MIO_13_DIRECTION {inout} \
   CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_13_PULLUP {enabled} \
   CONFIG.PCW_MIO_13_SLEW {slow} \
   CONFIG.PCW_MIO_14_DIRECTION {out} \
   CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_14_PULLUP {enabled} \
   CONFIG.PCW_MIO_14_SLEW {slow} \
   CONFIG.PCW_MIO_15_DIRECTION {out} \
   CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_15_PULLUP {enabled} \
   CONFIG.PCW_MIO_15_SLEW {slow} \
   CONFIG.PCW_MIO_16_DIRECTION {inout} \
   CONFIG.PCW_MIO_16_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_16_PULLUP {enabled} \
   CONFIG.PCW_MIO_16_SLEW {slow} \
   CONFIG.PCW_MIO_17_DIRECTION {inout} \
   CONFIG.PCW_MIO_17_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_17_PULLUP {enabled} \
   CONFIG.PCW_MIO_17_SLEW {slow} \
   CONFIG.PCW_MIO_18_DIRECTION {inout} \
   CONFIG.PCW_MIO_18_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_18_PULLUP {enabled} \
   CONFIG.PCW_MIO_18_SLEW {slow} \
   CONFIG.PCW_MIO_19_DIRECTION {inout} \
   CONFIG.PCW_MIO_19_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_19_PULLUP {enabled} \
   CONFIG.PCW_MIO_19_SLEW {slow} \
   CONFIG.PCW_MIO_1_DIRECTION {out} \
   CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_1_PULLUP {enabled} \
   CONFIG.PCW_MIO_1_SLEW {slow} \
   CONFIG.PCW_MIO_20_DIRECTION {inout} \
   CONFIG.PCW_MIO_20_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_20_PULLUP {enabled} \
   CONFIG.PCW_MIO_20_SLEW {slow} \
   CONFIG.PCW_MIO_21_DIRECTION {inout} \
   CONFIG.PCW_MIO_21_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_21_PULLUP {enabled} \
   CONFIG.PCW_MIO_21_SLEW {slow} \
   CONFIG.PCW_MIO_22_DIRECTION {inout} \
   CONFIG.PCW_MIO_22_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_22_PULLUP {enabled} \
   CONFIG.PCW_MIO_22_SLEW {slow} \
   CONFIG.PCW_MIO_23_DIRECTION {inout} \
   CONFIG.PCW_MIO_23_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_23_PULLUP {enabled} \
   CONFIG.PCW_MIO_23_SLEW {slow} \
   CONFIG.PCW_MIO_24_DIRECTION {inout} \
   CONFIG.PCW_MIO_24_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_24_PULLUP {enabled} \
   CONFIG.PCW_MIO_24_SLEW {slow} \
   CONFIG.PCW_MIO_25_DIRECTION {inout} \
   CONFIG.PCW_MIO_25_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_25_PULLUP {enabled} \
   CONFIG.PCW_MIO_25_SLEW {slow} \
   CONFIG.PCW_MIO_26_DIRECTION {inout} \
   CONFIG.PCW_MIO_26_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_26_PULLUP {enabled} \
   CONFIG.PCW_MIO_26_SLEW {slow} \
   CONFIG.PCW_MIO_27_DIRECTION {inout} \
   CONFIG.PCW_MIO_27_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_27_PULLUP {enabled} \
   CONFIG.PCW_MIO_27_SLEW {slow} \
   CONFIG.PCW_MIO_28_DIRECTION {inout} \
   CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_28_PULLUP {enabled} \
   CONFIG.PCW_MIO_28_SLEW {slow} \
   CONFIG.PCW_MIO_29_DIRECTION {in} \
   CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_29_PULLUP {enabled} \
   CONFIG.PCW_MIO_29_SLEW {slow} \
   CONFIG.PCW_MIO_2_DIRECTION {inout} \
   CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_2_PULLUP {disabled} \
   CONFIG.PCW_MIO_2_SLEW {slow} \
   CONFIG.PCW_MIO_30_DIRECTION {out} \
   CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_30_PULLUP {enabled} \
   CONFIG.PCW_MIO_30_SLEW {slow} \
   CONFIG.PCW_MIO_31_DIRECTION {in} \
   CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_31_PULLUP {enabled} \
   CONFIG.PCW_MIO_31_SLEW {slow} \
   CONFIG.PCW_MIO_32_DIRECTION {inout} \
   CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_32_PULLUP {enabled} \
   CONFIG.PCW_MIO_32_SLEW {slow} \
   CONFIG.PCW_MIO_33_DIRECTION {inout} \
   CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_33_PULLUP {enabled} \
   CONFIG.PCW_MIO_33_SLEW {slow} \
   CONFIG.PCW_MIO_34_DIRECTION {inout} \
   CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_34_PULLUP {enabled} \
   CONFIG.PCW_MIO_34_SLEW {slow} \
   CONFIG.PCW_MIO_35_DIRECTION {inout} \
   CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_35_PULLUP {enabled} \
   CONFIG.PCW_MIO_35_SLEW {slow} \
   CONFIG.PCW_MIO_36_DIRECTION {in} \
   CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_36_PULLUP {enabled} \
   CONFIG.PCW_MIO_36_SLEW {slow} \
   CONFIG.PCW_MIO_37_DIRECTION {inout} \
   CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_37_PULLUP {enabled} \
   CONFIG.PCW_MIO_37_SLEW {slow} \
   CONFIG.PCW_MIO_38_DIRECTION {inout} \
   CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_38_PULLUP {enabled} \
   CONFIG.PCW_MIO_38_SLEW {slow} \
   CONFIG.PCW_MIO_39_DIRECTION {inout} \
   CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_39_PULLUP {enabled} \
   CONFIG.PCW_MIO_39_SLEW {slow} \
   CONFIG.PCW_MIO_3_DIRECTION {inout} \
   CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_3_PULLUP {disabled} \
   CONFIG.PCW_MIO_3_SLEW {slow} \
   CONFIG.PCW_MIO_40_DIRECTION {inout} \
   CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_40_PULLUP {enabled} \
   CONFIG.PCW_MIO_40_SLEW {slow} \
   CONFIG.PCW_MIO_41_DIRECTION {inout} \
   CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_41_PULLUP {enabled} \
   CONFIG.PCW_MIO_41_SLEW {slow} \
   CONFIG.PCW_MIO_42_DIRECTION {inout} \
   CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_42_PULLUP {enabled} \
   CONFIG.PCW_MIO_42_SLEW {slow} \
   CONFIG.PCW_MIO_43_DIRECTION {inout} \
   CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_43_PULLUP {enabled} \
   CONFIG.PCW_MIO_43_SLEW {slow} \
   CONFIG.PCW_MIO_44_DIRECTION {inout} \
   CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_44_PULLUP {enabled} \
   CONFIG.PCW_MIO_44_SLEW {slow} \
   CONFIG.PCW_MIO_45_DIRECTION {inout} \
   CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_45_PULLUP {enabled} \
   CONFIG.PCW_MIO_45_SLEW {slow} \
   CONFIG.PCW_MIO_46_DIRECTION {inout} \
   CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_46_PULLUP {enabled} \
   CONFIG.PCW_MIO_46_SLEW {slow} \
   CONFIG.PCW_MIO_47_DIRECTION {inout} \
   CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_47_PULLUP {enabled} \
   CONFIG.PCW_MIO_47_SLEW {slow} \
   CONFIG.PCW_MIO_48_DIRECTION {inout} \
   CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_48_PULLUP {disabled} \
   CONFIG.PCW_MIO_48_SLEW {slow} \
   CONFIG.PCW_MIO_49_DIRECTION {inout} \
   CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_49_PULLUP {disabled} \
   CONFIG.PCW_MIO_49_SLEW {slow} \
   CONFIG.PCW_MIO_4_DIRECTION {inout} \
   CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_4_PULLUP {disabled} \
   CONFIG.PCW_MIO_4_SLEW {slow} \
   CONFIG.PCW_MIO_50_DIRECTION {inout} \
   CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_50_PULLUP {disabled} \
   CONFIG.PCW_MIO_50_SLEW {slow} \
   CONFIG.PCW_MIO_51_DIRECTION {inout} \
   CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_51_PULLUP {disabled} \
   CONFIG.PCW_MIO_51_SLEW {slow} \
   CONFIG.PCW_MIO_52_DIRECTION {out} \
   CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_52_PULLUP {enabled} \
   CONFIG.PCW_MIO_52_SLEW {slow} \
   CONFIG.PCW_MIO_53_DIRECTION {in} \
   CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_53_PULLUP {enabled} \
   CONFIG.PCW_MIO_53_SLEW {slow} \
   CONFIG.PCW_MIO_5_DIRECTION {inout} \
   CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_5_PULLUP {disabled} \
   CONFIG.PCW_MIO_5_SLEW {slow} \
   CONFIG.PCW_MIO_6_DIRECTION {out} \
   CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_6_PULLUP {disabled} \
   CONFIG.PCW_MIO_6_SLEW {slow} \
   CONFIG.PCW_MIO_7_DIRECTION {out} \
   CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_7_PULLUP {disabled} \
   CONFIG.PCW_MIO_7_SLEW {slow} \
   CONFIG.PCW_MIO_8_DIRECTION {out} \
   CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_8_PULLUP {disabled} \
   CONFIG.PCW_MIO_8_SLEW {slow} \
   CONFIG.PCW_MIO_9_DIRECTION {inout} \
   CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_9_PULLUP {enabled} \
   CONFIG.PCW_MIO_9_SLEW {slow} \
   CONFIG.PCW_MIO_PRIMITIVE {54} \
   CONFIG.PCW_MIO_TREE_PERIPHERALS {USB Reset#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#GPIO#Quad SPI Flash#GPIO#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#GPIO#GPIO#I2C 1#I2C 1#I2C 0#I2C 0#UART 1#UART 1} \
   CONFIG.PCW_MIO_TREE_SIGNALS {reset#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]/HOLD_B#qspi0_sclk#gpio[7]#qspi_fbclk#gpio[9]#mosi#miso#sclk#ss[0]#ss[1]#ss[2]#gpio[16]#gpio[17]#gpio[18]#gpio[19]#gpio[20]#gpio[21]#gpio[22]#gpio[23]#gpio[24]#gpio[25]#gpio[26]#gpio[27]#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#gpio[46]#gpio[47]#scl#sda#scl#sda#tx#rx} \
   CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP0_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP0_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP0_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP1_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP1_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_NAND_CYCLES_T_AR {1} \
   CONFIG.PCW_NAND_CYCLES_T_CLR {1} \
   CONFIG.PCW_NAND_CYCLES_T_RC {11} \
   CONFIG.PCW_NAND_CYCLES_T_REA {1} \
   CONFIG.PCW_NAND_CYCLES_T_RR {1} \
   CONFIG.PCW_NAND_CYCLES_T_WC {11} \
   CONFIG.PCW_NAND_CYCLES_T_WP {1} \
   CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
   CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_CS0_T_PC {1} \
   CONFIG.PCW_NOR_CS0_T_RC {11} \
   CONFIG.PCW_NOR_CS0_T_TR {1} \
   CONFIG.PCW_NOR_CS0_T_WC {11} \
   CONFIG.PCW_NOR_CS0_T_WP {1} \
   CONFIG.PCW_NOR_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_CS1_T_PC {1} \
   CONFIG.PCW_NOR_CS1_T_RC {11} \
   CONFIG.PCW_NOR_CS1_T_TR {1} \
   CONFIG.PCW_NOR_CS1_T_WC {11} \
   CONFIG.PCW_NOR_CS1_T_WP {1} \
   CONFIG.PCW_NOR_CS1_WE_TIME {0} \
   CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
   CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_SRAM_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_SRAM_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS1_WE_TIME {0} \
   CONFIG.PCW_OVERRIDE_BASIC_CLOCK {0} \
   CONFIG.PCW_P2F_CAN0_INTR {0} \
   CONFIG.PCW_P2F_CAN1_INTR {0} \
   CONFIG.PCW_P2F_CTI_INTR {0} \
   CONFIG.PCW_P2F_DMAC0_INTR {0} \
   CONFIG.PCW_P2F_DMAC1_INTR {0} \
   CONFIG.PCW_P2F_DMAC2_INTR {0} \
   CONFIG.PCW_P2F_DMAC3_INTR {0} \
   CONFIG.PCW_P2F_DMAC4_INTR {0} \
   CONFIG.PCW_P2F_DMAC5_INTR {0} \
   CONFIG.PCW_P2F_DMAC6_INTR {0} \
   CONFIG.PCW_P2F_DMAC7_INTR {0} \
   CONFIG.PCW_P2F_DMAC_ABORT_INTR {0} \
   CONFIG.PCW_P2F_ENET0_INTR {0} \
   CONFIG.PCW_P2F_ENET1_INTR {0} \
   CONFIG.PCW_P2F_GPIO_INTR {0} \
   CONFIG.PCW_P2F_I2C0_INTR {0} \
   CONFIG.PCW_P2F_I2C1_INTR {0} \
   CONFIG.PCW_P2F_QSPI_INTR {0} \
   CONFIG.PCW_P2F_SDIO0_INTR {0} \
   CONFIG.PCW_P2F_SDIO1_INTR {0} \
   CONFIG.PCW_P2F_SMC_INTR {0} \
   CONFIG.PCW_P2F_SPI0_INTR {0} \
   CONFIG.PCW_P2F_SPI1_INTR {0} \
   CONFIG.PCW_P2F_UART0_INTR {0} \
   CONFIG.PCW_P2F_UART1_INTR {0} \
   CONFIG.PCW_P2F_USB0_INTR {0} \
   CONFIG.PCW_P2F_USB1_INTR {0} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.101} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.097} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.102} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.106} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {0.005} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {0.016} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {0.001} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.009} \
   CONFIG.PCW_PACKAGE_NAME {fbg676} \
   CONFIG.PCW_PCAP_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {10} \
   CONFIG.PCW_PCAP_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_PERIPHERAL_BOARD_PRESET {None} \
   CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_PLL_BYPASSMODE_ENABLE {0} \
   CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
   CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PS7_SI_REV {PRODUCTION} \
   CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_FBCLK_IO {MIO 8} \
   CONFIG.PCW_QSPI_GRP_IO1_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
   CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFDFFFFFF} \
   CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {10} \
   CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
   CONFIG.PCW_SD0_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
   CONFIG.PCW_SD1_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SDIO0_BASEADDR {0xE0100000} \
   CONFIG.PCW_SDIO0_HIGHADDR {0xE0100FFF} \
   CONFIG.PCW_SDIO1_BASEADDR {0xE0101000} \
   CONFIG.PCW_SDIO1_HIGHADDR {0xE0101FFF} \
   CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {20} \
   CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
   CONFIG.PCW_SINGLE_QSPI_DATA_MODE {x4} \
   CONFIG.PCW_SMC_CYCLE_T0 {NA} \
   CONFIG.PCW_SMC_CYCLE_T1 {NA} \
   CONFIG.PCW_SMC_CYCLE_T2 {NA} \
   CONFIG.PCW_SMC_CYCLE_T3 {NA} \
   CONFIG.PCW_SMC_CYCLE_T4 {NA} \
   CONFIG.PCW_SMC_CYCLE_T5 {NA} \
   CONFIG.PCW_SMC_CYCLE_T6 {NA} \
   CONFIG.PCW_SMC_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SMC_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SMC_PERIPHERAL_VALID {0} \
   CONFIG.PCW_SPI0_BASEADDR {0xE0006000} \
   CONFIG.PCW_SPI0_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS0_IO {<Select>} \
   CONFIG.PCW_SPI0_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS1_IO {<Select>} \
   CONFIG.PCW_SPI0_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS2_IO {<Select>} \
   CONFIG.PCW_SPI0_HIGHADDR {0xE0006FFF} \
   CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI0_SPI0_IO {<Select>} \
   CONFIG.PCW_SPI1_BASEADDR {0xE0007000} \
   CONFIG.PCW_SPI1_GRP_SS0_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS0_IO {MIO 13} \
   CONFIG.PCW_SPI1_GRP_SS1_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS1_IO {MIO 14} \
   CONFIG.PCW_SPI1_GRP_SS2_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS2_IO {MIO 15} \
   CONFIG.PCW_SPI1_HIGHADDR {0xE0007FFF} \
   CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SPI1_SPI1_IO {MIO 10 .. 15} \
   CONFIG.PCW_SPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {13} \
   CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {150} \
   CONFIG.PCW_SPI_PERIPHERAL_VALID {1} \
   CONFIG.PCW_S_AXI_ACP_ARUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_AWUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_ID_WIDTH {3} \
   CONFIG.PCW_S_AXI_GP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_GP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP1_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP2_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP2_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP3_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP3_ID_WIDTH {6} \
   CONFIG.PCW_TPIU_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_TRACE_BUFFER_CLOCK_DELAY {12} \
   CONFIG.PCW_TRACE_BUFFER_FIFO_SIZE {128} \
   CONFIG.PCW_TRACE_GRP_16BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_2BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_32BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_4BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_8BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_INTERNAL_WIDTH {2} \
   CONFIG.PCW_TRACE_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TRACE_PIPELINE_WIDTH {8} \
   CONFIG.PCW_TTC0_BASEADDR {0xE0104000} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_HIGHADDR {0xE0104fff} \
   CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
   CONFIG.PCW_TTC1_BASEADDR {0xE0105000} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_HIGHADDR {0xE0105fff} \
   CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART0_BASEADDR {0xE0000000} \
   CONFIG.PCW_UART0_BAUD_RATE {115200} \
   CONFIG.PCW_UART0_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART0_GRP_FULL_IO {<Select>} \
   CONFIG.PCW_UART0_HIGHADDR {0xE0000FFF} \
   CONFIG.PCW_UART0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_UART0_UART0_IO {<Select>} \
   CONFIG.PCW_UART1_BASEADDR {0xE0001000} \
   CONFIG.PCW_UART1_BAUD_RATE {115200} \
   CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART1_HIGHADDR {0xE0001FFF} \
   CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_UART1_UART1_IO {MIO 52 .. 53} \
   CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {20} \
   CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
   CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
   CONFIG.PCW_UIPARAM_DDR_ADV_ENABLE {0} \
   CONFIG.PCW_UIPARAM_DDR_AL {0} \
   CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
   CONFIG.PCW_UIPARAM_DDR_BL {8} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.300} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.300} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.360} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.360} \
   CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {32 Bit} \
   CONFIG.PCW_UIPARAM_DDR_CL {7} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_STOP_EN {0} \
   CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
   CONFIG.PCW_UIPARAM_DDR_CWL {6} \
   CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {4096 MBits} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {97.8785} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {86.7455} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {102.1345} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {111.4085} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.090} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.080} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.170} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {99.127} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {91.501} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {100.241} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {110.0845} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
   CONFIG.PCW_UIPARAM_DDR_ECC {Disabled} \
   CONFIG.PCW_UIPARAM_DDR_ENABLE {1} \
   CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333333} \
   CONFIG.PCW_UIPARAM_DDR_HIGH_TEMP {Normal (0-85)} \
   CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3} \
   CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
   CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
   CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
   CONFIG.PCW_UIPARAM_DDR_T_FAW {40.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {35.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RC {48.91} \
   CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
   CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
   CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {0} \
   CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NA} \
   CONFIG.PCW_USB0_BASEADDR {0xE0102000} \
   CONFIG.PCW_USB0_HIGHADDR {0xE0102fff} \
   CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB0_RESET_ENABLE {1} \
   CONFIG.PCW_USB0_RESET_IO {MIO 0} \
   CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
   CONFIG.PCW_USB1_BASEADDR {0xE0103000} \
   CONFIG.PCW_USB1_HIGHADDR {0xE0103fff} \
   CONFIG.PCW_USB1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB1_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_ENABLE {1} \
   CONFIG.PCW_USB_RESET_POLARITY {Active Low} \
   CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_USE_AXI_FABRIC_IDLE {0} \
   CONFIG.PCW_USE_AXI_NONSECURE {0} \
   CONFIG.PCW_USE_CORESIGHT {0} \
   CONFIG.PCW_USE_CROSS_TRIGGER {0} \
   CONFIG.PCW_USE_CR_FABRIC {1} \
   CONFIG.PCW_USE_DDR_BYPASS {0} \
   CONFIG.PCW_USE_DEBUG {0} \
   CONFIG.PCW_USE_DEFAULT_ACP_USER_VAL {0} \
   CONFIG.PCW_USE_DMA0 {0} \
   CONFIG.PCW_USE_DMA1 {0} \
   CONFIG.PCW_USE_DMA2 {0} \
   CONFIG.PCW_USE_DMA3 {0} \
   CONFIG.PCW_USE_EXPANDED_IOP {0} \
   CONFIG.PCW_USE_EXPANDED_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
   CONFIG.PCW_USE_HIGH_OCM {0} \
   CONFIG.PCW_USE_M_AXI_GP0 {1} \
   CONFIG.PCW_USE_M_AXI_GP1 {0} \
   CONFIG.PCW_USE_PROC_EVENT_BUS {0} \
   CONFIG.PCW_USE_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_S_AXI_ACP {0} \
   CONFIG.PCW_USE_S_AXI_GP0 {0} \
   CONFIG.PCW_USE_S_AXI_GP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP0 {1} \
   CONFIG.PCW_USE_S_AXI_HP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP2 {0} \
   CONFIG.PCW_USE_S_AXI_HP3 {0} \
   CONFIG.PCW_USE_TRACE {0} \
   CONFIG.PCW_USE_TRACE_DATA_EDGE_DETECTOR {0} \
   CONFIG.PCW_VALUE_SILVERSION {3} \
   CONFIG.PCW_WDT_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_WDT_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_WDT_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
 ] $ps_0

  # Create instance: register_bank_0
  create_hier_cell_register_bank_0 [current_bd_instance .] register_bank_0

  # Create instance: reset_generator_0, and set properties
  set reset_generator_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:reset_generator reset_generator_0 ]
  set_property -dict [ list \
   CONFIG.CGN_INPUT_RESET_POLARITY {0} \
   CONFIG.CGN_NR_OF_BUS_RST {1} \
   CONFIG.CGN_NR_OF_BUS_RST_N {3} \
   CONFIG.CGN_NR_OF_PERIPHERAL_RST {2} \
   CONFIG.CGN_NR_OF_PERIPHERAL_RST_N {3} \
 ] $reset_generator_0

  # Create instance: sc_io_0, and set properties
  set sc_io_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:sc_io sc_io_0 ]

  # Create instance: system_ila_0, and set properties
  set system_ila_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:system_ila system_ila_0 ]
  set_property -dict [ list \
   CONFIG.ALL_PROBE_SAME_MU_CNT {2} \
   CONFIG.C_BRAM_CNT {8} \
   CONFIG.C_EN_STRG_QUAL {1} \
   CONFIG.C_INPUT_PIPE_STAGES {2} \
   CONFIG.C_MON_TYPE {MIX} \
   CONFIG.C_NUM_MONITOR_SLOTS {1} \
   CONFIG.C_NUM_OF_PROBES {14} \
   CONFIG.C_PROBE0_MU_CNT {2} \
   CONFIG.C_PROBE10_MU_CNT {2} \
   CONFIG.C_PROBE11_MU_CNT {2} \
   CONFIG.C_PROBE12_MU_CNT {2} \
   CONFIG.C_PROBE13_MU_CNT {2} \
   CONFIG.C_PROBE14_MU_CNT {2} \
   CONFIG.C_PROBE15_MU_CNT {2} \
   CONFIG.C_PROBE16_MU_CNT {2} \
   CONFIG.C_PROBE17_MU_CNT {2} \
   CONFIG.C_PROBE18_MU_CNT {2} \
   CONFIG.C_PROBE19_MU_CNT {2} \
   CONFIG.C_PROBE1_MU_CNT {2} \
   CONFIG.C_PROBE20_MU_CNT {2} \
   CONFIG.C_PROBE21_MU_CNT {2} \
   CONFIG.C_PROBE22_MU_CNT {2} \
   CONFIG.C_PROBE23_MU_CNT {2} \
   CONFIG.C_PROBE24_MU_CNT {2} \
   CONFIG.C_PROBE25_MU_CNT {2} \
   CONFIG.C_PROBE26_MU_CNT {2} \
   CONFIG.C_PROBE27_MU_CNT {2} \
   CONFIG.C_PROBE28_MU_CNT {2} \
   CONFIG.C_PROBE29_MU_CNT {2} \
   CONFIG.C_PROBE2_MU_CNT {2} \
   CONFIG.C_PROBE30_MU_CNT {2} \
   CONFIG.C_PROBE31_MU_CNT {2} \
   CONFIG.C_PROBE3_MU_CNT {2} \
   CONFIG.C_PROBE4_MU_CNT {2} \
   CONFIG.C_PROBE5_MU_CNT {2} \
   CONFIG.C_PROBE6_MU_CNT {2} \
   CONFIG.C_PROBE7_MU_CNT {2} \
   CONFIG.C_PROBE8_MU_CNT {2} \
   CONFIG.C_PROBE9_MU_CNT {2} \
 ] $system_ila_0

  # Create instance: tr_sync_manager_0, and set properties
  set tr_sync_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:tr_sync_manager tr_sync_manager_0 ]

  # Create instance: trigger_manager_0, and set properties
  set trigger_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:trigger_manager trigger_manager_0 ]

  # Create instance: util_build_info_0, and set properties
  set util_build_info_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:util_build_info util_build_info_0 ]
  set_property -dict [ list \
   CONFIG.CGN_USE_GENERICS {false} \
 ] $util_build_info_0

  # Create instance: util_ds_buf_fpga_clk, and set properties
  set util_ds_buf_fpga_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_fpga_clk ]

  # Create instance: util_ds_buf_wdb_clk, and set properties
  set util_ds_buf_wdb_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_wdb_clk ]

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0 ]

  # Create instance: xlconcat_irq_f2p, and set properties
  set xlconcat_irq_f2p [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_irq_f2p ]

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {135} \
   CONFIG.DIN_TO {128} \
   CONFIG.DIN_WIDTH {136} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_0

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {16} \
   CONFIG.DIN_TO {16} \
   CONFIG.DIN_WIDTH {17} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_1

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins axi_interconnect_0/S00_AXI] [get_bd_intf_pins ps_0/M_AXI_GP0]
  connect_bd_intf_net -intf_net S00_AXI_2 [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins register_bank_0/S00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins axi_quad_spi_to_bpl_0/AXI_LITE]
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins axi_interconnect_0/M02_AXI] [get_bd_intf_pins hier_clock_measure_0/s00_axi]
  connect_bd_intf_net -intf_net axi_interconnect_0_M03_AXI [get_bd_intf_pins axi_interconnect_0/M03_AXI] [get_bd_intf_pins hier_serdes_dma_if/S_Axi]
  connect_bd_intf_net -intf_net axi_interconnect_1_M00_AXI [get_bd_intf_pins axi_interconnect_mem/M00_AXI] [get_bd_intf_pins ps_0/S_AXI_HP0]
  connect_bd_intf_net -intf_net dma_pkt_sched_axi_0_M_Axi [get_bd_intf_pins axi_interconnect_mem/S00_AXI] [get_bd_intf_pins hier_serdes_dma_if/M_Axi]
connect_bd_intf_net -intf_net [get_bd_intf_nets dma_pkt_sched_axi_0_M_Axi] [get_bd_intf_pins hier_serdes_dma_if/M_Axi] [get_bd_intf_pins system_ila_0/SLOT_0_AXI]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_intf_nets dma_pkt_sched_axi_0_M_Axi]
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_0_sfp [get_bd_intf_ports ETH0_SFP] [get_bd_intf_pins hier_ethernet_0/ETH0_SFP]
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_1_sfp [get_bd_intf_ports ETH1_SFP] [get_bd_intf_pins hier_ethernet_0/ETH1_SFP]
  connect_bd_intf_net -intf_net ps_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins ps_0/DDR]
  connect_bd_intf_net -intf_net ps_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins ps_0/FIXED_IO]

  # Create port connections
  connect_bd_net -net ARESETN_1 [get_bd_pins axi_interconnect_mem/ARESETN] [get_bd_pins axi_interconnect_mem/S00_ARESETN] [get_bd_pins hier_serdes_dma_if/M_Axi_o_Aresetn]
  connect_bd_net -net BOARD_SELECT_N_I_1 [get_bd_ports BOARD_SELECT_N_I] [get_bd_pins sc_io_0/BPL_BOARD_SELECT_N_I]
  connect_bd_net -net BUSY_IN_N_I_1 [get_bd_ports BUSY_BPL_N_I] [get_bd_pins busy_manager_0/BUSY_FROM_BPL_N_I] [get_bd_pins register_bank_0/BUSY_IN_N_I]
  connect_bd_net -net BitClk_Bufg [get_bd_pins clk_wiz_serdes/BitClk_Bufg] [get_bd_pins hier_serdes_dma_if/BitClk_Serdes_i]
  connect_bd_net -net CLK_FPGA_N_I_1 [get_bd_ports CLK_FPGA_N_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_DS_N]
  connect_bd_net -net CLK_FPGA_P_I_1 [get_bd_ports CLK_FPGA_P_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_DS_P]
  connect_bd_net -net DCB_DATA_N_I_1 [get_bd_ports SD_DATA_N_I] [get_bd_pins hier_serdes_dma_if/SD_DATA_N_I]
  connect_bd_net -net DCB_DATA_P_I_1 [get_bd_ports SD_DATA_P_I] [get_bd_pins hier_serdes_dma_if/SD_DATA_P_I]
  connect_bd_net -net Din_1 [get_bd_pins bus_split_spi_ss_0/Din] [get_bd_pins sc_io_0/BPL_BOARD_SELECT_N_O]
  connect_bd_net -net DivClk_Bufg [get_bd_pins axi_interconnect_mem/ACLK] [get_bd_pins axi_interconnect_mem/S00_ACLK] [get_bd_pins clk_wiz_serdes/DivClk_Bufg] [get_bd_pins hier_serdes_dma_if/DivClk_Serdes_i] [get_bd_pins reset_generator_0/BUS_RST_N_01_CLK_I] [get_bd_pins reset_generator_0/PERIPHERAL_RST_01_CLK_I] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_01_CLK_I] [get_bd_pins system_ila_0/clk] [get_bd_pins tr_sync_manager_0/WDB_CLK_I] [get_bd_pins trigger_manager_0/WDB_CLK_I]
  connect_bd_net -net EXT_CLK_N_I_1 [get_bd_ports WDB_CLK_N_I] [get_bd_pins util_ds_buf_wdb_clk/IBUF_DS_N]
  connect_bd_net -net EXT_CLK_P_I_1 [get_bd_ports WDB_CLK_P_I] [get_bd_pins util_ds_buf_wdb_clk/IBUF_DS_P]
  connect_bd_net -net EXT_TRIG_IN_I_1 [get_bd_ports EXT_TRG_IN_I] [get_bd_pins trigger_manager_0/TRIGGER_MCX_I]
  connect_bd_net -net FRAME_ERRORS_01_I_1 [get_bd_pins hier_serdes_dma_if/Slot01_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_01_I]
  connect_bd_net -net FRAME_ERRORS_04_I_1 [get_bd_pins hier_serdes_dma_if/Slot04_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_04_I]
  connect_bd_net -net FRAME_ERRORS_05_I_1 [get_bd_pins hier_serdes_dma_if/Slot05_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_05_I]
  connect_bd_net -net FRAME_ERRORS_06_I_1 [get_bd_pins hier_serdes_dma_if/Slot06_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_06_I]
  connect_bd_net -net FRAME_ERRORS_07_I_1 [get_bd_pins hier_serdes_dma_if/Slot07_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_07_I]
  connect_bd_net -net FRAME_ERRORS_08_I_1 [get_bd_pins hier_serdes_dma_if/Slot08_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_08_I]
  connect_bd_net -net FRAME_ERRORS_09_I_1 [get_bd_pins hier_serdes_dma_if/Slot09_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_09_I]
  connect_bd_net -net FRAME_ERRORS_10_I_1 [get_bd_pins hier_serdes_dma_if/Slot10_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_10_I]
  connect_bd_net -net FRAME_ERRORS_11_I_1 [get_bd_pins hier_serdes_dma_if/Slot11_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_11_I]
  connect_bd_net -net FRAME_ERRORS_12_I_1 [get_bd_pins hier_serdes_dma_if/Slot12_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_12_I]
  connect_bd_net -net FRAME_ERRORS_13_I_1 [get_bd_pins hier_serdes_dma_if/Slot13_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_13_I]
  connect_bd_net -net FRAME_ERRORS_14_I_1 [get_bd_pins hier_serdes_dma_if/Slot14_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_14_I]
  connect_bd_net -net FRAME_ERRORS_15_I_1 [get_bd_pins hier_serdes_dma_if/Slot15_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_15_I]
  connect_bd_net -net FRAME_ERRORS_17_I_1 [get_bd_pins hier_serdes_dma_if/Slot16_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_17_I]
  connect_bd_net -net FS_INIT_N_I_1 [get_bd_ports FS_INIT_N_I] [get_bd_pins sc_io_0/BPL_FS_INIT_N_I]
  connect_bd_net -net GTREFCLK0_N_I_1 [get_bd_ports GTREFCLK0_N_I] [get_bd_pins hier_ethernet_0/GTREFCLK0_N_I]
  connect_bd_net -net GTREFCLK0_P_I_1 [get_bd_ports GTREFCLK0_P_I] [get_bd_pins hier_ethernet_0/GTREFCLK0_P_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_05_I_1 [get_bd_pins hier_serdes_dma_if/Slot05_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_05_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_07_I_1 [get_bd_pins hier_serdes_dma_if/Slot07_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_07_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_08_I_1 [get_bd_pins hier_serdes_dma_if/Slot08_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_08_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_09_I_1 [get_bd_pins hier_serdes_dma_if/Slot09_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_09_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_10_I_1 [get_bd_pins hier_serdes_dma_if/Slot10_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_10_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_11_I_1 [get_bd_pins hier_serdes_dma_if/Slot11_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_11_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_13_I_1 [get_bd_pins hier_serdes_dma_if/Slot13_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_13_I]
  connect_bd_net -net IDLE_PATTERN_DETECT_15_I_1 [get_bd_pins hier_serdes_dma_if/Slot15_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_15_I]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets IDLE_PATTERN_DETECT_15_I_1]
  connect_bd_net -net ISERDES_RCVR_PACKET_COUNT_RST [get_bd_pins hier_serdes_dma_if/Rst_RcvPktCount_i] [get_bd_pins register_bank_0/ISERDES_RCVR_PACKET_COUNT_RST_O]
  connect_bd_net -net ISERDES_RECEIVER_RESYNC [get_bd_pins hier_serdes_dma_if/Resync_i] [get_bd_pins register_bank_0/ISERDES_RECEIVER_RESYNC_O]
  connect_bd_net -net LD_I_1 [get_bd_ports LD_I] [get_bd_pins register_bank_0/LD_I]
  connect_bd_net -net LOCAL_TRIGGER_N_I_1 [get_bd_ports TRIGGER_BPL_N_I] [get_bd_pins trigger_manager_0/TRIGGER_BPL_N_I]
  connect_bd_net -net LOCAL_TRIGGER_P_I_1 [get_bd_ports TRIGGER_BPL_P_I] [get_bd_pins trigger_manager_0/TRIGGER_BPL_P_I]
  connect_bd_net -net LOCAL_TR_INFO_N_I_1 [get_bd_ports TR_INFO_BPL_N_I] [get_bd_pins trigger_manager_0/TRIGGER_BPL_SDATA_N_I]
  connect_bd_net -net LOCAL_TR_INFO_P_I_1 [get_bd_ports TR_INFO_BPL_P_I] [get_bd_pins trigger_manager_0/TRIGGER_BPL_SDATA_P_I]
  connect_bd_net -net MASTER_SPI_MISO_I_1 [get_bd_ports MASTER_SPI_MISO_I] [get_bd_pins axi_quad_spi_to_bpl_0/io1_i]
  connect_bd_net -net S00_ARESETN_1 [get_bd_pins axi_interconnect_mem/M00_ARESETN] [get_bd_pins reset_generator_0/BUS_RST_N_02_O]
  connect_bd_net -net SERDES_CLK_MGR_LOCK_I_1 [get_bd_pins clk_wiz_serdes/locked] [get_bd_pins hier_serdes_dma_if/SerdesClk_Lock_i] [get_bd_pins register_bank_0/SERDES_CLK_MGR_LOCK_I]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets SERDES_CLK_MGR_LOCK_I_1]
  connect_bd_net -net SFP_1_LOS_I_1 [get_bd_ports SFP_1_LOS_I] [get_bd_pins sc_io_0/SFP_1_LOS_I]
  connect_bd_net -net SFP_1_MOD_I_1 [get_bd_ports SFP_1_MOD_I] [get_bd_pins sc_io_0/SFP_1_MOD_I]
  connect_bd_net -net SFP_1_TX_FAULT_I_1 [get_bd_ports SFP_1_TX_FAULT_I] [get_bd_pins sc_io_0/SFP_1_TX_FAULT_I]
  connect_bd_net -net SFP_2_LOS_I_1 [get_bd_ports SFP_2_LOS_I] [get_bd_pins sc_io_0/SFP_2_LOS_I]
  connect_bd_net -net SFP_2_MOD_I_1 [get_bd_ports SFP_2_MOD_I] [get_bd_pins sc_io_0/SFP_2_MOD_I]
  connect_bd_net -net SFP_2_TX_FAULT_I_1 [get_bd_ports SFP_2_TX_FAULT_I] [get_bd_pins sc_io_0/SFP_2_TX_FAULT_I]
  connect_bd_net -net SPI_CS_FPGA_N_I_1 [get_bd_ports SPI_CS_FPGA_N_I] [get_bd_pins sc_io_0/BPL_SPI_CS_FPGA_N_I]
  connect_bd_net -net SPR_N_I_1 [get_bd_ports TR_INFO_FCI_N_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_SDATA_N_I]
  connect_bd_net -net SPR_P_I_1 [get_bd_ports TR_INFO_FCI_P_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_SDATA_P_I]
  connect_bd_net -net SYNC_DONE_05_I_1 [get_bd_pins hier_serdes_dma_if/Slot05_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_05_I]
  connect_bd_net -net SYNC_DONE_07_I_1 [get_bd_pins hier_serdes_dma_if/Slot07_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_07_I]
  connect_bd_net -net SYNC_DONE_08_I_1 [get_bd_pins hier_serdes_dma_if/Slot08_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_08_I]
  connect_bd_net -net SYNC_DONE_09_I_1 [get_bd_pins hier_serdes_dma_if/Slot09_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_09_I]
  connect_bd_net -net SYNC_DONE_10_I_1 [get_bd_pins hier_serdes_dma_if/Slot10_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_10_I]
  connect_bd_net -net SYNC_DONE_11_I_1 [get_bd_pins hier_serdes_dma_if/Slot11_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_11_I]
  connect_bd_net -net SYNC_DONE_13_I_1 [get_bd_pins hier_serdes_dma_if/Slot13_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_13_I]
  connect_bd_net -net SYNC_DONE_15_I_1 [get_bd_pins hier_serdes_dma_if/Slot15_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_15_I]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets SYNC_DONE_15_I_1]
  connect_bd_net -net SYNC_ERRORS_04_I_1 [get_bd_pins hier_serdes_dma_if/Slot04_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_04_I]
  connect_bd_net -net SYNC_ERRORS_05_I_1 [get_bd_pins hier_serdes_dma_if/Slot05_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_05_I]
  connect_bd_net -net SYNC_ERRORS_06_I_1 [get_bd_pins hier_serdes_dma_if/Slot06_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_06_I]
  connect_bd_net -net SYNC_ERRORS_07_I_1 [get_bd_pins hier_serdes_dma_if/Slot07_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_07_I]
  connect_bd_net -net SYNC_ERRORS_08_I_1 [get_bd_pins hier_serdes_dma_if/Slot08_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_08_I]
  connect_bd_net -net SYNC_ERRORS_09_I_1 [get_bd_pins hier_serdes_dma_if/Slot09_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_09_I]
  connect_bd_net -net SYNC_ERRORS_10_I_1 [get_bd_pins hier_serdes_dma_if/Slot10_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_10_I]
  connect_bd_net -net SYNC_ERRORS_11_I_1 [get_bd_pins hier_serdes_dma_if/Slot11_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_11_I]
  connect_bd_net -net SYNC_ERRORS_12_I_1 [get_bd_pins hier_serdes_dma_if/Slot12_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_12_I]
  connect_bd_net -net SYNC_ERRORS_13_I_1 [get_bd_pins hier_serdes_dma_if/Slot13_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_13_I]
  connect_bd_net -net SYNC_ERRORS_14_I_1 [get_bd_pins hier_serdes_dma_if/Slot14_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_14_I]
  connect_bd_net -net SYNC_ERRORS_15_I_1 [get_bd_pins hier_serdes_dma_if/Slot15_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_15_I]
  connect_bd_net -net SYNC_ERRORS_17_I_1 [get_bd_pins hier_serdes_dma_if/Slot16_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_17_I]
  connect_bd_net -net SYNC_N_I_1 [get_bd_ports SYNC_FCI_N_I] [get_bd_pins tr_sync_manager_0/TR_SYNC_FCI_N_I]
  connect_bd_net -net SYNC_P_I_1 [get_bd_ports SYNC_FCI_P_I] [get_bd_pins tr_sync_manager_0/TR_SYNC_FCI_P_I]
  connect_bd_net -net TRG_N_I_1 [get_bd_ports TRIGGER_FCI_N_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_N_I]
  connect_bd_net -net TRG_P_I_1 [get_bd_ports TRIGGER_FCI_P_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_P_I]
  connect_bd_net -net axi_dcb_register_bank_0_DAQ_SOFT_TRIGGER_O [get_bd_pins register_bank_0/DAQ_SOFT_TRIGGER_O] [get_bd_pins trigger_manager_0/TRIGGER_PS_I]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_CLK_SRC_SEL_O [get_bd_ports CLK_SEL_O] [get_bd_pins register_bank_0/CLK_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_SYNC_DELAY_O [get_bd_pins register_bank_0/SYNC_DELAY_O] [get_bd_pins tr_sync_manager_0/DELAY_I]
  connect_bd_net -net axi_dcb_register_bank_0_TRIGGER_MGR_RST_O [get_bd_pins register_bank_0/TRIGGER_MGR_RST_O] [get_bd_pins trigger_manager_0/RST_I]
  connect_bd_net -net axi_quad_spi_to_bpl_0_io0_o [get_bd_ports MASTER_SPI_MOSI_O] [get_bd_pins axi_quad_spi_to_bpl_0/io0_o]
  connect_bd_net -net axi_quad_spi_to_bpl_0_ip2intc_irpt [get_bd_pins axi_quad_spi_to_bpl_0/ip2intc_irpt] [get_bd_pins xlconcat_irq_f2p/In0]
  connect_bd_net -net axi_quad_spi_to_bpl_0_sck_o [get_bd_ports MASTER_SPI_CLK_O] [get_bd_pins axi_quad_spi_to_bpl_0/sck_o]
  connect_bd_net -net axi_quad_spi_to_bpl_0_ss_o [get_bd_pins axi_quad_spi_to_bpl_0/ss_o] [get_bd_pins sc_io_0/SPI_CS_N_I]
  connect_bd_net -net busy_manager_0_BUSY_TO_BPL_N_O [get_bd_ports BUSY_BPL_N_O] [get_bd_pins busy_manager_0/BUSY_TO_BPL_N_O]
  connect_bd_net -net busy_manager_0_BUSY_TO_FCI_N_O [get_bd_ports BUSY_FCI_N_N_O] [get_bd_pins busy_manager_0/BUSY_TO_FCI_N_N_O]
  connect_bd_net -net busy_manager_0_BUSY_TO_FCI_P_O [get_bd_ports BUSY_FCI_N_P_O] [get_bd_pins busy_manager_0/BUSY_TO_FCI_N_P_O]
  connect_bd_net -net clk_wiz_0_clk_out_200MHz [get_bd_pins clk_wiz_refclks/clk_out_200MHz] [get_bd_pins hier_serdes_dma_if/IdelayCtrl_Refclk_i]
  connect_bd_net -net clk_wiz_0_clk_out_240MHz [get_bd_pins clk_wiz_refclks/clk_out_240MHz] [get_bd_pins tr_sync_manager_0/DLY_CLK_I]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins clk_wiz_refclks/locked] [get_bd_pins hier_serdes_dma_if/Idelay_Refclk_Lock_i] [get_bd_pins register_bank_0/WDB_CLK_MGR_LOCK_I]
  connect_bd_net -net clk_wiz_serdes_clkfb_out [get_bd_pins clk_wiz_serdes/clkfb_in] [get_bd_pins clk_wiz_serdes/clkfb_out]
  connect_bd_net -net const_aux_clk_n_dout [get_bd_ports AUX_OUT_N_O] [get_bd_pins hier_const_aux_clk/AUX_OUT_N_O]
  connect_bd_net -net const_aux_clk_p_dout [get_bd_ports AUX_OUT_P_O] [get_bd_pins hier_const_aux_clk/AUX_OUT_P_O]
  connect_bd_net -net const_ce_dis_1_dout [get_bd_ports CE_DIS_1_O] [get_bd_pins hier_const_clk_distr_ctrl/CE_DIS_1_O]
  connect_bd_net -net const_ce_dis_2_dout [get_bd_ports CE_DIS_2_O] [get_bd_pins hier_const_clk_distr_ctrl/CE_DIS_2_O]
  connect_bd_net -net const_clk_dis_1_dout [get_bd_ports CLK_DIS_1_O] [get_bd_pins hier_const_clk_distr_ctrl/CLK_DIS_1_O]
  connect_bd_net -net const_clk_dis_2_dout [get_bd_ports CLK_DIS_2_O] [get_bd_pins hier_const_clk_distr_ctrl/CLK_DIS_2_O]
  connect_bd_net -net const_dir_ext_trig_in_dout [get_bd_ports DIR_EXT_TRG_IN_O] [get_bd_pins const_dir_ext_trig_0/DIR_EXT_TRG_IN_O]
  connect_bd_net -net const_dir_ext_trig_out_dout [get_bd_ports DIR_EXT_TRG_OUT_O] [get_bd_pins const_dir_ext_trig_0/DIR_EXT_TRG_OUT_O]
  connect_bd_net -net const_eth_col_dout [get_bd_pins hier_ethernet_0/dout2] [get_bd_pins ps_0/ENET0_GMII_COL] [get_bd_pins ps_0/ENET1_GMII_COL]
  connect_bd_net -net const_eth_crs_dout [get_bd_pins hier_ethernet_0/dout5] [get_bd_pins ps_0/ENET0_GMII_CRS] [get_bd_pins ps_0/ENET1_GMII_CRS]
  connect_bd_net -net const_info_dir_dout [get_bd_ports INFO_DIR_O] [get_bd_pins hier_const_bpl_dir/INFO_DIR_O]
  connect_bd_net -net const_led_hw_error_n_dout [get_bd_pins const_led_0/dout] [get_bd_pins led_ctrl_0/HW_ERROR_N_I]
  connect_bd_net -net const_led_hw_status_dout [get_bd_pins const_led_0/dout1] [get_bd_pins led_ctrl_0/HW_STATUS_I]
  connect_bd_net -net const_mscb_data_out_dout [get_bd_ports MSCB_DATA_O] [get_bd_pins const_bpl_signals_0/MSCB_DATA_O]
  connect_bd_net -net const_mscb_drv_en_dout [get_bd_ports MSCB_DRV_EN_O] [get_bd_pins const_bpl_signals_0/MSCB_DRV_EN_O]
  connect_bd_net -net const_slave_spi_miso_dout [get_bd_ports SLAVE_SPI_MISO_O] [get_bd_pins const_bpl_signals_0/SLAVE_SPI_MISO_O]
  connect_bd_net -net const_slave_spi_miso_en_n_dout [get_bd_ports SLAVE_SPI_MISO_EN_N_O] [get_bd_pins const_bpl_signals_0/SLAVE_SPI_MISO_EN_N_O]
  connect_bd_net -net const_sync_dir_dout [get_bd_ports SYNC_DIR_O] [get_bd_pins hier_const_bpl_dir/SYNC_DIR_O]
  connect_bd_net -net const_trg_dir_dout [get_bd_ports TRG_DIR_O] [get_bd_pins hier_const_bpl_dir/TRG_DIR_O]
  connect_bd_net -net dma_pkt_sched_axi_0_Irq [get_bd_pins hier_serdes_dma_if/Irq] [get_bd_pins system_ila_0/probe13] [get_bd_pins xlconcat_irq_f2p/In1]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets dma_pkt_sched_axi_0_Irq]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_dv [get_bd_pins hier_ethernet_0/gmii_rx_dv] [get_bd_pins led_ctrl_0/ETH0_RX_DV_I] [get_bd_pins ps_0/ENET0_GMII_RX_DV]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_er [get_bd_pins hier_ethernet_0/gmii_rx_er] [get_bd_pins ps_0/ENET0_GMII_RX_ER]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rxd [get_bd_pins hier_ethernet_0/gmii_rxd] [get_bd_pins ps_0/ENET0_GMII_RXD]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_bufg_out [get_bd_pins hier_clock_measure_0/In0] [get_bd_pins hier_ethernet_0/gtrefclk_bufg]
  connect_bd_net -net gig_ethernet_pcs_pma_0_mdio_o [get_bd_pins hier_ethernet_0/mdio_o] [get_bd_pins ps_0/ENET0_MDIO_I]
  connect_bd_net -net gig_ethernet_pcs_pma_0_sgmii_clk_r [get_bd_pins hier_clock_measure_0/In3] [get_bd_pins hier_ethernet_0/sgmii_clk_r] [get_bd_pins ps_0/ENET0_GMII_RX_CLK] [get_bd_pins ps_0/ENET0_GMII_TX_CLK]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk2_out [get_bd_pins hier_clock_measure_0/In2] [get_bd_pins hier_ethernet_0/userclk2]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk_out [get_bd_pins hier_clock_measure_0/In1] [get_bd_pins hier_ethernet_0/rxuserclk]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_dv [get_bd_pins hier_ethernet_0/gmii_rx_dv1] [get_bd_pins led_ctrl_0/ETH1_RX_DV_I] [get_bd_pins ps_0/ENET1_GMII_RX_DV]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_er [get_bd_pins hier_ethernet_0/gmii_rx_er1] [get_bd_pins ps_0/ENET1_GMII_RX_ER]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rxd [get_bd_pins hier_ethernet_0/gmii_rxd1] [get_bd_pins ps_0/ENET1_GMII_RXD]
  connect_bd_net -net gig_ethernet_pcs_pma_1_mdio_o [get_bd_pins hier_ethernet_0/mdio_o1] [get_bd_pins ps_0/ENET1_MDIO_I]
  connect_bd_net -net gig_ethernet_pcs_pma_1_sgmii_clk_r [get_bd_pins hier_clock_measure_0/In4] [get_bd_pins hier_ethernet_0/sgmii_clk_r1] [get_bd_pins ps_0/ENET1_GMII_RX_CLK] [get_bd_pins ps_0/ENET1_GMII_TX_CLK]
  connect_bd_net -net hier_serdes_dma_if_CrcOk_Dbg_o [get_bd_pins hier_serdes_dma_if/CrcOk_Dbg_o] [get_bd_pins xlslice_1/Din]
  connect_bd_net -net hier_serdes_dma_if_ParallelData_Dbg_o [get_bd_pins hier_serdes_dma_if/ParallelData_Dbg_o] [get_bd_pins xlslice_0/Din]
  connect_bd_net -net hier_serdes_dma_if_Slot00_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot00_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_00_I]
  connect_bd_net -net hier_serdes_dma_if_Slot00_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot00_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_00_I]
  connect_bd_net -net hier_serdes_dma_if_Slot00_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot00_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_0_I]
  connect_bd_net -net hier_serdes_dma_if_Slot00_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot00_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_0_I]
  connect_bd_net -net hier_serdes_dma_if_Slot00_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot00_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_0_I]
  connect_bd_net -net hier_serdes_dma_if_Slot01_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot01_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_01_I]
  connect_bd_net -net hier_serdes_dma_if_Slot01_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot01_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_01_I]
  connect_bd_net -net hier_serdes_dma_if_Slot01_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot01_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_1_I]
  connect_bd_net -net hier_serdes_dma_if_Slot01_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot01_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_1_I]
  connect_bd_net -net hier_serdes_dma_if_Slot01_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot01_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_1_I]
  connect_bd_net -net hier_serdes_dma_if_Slot02_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot02_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_02_I]
  connect_bd_net -net hier_serdes_dma_if_Slot02_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot02_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_02_I]
  connect_bd_net -net hier_serdes_dma_if_Slot02_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot02_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_2_I]
  connect_bd_net -net hier_serdes_dma_if_Slot02_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot02_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_2_I]
  connect_bd_net -net hier_serdes_dma_if_Slot02_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot02_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_2_I]
  connect_bd_net -net hier_serdes_dma_if_Slot03_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot03_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_03_I]
  connect_bd_net -net hier_serdes_dma_if_Slot03_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot03_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_03_I]
  connect_bd_net -net hier_serdes_dma_if_Slot03_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot03_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_3_I]
  connect_bd_net -net hier_serdes_dma_if_Slot03_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot03_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_3_I]
  connect_bd_net -net hier_serdes_dma_if_Slot03_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot03_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_3_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot04_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot04_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot04_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot04_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot04_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot04_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_4_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot04_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_04_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot04_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_4_I]
  connect_bd_net -net hier_serdes_dma_if_Slot04_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot04_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_4_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot05_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_05_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot05_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_05_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot05_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_05_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot05_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_05_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot05_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_5_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot05_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_5_I]
  connect_bd_net -net hier_serdes_dma_if_Slot05_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot05_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_5_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot06_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot06_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot06_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot06_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot06_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot06_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_6_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot06_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_06_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot06_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_6_I]
  connect_bd_net -net hier_serdes_dma_if_Slot06_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot06_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_6_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot07_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_07_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot07_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_07_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot07_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_07_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot07_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_07_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot07_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_7_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot07_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_7_I]
  connect_bd_net -net hier_serdes_dma_if_Slot07_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot07_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_7_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot08_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_08_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot08_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_08_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot08_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_08_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot08_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_08_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot08_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_8_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot08_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_8_I]
  connect_bd_net -net hier_serdes_dma_if_Slot08_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot08_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_8_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot09_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_09_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot09_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_09_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot09_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_09_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot09_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_09_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot09_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_9_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot09_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_9_I]
  connect_bd_net -net hier_serdes_dma_if_Slot09_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot09_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_9_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot10_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot10_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot10_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot10_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot10_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot10_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot10_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot10_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_10_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot11_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot11_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot11_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot11_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot11_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot11_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot11_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot11_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_11_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot12_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot12_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot12_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot12_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot12_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot12_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot12_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot12_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot12_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot12_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_12_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot13_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot13_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot13_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot13_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot13_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot13_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot13_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot13_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_13_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot14_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot14_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot14_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot14_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot14_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot14_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot14_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot14_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot14_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot14_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_14_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot15_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot15_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot15_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot15_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot15_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot15_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot15_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot15_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_15_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_BitslipOk_o [get_bd_pins hier_serdes_dma_if/Slot16_BitslipOk_o] [get_bd_pins register_bank_0/BITSLIP_OK_17_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_Bytes_o [get_bd_pins hier_serdes_dma_if/Slot16_Bytes_o] [get_bd_pins system_ila_0/probe4]
  connect_bd_net -net hier_serdes_dma_if_Slot16_CrcError_o [get_bd_pins hier_serdes_dma_if/Slot16_CrcError_o] [get_bd_pins system_ila_0/probe6]
  connect_bd_net -net hier_serdes_dma_if_Slot16_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot16_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_17_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_DatagramError_o [get_bd_pins hier_serdes_dma_if/Slot16_DatagramError_o] [get_bd_pins system_ila_0/probe8]
  connect_bd_net -net hier_serdes_dma_if_Slot16_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot16_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_17_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_DelayOk_o [get_bd_pins hier_serdes_dma_if/Slot16_DelayOk_o] [get_bd_pins register_bank_0/DELAY_OK_17_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_EOE_o [get_bd_pins hier_serdes_dma_if/Slot16_EOE_o] [get_bd_pins system_ila_0/probe10]
  connect_bd_net -net hier_serdes_dma_if_Slot16_FrameError_o [get_bd_pins hier_serdes_dma_if/Slot16_FrameError_o] [get_bd_pins system_ila_0/probe7]
  connect_bd_net -net hier_serdes_dma_if_Slot16_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot16_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_17_I] [get_bd_pins system_ila_0/probe5]
  connect_bd_net -net hier_serdes_dma_if_Slot16_PktBytes_o [get_bd_pins hier_serdes_dma_if/Slot16_PktBytes_o] [get_bd_pins system_ila_0/probe2]
  connect_bd_net -net hier_serdes_dma_if_Slot16_PktValid_o [get_bd_pins hier_serdes_dma_if/Slot16_PktValid_o] [get_bd_pins system_ila_0/probe3]
  connect_bd_net -net hier_serdes_dma_if_Slot16_RcvPktCount_o [get_bd_pins hier_serdes_dma_if/Slot16_RcvPktCount_o] [get_bd_pins register_bank_0/SD_PKT_CNT_17_I]
  connect_bd_net -net hier_serdes_dma_if_Slot16_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot16_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_17_I]
  set_property HDL_ATTRIBUTE.DEBUG {true} [get_bd_nets hier_serdes_dma_if_Slot16_SyncDone_o]
  connect_bd_net -net hier_serdes_dma_if_Slot16_SyncError_o [get_bd_pins hier_serdes_dma_if/Slot16_SyncError_o] [get_bd_pins system_ila_0/probe9]
  connect_bd_net -net hier_serdes_dma_if_Slot16_TestEye_o [get_bd_pins hier_serdes_dma_if/Slot16_TestEye_o] [get_bd_pins register_bank_0/SD_EYE_17_I] [get_bd_pins system_ila_0/probe11]
  connect_bd_net -net hier_serdes_dma_if_Slot16_TestTap_o [get_bd_pins hier_serdes_dma_if/Slot16_TestTap_o] [get_bd_pins register_bank_0/SD_TAP_17_I] [get_bd_pins system_ila_0/probe12]
  connect_bd_net -net hier_serdes_ready_const_DCB_READY_N_O1 [get_bd_ports SD_READY_N_O] [get_bd_pins hier_serdes_dma_if/SD_READY_N_O]
  connect_bd_net -net hier_serdes_ready_const_DCB_READY_P_O1 [get_bd_ports SD_READY_P_O] [get_bd_pins hier_serdes_dma_if/SD_READY_P_O]
  connect_bd_net -net ps_0_ENET0_GMII_TXD [get_bd_pins hier_ethernet_0/gmii_txd] [get_bd_pins ps_0/ENET0_GMII_TXD]
  connect_bd_net -net ps_0_ENET0_GMII_TX_EN [get_bd_pins hier_ethernet_0/gmii_tx_en] [get_bd_pins led_ctrl_0/ETH0_TX_EN_I] [get_bd_pins ps_0/ENET0_GMII_TX_EN]
  connect_bd_net -net ps_0_ENET0_GMII_TX_ER [get_bd_pins hier_ethernet_0/gmii_tx_er] [get_bd_pins ps_0/ENET0_GMII_TX_ER]
  connect_bd_net -net ps_0_ENET0_MDIO_MDC [get_bd_pins hier_ethernet_0/mdc] [get_bd_pins ps_0/ENET0_MDIO_MDC]
  connect_bd_net -net ps_0_ENET0_MDIO_O [get_bd_pins hier_ethernet_0/mdio_i] [get_bd_pins ps_0/ENET0_MDIO_O]
  connect_bd_net -net ps_0_ENET1_GMII_TXD [get_bd_pins hier_ethernet_0/gmii_txd1] [get_bd_pins ps_0/ENET1_GMII_TXD]
  connect_bd_net -net ps_0_ENET1_GMII_TX_EN [get_bd_pins hier_ethernet_0/gmii_tx_en1] [get_bd_pins led_ctrl_0/ETH1_TX_EN_I] [get_bd_pins ps_0/ENET1_GMII_TX_EN]
  connect_bd_net -net ps_0_ENET1_GMII_TX_ER [get_bd_pins hier_ethernet_0/gmii_tx_er1] [get_bd_pins ps_0/ENET1_GMII_TX_ER]
  connect_bd_net -net ps_0_ENET1_MDIO_MDC [get_bd_pins hier_ethernet_0/mdc1] [get_bd_pins ps_0/ENET1_MDIO_MDC]
  connect_bd_net -net ps_0_ENET1_MDIO_O [get_bd_pins hier_ethernet_0/mdio_i1] [get_bd_pins ps_0/ENET1_MDIO_O]
  connect_bd_net -net ps_0_FCLK_CLK0 [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M03_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins hier_serdes_dma_if/S_Axi_Aclk] [get_bd_pins ps_0/FCLK_CLK0] [get_bd_pins ps_0/M_AXI_GP0_ACLK] [get_bd_pins reset_generator_0/BUS_RST_00_CLK_I] [get_bd_pins reset_generator_0/BUS_RST_N_00_CLK_I] [get_bd_pins reset_generator_0/REF_CLK_I] [get_bd_pins sc_io_0/CLK_I]
  connect_bd_net -net ps_0_FCLK_CLK1 [get_bd_pins hier_ethernet_0/independent_clock_bufg] [get_bd_pins ps_0/FCLK_CLK1]
  connect_bd_net -net ps_0_FCLK_CLK2 [get_bd_pins axi_interconnect_mem/M00_ACLK] [get_bd_pins ps_0/FCLK_CLK2] [get_bd_pins ps_0/S_AXI_HP0_ACLK] [get_bd_pins reset_generator_0/BUS_RST_N_02_CLK_I] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_02_CLK_I]
  connect_bd_net -net ps_0_FCLK_RESET0_N [get_bd_pins ps_0/FCLK_RESET0_N] [get_bd_pins reset_generator_0/RST_I]
  connect_bd_net -net ps_0_GPIO_O [get_bd_pins ps_0/GPIO_O] [get_bd_pins sc_io_0/GPIO_EMIO_I]
  connect_bd_net -net register_bank_0_AUTO_TRIGGER_PERIOD_O [get_bd_pins register_bank_0/AUTO_TRIGGER_PERIOD_O] [get_bd_pins trigger_manager_0/AUTO_TRG_PERIOD_I]
  connect_bd_net -net register_bank_0_BUS_CLK_SEL_O [get_bd_ports BUS_CLK_SEL_O] [get_bd_pins register_bank_0/BUS_CLK_SEL_O]
  connect_bd_net -net register_bank_0_CLK_SEL_EXT_O [get_bd_ports CLK_SEL_EXT_O] [get_bd_pins register_bank_0/CLK_SEL_EXT_O]
  connect_bd_net -net register_bank_0_DAQ_AUTO_O [get_bd_pins register_bank_0/DAQ_AUTO_O] [get_bd_pins trigger_manager_0/DAQ_AUTO_I]
  connect_bd_net -net register_bank_0_ISERDES_RCVR_ERROR_COUNT_RST_O [get_bd_pins hier_serdes_dma_if/Rst_Errors_i] [get_bd_pins register_bank_0/ISERDES_RCVR_ERROR_COUNT_RST_O]
  connect_bd_net -net register_bank_0_LMK_SYNC_DCB_O [get_bd_pins register_bank_0/LMK_SYNC_DCB_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_DCB_PS_I]
  connect_bd_net -net register_bank_0_MOSI_DIS_2_O [get_bd_ports MOSI_DIS_1_O] [get_bd_ports MOSI_DIS_2_O] [get_bd_pins register_bank_0/MOSI_DIS_2_O]
  connect_bd_net -net register_bank_0_RECONFIGURE_FPGA_O [get_bd_pins register_bank_0/RECONFIGURE_FPGA_O] [get_bd_pins sc_io_0/FPGA_RESET_I]
  connect_bd_net -net register_bank_0_TR_SYNC_BPL_O [get_bd_pins register_bank_0/TR_SYNC_BPL_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_BPL_PS_I]
  connect_bd_net -net register_bank_0_WDB_REFCLK_MGR_RST_O [get_bd_pins clk_wiz_refclks/reset] [get_bd_pins register_bank_0/WDB_REFCLK_MGR_RST_O]
  connect_bd_net -net register_bank_0_WDB_SERDES_CLK_MGR_RST_O [get_bd_pins clk_wiz_serdes/reset] [get_bd_pins register_bank_0/WDB_SERDES_CLK_MGR_RST_O]
  connect_bd_net -net reset_generator_0_BUS_RST_N_01_O [get_bd_pins hier_serdes_dma_if/M_Axi_Aresetn] [get_bd_pins reset_generator_0/BUS_RST_N_01_O] [get_bd_pins system_ila_0/resetn]
  connect_bd_net -net reset_generator_0_BUS_RST_N_O [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/M03_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins hier_serdes_dma_if/S_Axi_Aresetn] [get_bd_pins reset_generator_0/BUS_RST_N_00_O]
  connect_bd_net -net reset_generator_0_PERIPHERAL_RST_00_O [get_bd_pins hier_serdes_dma_if/Rst_i] [get_bd_pins register_bank_0/ISERDES_RECEIVER_RST_O]
  connect_bd_net -net sc_io_0_BPL_FS_INIT_N_O [get_bd_ports FLASH_SELECT_INIT_N_O] [get_bd_pins sc_io_0/BPL_FS_INIT_N_O]
  connect_bd_net -net sc_io_0_BPL_MASTER_SPI_DE_N_O [get_bd_ports MASTER_SPI_DE_N_O] [get_bd_pins sc_io_0/BPL_MASTER_SPI_DE_N_O]
  connect_bd_net -net sc_io_0_BPL_SPI_CS_N_O [get_bd_ports SPI_CS_N_O] [get_bd_pins sc_io_0/BPL_SPI_CS_N_O]
  connect_bd_net -net sc_io_0_ETH0_RESET_O [get_bd_pins hier_ethernet_0/reset] [get_bd_pins sc_io_0/ETH0_RESET_O] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net sc_io_0_ETH1_RESET_O [get_bd_pins hier_ethernet_0/reset1] [get_bd_pins sc_io_0/ETH1_RESET_O] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net sc_io_0_FPGA_RESET_N_O [get_bd_ports RESET_FPGA_BUS_N_O] [get_bd_pins sc_io_0/FPGA_RESET_N_O]
  connect_bd_net -net sc_io_0_GPIO_EMIO_O [get_bd_pins ps_0/GPIO_I] [get_bd_pins sc_io_0/GPIO_EMIO_O]
  connect_bd_net -net sc_io_0_SFP_1_RS_O [get_bd_ports SFP_1_RS_O] [get_bd_pins sc_io_0/SFP_1_RS_O]
  connect_bd_net -net sc_io_0_SFP_1_TX_DISABLE_O [get_bd_ports SFP_1_TX_DISABLE_O] [get_bd_pins sc_io_0/SFP_1_TX_DISABLE_O]
  connect_bd_net -net sc_io_0_SFP_2_RS_O [get_bd_ports SFP_2_RS_O] [get_bd_pins sc_io_0/SFP_2_RS_O]
  connect_bd_net -net sc_io_0_SFP_2_TX_DISABLE_O [get_bd_ports SFP_2_TX_DISABLE_O] [get_bd_pins sc_io_0/SFP_2_TX_DISABLE_O]
  connect_bd_net -net sc_io_0_SW_STATE_O [get_bd_pins led_ctrl_0/SW_STATUS_I] [get_bd_pins sc_io_0/SW_STATE_O]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot00_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot00_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_FrameErrors_o [get_bd_pins hier_serdes_dma_if/Slot00_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot00_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_SyncErrors_o [get_bd_pins hier_serdes_dma_if/Slot00_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot00_SyncStatus_o [get_bd_pins hier_serdes_dma_if/Slot00_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_00_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot01_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_01_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot01_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_01_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot01_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_01_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot01_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_01_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot01_SyncErrors_o [get_bd_pins hier_serdes_dma_if/Slot01_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_01_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot02_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot02_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_FrameErrors_o [get_bd_pins hier_serdes_dma_if/Slot02_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot02_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot02_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot02_SyncErrors_o [get_bd_pins hier_serdes_dma_if/Slot02_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_02_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_CrcErrors_o [get_bd_pins hier_serdes_dma_if/Slot03_CrcErrors_o] [get_bd_pins register_bank_0/CRC_ERRORS_03_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_DatagramErrors_o [get_bd_pins hier_serdes_dma_if/Slot03_DatagramErrors_o] [get_bd_pins register_bank_0/DATAGRAM_ERRORS_03_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_FrameErrors_o [get_bd_pins hier_serdes_dma_if/Slot03_FrameErrors_o] [get_bd_pins register_bank_0/FRAME_ERRORS_03_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_LinkIdle_o [get_bd_pins hier_serdes_dma_if/Slot03_LinkIdle_o] [get_bd_pins register_bank_0/IDLE_PATTERN_DETECT_03_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_SyncDone_o [get_bd_pins hier_serdes_dma_if/Slot03_SyncDone_o] [get_bd_pins register_bank_0/SYNC_DONE_03_I]
  connect_bd_net -net serdes_pkt_rcvr_0_Slot03_SyncErrors_o [get_bd_pins hier_serdes_dma_if/Slot03_SyncErrors_o] [get_bd_pins register_bank_0/SYNC_ERRORS_03_I]
  connect_bd_net -net tr_sync_manager_0_TR_SYNC_BPL_O [get_bd_ports TR_SYNC_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_BPL_O]
  connect_bd_net -net tr_sync_manager_0_TR_SYNC_DCB_N_O [get_bd_ports SYNC_LMK_N_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_DCB_N_O]
  connect_bd_net -net trigger_manager_0_PERR_COUNT_O [get_bd_pins register_bank_0/TRB_PARITY_ERROR_COUNT_I] [get_bd_pins trigger_manager_0/PERR_COUNT_O]
  connect_bd_net -net trigger_manager_0_PERR_O [get_bd_pins register_bank_0/TRB_FLAG_PARITY_ERROR_I] [get_bd_pins trigger_manager_0/PERR_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_BPL_O [get_bd_ports TRIGGER_O] [get_bd_pins trigger_manager_0/TRIGGER_BPL_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_MCX_O [get_bd_ports EXT_TRG_OUT_O] [get_bd_pins trigger_manager_0/TRIGGER_MCX_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_PDATA_O [get_bd_pins register_bank_0/Din1] [get_bd_pins trigger_manager_0/TRIGGER_PDATA_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_SDATA_O [get_bd_ports TR_INFO_O] [get_bd_pins trigger_manager_0/TRIGGER_SDATA_O]
  connect_bd_net -net trigger_manager_0_VALID_O [get_bd_pins register_bank_0/TRB_FLAG_NEW_I] [get_bd_pins trigger_manager_0/VALID_O]
  connect_bd_net -net util_build_info_0_BUILD_DAY_O [get_bd_pins register_bank_0/FW_BUILD_DAY_I] [get_bd_pins util_build_info_0/BUILD_DAY_O]
  connect_bd_net -net util_build_info_0_BUILD_HOUR_O [get_bd_pins register_bank_0/FW_BUILD_HOUR_I] [get_bd_pins util_build_info_0/BUILD_HOUR_O]
  connect_bd_net -net util_build_info_0_BUILD_MINUTE_O [get_bd_pins register_bank_0/FW_BUILD_MINUTE_I] [get_bd_pins util_build_info_0/BUILD_MINUTE_O]
  connect_bd_net -net util_build_info_0_BUILD_MONTH_O [get_bd_pins register_bank_0/FW_BUILD_MONTH_I] [get_bd_pins util_build_info_0/BUILD_MONTH_O]
  connect_bd_net -net util_build_info_0_BUILD_SECOND_O [get_bd_pins register_bank_0/FW_BUILD_SECOND_I] [get_bd_pins util_build_info_0/BUILD_SECOND_O]
  connect_bd_net -net util_build_info_0_BUILD_YEAR_O [get_bd_pins register_bank_0/FW_BUILD_YEAR_I] [get_bd_pins util_build_info_0/BUILD_YEAR_O]
  connect_bd_net -net util_build_info_0_GIT_HASHTAG_O [get_bd_pins register_bank_0/FW_GIT_HASH_TAG_I] [get_bd_pins util_build_info_0/GIT_HASHTAG_O]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins axi_interconnect_0/M02_ACLK] [get_bd_pins axi_quad_spi_to_bpl_0/ext_spi_clk] [get_bd_pins axi_quad_spi_to_bpl_0/s_axi_aclk] [get_bd_pins hier_clock_measure_0/s00_axi_aclk] [get_bd_pins led_ctrl_0/LED_CLK_I] [get_bd_pins register_bank_0/s00_axi_aclk] [get_bd_pins reset_generator_0/PERIPHERAL_RST_00_CLK_I] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_00_CLK_I] [get_bd_pins util_build_info_0/CLK_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_OUT]
  connect_bd_net -net util_ds_buf_ext_clk_IBUF_OUT [get_bd_pins clk_wiz_refclks/clk_in1] [get_bd_pins clk_wiz_serdes/clk_in1] [get_bd_pins util_ds_buf_wdb_clk/IBUF_OUT]
  connect_bd_net -net util_led_ctrl_0_LED_B_N_O [get_bd_ports LED_B_N_O] [get_bd_pins led_ctrl_0/LED_B_N_O]
  connect_bd_net -net util_led_ctrl_0_LED_G_N_O [get_bd_ports LED_G_N_O] [get_bd_pins led_ctrl_0/LED_G_N_O]
  connect_bd_net -net util_led_ctrl_0_LED_R_N_O [get_bd_ports LED_R_N_O] [get_bd_pins led_ctrl_0/LED_R_N_O]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins led_ctrl_0/HW_ERROR_I] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconcat_irq_f2p_dout [get_bd_pins ps_0/IRQ_F2P] [get_bd_pins xlconcat_irq_f2p/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins system_ila_0/probe0] [get_bd_pins xlslice_0/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins system_ila_0/probe1] [get_bd_pins xlslice_1/Dout]
  connect_bd_net -net xlslice_peripheral_rst_n0_Dout [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/M01_ARESETN] [get_bd_pins axi_interconnect_0/M02_ARESETN] [get_bd_pins axi_quad_spi_to_bpl_0/s_axi_aresetn] [get_bd_pins hier_clock_measure_0/s00_axi_aresetn] [get_bd_pins register_bank_0/s00_axi_aresetn] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_00_O]
  connect_bd_net -net xlslice_spi_ss_tcb_0_Dout [get_bd_ports MASTER_SPI_TCB_SS_N_O] [get_bd_pins bus_split_spi_ss_0/MASTER_SPI_TCB_SS_N_O]
  connect_bd_net -net xlslice_spi_ss_wdb_0_Dout [get_bd_ports MASTER_SPI_WDB_SS_N_O] [get_bd_pins bus_split_spi_ss_0/MASTER_SPI_WDB_SS_N_O]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x44000000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs register_bank_0/axi_dcb_register_bank_0/S00_AXI/S00_AXI_reg] SEG_axi_dcb_register_bank_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41E00000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs axi_quad_spi_to_bpl_0/AXI_LITE/Reg] SEG_axi_quad_spi_to_bpl_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs hier_clock_measure_0/clock_measure_0/s00_axi/reg0] SEG_clock_measure_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x43C10000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs hier_serdes_dma_if/dma_pkt_sched_axi_0/S_Axi/reg0] SEG_dma_pkt_sched_axi_0_reg0
  create_bd_addr_seg -range 0x40000000 -offset 0x00000000 [get_bd_addr_spaces hier_serdes_dma_if/dma_pkt_sched_axi_0/M_Axi] [get_bd_addr_segs ps_0/S_AXI_HP0/HP0_DDR_LOWOCM] SEG_ps_0_HP0_DDR_LOWOCM


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


