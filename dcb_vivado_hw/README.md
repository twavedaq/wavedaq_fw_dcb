# General Information

## Maintainer
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Elmar Schmid [elmar.schmid@psi.ch]

## Changelog
-

## Description
This project contains the code for the Data Concentrator Board (DCB) of the MEG project.

# Dependencies

## Libraries
-

## Tools

* Vivado/SDK 2019.1

# How To ...

## ... Get a working-copy of the project

1. Clone the repository including all submodules to your PC
`git clone git@bitbucket.org:twavedaq/wavedaq.git`
2. Open Vivado
3. In the Vivado TCL shell navigate to [workingCopy]/dcb_vivado_hw
4. In the Vivado TCL shell run: `source ./project.tcl`. This command creates the complete project


## ... Add Vivado Project Changes to the project.tcl

The write_project_tcl/write_bd_tcl flow is used to ceck in the project into GIT. This is the official recommendation from Xilinx. It requires a few manual steps for checking in, which is a bit cumbersome, but it is the only way to bring a Vivado Project into a mergable and version control friendly form.

### Project Changes
If you changed the Vivado project itself and not the block-diagram (e.g. you added a new constraint file) it is easiest to write a new project-tcl file and merge the changes into the existing one (*[workingCopy]/Vivado/project.tcl*). To do so, follow the steps below.
1. In the Vivado TCL shell navigate to [workingCopy]/dcb_vivado_hw
2. execute `write_project_tcl project_new.tcl`
3. Merge changes from *project_new.tcl* to *project.tcl* using a merge tool (e.g. WinMerge)
4. Export the build results for SDK with `file copy -force dcb/dcb.runs/impl_1/dcb_wrapper.sysdef sdk_export/dcb_wrapper.hdf`

## ... Get the .bit file of the project
The bitfile of the FPGA design can be found in the dcb_wrapper.hdf file in [workingCopy]/dcb_vivado_hw/sdk_export. Note that the .hdf file is an archive containing the .bit file. It can be accessed with common archiving tools like 7Zip.
