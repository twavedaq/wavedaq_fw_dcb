# General Information

## Maintainer
Elmar Schmid [elmar.schmid@psi.ch]

## License
TBD

## Detailed Documentation
TBD

# Description
This IP core is developed for the Data Concentrator Board (DCB) of the WaveDAQ used in the MEGII project. It is used to write packets of measurement data received from the WaveDream2 Boards (WDB) or Trigger Control Boards (TCB) via the backplane in a crate (SERDES Link) to externa DDR3 memory via DMA.
The core is based on the multi stream DAQ core in the PSI opensource VHDL library. Since the original core was developed to handle continuous data streams, the WaveDAQ core had to be adapted in order to perform packet based operations. In many aspects, the packet based handling of the data is simpler than the original stream handling thus the code could be reduced in many places.

# Dependencies

This core was developed based on the multi stream DAQ core in the PSI opensource VHDL library (https://github.com/paulscherrerinstitute/psi_multi_stream_daq). In order to use this core, the dependencies of the library core must be resolved.

# Simulations and Testbenches

A regression test script for Modelsim is present. To run the regression test, execute the following command in modelsim from within the directory *sim*

```
source ./run.tcl
```
