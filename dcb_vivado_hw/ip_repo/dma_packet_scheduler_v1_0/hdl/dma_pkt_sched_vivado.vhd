------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
use work.psi_common_array_pkg.all;

------------------------------------------------------------------------------
-- Entity
------------------------------------------------------------------------------
entity dma_pkt_sched_vivado is
  generic (
    -- General Config
    Streams_g               : positive range 1 to 17   := 3;
    -- Recording
    MaxWindows_g            : positive range 1 to 32   := 16;
    MinBurstSize_g          : integer range 1 to 512   := 16;
    MaxBurstSize_g          : integer range 1 to 512   := 256;
    -- Axi
    AxiDataWidth_g          : natural range 64 to 1024 := 64;
    AxiMaxBurstBeats_g      : integer range 1 to 256   := 256;
    AxiMaxOpenTrasactions_g : natural range 1 to 8     := 8;
    AxiFifoDepth_g          : natural                  := 1024;

    -- Vivado BD Constants
    C_S_Axi_ID_WIDTH        : integer                  := 0
  );
  port (
    -- AXI Slave Interface for Register Access
    S_Axi_Aclk      : in  std_logic;
    S_Axi_ArId      : in  std_logic_vector(C_S_Axi_ID_WIDTH-1 downto 0);
    S_Axi_Aresetn   : in  std_logic;
    S_Axi_ArAddr    : in  std_logic_vector(15 downto 0);
    S_Axi_Arlen     : in  std_logic_vector(7 downto 0);
    S_Axi_ArSize    : in  std_logic_vector(2 downto 0);
    S_Axi_ArBurst   : in  std_logic_vector(1 downto 0);
    S_Axi_ArLock    : in  std_logic;
    S_Axi_ArCache   : in  std_logic_vector(3 downto 0);
    S_Axi_ArProt    : in  std_logic_vector(2 downto 0);
    S_Axi_ArValid   : in  std_logic;
    S_Axi_ArReady   : out std_logic;
    S_Axi_RId       : out std_logic_vector(C_S_Axi_ID_WIDTH-1 downto 0);
    S_Axi_RData     : out std_logic_vector(31 downto 0);
    S_Axi_RResp     : out std_logic_vector(1 downto 0);
    S_Axi_RLast     : out std_logic;
    S_Axi_RValid    : out std_logic;
    S_Axi_RReady    : in  std_logic;
    S_Axi_AwId      : in  std_logic_vector(C_S_Axi_ID_WIDTH-1 downto 0);
    S_Axi_AwAddr    : in  std_logic_vector(15 downto 0);
    S_Axi_AwLen     : in  std_logic_vector(7 downto 0);
    S_Axi_AwSize    : in  std_logic_vector(2 downto 0);
    S_Axi_AwBurst   : in  std_logic_vector(1 downto 0);
    S_Axi_AwLock    : in  std_logic;
    S_Axi_AwCache   : in  std_logic_vector(3 downto 0);
    S_Axi_AwProt    : in  std_logic_vector(2 downto 0);
    S_Axi_AwValid   : in  std_logic;
    S_Axi_AwReady   : out std_logic;
    S_Axi_WData     : in  std_logic_vector(31  downto 0);
    S_Axi_WStrb     : in  std_logic_vector(3 downto 0);
    S_Axi_WLast     : in  std_logic;
    S_Axi_WValid    : in  std_logic;
    S_Axi_WReady    : out std_logic;
    S_Axi_BId       : out std_logic_vector(C_S_Axi_ID_WIDTH-1 downto 0);
    S_Axi_BResp     : out std_logic_vector(1 downto 0);
    S_Axi_BValid    : out std_logic;
    S_Axi_BReady    : in  std_logic;

    -- AXI Master Interface for Memory Access
    M_Axi_Aclk      : in  std_logic;
    M_Axi_Aresetn   : in  std_logic;
    M_Axi_AwAddr    : out std_logic_vector(31 downto 0);
    M_Axi_AwLen     : out std_logic_vector(7 downto 0);
    M_Axi_AwSize    : out std_logic_vector(2 downto 0);
    M_Axi_AwBurst   : out std_logic_vector(1 downto 0);
    M_Axi_AwLock    : out std_logic;
    M_Axi_AwCache   : out std_logic_vector(3 downto 0);
    M_Axi_AwProt    : out std_logic_vector(2 downto 0);
    M_Axi_AwValid   : out std_logic;
    M_Axi_AwReady   : in  std_logic := '0';
    M_Axi_WData     : out std_logic_vector(AxiDataWidth_g-1 downto 0);
    M_Axi_WStrb     : out std_logic_vector(AxiDataWidth_g/8-1 downto 0);
    M_Axi_WLast     : out std_logic;
    M_Axi_WValid    : out std_logic;
    M_Axi_WReady    : in  std_logic                    := '0';
    M_Axi_BResp     : in  std_logic_vector(1 downto 0) := (others => '0');
    M_Axi_BValid    : in  std_logic                    := '0';
    M_Axi_BReady    : out std_logic;
    M_Axi_ArAddr    : out std_logic_vector(31 downto 0);
    M_Axi_ArLen     : out std_logic_vector(7 downto 0);
    M_Axi_ArSize    : out std_logic_vector(2 downto 0);
    M_Axi_ArBurst   : out std_logic_vector(1 downto 0);
    M_Axi_ArLock    : out std_logic;
    M_Axi_ArCache   : out std_logic_vector(3 downto 0);
    M_Axi_ArProt    : out std_logic_vector(2 downto 0);
    M_Axi_ArValid   : out std_logic;
    M_Axi_ArReady   : in  std_logic                                   := '0';
    M_Axi_RData     : in  std_logic_vector(AxiDataWidth_g-1 downto 0) := (others => '0');
    M_Axi_RResp     : in  std_logic_vector(1 downto 0)                := (others => '0');
    M_Axi_RLast     : in  std_logic                                   := '0';
    M_Axi_RValid    : in  std_logic                                   := '0';
    M_Axi_RReady    : out std_logic;

    M_Axi_o_Aresetn : out std_logic;

    -- Miscellaneous
    Irq             : out std_logic;

    -- Data Streams Input
    Slot00_Clk       : in  std_logic;
    Slot00_TData     : in  std_logic_vector(63 downto 0);
    Slot00_TValid    : in  std_logic;
    Slot00_TReady    : out std_logic;
    Slot00_TLast     : in  std_logic;
    Slot00_Bytes     : in  std_logic_vector(3 downto 0);
    Slot00_PktValid  : in  std_logic;
    Slot00_EOE       : in  std_logic;

    Slot01_Clk       : in  std_logic;
    Slot01_TData     : in  std_logic_vector(63 downto 0);
    Slot01_TValid    : in  std_logic;
    Slot01_TReady    : out std_logic;
    Slot01_TLast     : in  std_logic;
    Slot01_Bytes     : in  std_logic_vector(3 downto 0);
    Slot01_PktValid  : in  std_logic;
    Slot01_EOE       : in  std_logic;

    Slot02_Clk       : in  std_logic;
    Slot02_TData     : in  std_logic_vector(63 downto 0);
    Slot02_TValid    : in  std_logic;
    Slot02_TReady    : out std_logic;
    Slot02_TLast     : in  std_logic;
    Slot02_Bytes     : in  std_logic_vector(3 downto 0);
    Slot02_PktValid  : in  std_logic;
    Slot02_EOE       : in  std_logic;

    Slot03_Clk       : in  std_logic;
    Slot03_TData     : in  std_logic_vector(63 downto 0);
    Slot03_TValid    : in  std_logic;
    Slot03_TReady    : out std_logic;
    Slot03_TLast     : in  std_logic;
    Slot03_Bytes     : in  std_logic_vector(3 downto 0);
    Slot03_PktValid  : in  std_logic;
    Slot03_EOE       : in  std_logic;

    Slot04_Clk       : in  std_logic;
    Slot04_TData     : in  std_logic_vector(63 downto 0);
    Slot04_TValid    : in  std_logic;
    Slot04_TReady    : out std_logic;
    Slot04_TLast     : in  std_logic;
    Slot04_Bytes     : in  std_logic_vector(3 downto 0);
    Slot04_PktValid  : in  std_logic;
    Slot04_EOE       : in  std_logic;

    Slot05_Clk       : in  std_logic;
    Slot05_TData     : in  std_logic_vector(63 downto 0);
    Slot05_TValid    : in  std_logic;
    Slot05_TReady    : out std_logic;
    Slot05_TLast     : in  std_logic;
    Slot05_Bytes     : in  std_logic_vector(3 downto 0);
    Slot05_PktValid  : in  std_logic;
    Slot05_EOE       : in  std_logic;

    Slot06_Clk       : in  std_logic;
    Slot06_TData     : in  std_logic_vector(63 downto 0);
    Slot06_TValid    : in  std_logic;
    Slot06_TReady    : out std_logic;
    Slot06_TLast     : in  std_logic;
    Slot06_Bytes     : in  std_logic_vector(3 downto 0);
    Slot06_PktValid  : in  std_logic;
    Slot06_EOE       : in  std_logic;

    Slot07_Clk       : in  std_logic;
    Slot07_TData     : in  std_logic_vector(63 downto 0);
    Slot07_TValid    : in  std_logic;
    Slot07_TReady    : out std_logic;
    Slot07_TLast     : in  std_logic;
    Slot07_Bytes     : in  std_logic_vector(3 downto 0);
    Slot07_PktValid  : in  std_logic;
    Slot07_EOE       : in  std_logic;

    Slot08_Clk       : in  std_logic;
    Slot08_TData     : in  std_logic_vector(63 downto 0);
    Slot08_TValid    : in  std_logic;
    Slot08_TReady    : out std_logic;
    Slot08_TLast     : in  std_logic;
    Slot08_Bytes     : in  std_logic_vector(3 downto 0);
    Slot08_PktValid  : in  std_logic;
    Slot08_EOE       : in  std_logic;

    Slot09_Clk       : in  std_logic;
    Slot09_TData     : in  std_logic_vector(63 downto 0);
    Slot09_TValid    : in  std_logic;
    Slot09_TReady    : out std_logic;
    Slot09_TLast     : in  std_logic;
    Slot09_Bytes     : in  std_logic_vector(3 downto 0);
    Slot09_PktValid  : in  std_logic;
    Slot09_EOE       : in  std_logic;

    Slot10_Clk       : in  std_logic;
    Slot10_TData     : in  std_logic_vector(63 downto 0);
    Slot10_TValid    : in  std_logic;
    Slot10_TReady    : out std_logic;
    Slot10_TLast     : in  std_logic;
    Slot10_Bytes     : in  std_logic_vector(3 downto 0);
    Slot10_PktValid  : in  std_logic;
    Slot10_EOE       : in  std_logic;

    Slot11_Clk       : in  std_logic;
    Slot11_TData     : in  std_logic_vector(63 downto 0);
    Slot11_TValid    : in  std_logic;
    Slot11_TReady    : out std_logic;
    Slot11_TLast     : in  std_logic;
    Slot11_Bytes     : in  std_logic_vector(3 downto 0);
    Slot11_PktValid  : in  std_logic;
    Slot11_EOE       : in  std_logic;

    Slot12_Clk       : in  std_logic;
    Slot12_TData     : in  std_logic_vector(63 downto 0);
    Slot12_TValid    : in  std_logic;
    Slot12_TReady    : out std_logic;
    Slot12_TLast     : in  std_logic;
    Slot12_Bytes     : in  std_logic_vector(3 downto 0);
    Slot12_PktValid  : in  std_logic;
    Slot12_EOE       : in  std_logic;

    Slot13_Clk       : in  std_logic;
    Slot13_TData     : in  std_logic_vector(63 downto 0);
    Slot13_TValid    : in  std_logic;
    Slot13_TReady    : out std_logic;
    Slot13_TLast     : in  std_logic;
    Slot13_Bytes     : in  std_logic_vector(3 downto 0);
    Slot13_PktValid  : in  std_logic;
    Slot13_EOE       : in  std_logic;

    Slot14_Clk       : in  std_logic;
    Slot14_TData     : in  std_logic_vector(63 downto 0);
    Slot14_TValid    : in  std_logic;
    Slot14_TReady    : out std_logic;
    Slot14_TLast     : in  std_logic;
    Slot14_Bytes     : in  std_logic_vector(3 downto 0);
    Slot14_PktValid  : in  std_logic;
    Slot14_EOE       : in  std_logic;

    Slot15_Clk       : in  std_logic;
    Slot15_TData     : in  std_logic_vector(63 downto 0);
    Slot15_TValid    : in  std_logic;
    Slot15_TReady    : out std_logic;
    Slot15_TLast     : in  std_logic;
    Slot15_Bytes     : in  std_logic_vector(3 downto 0);
    Slot15_PktValid  : in  std_logic;
    Slot15_EOE       : in  std_logic;

    Slot16_Clk       : in  std_logic;
    Slot16_TData     : in  std_logic_vector(63 downto 0);
    Slot16_TValid    : in  std_logic;
    Slot16_TReady    : out std_logic;
    Slot16_TLast     : in  std_logic;
    Slot16_Bytes     : in  std_logic_vector(3 downto 0);
    Slot16_PktValid  : in  std_logic;
    Slot16_EOE       : in  std_logic
  );
end entity dma_pkt_sched_vivado;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture rtl of dma_pkt_sched_vivado is

  signal All_TData    : t_aslv64(0 to 16)          := (others => (others => '0'));
  signal All_TValid   : std_logic_vector(0 to 16)  := (others => '0');
  signal All_TReady   : std_logic_vector(0 to 16)  := (others => '0');
  signal All_TLast    : std_logic_vector(0 to 16)  := (others => '0');
  signal All_Bytes    : t_aslv4(0 to 16)           := (others => (others => '0'));
  signal All_PktValid : std_logic_vector(0 to 16)  := (others => '0');
  signal All_EOE      : std_logic_vector(0 to 16)  := (others => '0');
  signal Str_TData    : t_aslv64(Streams_g-1 downto 0);
  signal Str_TValid   : std_logic_vector(Streams_g-1 downto 0);
  signal Str_TReady   : std_logic_vector(Streams_g-1 downto 0);
  signal Str_TLast    : std_logic_vector(Streams_g-1 downto 0);
  signal Str_Bytes    : t_aslv4(Streams_g-1 downto 0);
  signal Str_PktValid : std_logic_vector(Streams_g-1 downto 0);
  signal Str_EOE      : std_logic_vector(Streams_g-1 downto 0);

begin

  All_TData(0)     <= Slot00_TData;
  All_TValid(0)    <= Slot00_TValid;
  All_TLast(0)     <= Slot00_TLast;
  All_Bytes(0)     <= Slot00_Bytes;
  All_PktValid(0)  <= Slot00_PktValid;
  All_EOE(0)       <= Slot00_EOE;
  Slot00_TReady    <= All_TReady(0);

  All_TData(1)     <= Slot01_TData;
  All_TValid(1)    <= Slot01_TValid;
  All_TLast(1)     <= Slot01_TLast;
  All_Bytes(1)     <= Slot01_Bytes;
  All_PktValid(1)  <= Slot01_PktValid;
  All_EOE(1)       <= Slot01_EOE;
  Slot01_TReady    <= All_TReady(1);

  All_TData(2)     <= Slot02_TData;
  All_TValid(2)    <= Slot02_TValid;
  All_TLast(2)     <= Slot02_TLast;
  All_Bytes(2)     <= Slot02_Bytes;
  All_PktValid(2)  <= Slot02_PktValid;
  All_EOE(2)       <= Slot02_EOE;
  Slot02_TReady    <= All_TReady(2);

  All_TData(3)     <= Slot03_TData;
  All_TValid(3)    <= Slot03_TValid;
  All_TLast(3)     <= Slot03_TLast;
  All_Bytes(3)     <= Slot03_Bytes;
  All_PktValid(3)  <= Slot03_PktValid;
  All_EOE(3)       <= Slot03_EOE;
  Slot03_TReady    <= All_TReady(3);

  All_TData(4)     <= Slot04_TData;
  All_TValid(4)    <= Slot04_TValid;
  All_TLast(4)     <= Slot04_TLast;
  All_Bytes(4)     <= Slot04_Bytes;
  All_PktValid(4)  <= Slot04_PktValid;
  All_EOE(4)       <= Slot04_EOE;
  Slot04_TReady       <= All_TReady(4);

  All_TData(5)     <= Slot05_TData;
  All_TValid(5)    <= Slot05_TValid;
  All_TLast(5)     <= Slot05_TLast;
  All_Bytes(5)     <= Slot05_Bytes;
  All_PktValid(5)  <= Slot05_PktValid;
  All_EOE(5)       <= Slot05_EOE;
  Slot05_TReady    <= All_TReady(5);

  All_TData(6)     <= Slot06_TData;
  All_TValid(6)    <= Slot06_TValid;
  All_TLast(6)     <= Slot06_TLast;
  All_Bytes(6)     <= Slot06_Bytes;
  All_PktValid(6)  <= Slot06_PktValid;
  All_EOE(6)       <= Slot06_EOE;
  Slot06_TReady    <= All_TReady(6);

  All_TData(7)     <= Slot07_TData;
  All_TValid(7)    <= Slot07_TValid;
  All_TLast(7)     <= Slot07_TLast;
  All_Bytes(7)     <= Slot07_Bytes;
  All_PktValid(7)  <= Slot07_PktValid;
  All_EOE(7)       <= Slot07_EOE;
  Slot07_TReady    <= All_TReady(7);

  All_TData(8)     <= Slot08_TData;
  All_TValid(8)    <= Slot08_TValid;
  All_TLast(8)     <= Slot08_TLast;
  All_Bytes(8)     <= Slot08_Bytes;
  All_PktValid(8)  <= Slot08_PktValid;
  All_EOE(8)       <= Slot08_EOE;
  Slot08_TReady    <= All_TReady(8);

  All_TData(9)     <= Slot09_TData;
  All_TValid(9)    <= Slot09_TValid;
  All_TLast(9)     <= Slot09_TLast;
  All_Bytes(9)     <= Slot09_Bytes;
  All_PktValid(9)  <= Slot09_PktValid;
  All_EOE(9)       <= Slot09_EOE;
  Slot09_TReady    <= All_TReady(9);

  All_TData(10)    <= Slot10_TData;
  All_TValid(10)   <= Slot10_TValid;
  All_TLast(10)    <= Slot10_TLast;
  All_Bytes(10)    <= Slot10_Bytes;
  All_PktValid(10) <= Slot10_PktValid;
  All_EOE(10)      <= Slot10_EOE;
  Slot10_TReady    <= All_TReady(10);

  All_TData(11)    <= Slot11_TData;
  All_TValid(11)   <= Slot11_TValid;
  All_TLast(11)    <= Slot11_TLast;
  All_Bytes(11)    <= Slot11_Bytes;
  All_PktValid(11) <= Slot11_PktValid;
  All_EOE(11)      <= Slot11_EOE;
  Slot11_TReady    <= All_TReady(11);

  All_TData(12)    <= Slot12_TData;
  All_TValid(12)   <= Slot12_TValid;
  All_TLast(12)    <= Slot12_TLast;
  All_Bytes(12)    <= Slot12_Bytes;
  All_PktValid(12) <= Slot12_PktValid;
  All_EOE(12)      <= Slot12_EOE;
  Slot12_TReady    <= All_TReady(12);

  All_TData(13)    <= Slot13_TData;
  All_TValid(13)   <= Slot13_TValid;
  All_TLast(13)    <= Slot13_TLast;
  All_Bytes(13)    <= Slot13_Bytes;
  All_PktValid(13) <= Slot13_PktValid;
  All_EOE(13)      <= Slot13_EOE;
  Slot13_TReady    <= All_TReady(13);

  All_TData(14)    <= Slot14_TData;
  All_TValid(14)   <= Slot14_TValid;
  All_TLast(14)    <= Slot14_TLast;
  All_Bytes(14)    <= Slot14_Bytes;
  All_PktValid(14) <= Slot14_PktValid;
  All_EOE(14)      <= Slot14_EOE;
  Slot14_TReady    <= All_TReady(14);

  All_TData(15)    <= Slot15_TData;
  All_TValid(15)   <= Slot15_TValid;
  All_TLast(15)    <= Slot15_TLast;
  All_Bytes(15)    <= Slot15_Bytes;
  All_PktValid(15) <= Slot15_PktValid;
  All_EOE(15)      <= Slot15_EOE;
  Slot15_TReady    <= All_TReady(15);

  All_TData(16)    <= Slot16_TData;
  All_TValid(16)   <= Slot16_TValid;
  All_TLast(16)    <= Slot16_TLast;
  All_Bytes(16)    <= Slot16_Bytes;
  All_PktValid(16) <= Slot16_PktValid;
  All_EOE(16)      <= Slot16_EOE;
  Slot16_TReady    <= All_TReady(16);

  g_stream : for s in 0 to Streams_g-1 generate
    Str_TData(s)    <= All_TData(s);
    Str_TValid(s)   <= All_TValid(s);
    Str_TLast(s)    <= All_TLast(s);
    Str_Bytes(s)    <= All_Bytes(s);
    Str_PktValid(s) <= All_PktValid(s);
    Str_EOE(s)      <= All_EOE(s);
    All_TReady(s)   <= Str_TReady(s);
  end generate;

  i_impl : entity work.dma_pkt_sched_axi
    generic map (
      Streams_g               => Streams_g,
      MaxWindows_g            => MaxWindows_g,
      MinBurstSize_g          => MinBurstSize_g,
      MaxBurstSize_g          => MaxBurstSize_g,
      AxiDataWidth_g          => AxiDataWidth_g,
      AxiMaxBurstBeats_g      => AxiMaxBurstBeats_g,
      AxiMaxOpenTrasactions_g => AxiMaxOpenTrasactions_g,
      AxiFifoDepth_g          => AxiFifoDepth_g,
      AxiSlaveIdWidth_g       => C_S_Axi_ID_WIDTH
    )
    port map (
      Slot_TData      => Str_TData,
      Slot_TValid     => Str_TValid,
      Slot_TReady     => Str_TReady,
      Slot_TLast      => Str_TLast,
      Slot_Bytes      => Str_Bytes,
      Slot_PktValid   => Str_PktValid,
      Slot_EOE        => Str_EOE,
      Irq             => Irq,
      S_Axi_Aclk      => S_Axi_Aclk,
      S_Axi_Aresetn   => S_Axi_Aresetn,
      S_Axi_ArId      => S_Axi_ArId,
      S_Axi_ArAddr    => S_Axi_ArAddr,
      S_Axi_Arlen     => S_Axi_Arlen,
      S_Axi_ArSize    => S_Axi_ArSize,
      S_Axi_ArBurst   => S_Axi_ArBurst,
      S_Axi_ArLock    => S_Axi_ArLock,
      S_Axi_ArCache   => S_Axi_ArCache,
      S_Axi_ArProt    => S_Axi_ArProt,
      S_Axi_ArValid   => S_Axi_ArValid,
      S_Axi_ArReady   => S_Axi_ArReady,
      S_Axi_RId       => S_Axi_RId,
      S_Axi_RData     => S_Axi_RData,
      S_Axi_RResp     => S_Axi_RResp,
      S_Axi_RLast     => S_Axi_RLast,
      S_Axi_RValid    => S_Axi_RValid,
      S_Axi_RReady    => S_Axi_RReady,
      S_Axi_AwId      => S_Axi_AwId,
      S_Axi_AwAddr    => S_Axi_AwAddr,
      S_Axi_AwLen     => S_Axi_AwLen,
      S_Axi_AwSize    => S_Axi_AwSize,
      S_Axi_AwBurst   => S_Axi_AwBurst,
      S_Axi_AwLock    => S_Axi_AwLock,
      S_Axi_AwCache   => S_Axi_AwCache,
      S_Axi_AwProt    => S_Axi_AwProt,
      S_Axi_AwValid   => S_Axi_AwValid,
      S_Axi_AwReady   => S_Axi_AwReady,
      S_Axi_WData     => S_Axi_WData,
      S_Axi_WStrb     => S_Axi_WStrb,
      S_Axi_WLast     => S_Axi_WLast,
      S_Axi_WValid    => S_Axi_WValid,
      S_Axi_WReady    => S_Axi_WReady,
      S_Axi_BId       => S_Axi_BId,
      S_Axi_BResp     => S_Axi_BResp,
      S_Axi_BValid    => S_Axi_BValid,
      S_Axi_BReady    => S_Axi_BReady,
      M_Axi_Aclk      => M_Axi_Aclk,
      M_Axi_Aresetn   => M_Axi_Aresetn,
      M_Axi_AwAddr    => M_Axi_AwAddr,
      M_Axi_AwLen     => M_Axi_AwLen,
      M_Axi_AwSize    => M_Axi_AwSize,
      M_Axi_AwBurst   => M_Axi_AwBurst,
      M_Axi_AwLock    => M_Axi_AwLock,
      M_Axi_AwCache   => M_Axi_AwCache,
      M_Axi_AwProt    => M_Axi_AwProt,
      M_Axi_AwValid   => M_Axi_AwValid,
      M_Axi_AwReady   => M_Axi_AwReady,
      M_Axi_WData     => M_Axi_WData,
      M_Axi_WStrb     => M_Axi_WStrb,
      M_Axi_WLast     => M_Axi_WLast,
      M_Axi_WValid    => M_Axi_WValid,
      M_Axi_WReady    => M_Axi_WReady,
      M_Axi_BResp     => M_Axi_BResp,
      M_Axi_BValid    => M_Axi_BValid,
      M_Axi_BReady    => M_Axi_BReady,
      M_Axi_ArAddr    => M_Axi_ArAddr,
      M_Axi_ArLen     => M_Axi_ArLen,
      M_Axi_ArSize    => M_Axi_ArSize,
      M_Axi_ArBurst   => M_Axi_ArBurst,
      M_Axi_ArLock    => M_Axi_ArLock,
      M_Axi_ArCache   => M_Axi_ArCache,
      M_Axi_ArProt    => M_Axi_ArProt,
      M_Axi_ArValid   => M_Axi_ArValid,
      M_Axi_ArReady   => M_Axi_ArReady,
      M_Axi_RData     => M_Axi_RData,
      M_Axi_RResp     => M_Axi_RResp,
      M_Axi_RLast     => M_Axi_RLast,
      M_Axi_RValid    => M_Axi_RValid,
      M_Axi_RReady    => M_Axi_RReady,
      M_Axi_o_Aresetn => M_Axi_o_Aresetn
    );

end rtl;
