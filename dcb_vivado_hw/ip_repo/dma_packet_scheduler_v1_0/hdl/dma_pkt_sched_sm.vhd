------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Description
------------------------------------------------------------------------------
-- This component calculates a binary division of two fixed point values.

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.dma_pkt_sched_pkg.all;

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
-- $$ testcases=single_simple,priorities,single_window,multi_window,enable,irq,timestamp $$
-- $$ processes=control,dma_cmd,dma_resp,ctx $$
-- $$ tbpkg=work.psi_tb_txt_util,work.psi_tb_compare_pkg $$
entity dma_pkt_sched_sm is
  generic (
    Streams_g      : positive range 1 to 32 := 4;                 -- $$ constant=4 $$
    Windows_g      : positive range 1 to 32 := 4;                 -- $$ constant=4 $$
    MinBurstSize_g : positive               := 512;               -- $$ constant=512 $$
    MaxBurstSize_g : positive               := 512                -- $$ constant=512 $$
  );
  port (
    -- Control signals
    Clk         : in  std_logic;                                -- $$ type=clk; freq=200e6; proc=control,dma_cmd,dma_resp,ctx $$
    Rst         : in  std_logic;                                -- $$ proc=control $$
    GlbEna      : in  std_logic;                                -- $$ proc=control; lowactive=true $$
    EventMode   : in  std_logic;                                -- $$ proc=control $$
    StrEna      : in  std_logic_vector(Streams_g-1 downto 0);   -- $$ proc=control; lowactive=true $$
    StrIrq      : out std_logic_vector(Streams_g-1 downto 0);   -- $$ proc=control,dma_resp,dma_cmd; $$
    StrLastWin  : out WinType_a(Streams_g-1 downto 0);

    -- Input logic Connections
    Inp_PktValid : in  std_logic_vector(Streams_g-1 downto 0);   -- $$ proc=control $$
    Inp_EOE      : in  std_logic_vector(Streams_g-1 downto 0);   -- $$ proc=control $$

    -- Dma Connections
    Dma_Cmd      : out  DaqSm2DaqDma_Cmd_t;   -- $$ proc=dma_cmd,control $$
    Dma_Cmd_Vld  : out  std_logic;            -- $$ proc=dma_cmd,control $$
    Dma_Resp     : in  DaqDma2DaqSm_Resp_t;   -- $$ proc=dma_resp $$
    Dma_Resp_Vld : in  std_logic;             -- $$ proc=dma_resp $$
    Dma_Resp_Rdy : out  std_logic;            -- $$ proc=dma_resp $$

    -- Memory Controller
    TfDone       : in  std_logic;   -- $$ proc=dma_resp $$

    -- Context RAM connections
    CtxStr_Cmd   : out ToCtxStr_t;   -- $$ proc=ctx $$
    CtxStr_Resp  : in  FromCtx_t;    -- $$ proc=ctx $$
    CtxWin_Cmd   : out ToCtxWin_t;   -- $$ proc=ctx $$
    CtxWin_Resp  : in  FromCtx_t     -- $$ proc=ctx $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of dma_pkt_sched_sm is

  -- Number of bits to encode stream is at least 1 (otherwise the special case for one stream would require separate code).
  -- .. The overhead generated by this is regarded as aceptable (better wasting a few LUTs than much development time)
  constant StreamBits_c      : integer    := max(log2ceil(Streams_g), 1);

  constant StreamWidth_c     : integer := 64;

  function GetStreamNrFromGrant( GrantVector : std_logic_vector )
  return integer is
  begin
    for idx in GrantVector'low to GrantVector'high loop
      if GrantVector(idx) = '1' then
        return idx;
      end if;
    end loop;
    return 0;
  end function;

  -- Vivado Workarounds (Synthesis fail)
  subtype Log2Bytes_t is integer range 0 to log2(MaxStreamWidth_c/8);
  type Log2Bytes_a is array (natural range <>) of Log2Bytes_t;
  function CalcLog2Bytes return Log2Bytes_a is
    variable arr : Log2Bytes_a(0 to Streams_g-1);
  begin
    for i in 0 to Streams_g-1 loop
      arr(i)  := log2(StreamWidth_c/8);
    end loop;
    return arr;
  end function;
  constant Log2StrBytes_c : Log2Bytes_a(0 to Streams_g-1) := CalcLog2Bytes;

  -- Component Connection Signals
  signal Grant          : std_logic_vector(Streams_g-1 downto 0);
  signal GrantVld       : std_logic;
  signal GrantRdy       : std_logic;
  signal IrqFifoAlmFull : std_logic;
  signal IrqFifoEmpty   : std_logic;
  signal IrqFifoGenIrq  : std_logic;
  signal IrqFifoStream  : std_logic_vector(StreamBits_c-1 downto 0);
  signal IrqLastWinNr   : std_logic_vector(log2ceil(Windows_g)-1 downto 0);
  signal IrqFifoIn      : std_logic_vector(StreamBits_c+log2ceil(Windows_g) downto 0);
  signal IrqFifoOut     : std_logic_vector(StreamBits_c+log2ceil(Windows_g) downto 0);

  -- Types
  type State_t is (Idle_s, CheckData, CheckResp_s,
                   ReadCtxStr_s, First_s, ReadCtxWin_s, CalcAccess0_s,
                   CalcAccess1_s, ProcResp0_s, NextWin_s, WriteCtx_s);

  -- Two process method
  type two_process_r is record
    GlbEnaReg       : std_logic;
    StrEnaReg       : std_logic_vector(Streams_g-1 downto 0);
    InpDataAvail    : std_logic_vector(Streams_g-1 downto 0);
    StrEventPending : std_logic_vector(Streams_g-1 downto 0);
    EndOfEvent      : std_logic_vector(Streams_g-1 downto 0);
    DataAvailArbIn  : std_logic_vector(Streams_g-1 downto 0);
    DataPending     : std_logic_vector(Streams_g-1 downto 0);
    OpenCommand     : std_logic_vector(Streams_g-1 downto 0);
    WinProtected    : std_logic_vector(Streams_g-1 downto 0); -- Set if the current window is not yet available
    NewBuffer       : std_logic_vector(Streams_g-1 downto 0);
    FirstAfterEna   : std_logic_vector(Streams_g-1 downto 0);
    FirstOngoing    : std_logic_vector(Streams_g-1 downto 0);
--    GrantVldReg     : std_logic;
--    GrantReg        : std_logic_vector(Grant'range);
    State           : State_t;
    HndlAfterCtxt   : State_t;
    HndlStream      : integer range 0 to Streams_g;
    HndlCtxCnt      : integer range 0 to 4;
    HndlWincnt      : std_logic_vector(log2ceil(Windows_g)-1 downto 0);
    HndlWincur      : std_logic_vector(log2ceil(Windows_g)-1 downto 0);
    HndlLastWinNr   : std_logic_vector(log2ceil(Windows_g)-1 downto 0);
    HndlBufstart    : std_logic_vector(31 downto 0);
    HndlWinSize     : std_logic_vector(31 downto 0);
    HndlPtr0        : std_logic_vector(31 downto 0);
    HndlPtr1        : std_logic_vector(31 downto 0);
    HndlPtr2        : std_logic_vector(31 downto 0);
    Hndl4kMax       : std_logic_vector(12 downto 0);
    HndlWinMax      : std_logic_vector(31 downto 0);
    HndlWinEnd      : std_logic_vector(31 downto 0);
    HndlWinBytes    : std_logic_vector(32 downto 0);
    HndlWinLast     : std_logic_vector(31 downto 0);
    TfDoneCnt       : std_logic_vector(StreamBits_c-1 downto 0);
    TfDoneReg       : std_logic;
    HndlWinDone     : std_logic;
    CtxStr_Cmd      : ToCtxStr_t;
    CtxWin_Cmd      : ToCtxWin_t;
    Dma_Cmd         : DaqSm2DaqDma_Cmd_t;
    Dma_Cmd_Vld     : std_logic;
    Dma_Resp_Rdy    : std_logic;
    ArbDelCnt       : integer range 0 to 4;
    IrqFifoWrite    : std_logic;
    IrqFifoRead     : std_logic;
    StrIrq          : std_logic_vector(Streams_g-1 downto 0);
    StrLastWin      : WinType_a(Streams_g-1 downto 0);
    EndOfPacket     : std_logic;
  end record;
  signal r, r_next : two_process_r;

  -- USED FOR DEBUGGING ONLY!
  -- attribute mark_debug : string;
  -- attribute mark_debug of r : signal is "true";

  -- Todo: mask streams that already have a transfer open at the input

begin
  --------------------------------------------
  -- Combinatorial Process
  --------------------------------------------
  p_comb : process( r, Dma_Resp, Dma_Resp_Vld, CtxStr_Resp, CtxWin_Resp, GlbEna, StrEna, TfDone, IrqFifoGenIrq, IrqFifoStream, IrqLastWinNr,
                    GrantVld, Grant, EventMode, IrqFifoAlmFull, IrqFifoEmpty, Inp_PktValid, Inp_EOE)
    variable v : two_process_r;
  begin
    -- *** Hold variables stable ***
    v := r;

    -- *** Default Values ***
    v.CtxStr_Cmd.WenLo  := '0';
    v.CtxStr_Cmd.WenHi  := '0';
    v.CtxWin_Cmd.WenLo  := '0';
    v.CtxWin_Cmd.WenHi  := '0';
    v.CtxStr_Cmd.Rd     := '0';
    v.CtxWin_Cmd.Rd     := '0';
    v.Dma_Cmd_Vld       := '0';
    v.Dma_Resp_Rdy      := '0';
    v.CtxWin_Cmd.WdatLo := (others => '0');
    v.CtxWin_Cmd.WdatHi := (others => '0');
    v.CtxStr_Cmd.WdatLo := (others => '0');
    v.CtxStr_Cmd.WdatHi := (others => '0');
    v.IrqFifoWrite      := '0';
    v.IrqFifoRead       := '0';
    v.StrIrq            := (others => '0');
    GrantRdy            <= '0';

    -- *** Pure Pipelining (no functional registers) ***
--    v.GrantVldReg       := GrantVld;
--    v.GrantReg          := Grant;
    v.StrEnaReg         := StrEna;
    v.GlbEnaReg         := GlbEna;
    v.TfDoneReg         := TfDone;

    -- *** Check Availability of a full burst ***
    for str in 0 to Streams_g-1 loop
      if Inp_PktValid(str) = '1' then
        v.InpDataAvail(str) := r.StrEventPending(str) and r.StrEnaReg(str) and r.GlbEnaReg;
      else
        v.InpDataAvail(str)  := '0';
      end if;
    end loop;
    v.DataAvailArbIn := r.InpDataAvail and (not r.OpenCommand) and (not r.WinProtected);  -- Do not arbitrate new commands on streams that already have a command
    v.DataPending    := r.InpDataAvail and (not r.WinProtected);  -- Do not prevent lower priority channels from access if the window of a higher priority stream is protected

    -- *** State Machine ***
    case r.State is
      -- *** Idle state ***
      when Idle_s =>
        v.HndlCtxCnt  := 0;
        v.HndlWinDone := '0';
        -- check if data to write is available  (only if IRQ FIFO has space for the response for sure)
        if IrqFifoAlmFull = '0' then
          v.State         := CheckData;
          v.HndlAfterCtxt := CalcAccess0_s;
        end if;
        -- Delay arbitration in simulation to allow TB to react
        if r.ArbDelCnt /= 4 then
          v.State     := Idle_s;
          v.ArbDelCnt := r.ArbDelCnt + 1;
        else
          v.ArbDelCnt := 0;
        end if;

      -- *** Check for next stream to handle ***
      when CheckData =>
        -- Handle command if data is available
--        if r.GrantVldReg = '1' then
        if GrantVld = '1' then
          v.State      := ReadCtxStr_s;
--          v.HndlStream := GetStreamNrFromGrant(r.GrantReg);
          v.HndlStream := GetStreamNrFromGrant(Grant);
          v.EndOfEvent(v.HndlStream) := Inp_EOE(v.HndlStream);
          GrantRdy <= '1';
        else
          v.State := CheckResp_s;
          v.WinProtected := (others => '0');  -- No bursts where available on any stream, so all of them were checked and we can retry whether SW emptied a window.
--        elsif unsigned(r.DataPending) /= 0 then
--          v.State := CheckResp_s;
--        else
--          v.State := Idle_s;
--          v.WinProtected := (others => '0');  -- No bursts where available on any stream, so all of them were checked and we can retry whether SW emptied a window.
        end if;

      when CheckResp_s =>
        -- Handle response if one is pending (less important thandata transer, therefore at the end)
        if Dma_Resp_Vld = '1' then
          v.State         := ReadCtxStr_s;
          v.HndlAfterCtxt := ProcResp0_s;
          v.HndlStream    := Dma_Resp.Stream;
          v.EndOfPacket   := Dma_Resp.PktCmplt;
        else
          v.State := Idle_s;
        end if;

      -- *** Read Context Memory ***
      -- Read information from stream memory
      when ReadCtxStr_s =>
        -- State handling
        if r.HndlCtxCnt = 4 then
          v.State      := First_s;
          v.HndlCtxCnt := 0;
        else
          v.HndlCtxCnt := r.HndlCtxCnt + 1;
        end if;

        -- Command Assertions
        v.CtxStr_Cmd.Stream := r.HndlStream;
        case r.HndlCtxCnt is
          when 0 =>
            v.CtxStr_Cmd.Sel := CtxStr_Sel_Winend_c;
            v.CtxStr_Cmd.Rd  := '1';
          when 1 =>
            v.CtxStr_Cmd.Sel := CtxStr_Sel_WinsizePtr_c;
            v.CtxStr_Cmd.Rd  := '1';
          when 2 =>
            v.CtxStr_Cmd.Sel := CtxStr_Sel_ScfgBufstart_c;
            v.CtxStr_Cmd.Rd  := '1';
          when others => null;
        end case;

        -- Response handling
        case r.HndlCtxCnt is
          when 2 =>
            v.HndlWinEnd    := CtxStr_Resp.RdatLo;
          when 3 =>
            v.HndlWinSize   := CtxStr_Resp.RdatLo;
            v.HndlPtr0      := CtxStr_Resp.RdatHi;
          when 4 =>
            v.HndlWincnt    := CtxStr_Resp.RdatLo(CtxStr_Sft_SCFG_WINCNT_c+v.HndlWincnt'high downto CtxStr_Sft_SCFG_WINCNT_c);
            v.HndlWincur    := CtxStr_Resp.RdatLo(CtxStr_Sft_SCFG_WINCUR_c+v.HndlWincur'high downto CtxStr_Sft_SCFG_WINCUR_c);
            v.HndlBufstart  := CtxStr_Resp.RdatHi;
            v.Hndl4kMax     := std_logic_vector(to_unsigned(4096, 13) - unsigned(r.HndlPtr0(11 downto 0)));   -- Calculate maximum size within this 4k Region
            v.HndlWinMax    := std_logic_vector(unsigned(r.HndlWinEnd) - unsigned(r.HndlPtr0));   -- Calculate maximum size within this window
          when others => null;
        end case;

      -- Handle first access after enable
      when First_s =>
        -- State handling
        v.State := ReadCtxWin_s;

        -- Ensure that command and response are both handled as first or not
        if r.HndlAfterCtxt = ProcResp0_s then -- responses
          -- nothing to do
        else  -- command
          v.FirstAfterEna(r.HndlStream) := '0';
          v.FirstOngoing(r.HndlStream)  := r.FirstAfterEna(r.HndlStream);
        end if;

        -- Update values for first access
        if v.FirstOngoing(r.HndlStream) = '1' then
          v.HndlWinEnd := std_logic_vector(unsigned(r.HndlBufstart) + unsigned(r.HndlWinSize));
          v.HndlPtr0   := r.HndlBufstart;
          v.HndlWincur := (others => '0');
          v.Hndl4kMax  := std_logic_vector(to_unsigned(4096, 13) - unsigned(r.HndlBufstart(11 downto 0)));
          v.HndlWinMax := r.HndlWinSize;
        end if;

      -- Read information from window memory
      when ReadCtxWin_s =>
        -- State handling
        if r.HndlCtxCnt = 2 then
          v.State      := r.HndlAfterCtxt;   -- Goto state depends on the context of the read procedure
        else
          v.HndlCtxCnt := r.HndlCtxCnt + 1;
        end if;

        -- Command Assertions
        v.CtxWin_Cmd.Stream := r.HndlStream;
        v.CtxWin_Cmd.Window := to_integer(unsigned(r.HndlWincur));
        case r.HndlCtxCnt is
          when 0 =>
            --v.CtxWin_Cmd.Sel := CtxWin_Sel_WincntWinlast_c;
            v.CtxWin_Cmd.Rd  := '1';
          when others => null;
        end case;

        -- Response handling
        case r.HndlCtxCnt is
          when 2 =>
            -- Workaround for Vivado (Range expression was resolved incorrectly)
            for i in 0 to Streams_g-1 loop
              if i = r.HndlStream then
                v.HndlWinBytes  := '0' & CtxWin_Resp.RdatLo;   -- guard bit required for calculations
              end if;
            end loop;
          when others => null;
        end case;

      -- *** Calculate next access ***
      when CalcAccess0_s =>
        -- Calculate Command
        v.Dma_Cmd.Address := r.HndlPtr0;
        v.Dma_Cmd.Stream  := r.HndlStream;
        v.Dma_Cmd.MaxSize := std_logic_vector(to_unsigned(MaxBurstSize_g*8, v.Dma_Cmd.MaxSize'length));   -- 8 bytes per 64-bit QWORD
        -- State update (abort if window is not free)
        if (unsigned(r.HndlWinBytes) /= 0) and (r.NewBuffer(r.HndlStream) = '1') then
          v.State                      := Idle_s;
          v.WinProtected(r.HndlStream) := '1';
        else
          v.State                      := CalcAccess1_s;
          v.NewBuffer(r.HndlStream)    := '0';
          -- Mark stream as active
          v.OpenCommand(r.HndlStream)  := '1';
        end if;

      when CalcAccess1_s =>
        if unsigned(r.Hndl4kMax) < unsigned(r.HndlWinMax) then
          if unsigned(r.Dma_Cmd.MaxSize) > unsigned(r.Hndl4kMax) then
            v.Dma_Cmd.MaxSize := std_logic_vector(resize(unsigned(r.Hndl4kMax), v.dma_Cmd.MaxSize'length));
          end if;
        else
          if unsigned(r.Dma_Cmd.MaxSize) > unsigned(r.HndlWinMax) then
            v.Dma_Cmd.MaxSize := std_logic_vector(resize(unsigned(r.HndlWinMax), v.dma_Cmd.MaxSize'length));
          end if;
        end if;
        v.Dma_Cmd_Vld := '1';
        v.State       := Idle_s;

      -- *** Handle response ***
      -- Calculate next pointer
      when ProcResp0_s =>
        v.OpenCommand(r.HndlStream)  := '0';
        v.FirstOngoing(r.HndlStream) := '0';
        v.HndlPtr1     := std_logic_vector(unsigned(r.HndlPtr0) + unsigned(Dma_Resp.Size));
        v.State        := NextWin_s;
        -- Update window information step 1
        v.HndlWinBytes := std_logic_vector(unsigned(r.HndlWinBytes) + unsigned(Dma_Resp.Size));


      -- Calculate next window to use
      when NextWin_s =>
        -- Default Values
        v.HndlPtr2 := r.HndlPtr1;
        -- Do not wait for "transfer done" for zero size transfers (they are not passed to the memory interface)
        if unsigned(Dma_Resp.Size) /= 0 then
          v.IrqFifoWrite  := '1';
        end if;
        -- Switch to next window if required
        v.HndlLastWinNr  := r.HndlWincur;
        if (r.HndlPtr1 = r.HndlWinEnd) or (Dma_Resp.PktCmplt = '1') then
          v.HndlWinDone := '1';
          v.NewBuffer(r.HndlStream) := '1';
          if r.HndlWincur = r.HndlWincnt then
            v.HndlWincur := (others => '0');
            v.HndlPtr2   := r.HndlBufstart;
            v.HndlWinEnd := std_logic_vector(unsigned(r.HndlBufstart) + unsigned(r.HndlWinSize));
          else
            v.HndlWincur := std_logic_vector(unsigned(r.HndlWincur) + 1);
            v.HndlPtr2   := r.HndlWinEnd;
            v.HndlWinEnd := std_logic_vector(unsigned(r.HndlWinEnd) + unsigned(r.HndlWinSize));
          end if;
        end if;
        -- Update window information step 2 (limit to maximum value)
        if unsigned(r.HndlWinBytes) > unsigned(r.HndlWinSize) then
          v.HndlWinBytes := '0' & r.HndlWinSize; -- value has a guard bit
        end if;
        -- Store address of last byte in window
        v.HndlWinLast := std_logic_vector(unsigned(r.HndlPtr1) - 1);
        -- Handle end of event
        if (Dma_Resp.PktCmplt = '1') and (r.EndOfEvent(r.HndlStream) = '1') and (EventMode = '1') then
          v.StrEventPending(r.HndlStream) := '0';
        end if;
        -- Write values
        v.State := WriteCtx_s;
        v.HndlCtxCnt := 0;
        -- Response is processed
        v.Dma_Resp_Rdy := '1';

      -- Write Context Memory Content
      when WriteCtx_s =>
        -- Update State
        if r.HndlCtxCnt = 2 then
          v.State := Idle_s;
        else
          v.HndlCtxCnt := r.HndlCtxCnt + 1;
        end if;
        -- Write Context Memory
        v.CtxStr_Cmd.Stream := r.HndlStream;
        case r.HndlCtxCnt is
          when 0 =>
            -- Stream Memory
            v.CtxStr_Cmd.Sel   := CtxStr_Sel_ScfgBufstart_c;
            v.CtxStr_Cmd.WenLo := '1';
            v.CtxStr_Cmd.WdatLo(CtxStr_Sft_SCFG_WINCNT_c+v.HndlWincnt'high downto CtxStr_Sft_SCFG_WINCNT_c) := r.HndlWincnt;
            v.CtxStr_Cmd.WdatLo(CtxStr_Sft_SCFG_WINCUR_c+v.HndlWincur'high downto CtxStr_Sft_SCFG_WINCUR_c) := r.HndlWincur;
            -- Window Memory
            --v.CtxWin_Cmd.Sel        := CtxWin_Sel_WincntWinlast_c;
            v.CtxWin_Cmd.WenLo      := '1';
            v.CtxWin_Cmd.WenHi      := '1';
            v.CtxWin_Cmd.WdatLo     := r.HndlWinBytes(31 downto 0);
            v.CtxWin_Cmd.WdatLo(30) := r.EndOfEvent(r.HndlStream);
            v.CtxWin_Cmd.WdatLo(31) := r.EndOfPacket;
            v.CtxWin_Cmd.WdatHi     := r.HndlWinLast;
          when 1 =>
            -- Stream Memory
            v.CtxStr_Cmd.Sel    := CtxStr_Sel_WinsizePtr_c;
            v.CtxStr_Cmd.WenHi  := '1';
            v.CtxStr_Cmd.WdatHi := r.HndlPtr2;
          when 2 =>
            -- Stream Memory
            v.CtxStr_Cmd.Sel    := CtxStr_Sel_Winend_c;
            v.CtxStr_Cmd.WenLo  := '1';
            v.CtxStr_Cmd.WdatLo := r.HndlWinEnd;
          when others => null;
        end case;
        -- Reset pending events if all event packets arrived or events are not synchronized
        if unsigned(r.StrEventPending and r.StrEnaReg) = 0 or (EventMode = '0') then
          v.StrEventPending := (others=>'1');
        end if;
    end case;

    -- *** Handle Disabled Streams ***
    for str in 0 to Streams_g-1 loop
      if (r.GlbEnaReg = '0') or (r.StrEnaReg(str) = '0') then
        v.FirstAfterEna(str) := '1';
        v.NewBuffer(str)     := '1';
      end if;
    end loop;

    -- *** IRQ Handling ***
    -- Feedback from memory controller
    if r.TfDoneReg = '1' then
      v.TfDoneCnt := std_logic_vector(unsigned(r.TfDoneCnt) + 1);
    end if;

    -- Process transfer completion
    if (unsigned(r.TfDoneCnt) /= 0) and (IrqFifoEmpty = '0') then
      v.IrqFifoRead  := '1';
      v.TfDoneCnt := std_logic_vector(unsigned(v.TfDoneCnt) - 1);
      -- Generate IRQ if required
      if IrqFifoGenIrq = '1' then
        v.StrIrq(to_integer(unsigned(IrqFifoStream)))     := '1';
        v.StrLastWin(to_integer(unsigned(IrqFifoStream))) := std_logic_vector(resize(unsigned(IrqLastWinNr), 5));
      end if;
    end if;

    -- *** Assign to signal ***
    r_next <= v;

  end process;

  -- *** Registered Outputs ***
  CtxStr_Cmd   <= r.CtxStr_Cmd;
  CtxWin_Cmd   <= r.CtxWin_Cmd;
  Dma_Cmd_Vld  <= r.Dma_Cmd_Vld;
  Dma_Cmd      <= r.Dma_Cmd;
  Dma_Resp_Rdy <= r.Dma_Resp_Rdy;
  StrIrq       <= r.StrIrq;
  StrLastWin   <= r.StrLastWin;

  --------------------------------------------
  -- Sequential Process
  --------------------------------------------
  p_seq : process(Clk)
  begin
    if rising_edge(Clk) then
      r <= r_next;
      if Rst = '1' then
        r.ArbDelCnt        <= 0;
        r.InpDataAvail     <= (others => '0');
        r.StrEventPending  <= (others=>'1');
        r.EndOfEvent       <= (others => '0');
        r.DataAvailArbIn   <= (others => '0');
        r.HndlStream       <= 0;
        r.State            <= Idle_s;
        r.CtxStr_Cmd.WenLo <= '0';
        r.CtxStr_Cmd.WenHi <= '0';
        r.CtxWin_Cmd.WenLo <= '0';
        r.CtxWin_Cmd.WenHi <= '0';
        r.Dma_Cmd_Vld      <= '0';
        r.OpenCommand      <= (others => '0');
        r.WinProtected     <= (others => '0');
        r.Dma_Resp_Rdy     <= '0';
        r.GlbEnaReg        <= '0';
        r.FirstOngoing     <= (others => '0');
        r.TfDoneCnt        <= (others => '0');
        r.TfDoneReg        <= '0';
        r.IrqFifoWrite     <= '0';
        r.IrqFifoRead      <= '0';
        r.StrIrq           <= (others => '0');
        r.StrLastWin       <= (others => (others => '0'));
      end if;
    end if;
  end process;

  --------------------------------------------
  -- Component Instantiation
  --------------------------------------------
  -- *** Round Robin Arbiter ***
  i_rrarb : entity work.psi_common_arb_round_robin
    generic map (
      Size_g => Streams_g
    )
    port map(
      -- Control Signals
      Clk       => Clk,
      Rst       => Rst,

      -- Data Ports
      Request   => r.DataAvailArbIn,
      Grant     => Grant,
      Grant_Rdy => GrantRdy,
      Grant_Vld => GrantVld
    );
  -- *** Priority Arbiter ***
--  i_rrarb : entity work.psi_common_arb_priority
--    generic map (
--      Size_g  => Streams_g
--    )
--    port map (
--      Clk     => Clk,
--      Rst     => Rst,
--      Request => r.DataAvailArbIn,
--      Grant   => Grant
--    );
--    GrantVld <= '1' when (unsigned(Grant) /= 0) and (Grant'length > 0)  else '0';

  -- *** IRQ Information FIFO ***
  -- input assembly
  IrqFifoIn(StreamBits_c-1 downto 0) <= std_logic_vector(to_unsigned(r.HndlStream, StreamBits_c));
  IrqFifoIn(StreamBits_c+log2ceil(Windows_g)-1 downto StreamBits_c) <= r.HndlLastWinNr;
  IrqFifoIn(IrqFifoIn'high) <= r.HndlWinDone;

  -- Instantiation
  i_irq_fifo : entity work.psi_common_sync_fifo
    generic map (
      Width_g        => StreamBits_c+log2ceil(Windows_g)+1,
      Depth_g        => 2**StreamBits_c*4,
      AlmFullOn_g    => true,
      AlmFullLevel_g => Streams_g*3,
      RamStyle_g     => "distributed"
    )
    port map (
      Clk     => Clk,
      Rst     => Rst,
      InData  => IrqFifoIn,
      InVld   => r.IrqFifoWrite,
      OutData => IrqFifoOut,
      OutRdy  => r_next.IrqFifoRead,
      AlmFull => IrqFifoAlmFull,
      Empty   => IrqFifoEmpty
    );

  -- Output disassembly
  IrqFifoStream <= IrqFifoOut(StreamBits_c-1 downto 0);
  IrqLastWinNr  <= IrqFifoOut(StreamBits_c+log2ceil(Windows_g)-1 downto StreamBits_c);
  IrqFifoGenIrq <= IrqFifoOut(IrqFifoOut'high);

end;
