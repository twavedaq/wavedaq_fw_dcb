------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.dma_pkt_sched_pkg.all;

library work;
  use work.dma_pkt_sched_sm_tb_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package dma_pkt_sched_sm_tb_case_single_window is

  procedure control (
    signal Clk : in std_logic;
    signal Rst : inout std_logic;
    signal GlbEna : inout std_logic;
    signal StrEna : inout std_logic_vector;
    signal StrIrq : in std_logic_vector;
    signal Inp_EOE : inout std_logic_vector;
    signal Inp_PktValid : inout std_logic_vector;
    signal EventMode : inout std_logic;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t);

  procedure dma_cmd (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t);

  procedure dma_resp (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Resp : inout DaqDma2DaqSm_Resp_t;
    signal Dma_Resp_Vld : inout std_logic;
    signal Dma_Resp_Rdy : in std_logic;
    signal TfDone : inout std_logic;
    signal StrLastWin : in WinType_a(Streams_g-1 downto 0);
    constant Generics_c : Generics_t);

  procedure ctx (
    signal Clk : in std_logic;
    signal CtxStr_Cmd : in ToCtxStr_t;
    signal CtxStr_Resp : inout FromCtx_t;
    signal CtxWin_Cmd : in ToCtxWin_t;
    signal CtxWin_Resp : inout FromCtx_t;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body dma_pkt_sched_sm_tb_case_single_window is
  procedure control (
    signal Clk : in std_logic;
    signal Rst : inout std_logic;
    signal GlbEna : inout std_logic;
    signal StrEna : inout std_logic_vector;
    signal StrIrq : in std_logic_vector;
    signal Inp_EOE : inout std_logic_vector;
    signal Inp_PktValid : inout std_logic_vector;
    signal EventMode : inout std_logic;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    print(">> -- single window --");

    -- Linear without overwrite, no packet complete
    print(">> Linear without overwrite, no packet complete");
    InitTestCase(Clk, Rst);
    TestCase := 0;
    ConfigureAuto(  WinSize => 4096+128, Wincnt => 0, Wincur => 0);
    Inp_PktValid(2) <= '1';
    for i in 0 to 2 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    end loop;
    Inp_PktValid(2) <= '0';
    ControlWaitCompl(Clk);

    -- Linear without overwrite, packet complete
    print(">> Linear without overwrite, packet complete");
    InitTestCase(Clk, Rst);
    TestCase := 1;
    ConfigureAuto(  WinSize => 4096+128, Wincnt => 0, Wincur => 0);
    Inp_PktValid(2) <= '1';
    for i in 0 to 1 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    end loop;
    Inp_PktValid(2) <= '0';
    ControlWaitCompl(Clk);
    wait for 1 us;

    -- Check End Of Event on 3 of four slots/streams with coordinated events
    print(">> EOE check (Event Mode = 1 - coordinated)");
    InitTestCase(Clk, Rst);
    TestCase := 2;
    ConfigureAuto(  WinSize => Size4k_c*2, Wincnt => 7, Wincur => 0);
    -- 1. run: end all except Str 1
    Inp_PktValid <= "1111";
    Inp_EOE <= "1101";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 700 ns;
    -- 2. run: end Str 1 only
    Inp_PktValid <= "1111";
    Inp_EOE <= "0010";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 700 ns;
    -- 3. run: next event
    Inp_PktValid <= "1111";
    Inp_EOE <= "0000";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 1 us;
    ControlWaitCompl(Clk);

    -- Check End Of Event on 3 of four slots/streams with free running events
    print(">> EOE check (Event Mode = 0 - free running)");
    InitTestCase(Clk, Rst);
    TestCase := 3;
    ConfigureAuto(  WinSize => Size4k_c*2, Wincnt => 7, Wincur => 0);
    EventMode <= '0';
    -- 1. run: end all except Str 1
    Inp_PktValid <= "1111";
    Inp_EOE <= "1101";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 700 ns;
    -- 2. run: end Str 1 only
    Inp_PktValid <= "1111";
    Inp_EOE <= "0010";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 700 ns;
    -- 3. run: next event
    Inp_PktValid <= "1111";
    Inp_EOE <= "0000";
    for i in 3 downto 0 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(Dma_Cmd.Stream) <= '0';
      wait until rising_edge(Clk);
    end loop;
    wait for 1 us;
    EventMode <= '1';
    ControlWaitCompl(Clk);

  FinishTestCase;

end procedure;

  procedure dma_cmd (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t) is
  begin

    -- Linear without overwrite, no packet complete
    WaitForCase(0,  Clk);
    ExpectDmaCmdAuto(  Stream  => 2, MaxSize => 4096, Msg => "Wr0.0",
              Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    ExpectDmaCmdAuto(  Stream  => 2, MaxSize => 128, Msg => "Wr0.1", NextWin => true,
              Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    ExpectDmaCmdAuto(  Stream  => 2, MaxSize => 4096, Msg => "Wr1.0",
              Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    ProcDone(2)  := '1';

    -- Linear without overwrite, packet complete
    WaitForCase(1,  Clk);
    ExpectDmaCmdAuto(  Stream  => 2, MaxSize => 4096, ExeSize => 4000, Msg => "Wr0.0", NextWin => true,
              Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    ExpectDmaCmdAuto(  Stream  => 2, MaxSize => 4096, Msg => "Wr1.0",
              Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    ProcDone(2)  := '1';

    -- Check End Of Event on 3 of four slots/streams with coordinated events
    WaitForCase(2,  Clk);
    for str in 3 downto 0 loop
      ExpectDmaCmdAuto( Stream => str, MaxSize => Size4k_c, ExeSize=>Size4k_c, Msg => "Wr0." & integer'image(str), NextWin => true,
                        Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    end loop;
    ExpectDmaCmdAuto( Stream => 1, MaxSize => Size4k_c, ExeSize=>Size4k_c, Msg => "Wr0.1", NextWin => true,
    Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    for str in 3 downto 1 loop -- str = 0, 3, 2
      ExpectDmaCmdAuto( Stream => ((str-3) mod 4), MaxSize => Size4k_c, ExeSize=>Size4k_c, Msg => "Wr1." & integer'image((str-3) mod 4), NextWin => true,
                        Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    end loop;
    for str in 3 downto 0 loop -- str = 1, 0, 3, 2
      ExpectDmaCmdAuto( Stream => ((str-2) mod 4), MaxSize => Size4k_c, ExeSize=>Size4k_c, Msg => "Wr2." & integer'image((str-2) mod 4), NextWin => true,
                        Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
    end loop;
    ProcDone(2)  := '1';

    -- Check End Of Event on 3 of four slots/streams with free running events
    WaitForCase(3,  Clk);
    for i in 0 to 2 loop
      for str in 3 downto 0 loop
        ExpectDmaCmdAuto( Stream => str, MaxSize => Size4k_c, ExeSize=>Size4k_c, Msg => "Wr" & integer'image(i) & "." & integer'image(str), NextWin => true,
                          Clk  => Clk,  Dma_Cmd  => Dma_Cmd,  Dma_Vld  => Dma_Cmd_Vld);
      end loop;
    end loop;
    ProcDone(2)  := '1';

  end procedure;

  procedure dma_resp (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Resp : inout DaqDma2DaqSm_Resp_t;
    signal Dma_Resp_Vld : inout std_logic;
    signal Dma_Resp_Rdy : in std_logic;
    signal TfDone : inout std_logic;
    signal StrLastWin : in WinType_a(Streams_g-1 downto 0);
    constant Generics_c : Generics_t) is
  begin

    -- Linear without overwrite, no packet complete
    WaitForCase(0,  Clk);
    ApplyDmaRespAuto(  Stream => 2, PktCmplt => '0',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    ApplyDmaRespAuto(  Stream => 2, PktCmplt => '0',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    ApplyDmaRespAuto(  Stream => 2, PktCmplt => '0',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    ProcDone(1)  := '1';

    -- Linear without overwrite, packet complete
    WaitForCase(1,  Clk);
    ApplyDmaRespAuto(  Stream => 2, PktCmplt => '1',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    ApplyDmaRespAuto(  Stream => 2, PktCmplt => '0',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    ProcDone(1)  := '1';

    -- Check End Of Event on 3 of four slots/streams with coordinated events
    WaitForCase(2,  Clk);
    for str in 3 downto 0 loop
      ApplyDmaRespAuto(  Stream => str, PktCmplt => '1',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    end loop;
    ApplyDmaRespAuto(  Stream => 1, PktCmplt => '1',
    Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    for str in 3 downto 1 loop -- str = 0, 3, 2
      ApplyDmaRespAuto(  Stream => ((str-3) mod 4), PktCmplt => '1',
            Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    end loop;
    for str in 3 downto 0 loop -- str = 1, 0, 3, 2
      ApplyDmaRespAuto(  Stream => ((str-2) mod 4), PktCmplt => '1',
              Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
    end loop;
    ProcDone(1)  := '1';

    -- Check End Of Event on 3 of four slots/streams with free running events
    WaitForCase(3,  Clk);
    for i in 0 to 2 loop
      for str in 3 downto 0 loop
        ApplyDmaRespAuto(  Stream => str, PktCmplt => '1',
                Clk  => Clk,  Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,  Dma_Resp_Rdy => Dma_Resp_Rdy);
      end loop;
    end loop;
    ProcDone(1)  := '1';

  end procedure;

  procedure ctx (
    signal Clk : in std_logic;
    signal CtxStr_Cmd : in ToCtxStr_t;
    signal CtxStr_Resp : inout FromCtx_t;
    signal CtxWin_Cmd : in ToCtxWin_t;
    signal CtxWin_Resp : inout FromCtx_t;
    constant Generics_c : Generics_t) is
  begin

    -- Linear without overwrite, no packet complete
    WaitForCase(0, Clk);
    ExpCtxFullBurstAuto(  Stream => 2 ,Msg => "Wr0.0",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxFullBurstAuto(  Stream => 2, NextWin => true, Msg => "Wr0.1",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxReadAuto(    Stream => 2, Msg => "SW not ready 0",
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxReadAuto(    Stream => 2, Msg => "SW not ready 1",
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    SplsWinStr_v(2)(0) := 0;
    ExpCtxFullBurstAuto(  Stream => 2 ,Msg => "SW ready",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)  := '1';

    -- Linear without overwrite, packet complete
    WaitForCase(1, Clk);
    ExpCtxFullBurstAuto(  Stream => 2, NextWin => true, PktIsCmplt => true, Msg => "Wr0.0",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxReadAuto(    Stream => 2, Msg => "SW not ready 0",
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxReadAuto(    Stream => 2, Msg => "SW not ready 1",
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    SplsWinStr_v(2)(0) := 0;
    ExpCtxFullBurstAuto(  Stream => 2 ,Msg => "SW ready",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)  := '1';

    -- Check End Of Event on 3 of four slots/streams with coordinated events
    WaitForCase(2, Clk);
    for str in 3 downto 0 loop
      ExpCtxReadAuto(    Stream => str, Msg => "Rd0." & integer'image(str),
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    for str in 3 downto 0 loop
      ExpCtxUpdateAuto(  Stream => str, NextWin => true, PktIsCmplt => true, Msg => "Wr0." & integer'image(str),
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    ExpCtxFullBurstAuto(  Stream => 1, NextWin => true, PktIsCmplt => true, Msg => "Wr1.1",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    for str in 3 downto 1 loop -- str = 0, 3, 2
      ExpCtxReadAuto(    Stream => ((str-3) mod 4), Msg => "Rd1." & integer'image((str-3) mod 4),
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    for str in 3 downto 1 loop -- str = 0, 3, 2
      ExpCtxUpdateAuto(  Stream => ((str-3) mod 4), NextWin => true, PktIsCmplt => true, Msg => "Wr1." & integer'image((str-3) mod 4),
              Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    for str in 3 downto 0 loop -- str = 1, 0, 3, 2
      ExpCtxReadAuto(    Stream => ((str-2) mod 4), Msg => "Rd2." & integer'image((str-2) mod 4),
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    for str in 3 downto 0 loop -- str = 1, 0, 3, 2
      ExpCtxUpdateAuto(  Stream => ((str-2) mod 4), NextWin => true, PktIsCmplt => true, Msg => "Wr2." & integer'image((str-2) mod 4),
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    ProcDone(0)  := '1';

    -- Check End Of Event on 3 of four slots/streams with free running events
    WaitForCase(3,  Clk);
    for i in 0 to 2 loop
      for str in 3 downto 0 loop
        ExpCtxReadAuto(    Stream => str, Msg => "Rd" & integer'image(i) & "." & integer'image(str),
                  Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
      end loop;
      for str in 3 downto 0 loop
        ExpCtxUpdateAuto(  Stream => str, NextWin => true, PktIsCmplt => true, Msg => "Wr" & integer'image(i) & "." & integer'image(str),
                  Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
      end loop;
    end loop;
    ProcDone(0)  := '1';

  end procedure;

end;
