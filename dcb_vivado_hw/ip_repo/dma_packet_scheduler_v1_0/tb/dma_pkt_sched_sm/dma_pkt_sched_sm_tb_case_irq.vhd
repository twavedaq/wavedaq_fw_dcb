------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.dma_pkt_sched_pkg.all;

library work;
  use work.dma_pkt_sched_sm_tb_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package dma_pkt_sched_sm_tb_case_irq is

  procedure control (
    signal Clk : in std_logic;
    signal Rst : inout std_logic;
    signal GlbEna : inout std_logic;
    signal StrEna : inout std_logic_vector;
    signal StrIrq : in std_logic_vector;
    signal Inp_EOE : inout std_logic_vector;
    signal Inp_PktValid : inout std_logic_vector;
    signal EventMode : inout std_logic;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t);

  procedure dma_cmd (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t);

  procedure dma_resp (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Resp : inout DaqDma2DaqSm_Resp_t;
    signal Dma_Resp_Vld : inout std_logic;
    signal Dma_Resp_Rdy : in std_logic;
    signal TfDone : inout std_logic;
    signal StrLastWin : in WinType_a(Streams_g-1 downto 0);
    constant Generics_c : Generics_t);

  procedure ctx (
    signal Clk : in std_logic;
    signal CtxStr_Cmd : in ToCtxStr_t;
    signal CtxStr_Resp : inout FromCtx_t;
    signal CtxWin_Cmd : in ToCtxWin_t;
    signal CtxWin_Resp : inout FromCtx_t;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body dma_pkt_sched_sm_tb_case_irq is
  procedure control (
    signal Clk : in std_logic;
    signal Rst : inout std_logic;
    signal GlbEna : inout std_logic;
    signal StrEna : inout std_logic_vector;
    signal StrIrq : in std_logic_vector;
    signal Inp_EOE : inout std_logic_vector;
    signal Inp_PktValid : inout std_logic_vector;
    signal EventMode : inout std_logic;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    print(">> -- irq --");
    EventMode <= '1';

    -- Normal Order
    print(">> Normal Order");
    InitTestCase(Clk, Rst);
    TestCase := 0;
    ConfigureAuto(	WinSize => 4096, Wincnt => 2, Wincur => 0);
    Inp_PktValid(0) <= '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    Inp_PktValid(0) <= '0';
    ControlWaitCompl(Clk);

    -- Flipped Order
    print(">> Flipped Order");
    InitTestCase(Clk, Rst);
    TestCase := 1;
    ConfigureAuto(	WinSize => 4096, Wincnt => 2, Wincur => 0);
    Inp_PktValid(0) <= '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    Inp_PktValid(0) <= '0';
    ControlWaitCompl(Clk);

    -- IRQ FIFO full
    -- ... FIFO is full after Streams (4) x 3 = 12 open transfers
    print(">> IRQ FIFO full");
    InitTestCase(Clk, Rst);
    TestCase := 2;
    ConfigureAuto(	WinSize => 4096, Wincnt => 31, Wincur => 0);
    Inp_PktValid(0) <= '1';
    -- Fill FIFO
    for i in 0 to 11 loop
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    end loop;
    -- Checked transfer
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    Inp_PktValid(0) <= '0';
    ControlWaitCompl(Clk);

    -- Multi-Stream
    wait for 1 us;
    print(">> Multi-Stream");
    InitTestCase(Clk, Rst);
    TestCase := 3;
    ConfigureAuto(	WinSize => 4096, Wincnt => 2, Wincur => 0);
    for i in 3 downto 0 loop
      Inp_PktValid(i) <= '1';
      wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
      Inp_PktValid(i) <= '0';
      wait for 200 ns;
    end loop;
    ControlWaitCompl(Clk);

    -- Enable Stream 3 only
    wait for 1 us;
    print(">> Enable Stream 4 only");
    StrEna <= "1000";
    InitTestCase(Clk, Rst);
    TestCase := 4;
    ConfigureAuto(	WinSize => 4096, Wincnt => 2, Wincur => 0);
    Inp_PktValid(3) <= '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    Inp_PktValid(3) <= '0';
    ControlWaitCompl(Clk);
    StrEna <= "1111";

    -- Win-Change without packet complete
    wait for 1 us;
    print(">> Win-Change without packet complete");
    InitTestCase(Clk, Rst);
    TestCase := 5;
    ConfigureAuto(	WinSize => 4096*2, Wincnt => 2, Wincur => 0);
    Inp_PktValid(0) <= '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    wait until rising_edge(Clk) and Dma_Cmd_Vld = '1';
    Inp_PktValid(0) <= '0';
    ControlWaitCompl(Clk);

    FinishTestCase;

  end procedure;

  procedure dma_cmd (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Cmd : in DaqSm2DaqDma_Cmd_t;
    signal Dma_Cmd_Vld : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    -- Normal Order
    WaitForCase(0,  Clk);
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, ExeSize=> 512, Msg => "Wr0.0",
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ProcDone(2)	:= '1';

    -- Flipped Order
    WaitForCase(1,  Clk);
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, ExeSize=> 512, Msg => "Wr0.0",
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ProcDone(2)	:= '1';

    -- IRQ FIFO full
    WaitForCase(2,  Clk);
    -- Fill FIFO
    for i in 0 to 11 loop
      ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, ExeSize=> 512, Msg => "Wr" & to_string(i), NextWin => true,
                Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    end loop;
    -- Checked transfer
    CheckNoActivity(Dma_Cmd_Vld, 1 us, 0, "Full");
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, ExeSize=> 512, Msg => "Wr12", NextWin => true,
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ProcDone(2)	:= '1';

    -- Multi-Stream
    WaitForCase(3,  Clk);
    for i in 3 downto 0 loop
      ExpectDmaCmdAuto(	Stream	=> i, MaxSize => 4096, ExeSize=> 512*(i+1), Msg => "Wr" & to_string(i), NextWin => true,
                Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    end loop;
    ProcDone(2)	:= '1';

    -- Enable Stream 3 only
    WaitForCase(4,  Clk);
    ExpectDmaCmdAuto(	Stream	=> 3, MaxSize => 4096, ExeSize=> 512, Msg => "Wr3",
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ProcDone(2)	:= '1';

    -- Win-Change without packet complete
    WaitForCase(5,  Clk);
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0",
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0", NextWin => true,
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0",
              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
    ProcDone(2)	:= '1';

--    -- No IRQ on Ringbuf Wrap
--    WaitForCase(6,  Clk);
--    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0",
--              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
--    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0",
--              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
--    ExpectDmaCmdAuto(	Stream	=> 0, MaxSize => 4096, Msg => "Wr0",
--              Clk	=> Clk,	Dma_Cmd	=> Dma_Cmd,	Dma_Vld	=> Dma_Cmd_Vld);
--    ProcDone(2)	:= '1';

  end procedure;

  procedure dma_resp (
    signal Clk : in std_logic;
    signal StrIrq : in std_logic_vector;
    signal Dma_Resp : inout DaqDma2DaqSm_Resp_t;
    signal Dma_Resp_Vld : inout std_logic;
    signal Dma_Resp_Rdy : in std_logic;
    signal TfDone : inout std_logic;
    signal StrLastWin : in WinType_a(Streams_g-1 downto 0);
    constant Generics_c : Generics_t) is
  begin
    -- Normal Order
    WaitForCase(0,  Clk);
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '1',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    wait for 200 ns;
    StdlvCompareStdlv ("0000", StrIrq, "IRQs asserted unexpectedly");
    assert StrIrq'last_event > 200 ns report "###ERROR###: IRQs not idle" severity error;
    AssertTfDone(Clk, TfDone);
    CheckIrq(MaxWait => (1 us), Stream => 0, LastWin => 0, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    ProcDone(1)	:= '1';

    -- Flipped Order
    WaitForCase(1,  Clk);
    AssertTfDone(Clk, TfDone);
    wait for 200 ns;
    StdlvCompareStdlv ("0000", StrIrq, "IRQs asserted unexpectedly");
    assert StrIrq'last_event > 200 ns report "###ERROR###: IRQs not idle" severity error;
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '1',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    CheckIrq(MaxWait => (1 us), Stream => 0, LastWin => 0, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    ProcDone(1)	:= '1';

    -- IRQ FIFO full
    WaitForCase(2,  Clk);
    -- Fill FIFO
    for i in 0 to 11 loop
      ApplyDmaRespAuto(	Stream => 0, PktCmplt => '1',
                Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    end loop;
    -- Wait
    wait for 1.1 us;
    -- Send IRQs
    for i in 0 to 11 loop
      AssertTfDone(Clk, TfDone);
      CheckIrq(MaxWait => (1 us), Stream => 0, LastWin => i, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    end loop;
    -- Last transfer
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '1',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    AssertTfDone(Clk, TfDone);
    CheckIrq(MaxWait => (1 us), Stream => 0, LastWin => 12, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    ProcDone(1)	:= '1';

    -- Multi-Stream
    WaitForCase(3,  Clk);
    for i in 3 downto 0 loop
      ApplyDmaRespAuto(	Stream => i, PktCmplt => '1',
                Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    end loop;
    for i in 3 downto 0 loop
      AssertTfDone(Clk, TfDone);
      CheckIrq(MaxWait => (1 us), Stream => i, LastWin => 0, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    end loop;
    ProcDone(1)	:= '1';

    -- Enable Stream 3 only
    WaitForCase(4,  Clk);
    ApplyDmaRespAuto(	Stream => 3, PktCmplt => '1',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    wait for 200 ns;
    StdlvCompareStdlv ("0000", StrIrq, "IRQs asserted unexpectedly");
    assert StrIrq'last_event > 200 ns report "###ERROR###: IRQs not idle" severity error;
    AssertTfDone(Clk, TfDone);
    CheckIrq(MaxWait => (1 us), Stream => 3, LastWin => 0, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    ProcDone(1)	:= '1';

    -- Win-Change without packet complete
    WaitForCase(5,  Clk);
    wait for 100 ns;
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '0',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    AssertTfDone(Clk, TfDone);
    CheckNoActivityStlv(StrIrq, 100 ns, 0, "Before Window Change");
    wait for 100 ns;
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '0',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    AssertTfDone(Clk, TfDone);
    CheckIrq(MaxWait => (1 us), Stream => 0, LastWin => 0, Clk => Clk, StrIrq => StrIrq, StrLastWin => StrLastWin);
    wait for 100 ns;
    ApplyDmaRespAuto(	Stream => 0, PktCmplt => '0',
              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
    AssertTfDone(Clk, TfDone);
    CheckNoActivityStlv(StrIrq, 100 ns, 0, "After Window Change");
    ProcDone(1)	:= '1';

--    -- No IRQ on Ringbuf Wrap
--    WaitForCase(6,  Clk);
--    wait for 100 ns;
--    ApplyDmaRespAuto(	Stream => 0, Trigger => '0',
--              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
--    AssertTfDone(Clk, TfDone);
--    CheckNoActivityStlv(StrIrq, 100 ns, 0, "Before Wrap");
--    wait for 100 ns;
--    ApplyDmaRespAuto(	Stream => 0, Trigger => '0',
--              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
--    AssertTfDone(Clk, TfDone);
--    CheckNoActivityStlv(StrIrq, 100 ns, 0, "Wrap");
--    wait for 100 ns;
--    ApplyDmaRespAuto(	Stream => 0, Trigger => '0',
--              Clk	=> Clk,	Dma_Resp => Dma_Resp, Dma_Resp_Vld => Dma_Resp_Vld,	Dma_Resp_Rdy => Dma_Resp_Rdy);
--    AssertTfDone(Clk, TfDone);
--    CheckNoActivityStlv(StrIrq, 100 ns, 0, "After Wrap");
--    ProcDone(1)	:= '1';

  end procedure;

  procedure ctx (
    signal Clk : in std_logic;
    signal CtxStr_Cmd : in ToCtxStr_t;
    signal CtxStr_Resp : inout FromCtx_t;
    signal CtxWin_Cmd : in ToCtxWin_t;
    signal CtxWin_Resp : inout FromCtx_t;
    constant Generics_c : Generics_t) is
  begin
    -- Normal Order
    WaitForCase(0, Clk);
    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.0", NextWin => true, PktIsCmplt => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)	:= '1';

    -- Flipped Order
    WaitForCase(1, Clk);
    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.0", NextWin => true, PktIsCmplt => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)	:= '1';

    -- IRQ FIFO full
    WaitForCase(2, Clk);
    for i in 0 to 11 loop
      ExpCtxFullBurstAuto(	Stream => 0, NextWin => true, PktIsCmplt => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    ExpCtxFullBurstAuto(	Stream => 0, NextWin => true, PktIsCmplt => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)	:= '1';

    -- Multi-Stream
    WaitForCase(3, Clk);
    for i in 3 downto 0 loop
      ExpCtxFullBurstAuto(	Stream => i, NextWin => true, PktIsCmplt => true,
                  Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    end loop;
    ProcDone(0)	:= '1';

    -- Enable Stream 3 only
    WaitForCase(4, Clk);
    ExpCtxFullBurstAuto(	Stream => 3, Msg => "Wr3", NextWin => true, PktIsCmplt => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)	:= '1';

    -- Win-Change without packet complete
    WaitForCase(5, Clk);
    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.0",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.1", NextWin => true,
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr1.0",
                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
    ProcDone(0)	:= '1';

--    -- No IRQ on Ringbuf Wrap
--    WaitForCase(6, Clk);
--    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.0",
--                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
--    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.1",
--                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
--    ExpCtxFullBurstAuto(	Stream => 0, Msg => "Wr0.2",
--                Clk => Clk, CtxStr_Cmd => CtxStr_Cmd, CtxStr_Resp => CtxStr_Resp, CtxWin_Cmd => CtxWin_Cmd, CtxWin_Resp => CtxWin_Resp);
--    ProcDone(0)	:= '1';

  end procedure;

end;
