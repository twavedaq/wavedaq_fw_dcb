------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.dma_pkt_sched_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_axi_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package dma_pkt_sched_axi_tb_pkg is

  --------------------------------------------------------
  -- Global Stuff
  --------------------------------------------------------
  constant MemSize_c      : integer := 16#10000#;
  signal Memory           : t_aslv8(0 to MemSize_c-1);
  constant MaxWindows_c   : integer := 16;

  constant PrintDefault_c : boolean := true;--false;

  constant PrintStr_c     : t_abool(0 to 3) := (others=>PrintDefault_c);

  --------------------------------------------------------
  -- Register MAP
  --------------------------------------------------------
  constant REG_CONF_REGION    : integer  := 16#0000#;
  constant REG_CONF_GCFG_ADDR    : integer  := REG_CONF_REGION+16#000#;
  constant REG_CONF_GSTAT_ADDR  : integer  := REG_CONF_REGION+16#004#;
  constant REG_CONF_IRQVEC_ADDR  : integer  := REG_CONF_REGION+16#010#;
  constant REG_CONF_IRQENA_ADDR  : integer  := REG_CONF_REGION+16#014#;
  constant REG_CONF_STRENA_ADDR  : integer  := REG_CONF_REGION+16#020#;
  constant REG_CONF_Xn_STEP    : integer  := 16#04#;
--  constant REG_CONF_Xn_STEP    : integer  := 16#10#;
--  constant REG_CONF_MAXLVLn    : integer  := 16#200#;
--  constant REG_CONF_POSTTRIGn    : integer  := 16#204#;
--  constant REG_CONF_MODEn      : integer  := 16#208#;
--  constant REG_CONF_LASTWINn    : integer  := 16#20C#;
  constant REG_CONF_LASTWINn    : integer  := 16#200#;
--  constant VAL_MODE_RECM_CONT    : integer  := 0*2**0;
--  constant VAL_MODE_RECM_TRIGMASK  : integer  := 1*2**0;
--  constant VAL_MODE_RECM_SINGLE  : integer  := 2*2**0;
--  constant VAL_MODE_RECM_MANUAL  : integer  := 3*2**0;
--  constant VAL_MODE_ARM      : integer  := 1*2**8;
--  constant VAL_MODE_RECORDING    : integer  := 1*2**16;

  constant REG_CTX_REGION      : integer  := 16#1000#;
  constant REG_CTX_Xn_STEP    : integer  := 16#20#;
  constant REG_CTX_SCFGn      : integer  := 16#00#;
--  constant VAL_SCFG_RINGBUF    : integer  := 1*2**0;
--  constant VAL_SCFG_OVERWRITE    : integer  := 1*2**8;
  constant SFT_SCFG_WINCNT    : integer  := 16;
  constant SFT_SCFG_WINCUR    : integer  := 24;
  constant MSK_SCFG_WINCUR     : integer  := 16#1F000000#;
  constant REG_CTX_BUFSTARTn    : integer  := 16#04#;
  constant REG_CTX_WINSIZEn    : integer  := 16#08#;
  constant REG_CTX_PTRn      : integer  := 16#0C#;
  constant REG_CTX_WINENDn    : integer  := 16#10#;

  constant REG_WIN_REGION      : integer  := 16#4000#;
  constant REG_WIN_STRn_STEP    : integer  := MaxWindows_c*16#08#;
  constant REG_WIN_WINn_STEP    : integer  := 16#08#;
  constant REG_WIN_WINCNT      : integer  := 16#00#;
  constant MSK_WIN_WINCNT_CNT    : integer  := 16#3FFFFFFF#;
  constant MSK_WIN_WINCNT_EOE       : integer  := 16#40000000#;
  constant MSK_WIN_WINCNT_PKTCMPLT  : integer  := 16#80000000#;
  constant REG_WIN_WINLAST    : integer  := 16#04#;
--  constant REG_WIN_WINLAST    : integer  := 16#04#;
--  constant REG_WIN_TSLO      : integer  := 16#08#;
--  constant REG_WIN_TSHI      : integer  := 16#0C#;

  --------------------------------------------------------
  -- Buffer configuration
  --------------------------------------------------------
  --                                           Stream         0         1         2         3
  constant StrBufStart_c     : t_ainteger(0 to 3)  := (16#1000#, 16#2000#, 16#3000#, 16#4000#);
  constant StrWinSize_c      : t_ainteger(0 to 3)  := (     104,      488,      256,     1024);
  constant StrWindows_c      : t_ainteger(0 to 3)  := (       3,        1,        3,        3);
  constant SlotPacketSize_c  : t_ainteger(0 to 3)  := (     150,      150,      100,      100);
  constant StrPktsPerEvent_c : t_ainteger(0 to 3)  := (       2,        3,        4,        1);
  alias Memory0 : t_aslv8(0 to StrWinSize_c(0)*StrWindows_c(0)) is Memory(StrBufStart_c(0) to StrBufStart_c(0)+StrWinSize_c(0)*StrWindows_c(0));
  alias Memory1 : t_aslv8(0 to StrWinSize_c(1)*StrWindows_c(1)) is Memory(StrBufStart_c(1) to StrBufStart_c(1)+StrWinSize_c(1)*StrWindows_c(1));
  alias Memory2 : t_aslv8(0 to StrWinSize_c(2)*StrWindows_c(2)) is Memory(StrBufStart_c(2) to StrBufStart_c(2)+StrWinSize_c(2)*StrWindows_c(2));
  alias Memory3 : t_aslv8(0 to StrWinSize_c(3)*StrWindows_c(3)) is Memory(StrBufStart_c(3) to StrBufStart_c(3)+StrWinSize_c(3)*StrWindows_c(3));

  --------------------------------------------------------
  -- Persistent State
  --------------------------------------------------------
  shared variable StrNextWin     : t_ainteger(0 to 3) := (others=>0);
  shared variable StrLastTs      : t_ainteger(0 to 3);
  shared variable StrIrqOn       : t_abool(0 to 3)    := (others=>false);
  shared variable StrDisabled    : t_abool(0 to 3)    := (others=>false);
  shared variable StrDataCnt     : t_ainteger(0 to 3) := (others=>0);
  shared variable StrSplCnt      : t_ainteger(0 to 3) := (others=>0);
  shared variable StrExpFrame    : t_ainteger(0 to 3) := (others=>0);
  shared variable StrFrameCnt    : t_ainteger(0 to 3) := (others=>0);
  shared variable StreamArmed_v  : t_abool(0 to 3)    := (others=>false);
  shared variable StrCurrentVal  : t_ainteger(0 to 3) := (others=>-1);
  shared variable StrActiveEvent : t_ainteger(0 to 3) := (others=>0);
  shared variable RcvActiveEvent : t_ainteger(0 to 3) := (others=>0);
  shared variable RcvNewEvent    : t_abool(0 to 3)    := (others=>true);

  --------------------------------------------------------
  -- Data Generation
  --------------------------------------------------------
  procedure SlotGenerateCountPacket(        PktSize : in  natural   := 16;
                                            EventNr : in  natural   := 0;
                                            FlagEOE : in  std_logic := '0';
                                     signal Clk     : in  std_logic;
                                     signal PktVld  : out std_logic;
                                     signal PktEOE  : out std_logic;
                                     signal Bytes   : out std_logic_vector(3 downto 0);
                                     signal Last    : out std_logic;
                                     signal Valid   : out std_logic;
                                     signal Ready   : in  std_logic;
                                     signal Data    : out std_logic_vector(63 downto 0));

  --------------------------------------------------------
  -- Data Checking
  --------------------------------------------------------
  procedure CheckWinData(         stream      : in  integer;
                                  winstart    : in  integer;
                                  curwin      : in  integer;
                                  wincnt      : in  integer;
                                  PktComplete : in  boolean;
                                  EOE         : in  boolean;
                         variable addrlast    : out integer);

  --------------------------------------------------------
  -- Window Count Checking
  --------------------------------------------------------
  procedure CheckWinCount(stream      : in integer;
                          wincnt      : in integer;
                          PktComplete : in boolean);

  --------------------------------------------------------
  -- IRQ Handler
  --------------------------------------------------------
  procedure StrHandler(       stream : in  integer;
                        signal  clk  : in  std_logic;
                        signal  rqst : out axi_ms_r;
                        signal  rsp  : in  axi_sm_r);

  --------------------------------------------------------
  -- Setup
  --------------------------------------------------------
  procedure StrSetup(        stream : integer;
                      signal clk    : in  std_logic;
                      signal rqst   : out axi_ms_r;
                      signal rsp    : in  axi_sm_r);

  --------------------------------------------------------
  -- Update
  --------------------------------------------------------
  procedure StrUpdate(        stream : integer;
                       signal clk    : in  std_logic;
                       signal rqst   : out axi_ms_r;
                       signal rsp    : in  axi_sm_r);

  --------------------------------------------------------
  -- Helper Procedures
  --------------------------------------------------------
  function IntAnd(  int : in integer;
            op  : in integer) return integer;

  procedure print(  str : in string;
            ena  : in boolean);

  --------------------------------------------------------
  -- Axi Procedures
  --------------------------------------------------------
  procedure AxiWrite32(      address    : in  integer;
                  value    : in   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r);

  procedure AxiRead32(      address    : in  integer;
                  value    : out   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r);

  procedure AxiExpect32(      address    : in  integer;
                  value    : in   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r;
                  msg      : in  string  := "");

  procedure AxiWriteAndRead32(      address    : in  integer;
                      value    : in   integer;
                  signal  clk      : in  std_logic;
                  signal  ms      : out  axi_ms_r;
                  signal   sm      : in  axi_sm_r;
                      msg      : in  string  := "");

  --------------------------------------------------------
  -- High Level Procedures
  --------------------------------------------------------
  procedure HlConfStream(        str      : in  integer;
                    bufstart  : in  integer;
                    wincnt    : in  integer;
                    winsize    : in  integer;
                signal  clk      : in  std_logic;
                signal  rqst    : out  axi_ms_r;
                signal   rsp      : in  axi_sm_r);

  procedure HlGetPtr(          str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer);

  procedure HlGetLastWin(        str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer);

  procedure HlGetCurWin(        str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer);

  procedure HlGetWinInfo(        str      : in  integer;
                    win      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    ByteCount    : out  integer;
                    EndOfEvent   : out  boolean;
                    PktCmplt     : out  boolean);

  procedure HlClrWinCnt(        str      : in  integer;
                    win     : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r);

  procedure HlGetWinLast(        str      : in  integer;
                    win     : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body dma_pkt_sched_axi_tb_pkg is

  --------------------------------------------------------
  -- Data Generation
  --------------------------------------------------------
  procedure SlotGenerateCountPacket(        PktSize : in  natural   := 16;
                                            EventNr : in  natural   := 0;
                                            FlagEOE : in  std_logic := '0';
                                     signal Clk     : in  std_logic;
                                     signal PktVld  : out std_logic;
                                     signal PktEOE  : out std_logic;
                                     signal Bytes   : out std_logic_vector(3 downto 0);
                                     signal Last    : out std_logic;
                                     signal Valid   : out std_logic;
                                     signal Ready   : in  std_logic;
                                     signal Data    : out std_logic_vector(63 downto 0)) is
    variable byte_count : natural;
    variable range_high : integer;
    variable data_draft : std_logic_vector(63 downto 0);
    variable first_word : boolean := true;
  begin
    -- Initial values
    PktVld <= '0';
    PktEOE <= '0';
    Bytes  <= (others=>'0');
    Last   <= '0';
    Valid  <= '0';
    Data   <= (others=>'0');
    -- send data
    wait until rising_edge(Clk);
    PktVld <= '1';
    Bytes  <= "1000";
    Valid  <= '1';
    PktEOE <= FlagEOE;
    wait until rising_edge(Clk);
    byte_count := PktSize;
    while byte_count > 0 loop
      if byte_count < 8 then
        range_high := byte_count-1;
      else
        range_high := 7;
      end if;
      data_draft := (others=>'0');
      for byte_idx in 0 to range_high loop
        byte_count := byte_count - 1;
        data_draft((byte_idx+1)*8-1 downto byte_idx*8) := std_logic_vector(to_unsigned((byte_count) mod (2**8), 8));
      end loop;
      if first_word then
        -- insert event number in first byte
        first_word := false;
        data_draft(7 downto 0) := std_logic_vector(to_unsigned(EventNr mod 255, 8));
      end if;
      Data <= data_draft;
      if byte_count = 0 then
        Bytes <= std_logic_vector(to_signed(range_high+1, Bytes'length));
        Last  <= '1';
      end if;
      wait until rising_edge(Clk) and Ready = '1';
    end loop;
    PktVld <= '0';
    PktEOE <= '0';
    Bytes  <= (others=>'X');
    Last   <= '0';
    Valid  <= '0';
    Data   <= (others=>'X');
    wait until rising_edge(Clk);
  end procedure;

  --------------------------------------------------------
  -- Data Checking
  --------------------------------------------------------
  procedure CheckWinData(         stream      : in  integer;
                                  winstart    : in  integer;
                                  curwin      : in  integer;
                                  wincnt      : in  integer;
                                  PktComplete : in  boolean;
                                  EOE         : in  boolean;
                         variable addrlast    : out integer) is
    variable addr       : integer;
    variable first_byte : boolean := false;
  begin
    -- if first window of packet: initialize value
    print("Checking Data (Stream " & to_string(stream) & ", Window " & to_string(curwin) & ") ...", PrintStr_c(stream));
    if StrCurrentVal(stream) < 0 then
      StrCurrentVal(stream) := (SlotPacketSize_c(stream)-1) mod 255;
      first_byte := true;
    end if;
    addr := winstart;
    for i in wincnt-1 downto 0 loop
      if first_byte then
        first_byte := false;
        -- check event number
        if RcvNewEvent(stream) then
          RcvNewEvent(stream) := false;
          print("New Event: " & to_string(RcvActiveEvent(stream)), PrintStr_c(stream));
        end if;
        StdlvCompareInt(RcvActiveEvent(stream), Memory(addr), "Stream" & to_string(stream) & ": Wrong value (event number) at 0x" & to_hstring(to_unsigned(addr,32)), false);
      else
        StdlvCompareInt(StrCurrentVal(stream), Memory(addr), "Stream" & to_string(stream) & ": Wrong value at 0x" & to_hstring(to_unsigned(addr,32)), false);
      end if;
      if StrCurrentVal(stream) > 0 then
        StrCurrentVal(stream) := StrCurrentVal(stream)-1;
      else
        StrCurrentVal(stream) := 255;
      end if;
      addr := addr + 1;
    end loop;
      -- Last window of packet: set value to trigger initialization when checking next window
    if PktComplete then
      StrCurrentVal(stream) := -1;
      if EOE then
        RcvActiveEvent(stream) := (RcvActiveEvent(stream)+1) mod 255;
        RcvNewEvent(stream) := true;
      end if;
    end if;
    addrlast := addr;
  end procedure;

  procedure CheckWinCount(stream      : in integer;
                          wincnt      : in integer;
                          PktComplete : in boolean) is
  begin
    if PktComplete then
      IntCompare(SlotPacketSize_c(stream) mod StrWinSize_c(stream), wincnt, "Stream" & to_string(stream) & ": WINCNT wrong");
    else
      IntCompare(StrWinSize_c(stream), wincnt, "Stream" & to_string(stream) & ": WINCNT wrong");
    end if;
  end procedure;

  --------------------------------------------------------
  -- IRQ Handler
  --------------------------------------------------------
  procedure StrHandler(       stream : in  integer;
                       signal clk    : in  std_logic;
                       signal rqst   : out axi_ms_r;
                       signal rsp    : in  axi_sm_r) is
    variable v : integer;
    variable curwin : integer;
    variable lastwin : integer;
    variable wincnt : integer;
    variable winstart, winend : integer;
    variable winlast : integer;
    variable addrlast : integer;
    variable firstLoop : boolean := true;
    variable PktComplete : boolean;
    variable EndOfEvent : boolean;
  begin
    print("------------ Stream " & to_string(stream) & " Handler ------------", PrintStr_c(stream));
    HlGetCurWin(stream, clk, rqst, rsp, curwin);
    print("CURWIN: " & to_string(curwin), PrintStr_c(stream));
    HlGetLastWin(stream, clk, rqst, rsp, lastwin);
    print("LASTWIN: " & to_string(lastwin), PrintStr_c(stream));
    print("", PrintStr_c(stream));
    if StrDisabled(stream) then
      print("Skipped, stream disabled", PrintStr_c(stream));
      print("", PrintStr_c(stream));
    else
      HlGetWinInfo(stream, StrNextWin(stream), clk, rqst, rsp, wincnt, EndOfEvent, PktComplete);
      -- lastwin = nextwin can occur if al lwindows are filled. In all cases we only interpret windows containing triggers.
      while ((StrNextWin(stream) /= ((lastwin+1) mod 3)) or firstLoop) and
            (PktComplete or (wincnt = StrWinSize_c(stream))) loop
        firstLoop := false;
        print("*** Window " & to_string(StrNextWin(stream)) & " ***", PrintStr_c(stream));
        HlGetWinInfo(stream, StrNextWin(stream), clk, rqst, rsp, wincnt, EndOfEvent, PktComplete);
        print("WINCNT: " & to_string(wincnt), PrintStr_c(stream));
        HlClrWinCnt(stream, StrNextWin(stream), clk, rqst, rsp);
        HlGetWinLast(stream, StrNextWin(stream), clk, rqst, rsp, winlast);
        print("WINLAST: " & to_string(winlast), PrintStr_c(stream));
        winstart := StrBufStart_c(stream) + StrNextWin(stream)*StrWinSize_c(stream);
        winend := winstart + StrWinSize_c(stream) - 1;
        CheckWinCount(stream, wincnt, PktComplete);
        CheckWinData(stream, winstart, curwin, wincnt, PktComplete, EndOfEvent, addrlast);
        IntCompare(addrlast-1, winlast, "Stream" & to_string(stream) & ": WINLAST wrong");
        print("", PrintStr_c(stream));
        StrNextWin(stream)  := (StrNextWin(stream) + 1) mod StrWindows_c(stream);
        HlGetWinInfo(stream, StrNextWin(stream), clk, rqst, rsp, wincnt, EndOfEvent, PktComplete);
      end loop;
    end if;
  end procedure;

  --------------------------------------------------------
  -- Setup
  --------------------------------------------------------
  procedure StrSetup(        stream : integer;
                      signal clk    : in  std_logic;
                      signal rqst   : out axi_ms_r;
                      signal rsp    : in  axi_sm_r) is
  begin
    HlConfStream(  str => stream, bufstart => StrBufStart_c(stream), wincnt => StrWindows_c(stream), winsize => StrWinSize_c(stream),
            clk => clk, rqst => rqst, rsp => rsp);
  end procedure;

  --------------------------------------------------------
  -- Update
  --------------------------------------------------------
  procedure StrUpdate(        stream : integer;
                       signal clk    : in  std_logic;
                       signal rqst   : out  axi_ms_r;
                       signal rsp    : in  axi_sm_r) is
  begin
  end;

  --------------------------------------------------------
  -- Helper Procedures
  --------------------------------------------------------
  function IntAnd(  int : in integer;
            op  : in integer) return integer is
    variable intu, opu : signed(31 downto 0);
  begin
    intu := to_signed(int, 32);
    opu := to_signed(op, 32);
    return to_integer(intu and opu);
  end function;

  procedure print(  str : in string;
            ena  : in boolean) is
  begin
    if ena then
      print(str);
    end if;
  end procedure;

  --------------------------------------------------------
  -- Axi Procedures
  --------------------------------------------------------
  procedure AxiWrite32(      address    : in  integer;
                  value    : in   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r) is
  begin
    axi_single_write(address, value, ms, sm, clk);
  end procedure;

  procedure AxiRead32(      address    : in  integer;
                  value    : out   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r) is
  begin
    axi_single_read(address, value, ms, sm, clk);
  end procedure;

  procedure AxiExpect32(      address    : in  integer;
                  value    : in   integer;
              signal  clk      : in  std_logic;
              signal  ms      : out   axi_ms_r;
              signal  sm      : in  axi_sm_r;
                  msg      : in  string  := "") is
  begin
    axi_single_expect(address, value, ms, sm, clk, msg);
  end procedure;

  procedure AxiWriteAndRead32(      address    : in  integer;
                      value    : in   integer;
                  signal  clk      : in  std_logic;
                  signal  ms      : out  axi_ms_r;
                  signal   sm      : in  axi_sm_r;
                      msg      : in  string  := "") is
  begin
    axi_single_write(address, value, ms, sm, clk);
    wait for 400 ns;
    wait until rising_edge(clk);
    axi_single_expect(address, value, ms, sm, clk, msg);
  end procedure;

  --------------------------------------------------------
  -- High Level Procedures
  --------------------------------------------------------
  procedure HlConfStream(        str      : in  integer;
                    bufstart  : in  integer;
                    wincnt    : in  integer;
                    winsize    : in  integer;
                signal  clk      : in  std_logic;
                signal  rqst    : out  axi_ms_r;
                signal   rsp      : in  axi_sm_r) is
    variable v : integer := 0;
  begin
    AxiWriteAndRead32(  REG_CTX_REGION+REG_CTX_BUFSTARTn+REG_CTX_Xn_STEP*str,
              bufstart, clk, rqst, rsp, "HlConfStream failed BUFSTART");
    AxiWriteAndRead32(  REG_CTX_REGION+REG_CTX_WINSIZEn+REG_CTX_Xn_STEP*str,
              winsize, clk, rqst, rsp, "HlConfStream failed WINSIZE");
    v := v + (2**SFT_SCFG_WINCNT)*(wincnt-1);
    AxiWriteAndRead32(  REG_CTX_REGION+REG_CTX_SCFGn+REG_CTX_Xn_STEP*str,
              v, clk, rqst, rsp, "HlConfStream failed SCFG");
  end procedure;

  procedure HlGetPtr(          str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer) is
  begin
    axi_single_read(REG_CTX_REGION+REG_CTX_PTRn+REG_CTX_Xn_STEP*str,
            val, ms, sm, clk);
  end procedure;

  procedure HlGetLastWin(        str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer) is
  begin
    axi_single_read(REG_CONF_REGION+REG_CONF_LASTWINn+REG_CONF_Xn_STEP*str,
            val, ms, sm, clk);
  end procedure;

  procedure HlGetCurWin(        str      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer) is
    variable v : integer;
  begin
    axi_single_read(REG_CTX_REGION+REG_CTX_SCFGn+REG_CTX_Xn_STEP*str,
            v, ms, sm, clk);
    v := IntAnd(v, MSK_SCFG_WINCUR);
    v := v / (2**SFT_SCFG_WINCUR);
    val := v;
  end procedure;

  procedure HlGetWinInfo(        str      : in  integer;
                    win      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    ByteCount    : out  integer;
                    EndOfEvent   : out  boolean;
                    PktCmplt     : out  boolean) is
    variable v : integer;
  begin
    axi_single_read(REG_WIN_REGION+REG_WIN_WINCNT+REG_WIN_STRn_STEP*str+REG_WIN_WINn_STEP*win,
            v, ms, sm, clk);
    ByteCount  := IntAnd(v, MSK_WIN_WINCNT_CNT);
    EndOfEvent := IntAnd(v, MSK_WIN_WINCNT_EOE) > 0;
    PktCmplt   := v < 0;
  end procedure;

  procedure HlClrWinCnt(        str      : in  integer;
                    win      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r) is
    variable v : integer;
  begin
    axi_single_write(REG_WIN_REGION+REG_WIN_WINCNT+REG_WIN_STRn_STEP*str+REG_WIN_WINn_STEP*win,
              0, ms, sm, clk);
  end procedure;

  procedure HlGetWinLast(        str      : in  integer;
                    win      : in  integer;
                signal  clk      : in  std_logic;
                signal  ms      : out  axi_ms_r;
                signal   sm      : in  axi_sm_r;
                    val      : out  integer) is
    variable v : integer;
  begin
    axi_single_read(REG_WIN_REGION+REG_WIN_WINLAST+REG_WIN_STRn_STEP*str+REG_WIN_WINn_STEP*win,
            val, ms, sm, clk);
  end procedure;
end;
