------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------
-- Description
------------------------------------------------------------
-- Stream 0 works in ringbuffer mode (without overwrite). It
-- produces 8-bit data (modulo counter). IRQs are located at samples
-- containing data 30, 60 and 90. IRQs are suppressed until 15 us after
-- simulation to see if IRQ enable works correctly.
-- The IRQ handler also sets the window sample counter to zero to ensure
-- more data can be recorded after the IRQ.

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.dma_pkt_sched_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.dma_pkt_sched_axi_tb_pkg.all;
  use work.psi_tb_axi_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package dma_pkt_sched_axi_1s_tb_str0_pkg is

  constant PrintStr0_c			: boolean	:= PrintDefault_c;

  -- Memory
  alias Memory0 : t_aslv8(0 to StrWinSize_c(0)*StrWindows_c(0)) is Memory(StrBufStart_c(0) to StrBufStart_c(0)+StrWinSize_c(0)*StrWindows_c(0));

  --------------------------------------------------------
  -- Persistent State
  --------------------------------------------------------
  shared variable Str0NextWin 	: integer := 0;
  shared variable Str0WinCheck 	: integer := 0;
  shared variable Str0LastTs 		: integer;
  shared variable Str0IrqOn 		: boolean := false;
  shared variable Str0Disabled	: boolean := false;

  --------------------------------------------------------
  -- Data Generation
  --------------------------------------------------------
  procedure SlotGenerateWDAQPacket(        PldSize : in  natural   := 16;
                                           FlagEOE : in  std_logic := '0';
                                    signal Clk     : in  std_logic;
                                    signal PktVld  : out std_logic;
                                    signal PktEOE  : out std_logic;
                                    signal Bytes   : out std_logic_vector(3 downto 0);
                                    signal Last    : out std_logic;
                                    signal Valid   : out std_logic;
                                    signal Ready   : in  std_logic;
                                    signal Data    : out std_logic_vector(63 downto 0));

  --------------------------------------------------------
  -- IRQ Handler
  --------------------------------------------------------
  procedure Str0Handler(	signal	clk			: in	std_logic;
              signal	rqst		: out 	axi_ms_r;
              signal	rsp			: in	axi_sm_r);

  --------------------------------------------------------
  -- Setup
  --------------------------------------------------------
  procedure Str0Setup(	signal clk			: in	std_logic;
              signal rqst			: out	axi_ms_r;
              signal rsp			: in	axi_sm_r);

  --------------------------------------------------------
  -- Update
  --------------------------------------------------------
  procedure Str0Update(	signal clk			: in	std_logic;
              signal rqst			: out	axi_ms_r;
              signal rsp			: in	axi_sm_r);


end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body dma_pkt_sched_axi_1s_tb_str0_pkg is

  --------------------------------------------------------
  -- Data Generation
  --------------------------------------------------------
  procedure SlotGenerateWDAQPacket(     PldSize : in  natural   := 16;
                                        FlagEOE : in  std_logic := '0';
                                 signal Clk     : in  std_logic;
                                 signal PktVld  : out std_logic;
                                 signal PktEOE  : out std_logic;
                                 signal Bytes   : out std_logic_vector(3 downto 0);
                                 signal Last    : out std_logic;
                                 signal Valid   : out std_logic;
                                 signal Ready   : in  std_logic;
                                 signal Data    : out std_logic_vector(63 downto 0)) is
    constant protocol_version : std_logic_vector( 7 downto 0) := x"08";
    constant board_type       : std_logic_vector( 3 downto 0) := x"2";
    constant board_revision   : std_logic_vector( 3 downto 0) := x"6";
    constant serial_number    : std_logic_vector(15 downto 0) := x"A2A3";
    constant crate_id         : std_logic_vector( 7 downto 0) := x"A4";
    constant slot_id          : std_logic_vector( 7 downto 0) := x"00";
    constant packet_number    : std_logic_vector(15 downto 0) := x"A6A7";
    constant data_type        : std_logic_vector( 7 downto 0) := x"B0";
    variable wdaq_flags       : std_logic_vector( 7 downto 0) := x"00";
    constant trigger_info     : std_logic_vector(47 downto 0) := x"B2B3B4B5B6B7";
    constant event_number     : std_logic_vector(31 downto 0) := x"C0C1C2C3";
    variable payload_length   : std_logic_vector(15 downto 0);
    constant data_chunk_offs  : std_logic_vector(15 downto 0) := x"C6C7";
    constant chnl_info        : std_logic_vector( 7 downto 0) := x"D0";
    constant wd_flags         : std_logic_vector( 7 downto 0) := x"D1";
    constant smpls_evnt_ch    : std_logic_vector(15 downto 0) := x"D2D3";
    constant sampling_freq    : std_logic_vector(31 downto 0) := x"D4D5D6D7";
    constant bits_per_smpl    : std_logic_vector( 7 downto 0) := x"E0";
    constant trig_src         : std_logic_vector( 7 downto 0) := x"E1";
    constant zero_suppr_mask  : std_logic_vector(15 downto 0) := x"E2E3";
    constant tx_enable        : std_logic_vector(31 downto 0) := x"E4E5E6E7";
    constant trigger_cell     : std_logic_vector(15 downto 0) := x"F0F1";
    constant reserved         : std_logic_vector(31 downto 0) := x"F2F3F4F5";
    constant temperature      : std_logic_vector(15 downto 0) := x"F6F7";
    constant dac_pzc          : std_logic_vector(15 downto 0) := x"A8A9";
    constant dac_ofs          : std_logic_vector(15 downto 0) := x"AAAB";
    constant dac_rofs         : std_logic_vector(15 downto 0) := x"ACAD";
    constant fe_settings      : std_logic_vector(15 downto 0) := x"AEAF";
    constant time_stamp       : std_logic_vector(63 downto 0) := x"B8B9BABBBCBDBEBF";
    variable header           : t_aslv64(0 to 7);
    variable payload_count    : natural;
    variable range_high        : integer;
    variable data_draft       : std_logic_vector(63 downto 0);
  begin
    -- Initial values
    PktVld <= '0';
    PktEOE <= '0';
    Bytes  <= (others=>'0');
    Last   <= '0';
    Valid  <= '0';
    Data   <= (others=>'0');
    -- Specific header data
    wdaq_flags(0)  := FlagEOE;
    payload_length := std_logic_vector(to_signed(PldSize, payload_length'length));
    -- build header
    header(0) := protocol_version & board_type & board_revision & serial_number & crate_id & slot_id & packet_number;
    header(1) := data_type & wdaq_flags & trigger_info;
    header(2) := event_number & payload_length & data_chunk_offs;
    header(3) := chnl_info & wd_flags & smpls_evnt_ch & sampling_freq;
    header(4) := bits_per_smpl & trig_src & zero_suppr_mask & tx_enable;
    header(5) := trigger_cell & reserved & temperature;
    header(6) := dac_pzc & dac_ofs & dac_rofs & fe_settings;
    header(7) := time_stamp;
    -- send header
    wait until rising_edge(Clk);
    PktVld <= '1';
    Bytes  <= "1000";
    Valid  <= '1';
    PktEOE <= FlagEOE;
    for words in 0 to 7 loop
      Data <= header(words);
      wait until rising_edge(Clk) and Ready = '1';
    end loop;
    -- send payload
    payload_count := PldSize;
    while payload_count > 0 loop
      if payload_count < 8 then
        range_high := payload_count-1;
      else
      range_high := 7;
      end if;
      data_draft := (others=>'0');
      for byte_idx in 7 downto range_high loop
        payload_count := payload_count - 1;
        data_draft((byte_idx+1)*8-1 downto byte_idx*8) := std_logic_vector(to_unsigned(payload_count mod 2**8, 8));
      end loop;
      Data <= data_draft;
      if payload_count = 0 then
        Bytes <= std_logic_vector(to_signed(range_high+1, Bytes'length));
        Last  <= '1';
      end if;
      wait until rising_edge(Clk) and Ready = '1';
    end loop;
    PktVld <= '0';
    PktEOE <= '0';
    Bytes  <= (others=>'X');
    Last   <= '0';
    Valid  <= '0';
    Data   <= (others=>'X');
    wait until rising_edge(Clk);
  end procedure;

  --------------------------------------------------------
  -- IRQ Handler
  --------------------------------------------------------
  procedure Str0Handler(	signal	clk			: in	std_logic;
              signal	rqst		: out 	axi_ms_r;
              signal	rsp			: in	axi_sm_r) is
    variable v : integer;
    variable curwin : integer;
    variable lastwin : integer;
    variable wincnt : integer;
    variable winstart, winend : integer;
    variable winlast : integer;
    variable addr : integer;
    variable tslo : integer;
    variable firstLoop : boolean := true;
    variable PktComplete : boolean;
  begin
    StrHandler(stream=>0, clk=>clk, rqst=>rqst, rsp=>rsp);
--    print("------------ Stream 0 Handler ------------", PrintStr0_c);
--    HlGetCurWin(0, clk, rqst, rsp, curwin);
--    print("CURWIN: " & to_string(curwin), PrintStr0_c);
--    HlGetLastWin(0, clk, rqst, rsp, lastwin);
--    print("LASTWIN: " & to_string(lastwin), PrintStr0_c);
--    print("", PrintStr0_c);
--    if Str0Disabled then
--      print("Skipped, stream disabled", PrintStr0_c);
--      print("", PrintStr0_c);
--    else
--      HlPktIsCmpltWin(0, Str0NextWin, clk, rqst, rsp, PktComplete);
--      -- lastwin = nextwin can occur if al lwindows are filled. In all cases we only interpret windows containing triggers.
--      while ((Str0NextWin /= ((lastwin+1) mod 3)) or firstLoop) and PktComplete loop
--        firstLoop := false;
--        print("*** Window " & to_string(Str0NextWin) & " / Number: " & to_string(Str0WinCheck) & " ***", PrintStr0_c);
--        HlGetWinCnt(0, Str0NextWin, clk, rqst, rsp, wincnt);
--        print("WINCNT: " & to_string(wincnt), PrintStr0_c);
--        HlClrWinCnt(0, Str0NextWin, clk, rqst, rsp);
--				HlGetWinLast(0, Str0NextWin, clk, rqst, rsp, winlast);
--				print("WINLAST: " & to_string(winlast), PrintStr0_c);
--        winstart := StrBufStart_c(0) + Str0NextWin*StrWinSize_c(0);
--        winend := winstart + StrWinSize_c(0) - 1;
--        case Str0WinCheck is
--          when 0 =>
--            -- Window done because packet complete
--            IntCompare(SlotPacketSize_c(0), wincnt, "Stream0: WINCNT wrong");
--            -- Check Values
--            addr := winstart;
--            for i in wincnt-1 downto 0 loop
--              StdlvCompareInt (i mod 2**8, Memory(addr), "Stream0: Wrong value at 0x" & to_hstring(to_unsigned(addr,32)), false);
--              addr := addr + 1;
--            end loop;
--            IntCompare(addr-1, winlast, "Stream0: WINLAST wrong");
--
----          when 1 =>
----            -- Window full because packet larger than window
----            winend := winstart + StrWinSize_c(0) - 1;
----
----          when 0 =>
----            -- Windows full because dat received for quite some time
----            IntCompare(StrWinSize_c(0), wincnt, "Stream0: WINCNT wrong");
----            -- Check Values
----            addr := winlast;
----
----            for i in 256+30+3-99 to 256+30+3 loop
----              if addr = winend then
----                addr := winstart;
----              else
----                addr := addr + 1;
----              end if;
----              StdlvCompareInt (i mod 256, Memory(addr), "Stream0: Wrong value at 0x" & to_hstring(to_unsigned(addr,32)), false);
----            end loop;
----
----          when 1 =>
----            -- Trigger following each other with 30 samples difference
----            IntCompare(30, wincnt, "Stream0: WINCNT wrong");
----            IntCompare(30*2, tslo-Str0LastTs, "Stream0: TS difference wrong");
----            -- Check Values
----            addr := winstart;
----            for i in 34 to 63 loop
----              StdlvCompareInt (i, Memory(addr), "Stream0: Wrong value", false);
----              addr := addr + 1; -- does never wrap
----            end loop;
----
----          when 2 =>
----            -- Trigger following each other with 30 samples difference
----            IntCompare(30, wincnt, "Stream0: WINCNT wrong");
----            IntCompare(30*2, tslo-Str0LastTs, "Stream0: TS difference wrong");
----            -- Check Values
----            addr := winstart;
----            for i in 64 to 93 loop
----              StdlvCompareInt (i, Memory(addr), "Stream0: Wrong value", false);
----              addr := addr + 1; -- does never wrap
----            end loop;
----          when 3 =>
----            -- Full buffer recorded after emptying first buffer
----            IntCompare(100, wincnt, "Stream0: WINCNT wrong");
----            IntCompare((256-2*30)*2, tslo-Str0LastTs, "Stream0: TS difference wrong");
----            -- Disable stream IRQ
----            AxiRead32(REG_CONF_IRQENA_ADDR, v, clk, rqst, rsp);
----            v := IntAnd(v, 16#0FE#);
----            AxiWrite32(REG_CONF_IRQENA_ADDR, v, clk, rqst, rsp);
----            AxiRead32(REG_CONF_STRENA_ADDR, v, clk, rqst, rsp);
----            v := IntAnd(v, 16#0FE#);
----            AxiWrite32(REG_CONF_STRENA_ADDR, v, clk, rqst, rsp);
----            Str0Disabled := true;
----            -- Check Values
----            addr := winlast + 1;
----            for i in 256+30+3-99 to 256+30+3 loop
----              StdlvCompareInt (i mod 256, Memory(addr), "Stream0: Wrong value", false);
----              if addr = winend then
----                addr := winstart;
----              else
----                addr := addr + 1;
----              end if;
----            end loop;
--          when others => null;
--        end case;
--        print("", PrintStr0_c);
--        Str0LastTs := tslo;
--        Str0NextWin := (Str0NextWin + 1) mod 3;
--        Str0WinCheck := Str0WinCheck + 1;
--      end loop;
--    end if;
  end procedure;

  --------------------------------------------------------
  -- Setup
  --------------------------------------------------------
  procedure Str0Setup(	signal clk			: in	std_logic;
              signal rqst			: out	axi_ms_r;
              signal rsp			: in	axi_sm_r) is
  begin
    HlConfStream(	str => 0, bufstart => StrBufStart_c(0), wincnt => StrWindows_c(0), winsize => StrWinSize_c(0),
            clk => clk, rqst => rqst, rsp => rsp);
  end procedure;

  --------------------------------------------------------
  -- Update
  --------------------------------------------------------
  procedure Str0Update(	signal clk			: in	std_logic;
              signal rqst			: out	axi_ms_r;
              signal rsp			: in	axi_sm_r) is
  begin
  end;

end;
