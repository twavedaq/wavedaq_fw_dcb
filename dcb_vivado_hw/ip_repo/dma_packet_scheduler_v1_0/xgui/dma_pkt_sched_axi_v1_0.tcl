# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set General_Configuration [ipgui::add_page $IPINST -name "General Configuration"]
  ipgui::add_param $IPINST -name "Streams_g" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "MaxWindows_g" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "MinBurstSize_g" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "MaxBurstSize_g" -parent ${General_Configuration}

  #Adding Page
  set AXI_Master [ipgui::add_page $IPINST -name "AXI Master"]
  ipgui::add_param $IPINST -name "AxiDataWidth_g" -parent ${AXI_Master} -widget comboBox
  ipgui::add_param $IPINST -name "AxiMaxBurstBeats_g" -parent ${AXI_Master}
  ipgui::add_param $IPINST -name "AxiMaxOpenTrasactions_g" -parent ${AXI_Master}
  ipgui::add_param $IPINST -name "AxiFifoDepth_g" -parent ${AXI_Master}


}

proc update_PARAM_VALUE.AxiDataWidth_g { PARAM_VALUE.AxiDataWidth_g } {
	# Procedure called to update AxiDataWidth_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AxiDataWidth_g { PARAM_VALUE.AxiDataWidth_g } {
	# Procedure called to validate AxiDataWidth_g
	return true
}

proc update_PARAM_VALUE.AxiFifoDepth_g { PARAM_VALUE.AxiFifoDepth_g } {
	# Procedure called to update AxiFifoDepth_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AxiFifoDepth_g { PARAM_VALUE.AxiFifoDepth_g } {
	# Procedure called to validate AxiFifoDepth_g
	return true
}

proc update_PARAM_VALUE.AxiMaxBurstBeats_g { PARAM_VALUE.AxiMaxBurstBeats_g } {
	# Procedure called to update AxiMaxBurstBeats_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AxiMaxBurstBeats_g { PARAM_VALUE.AxiMaxBurstBeats_g } {
	# Procedure called to validate AxiMaxBurstBeats_g
	return true
}

proc update_PARAM_VALUE.AxiMaxOpenTrasactions_g { PARAM_VALUE.AxiMaxOpenTrasactions_g } {
	# Procedure called to update AxiMaxOpenTrasactions_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.AxiMaxOpenTrasactions_g { PARAM_VALUE.AxiMaxOpenTrasactions_g } {
	# Procedure called to validate AxiMaxOpenTrasactions_g
	return true
}

proc update_PARAM_VALUE.C_S_Axi_ID_WIDTH { PARAM_VALUE.C_S_Axi_ID_WIDTH } {
	# Procedure called to update C_S_Axi_ID_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_Axi_ID_WIDTH { PARAM_VALUE.C_S_Axi_ID_WIDTH } {
	# Procedure called to validate C_S_Axi_ID_WIDTH
	return true
}

proc update_PARAM_VALUE.MaxBurstSize_g { PARAM_VALUE.MaxBurstSize_g } {
	# Procedure called to update MaxBurstSize_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MaxBurstSize_g { PARAM_VALUE.MaxBurstSize_g } {
	# Procedure called to validate MaxBurstSize_g
	return true
}

proc update_PARAM_VALUE.MaxWindows_g { PARAM_VALUE.MaxWindows_g } {
	# Procedure called to update MaxWindows_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MaxWindows_g { PARAM_VALUE.MaxWindows_g } {
	# Procedure called to validate MaxWindows_g
	return true
}

proc update_PARAM_VALUE.MinBurstSize_g { PARAM_VALUE.MinBurstSize_g } {
	# Procedure called to update MinBurstSize_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MinBurstSize_g { PARAM_VALUE.MinBurstSize_g } {
	# Procedure called to validate MinBurstSize_g
	return true
}

proc update_PARAM_VALUE.Streams_g { PARAM_VALUE.Streams_g } {
	# Procedure called to update Streams_g when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Streams_g { PARAM_VALUE.Streams_g } {
	# Procedure called to validate Streams_g
	return true
}


proc update_MODELPARAM_VALUE.Streams_g { MODELPARAM_VALUE.Streams_g PARAM_VALUE.Streams_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Streams_g}] ${MODELPARAM_VALUE.Streams_g}
}

proc update_MODELPARAM_VALUE.MaxWindows_g { MODELPARAM_VALUE.MaxWindows_g PARAM_VALUE.MaxWindows_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MaxWindows_g}] ${MODELPARAM_VALUE.MaxWindows_g}
}

proc update_MODELPARAM_VALUE.MinBurstSize_g { MODELPARAM_VALUE.MinBurstSize_g PARAM_VALUE.MinBurstSize_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MinBurstSize_g}] ${MODELPARAM_VALUE.MinBurstSize_g}
}

proc update_MODELPARAM_VALUE.MaxBurstSize_g { MODELPARAM_VALUE.MaxBurstSize_g PARAM_VALUE.MaxBurstSize_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MaxBurstSize_g}] ${MODELPARAM_VALUE.MaxBurstSize_g}
}

proc update_MODELPARAM_VALUE.AxiDataWidth_g { MODELPARAM_VALUE.AxiDataWidth_g PARAM_VALUE.AxiDataWidth_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AxiDataWidth_g}] ${MODELPARAM_VALUE.AxiDataWidth_g}
}

proc update_MODELPARAM_VALUE.AxiMaxBurstBeats_g { MODELPARAM_VALUE.AxiMaxBurstBeats_g PARAM_VALUE.AxiMaxBurstBeats_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AxiMaxBurstBeats_g}] ${MODELPARAM_VALUE.AxiMaxBurstBeats_g}
}

proc update_MODELPARAM_VALUE.AxiMaxOpenTrasactions_g { MODELPARAM_VALUE.AxiMaxOpenTrasactions_g PARAM_VALUE.AxiMaxOpenTrasactions_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AxiMaxOpenTrasactions_g}] ${MODELPARAM_VALUE.AxiMaxOpenTrasactions_g}
}

proc update_MODELPARAM_VALUE.AxiFifoDepth_g { MODELPARAM_VALUE.AxiFifoDepth_g PARAM_VALUE.AxiFifoDepth_g } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.AxiFifoDepth_g}] ${MODELPARAM_VALUE.AxiFifoDepth_g}
}

proc update_MODELPARAM_VALUE.C_S_Axi_ID_WIDTH { MODELPARAM_VALUE.C_S_Axi_ID_WIDTH PARAM_VALUE.C_S_Axi_ID_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_Axi_ID_WIDTH}] ${MODELPARAM_VALUE.C_S_Axi_ID_WIDTH}
}

