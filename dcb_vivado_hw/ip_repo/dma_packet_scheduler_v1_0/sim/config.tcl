##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

#Constants
set LibPath "../../../../library/VHDL"

#Import psi::sim
namespace import psi::sim::*

#Set library
add_library dma_pkt_sched

#suppress messages
compile_suppress 135,1236,1073,1246
run_suppress 8684,3479,3813,8009,3812

# Library
add_sources $LibPath {
	psi_tb/hdl/psi_tb_txt_util.vhd \
	psi_tb/hdl/psi_tb_compare_pkg.vhd \
	psi_tb/hdl/psi_tb_activity_pkg.vhd \
	psi_common/hdl/psi_common_array_pkg.vhd \
	psi_common/hdl/psi_common_math_pkg.vhd \
	psi_tb/hdl/psi_tb_axi_pkg.vhd \
	psi_common/hdl/psi_common_logic_pkg.vhd \
	psi_common/hdl/psi_common_sdp_ram.vhd \
	psi_common/hdl/psi_common_pulse_cc.vhd \
	psi_common/hdl/psi_common_bit_cc.vhd \
	psi_common/hdl/psi_common_simple_cc.vhd \
	psi_common/hdl/psi_common_status_cc.vhd \
	psi_common/hdl/psi_common_async_fifo.vhd \
	psi_common/hdl/psi_common_arb_priority.vhd \
	psi_common/hdl/psi_common_arb_round_robin.vhd \
	psi_common/hdl/psi_common_sync_fifo.vhd \
	psi_common/hdl/psi_common_tdp_ram.vhd \
	psi_common/hdl/psi_common_axi_master_simple.vhd \
	psi_common/hdl/psi_common_wconv_n2xn.vhd \
	psi_common/hdl/psi_common_axi_master_full.vhd \
	psi_common/hdl/psi_common_pl_stage.vhd \
	psi_common/hdl/psi_common_axi_slave_ipif.vhd \
} -tag lib

# project sources
add_sources "../hdl" {
	dma_pkt_sched_pkg.vhd \
	dma_pkt_sched_sm.vhd \
	dma_pkt_sched_dma.vhd \
	dma_pkt_sched_axi_if.vhd \
	dma_pkt_sched_reg_axi.vhd \
	dma_pkt_sched_axi.vhd \
} -tag src

# testbenches
add_sources "../tb" {
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_pkg.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_single_window.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_single_simple.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_multi_window.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_enable.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_irq.vhd \
	dma_pkt_sched_sm/dma_pkt_sched_sm_tb.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_pkg.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_unaligned.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_no_data_read.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_input_empty.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_empty_timeout.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_data_full.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_cmd_full.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_aligned.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb_case_errors.vhd \
	dma_pkt_sched_dma/dma_pkt_sched_dma_tb.vhd \
	dma_pkt_sched_axi/dma_pkt_sched_axi_tb_pkg.vhd \
	dma_pkt_sched_axi/dma_pkt_sched_axi_tb.vhd \
	dma_pkt_sched_axi_1s/dma_pkt_sched_axi_1s_tb_str0_pkg.vhd \
	dma_pkt_sched_axi_1s/dma_pkt_sched_axi_1s_tb.vhd \
} -tag tb
#	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_priorities.vhd \
#	dma_pkt_sched_sm/dma_pkt_sched_sm_tb_case_timestamp.vhd \

#TB Runs
create_tb_run "dma_pkt_sched_sm_tb"
add_tb_run

create_tb_run "dma_pkt_sched_dma_tb"
add_tb_run

create_tb_run "dma_pkt_sched_axi_tb"
add_tb_run

create_tb_run "dma_pkt_sched_axi_1s_tb"
add_tb_run
