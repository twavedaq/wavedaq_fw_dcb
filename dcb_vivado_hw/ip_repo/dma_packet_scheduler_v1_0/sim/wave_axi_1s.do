onerror {resume}
quietly virtual signal -install /dma_pkt_sched_axi_1s_tb/i_dut { (context /dma_pkt_sched_axi_1s_tb/i_dut )(S_Axi_Aclk & S_Axi_Aresetn & S_Axi_ArId &S_Axi_ArAddr &S_Axi_Arlen &S_Axi_ArSize &S_Axi_ArBurst & S_Axi_ArLock &S_Axi_ArCache &S_Axi_ArProt & S_Axi_ArValid & S_Axi_ArReady & S_Axi_RId &S_Axi_RData &S_Axi_RResp & S_Axi_RLast & S_Axi_RValid & S_Axi_RReady & S_Axi_AwId &S_Axi_AwAddr &S_Axi_AwLen &S_Axi_AwSize &S_Axi_AwBurst & S_Axi_AwLock &S_Axi_AwCache &S_Axi_AwProt & S_Axi_AwValid & S_Axi_AwReady &S_Axi_WData &S_Axi_WStrb & S_Axi_WLast & S_Axi_WValid & S_Axi_WReady & S_Axi_BId &S_Axi_BResp & S_Axi_BValid & S_Axi_BReady )} S_Axi
quietly WaveActivateNextPane {} 0
add wave -noupdate /dma_pkt_sched_axi_1s_tb/TbRunning
add wave -noupdate /dma_pkt_sched_axi_1s_tb/PrintIrq_c
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot0_Data
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot_Vld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot_Rdy
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot_Last
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot0_Bytes
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot_PktValid
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Slot_EOE
add wave -noupdate /dma_pkt_sched_axi_1s_tb/M_Axi_Aclk
add wave -noupdate /dma_pkt_sched_axi_1s_tb/S_Axi_Aclk
add wave -noupdate /dma_pkt_sched_axi_1s_tb/M_Axi_Aresetn
add wave -noupdate /dma_pkt_sched_axi_1s_tb/S_Axi_Aresetn
add wave -noupdate /dma_pkt_sched_axi_1s_tb/Irq
add wave -noupdate /dma_pkt_sched_axi_1s_tb/axi_ms
add wave -noupdate /dma_pkt_sched_axi_1s_tb/axi_sm
add wave -noupdate /dma_pkt_sched_axi_1s_tb/reg_axi_ms
add wave -noupdate /dma_pkt_sched_axi_1s_tb/reg_axi_sm
add wave -noupdate -divider <NULL>
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TData
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TValid
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TReady
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TLast
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_Bytes
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_PktValid
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Slot_EOE
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Irq
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Aclk
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Aresetn
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArId
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArAddr
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Arlen
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArSize
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArBurst
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArLock
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArCache
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArProt
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArValid
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArReady
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RId
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RData
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RResp
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RLast
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RValid
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RReady
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwId
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwAddr
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwLen
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwSize
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwBurst
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwLock
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwCache
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwProt
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwValid
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwReady
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WData
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WStrb
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WLast
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WValid
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WReady
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BId
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BResp
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BValid
add wave -noupdate -group S_Axi /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BReady
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Aclk
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Aresetn
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwAddr
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwLen
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwSize
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwBurst
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwLock
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwCache
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwProt
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwValid
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwReady
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WData
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WStrb
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WLast
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WValid
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WReady
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BResp
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BValid
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BReady
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArAddr
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArLen
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArSize
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArBurst
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArLock
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArCache
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArProt
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArValid
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArReady
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RData
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RResp
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RLast
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RValid
add wave -noupdate -group M_Axi /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RReady
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/InpSm_EOE
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/InpSm_PktValid
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/SmDma_Cmd
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/SmDma_CmdVld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_Resp
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_RespVld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_RespRdy
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_PktCmplt
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Vld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Rdy
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Data
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdAddr
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdSize
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdVld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdRdy
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatData
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatVld
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatRdy
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/MemSm_Done
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Cfg_StrEna
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Cfg_GlbEna
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Stat_StrIrq
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/Stat_StrLastWin
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/CtxStr_Cmd
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/CtxStr_Resp
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/CtxWin_Cmd
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/CtxWin_Resp
add wave -noupdate /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Areset
add wave -noupdate -divider <NULL>
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Clk
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Rst
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_Cmd
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_Cmd_Vld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_Resp
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_Resp_Vld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_Resp_Rdy
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DaqSm_PktCmplt
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Inp_Vld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Inp_Rdy
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Inp_Data
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_CmdAddr
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_CmdSize
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_CmdVld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_CmdRdy
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_DatData
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_DatVld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/Mem_DatRdy
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/CmdFifo_Level_Dbg
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/CmdFifo_InData
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/CmdFifo_OutData
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/CmdFifo_Cmd
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/CmdFifo_Vld
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/RspFifo_Level_Dbg
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/RspFifo_InData
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/RspFifo_OutData
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DatFifo_Level_Dbg
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/DatFifo_AlmFull
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/r
add wave -noupdate -group DMA /dma_pkt_sched_axi_1s_tb/i_dut/i_dma/r_next
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Clk
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Rst_n
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Cmd_Addr
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Cmd_Size
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Cmd_Vld
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Cmd_Rdy
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Dat_Data
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Dat_Vld
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Dat_Rdy
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Done
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwAddr
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwLen
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwSize
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwBurst
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwLock
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwCache
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwProt
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwValid
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_AwReady
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_WData
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_WStrb
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_WLast
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_WValid
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_WReady
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_BResp
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_BValid
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_BReady
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArAddr
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArLen
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArSize
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArBurst
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArLock
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArCache
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArProt
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArValid
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_ArReady
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_RData
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_RResp
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_RLast
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_RValid
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/M_Axi_RReady
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/Rst
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/InfoFifoIn
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/InfoFifoOut
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/WrCmdFifo_Vld
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/WrCmdFifo_Rdy
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/WrCmdFifo_Addr
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/WrCmdFifo_Size
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/DoneI
add wave -noupdate -group MemIF /dma_pkt_sched_axi_1s_tb/i_dut/i_memif/ErrorI
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Clk
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Rst
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/GlbEna
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/StrEna
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/StrIrq
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/StrLastWin
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Inp_PktValid
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Inp_EOE
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Dma_Cmd
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Dma_Cmd_Vld
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Dma_Resp
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Dma_Resp_Vld
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Dma_Resp_Rdy
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/TfDone
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/CtxStr_Cmd
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/CtxStr_Resp
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/CtxWin_Cmd
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/CtxWin_Resp
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/Grant
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/GrantVld
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/GrantRdy
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoAlmFull
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoEmpty
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoGenIrq
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoStream
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqLastWinNr
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoIn
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/IrqFifoOut
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/r
add wave -noupdate -expand -group FSM /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/r_next
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/GrantRdy
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/GrantVld
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/Grant
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/GrantMasked
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/GrantUnmasked
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/Request
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/RequestMasked
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/r
add wave -noupdate -expand -group {Slot Arbiter} /dma_pkt_sched_axi_1s_tb/i_dut/i_statemachine/i_rrarb/r_next
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/AxiDataWidth_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/AxiFifoDepth_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/AxiMaxBurstBeats_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/AxiMaxOpenTrasactions_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/AxiSlaveIdWidth_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Cfg_GlbEna
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Cfg_StrEna
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/CtxStr_Cmd
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/CtxStr_Resp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/CtxWin_Cmd
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/CtxWin_Resp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdAddr
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdRdy
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdSize
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_CmdVld
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatRdy
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaMem_DatVld
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_PktCmplt
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_Resp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_RespRdy
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/DmaSm_RespVld
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Data
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Rdy
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/InpDma_Vld
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/InpSm_EOE
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/InpSm_PktValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Irq
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Aclk
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArAddr
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArBurst
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArCache
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Areset
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_Aresetn
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArLen
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArLock
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArProt
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArSize
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_ArValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwAddr
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwBurst
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwCache
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwLen
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwLock
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwProt
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwSize
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_AwValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BResp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_BValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RLast
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RResp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_RValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WLast
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WStrb
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/M_Axi_WValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/MaxBurstSize_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/MaxWindows_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/MemSm_Done
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/MinBurstSize_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Aclk
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArAddr
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArBurst
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArCache
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Aresetn
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArId
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_Arlen
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArLock
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArProt
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArSize
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_ArValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwAddr
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwBurst
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwCache
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwId
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwLen
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwLock
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwProt
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwSize
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_AwValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BId
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BResp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_BValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RId
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RLast
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RResp
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_RValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WLast
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WStrb
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/S_Axi_WValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_Bytes
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TData
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_EOE
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TLast
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_PktValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TReady
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Slot_TValid
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/SmDma_Cmd
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/SmDma_CmdVld
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Stat_StrIrq
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Stat_StrLastWin
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/Streams_g
add wave -noupdate -group g_input_mapping /dma_pkt_sched_axi_1s_tb/i_dut/StreamWidth_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/A_Axi_Areset
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AccAddr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AccAddrOffs
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AccRdData
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AccWr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AccWrData
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AddrCtxStr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AddrCtxWin
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/AxiSlaveIdWidth_g
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/ClkMem
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_AddrB
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_Cmd
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_Rdval
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_Resp
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_WeHi
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStr_WeLo
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxStrAddrHigh_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_AddrB
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_Cmd
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_Rdval
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_Resp
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_WeHi
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWin_WeLo
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/CtxWinAddrHigh_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/DepthCtxStr_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/DepthCtxWin_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/DwWrite_c
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/GlbEna
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/IrqOut
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/MaxWindows_g
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/r
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/r_next
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/RegRdVal
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/RegWr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/RegWrVal
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/RstMem
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_Aclk
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArAddr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArBurst
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArCache
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_Aresetn
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArId
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_Arlen
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArLock
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArProt
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArReady
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArSize
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_ArValid
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwAddr
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwBurst
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwCache
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwId
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwLen
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwLock
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwProt
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwReady
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwSize
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_AwValid
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_BId
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_BReady
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_BResp
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_BValid
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RData
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RId
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RLast
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RReady
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RResp
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_RValid
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_WData
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_WLast
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_WReady
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_WStrb
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/S_Axi_WValid
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/Streams_g
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/StrEna
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/StrIrq
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/StrIrq_Sync
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/StrLastWin
add wave -noupdate -group i_reg /dma_pkt_sched_axi_1s_tb/i_dut/i_reg/StrLastWin_Sync
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {580000 ps} 0} {{Cursor 2} {836341 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {168 us}
