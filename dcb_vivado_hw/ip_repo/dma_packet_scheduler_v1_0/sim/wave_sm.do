onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /dma_pkt_sched_sm_tb/AllProcessesDone_c
add wave -noupdate /dma_pkt_sched_sm_tb/ProcessDone
add wave -noupdate /dma_pkt_sched_sm_tb/TbRunning
add wave -noupdate /dma_pkt_sched_sm_tb/Clk
add wave -noupdate /dma_pkt_sched_sm_tb/Rst
add wave -noupdate /dma_pkt_sched_sm_tb/GlbEna
add wave -noupdate /dma_pkt_sched_sm_tb/MaxBurstSize_g
add wave -noupdate /dma_pkt_sched_sm_tb/MinBurstSize_g
add wave -noupdate /dma_pkt_sched_sm_tb/NextCase
add wave -noupdate /dma_pkt_sched_sm_tb/Streams_g
add wave -noupdate /dma_pkt_sched_sm_tb/StrEna
add wave -noupdate /dma_pkt_sched_sm_tb/StrIrq
add wave -noupdate /dma_pkt_sched_sm_tb/StrLastWin
add wave -noupdate /dma_pkt_sched_sm_tb/TbProcNr_control_c
add wave -noupdate /dma_pkt_sched_sm_tb/TbProcNr_ctx_c
add wave -noupdate /dma_pkt_sched_sm_tb/TbProcNr_dma_cmd_c
add wave -noupdate /dma_pkt_sched_sm_tb/TbProcNr_dma_resp_c
add wave -noupdate /dma_pkt_sched_sm_tb/TfDone
add wave -noupdate /dma_pkt_sched_sm_tb/Windows_g
add wave -noupdate /dma_pkt_sched_sm_tb/Inp_EOE
add wave -noupdate /dma_pkt_sched_sm_tb/Inp_PktValid
add wave -noupdate /dma_pkt_sched_sm_tb/Dma_Cmd
add wave -noupdate /dma_pkt_sched_sm_tb/Dma_Cmd_Vld
add wave -noupdate /dma_pkt_sched_sm_tb/Dma_Resp
add wave -noupdate /dma_pkt_sched_sm_tb/Dma_Resp_Rdy
add wave -noupdate /dma_pkt_sched_sm_tb/Dma_Resp_Vld
add wave -noupdate /dma_pkt_sched_sm_tb/CtxStr_Cmd
add wave -noupdate /dma_pkt_sched_sm_tb/CtxStr_Resp
add wave -noupdate /dma_pkt_sched_sm_tb/CtxWin_Cmd
add wave -noupdate /dma_pkt_sched_sm_tb/CtxWin_Resp
add wave -noupdate -divider <NULL>
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Rst
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Clk
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/r.State
add wave -noupdate -expand -subitemconfig {/dma_pkt_sched_sm_tb/i_dut/r.Dma_Cmd -expand} /dma_pkt_sched_sm_tb/i_dut/r
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/r_next
add wave -noupdate -expand /dma_pkt_sched_sm_tb/i_dut/CtxStr_Cmd
add wave -noupdate -expand /dma_pkt_sched_sm_tb/i_dut/CtxStr_Resp
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/CtxWin_Cmd
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/CtxWin_Resp
add wave -noupdate -expand /dma_pkt_sched_sm_tb/i_dut/Dma_Cmd
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Dma_Cmd_Vld
add wave -noupdate -expand /dma_pkt_sched_sm_tb/i_dut/Dma_Resp
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Dma_Resp_Rdy
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Dma_Resp_Vld
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/EventMode
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/GlbEna
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Grant
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/GrantVld
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/GrantRdy
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Inp_EOE
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Inp_PktValid
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoAlmFull
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoEmpty
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoGenIrq
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoIn
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoOut
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqFifoStream
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/IrqLastWinNr
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/MaxBurstSize_g
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/MinBurstSize_g
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/StreamBits_c
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Streams_g
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/StrEna
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/StrIrq
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/StrLastWin
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/TfDone
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/Windows_g
add wave -noupdate -divider <NULL>
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/i_rrarb/Request
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/i_rrarb/Grant
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/GrantRdy
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/GrantVld
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/i_rrarb/GrantMasked
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/i_rrarb/GrantUnmasked
add wave -noupdate /dma_pkt_sched_sm_tb/i_dut/i_rrarb/RequestMasked
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {17219697 ps} 0} {{Cursor 2} {54402112 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {63 us}
