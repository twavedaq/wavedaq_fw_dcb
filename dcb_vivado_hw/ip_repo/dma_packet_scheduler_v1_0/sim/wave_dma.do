onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /dma_pkt_sched_dma_tb/AllProcessesDone_c
add wave -noupdate /dma_pkt_sched_dma_tb/Streams_g
add wave -noupdate /dma_pkt_sched_dma_tb/Generics_c
add wave -noupdate /dma_pkt_sched_dma_tb/TbProcNr_control_c
add wave -noupdate /dma_pkt_sched_dma_tb/TbProcNr_input_c
add wave -noupdate /dma_pkt_sched_dma_tb/TbProcNr_mem_cmd_c
add wave -noupdate /dma_pkt_sched_dma_tb/TbProcNr_mem_dat_c
add wave -noupdate /dma_pkt_sched_dma_tb/ProcessDone
add wave -noupdate /dma_pkt_sched_dma_tb/TbRunning
add wave -noupdate /dma_pkt_sched_dma_tb/Rst
add wave -noupdate /dma_pkt_sched_dma_tb/Clk
add wave -noupdate -expand /dma_pkt_sched_dma_tb/DaqSm_Cmd
add wave -noupdate /dma_pkt_sched_dma_tb/DaqSm_Cmd_Vld
add wave -noupdate -expand /dma_pkt_sched_dma_tb/DaqSm_Resp
add wave -noupdate /dma_pkt_sched_dma_tb/DaqSm_Resp_Rdy
add wave -noupdate /dma_pkt_sched_dma_tb/DaqSm_Resp_Vld
add wave -noupdate -expand /dma_pkt_sched_dma_tb/Inp_Data
add wave -noupdate /dma_pkt_sched_dma_tb/Inp_Vld
add wave -noupdate /dma_pkt_sched_dma_tb/Inp_Rdy
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_CmdAddr
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_CmdSize
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_CmdVld
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_CmdRdy
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_DatData
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_DatVld
add wave -noupdate /dma_pkt_sched_dma_tb/Mem_DatRdy
add wave -noupdate /dma_pkt_sched_dma_tb/NextCase
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/AlmEmpty
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/AlmEmptyLevel_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/AlmEmptyOn_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/AlmFullLevel_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/AlmFullOn_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/Depth_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/Empty
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/Full
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/InData
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/InLevel
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/InRdy
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/OutData
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/OutLevel
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/r
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/r_next
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/RamBehavior_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/RamRdAddr
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/RamStyle_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/RamWr
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/RdyRstState_g
add wave -noupdate -expand -group Data_FIFO /dma_pkt_sched_dma_tb/i_dut/i_fifodata/Width_g
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1530000 ps} 0} {{Cursor 2} {1433203 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {10500 ns}
