------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.serdes_pkt_rcvr_tb_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_tb_case_valid_data is

  procedure control (
    signal DivClk_i : in std_logic;
    signal Rst_i : inout std_logic;
    signal Resync_i : inout std_logic;
    signal Rst_Errors_i : inout std_logic;
    signal SyncStatus_o : in std_logic
  );

  procedure input (
    signal BitClk_i : in std_logic;
    signal DivClk_i : in std_logic;
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic;
    signal ReadyToRcv_o_p : in std_logic;
    signal ReadyToRcv_o_n : in std_logic);

  procedure output (
    signal DivClk_i : in std_logic;
    signal Axis_Data_o : in Output_Data_t;
    signal Axis_Vld_o : in std_logic;
    signal Axis_Rdy_i : inout std_logic;
    signal PktParam_o : in Pkt_Param_t);

  procedure status (
    signal DivClk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal SyncStatus_o : in std_logic;
    signal DelayOk_o : std_logic := '0';
    signal BitslipOk_o : std_logic := '0';
    signal LinkIdle_o : in std_logic);

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_tb_case_valid_data is
  procedure control (
    signal DivClk_i : in std_logic;
    signal Rst_i : inout std_logic;
    signal Resync_i : inout std_logic;
    signal Rst_Errors_i : inout std_logic;
    signal SyncStatus_o : in std_logic
  ) is
  begin
    -- Frame Reception all cases
    for TestCase in 0 to 3 loop
      WaitForCase(TestCase, DivClk_i);
      ProcDone(ProcControl) := '0';
      ApplyReset(DivClk_i, Rst_i);
      wait until SyncStatus_o = '1';
      wait until rising_edge(DivClk_i);
      ApplyReset(DivClk_i, Rst_Errors_i);
      ProcDone(ProcControl) := '1';
    end loop;
  end procedure;

  procedure input (
    signal BitClk_i : in std_logic;
    signal DivClk_i : in std_logic;
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic;
    signal ReadyToRcv_o_p : in std_logic;
    signal ReadyToRcv_o_n : in std_logic) is
    constant PayloadLen_aligned  : integer := 32;
    constant PayloadLen_odd_7of8 : integer := 31;
    constant PayloadLen_odd_9of8 : integer := 33;
    constant PayloadLen_odd_4of8 : integer := 36;
    constant FrameSize_aligned   : integer := 2+2+24+PayloadLen_aligned+4; -- FrameLen+SOF+header+payload+CRC
    constant FrameSize_odd_7of8  : integer := 2+2+24+PayloadLen_odd_7of8+4; -- FrameLen+SOF+header+payload+CRC
    constant FrameSize_odd_9of8  : integer := 2+2+24+PayloadLen_odd_9of8+4; -- FrameLen+SOF+header+payload+CRC
    constant FrameSize_odd_4of8  : integer := 2+2+24+PayloadLen_odd_4of8+4; -- FrameLen+SOF+header+payload+CRC
    variable FrameData_aligned   : t_aslv8(0 to FrameSize_aligned-1);
    variable FrameData_odd_7of8  : t_aslv8(0 to FrameSize_odd_7of8-1);
    variable FrameData_odd_9of8  : t_aslv8(0 to FrameSize_odd_9of8-1);
    variable FrameData_odd_4of8  : t_aslv8(0 to FrameSize_odd_4of8-1);
  begin
    print(">>> -- Frame Reception --");
    InitTestCase;
    print(">>> -- Frame aligned --");
    ProcDone(ProcInput) := '0';
    TestCase := 0;
    wait for 400 ps; -- time offset
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DataGen_Frame(PayloadLen_aligned, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData_aligned);
    DesDataGen_Frame (Data=>FrameData_aligned, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    print("<<< -- Frame aligned done --");
    ControlWaitCompl(DivClk_i);

    print(">>> -- Frame unaligned 7/8 --");
    ProcDone(ProcInput) := '0';
    TestCase := 1;
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DataGen_Frame(PayloadLen_odd_7of8, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData_odd_7of8);
    DesDataGen_Frame (Data=>FrameData_odd_7of8, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    print("<<< -- Frame unaligned 7/8 done --");
    ControlWaitCompl(DivClk_i);

    print(">>> -- Frame unaligned 9/8 --");
    ProcDone(ProcInput) := '0';
    TestCase := 2;
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DataGen_Frame(PayloadLen_odd_9of8, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData_odd_9of8);
    DesDataGen_Frame (Data=>FrameData_odd_9of8, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    print("<<< -- Frame unaligned 9/8 done --");
    ControlWaitCompl(DivClk_i);

    print(">>> -- Frame unaligned 4/8 --");
    ProcDone(ProcInput) := '0';
    TestCase := 3;
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DataGen_Frame(PayloadLen_odd_4of8, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData_odd_4of8);
    DesDataGen_Frame (Data=>FrameData_odd_4of8, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    print("<<< -- Frame unaligned 4/8 done --");
    ControlWaitCompl(DivClk_i);

    print(">>> Frame reception done <<<");
    print("");

    FinishTestCase;
  end procedure;

  procedure output (
    signal DivClk_i : in std_logic;
    signal Axis_Data_o : in Output_Data_t;
    signal Axis_Vld_o : in std_logic;
    signal Axis_Rdy_i : inout std_logic;
    signal PktParam_o : in Pkt_Param_t) is
    constant PayloadLen_aligned  : integer := 32;
    constant PayloadLen_odd_7of8 : integer := 31;
    constant PayloadLen_odd_9of8 : integer := 33;
    constant PayloadLen_odd_4of8 : integer := 36;
    variable FrameData_aligned   : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen_aligned-1);
    variable FrameData_odd_7of8  : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen_odd_7of8-1);
    variable FrameData_odd_9of8  : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen_odd_9of8-1);
    variable FrameData_odd_4of8  : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen_odd_4of8-1);
  begin
    -- Frame Reception aligned
    WaitForCase(0,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    OutDataGen_Packet(PayloadLen_aligned, 16#00C0FFEE#, true, true, false, false, false, FrameData_aligned);
    OutData_Expect(OutData=>FrameData_aligned, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Reception (aligned) case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Frame Reception unaligned 7/8
    WaitForCase(1,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    OutDataGen_Packet(PayloadLen_odd_7of8, 16#00C0FFEE#, true, true, false, false, false, FrameData_odd_7of8);
    OutData_Expect(OutData=>FrameData_odd_7of8, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Reception (unalined 7/8) case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Frame Reception unaligned 9/8
    WaitForCase(2,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    OutDataGen_Packet(PayloadLen_odd_9of8, 16#00C0FFEE#, true, true, false, false, false, FrameData_odd_9of8);
    OutData_Expect(OutData=>FrameData_odd_9of8, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Reception (unalined 9/8) case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Frame Reception unaligned 4/8
    WaitForCase(3,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    OutDataGen_Packet(PayloadLen_odd_4of8, 16#00C0FFEE#, true, true, false, false, false, FrameData_odd_4of8);
    OutData_Expect(OutData=>FrameData_odd_4of8, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Reception (unalined 4/8) case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

  end procedure;

  procedure status (
    signal DivClk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal SyncStatus_o : in std_logic;
    signal DelayOk_o : std_logic := '0';
    signal BitslipOk_o : std_logic := '0';
    signal LinkIdle_o : in std_logic) is
  begin
    -- Frame Reception all cases
    WaitForCase(0,  DivClk_i);
    ProcDone(ProcStatus) := '0';
    for i in 1 to 10 loop
      wait until rising_edge(DivClk_i);
    end loop;
    Errors_Expect (SyncErrors=>0, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Reception case " & integer'image(TestCase));
    wait until SyncStatus_o = '1';
    for i in 1 to 10 loop
      wait until rising_edge(DivClk_i);
    end loop;
    Errors_Expect (SyncErrors=>0, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Reception case " & integer'image(TestCase));
    ProcDone(ProcStatus) := '1';
    -- do not check initial status for rest of cases
    for TestCase in 1 to 3 loop
      WaitForCase(TestCase,  DivClk_i);
      ProcDone(ProcStatus) := '0';
      wait until SyncStatus_o = '1';
      for i in 1 to 10 loop
        wait until rising_edge(DivClk_i);
      end loop;
      Errors_Expect (SyncErrors=>0, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Reception case " & integer'image(TestCase));
      ProcDone(ProcStatus) := '1';
    end loop;
  end procedure;

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    constant Generics_c : Generics_t
  ) is
  begin
    -- Frame Reception all cases
    for TestCase in 0 to 3 loop
      WaitForCase(TestCase, Clk_i);
      ProcDone(ProcPktCount) := '0';
      ApplyReset(Clk_i, Rst_RcvPktCount_i);
      PktCount_Expect(0, RcvPktCount_o, "Frame Reception (pre) case " & integer'image(TestCase));
      while ProcDone(ProcInput) = '0' loop
        wait until rising_edge(Clk_i);
      end loop;
      PktCount_Expect(1, RcvPktCount_o, "Frame Reception (post) case " & integer'image(TestCase));
      ProcDone(ProcPktCount) := '1';
    end loop;

  end procedure;

end;
