------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.serdes_pkt_rcvr_tb_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_tb_case_errors is

  procedure control (
    signal DivClk_i : in std_logic;
    signal Rst_i : inout std_logic;
    signal Resync_i : inout std_logic;
    signal Rst_Errors_i : inout std_logic;
    signal SyncStatus_o : in std_logic
  );

  procedure input (
    signal BitClk_i : in std_logic;
    signal DivClk_i : in std_logic;
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic;
    signal ReadyToRcv_o_p : in std_logic;
    signal ReadyToRcv_o_n : in std_logic);

  procedure output (
    signal DivClk_i : in std_logic;
    signal Axis_Data_o : in Output_Data_t;
    signal Axis_Vld_o : in std_logic;
    signal Axis_Rdy_i : inout std_logic;
    signal PktParam_o : in Pkt_Param_t);

  procedure status (
    signal DivClk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal SyncStatus_o : in std_logic;
    signal DelayOk_o : std_logic := '0';
    signal BitslipOk_o : std_logic := '0';
    signal LinkIdle_o : in std_logic);

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal Rst_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_tb_case_errors is
  procedure control (
    signal DivClk_i : in std_logic;
    signal Rst_i : inout std_logic;
    signal Resync_i : inout std_logic;
    signal Rst_Errors_i : inout std_logic;
    signal SyncStatus_o : in std_logic
  ) is
  begin
    -- CRC Err Detection Check --
    WaitForCase(0, DivClk_i);
    ProcDone(ProcControl) := '0';
    ApplyReset(DivClk_i, Rst_i);
    wait until SyncStatus_o = '1';
    wait until rising_edge(DivClk_i);
    ApplyReset(DivClk_i, Rst_Errors_i);
    ProcDone(ProcControl) := '1';

    -- Frame Err Detection Check (double packet) --
    WaitForCase(1, DivClk_i);
    ProcDone(ProcControl) := '0';
    ApplyReset(DivClk_i, Rst_i);
    wait until SyncStatus_o = '1';
    wait until rising_edge(DivClk_i);
    ApplyReset(DivClk_i, Rst_Errors_i);
    ProcDone(ProcControl) := '1';

    -- Frame Err Detection Check (length missmatch) --
--    WaitForCase(2, DivClk_i);
--    ProcDone(ProcControl) := '0';
--    ApplyReset(DivClk_i, Rst_i);
--    wait until SyncStatus_o = '1';
--    wait until rising_edge(DivClk_i);
--    ApplyReset(DivClk_i, Rst_Errors_i);
--    ProcDone(ProcControl) := '1';

    -- Buffer Overflow Err Detection Check --
    WaitForCase(3,  DivClk_i);
    ProcDone(ProcControl) := '0';
    ApplyReset(DivClk_i, Rst_i);
    wait until SyncStatus_o = '1';
    wait until rising_edge(DivClk_i);
    ApplyReset(DivClk_i, Rst_Errors_i);
    ProcDone(ProcControl) := '1';

  end procedure;

  procedure input (
    signal BitClk_i : in std_logic;
    signal DivClk_i : in std_logic;
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic;
    signal ReadyToRcv_o_p : in std_logic;
    signal ReadyToRcv_o_n : in std_logic) is
    constant FrameSize          : integer := 2+2+24+32+4; -- FrameLen+SOF+header+payload+CRC
    variable FrameData          : t_aslv8(0 to FrameSize-1);
    constant PayloadLenOverflow : integer := FifoDepth_cgn*8 + 100;
    constant FrameSizeOverflow  : integer := 2+2+24+PayloadLenOverflow+4; -- FrameLen+SOF+header+payload+CRC
    variable FrameDataOverflow  : t_aslv8(0 to FrameSizeOverflow-1);
  begin
    print(">>> -- CRC Err Detection Check --");
    InitTestCase;
    ProcDone(ProcInput) := '0';
    TestCase := 0;
    DataGen_Frame(32, 16#00C0FFEE#, true, true, false, false, false, false, true, FrameData);
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DesDataGen_Frame (Data=>FrameData, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    ControlWaitCompl(DivClk_i);
    print(">>> CRC Err Detection Check <<<");
    print("");

    print(">>> -- Frame Err Detection Check (double packet) --");
    ProcDone(ProcInput) := '0';
    TestCase := 1;
    DataGen_Frame(32, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData);
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    -- Transmit twice to generate Frame Error
    for i in 1 to 2 loop
      DesDataGen_Frame (Data=>FrameData, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
      DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    ProcDone(ProcInput) := '1';
    ControlWaitCompl(DivClk_i);
    print(">>> Frame Err Detection Check  (double packet) <<<");
    print("");

--    print(">>> -- Frame Err Detection Check (length missmatch) --");
--    ProcDone(ProcInput) := '0';
--    TestCase := 2;
--    DataGen_Frame(32, 16#00C0FFEE#, true, true, false, false, false, true, false, FrameData);
--    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
--    while ReadyToRcv_o_p = '0' loop
--      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
--    end loop;
--    DesDataGen_Frame (Data=>FrameData, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
--    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
--    ProcDone(ProcInput) := '1';
--    ControlWaitCompl(DivClk_i);
--    print(">>> Frame Err Detection Check  (length missmatch) <<<");
--    print("");

    print(">>> -- Buffer Overflow Err Detection Check --");
    ProcDone(ProcInput) := '0';
    TestCase := 3;
    DataGen_Frame(PayloadLenOverflow, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameDataOverflow);
    DesDataGen_Stable (NrOfBytes=>10, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    while ReadyToRcv_o_p = '0' loop
      DesDataGen_Stable (NrOfBytes=>1, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    end loop;
    DesDataGen_Frame (Data=>FrameDataOverflow, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i_p=>SerialData_i_p, SerialData_i_n=>SerialData_i_n);
    ProcDone(ProcInput) := '1';
    ControlWaitCompl(DivClk_i);
    print(">>> Buffer Overflow Err Detection Check <<<");
    print("");

    FinishTestCase;
  end procedure;

  procedure output (
    signal DivClk_i : in std_logic;
    signal Axis_Data_o : in Output_Data_t;
    signal Axis_Vld_o : in std_logic;
    signal Axis_Rdy_i : inout std_logic;
    signal PktParam_o : in Pkt_Param_t) is
    constant PayloadLen       : integer := 32; -- header+payload
    variable FrameData        : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen-1);
    constant OvflPayloadLen   : integer := FifoDepth_cgn*8 + 100; -- header+payload
    variable OvflFrameData    : t_aslv8(0 to TbHdrBytes_cpk+OvflPayloadLen-1);
  begin
    -- CRC Err Detection Check --
    WaitForCase(0,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    -- expect data even though CRC error occures
    OutDataGen_Packet(PayloadLen, 16#00C0FFEE#, true, true, true, false, false, FrameData);
    OutData_Expect(OutData=>FrameData, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"CRC Err Check case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Frame Err Detection Check (double packet) --
    WaitForCase(1,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(DivClk_i);
    end loop;
    wait until rising_edge(DivClk_i);
    OutDataGen_Packet(PayloadLen, 16#00C0FFEE#, true, true, false, false, false, FrameData);
    OutData_Expect(OutData=>FrameData, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Err Check (double packet) case " & integer'image(TestCase));
    -- Verify that there is no more data there
    for i in 1 to 50 loop
      wait until rising_edge(DivClk_i);
    end loop;
    StdlCompare(0, Axis_Vld_o, "Frame Err Dection check: Axis_Vld_o ");
    ProcDone(ProcOutput) := '1';

    -- Frame Err Detection Check (length missmatch) --
--    WaitForCase(2,  DivClk_i);
--    ProcDone(ProcOutput) := '0';
--    OutDataGen_Packet(PayloadLen, 16#00C0FFEE#, true, true, false, false, false, false, false, FrameData);
--    OutData_Expect(OutData=>FrameData, Words=>0, WordOffset=>0,
--                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
--                   PktParam_o=>PktParam_o,
--                   Msg=>"Frame Err Check (length missmatch) case " & integer'image(TestCase));
--    ProcDone(ProcOutput) := '1';

    -- Buffer Overflow Err Detection Check --
    WaitForCase(3,  DivClk_i);
    ProcDone(ProcOutput) := '0';
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(DivClk_i);
    end loop;
    wait until rising_edge(DivClk_i);
    OutDataGen_Packet(OvflPayloadLen, 16#00C0FFEE#, true, true, false, false, true, OvflFrameData);
    OutData_Expect(OutData=>OvflFrameData, Words=>0, WordOffset=>0,
                   Clk_i=>DivClk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Overflow Err Check (overflow) case " & integer'image(TestCase));
    -- Verify that there is no more data there
    for i in 1 to 50 loop
      wait until rising_edge(DivClk_i);
    end loop;
    StdlCompare(0, Axis_Vld_o, "Overflow Err Dection check: Axis_Vld_o ");
    ProcDone(ProcOutput) := '1';

  end procedure;

  procedure status (
    signal DivClk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal SyncStatus_o : in std_logic;
    signal DelayOk_o : std_logic := '0';
    signal BitslipOk_o : std_logic := '0';
    signal LinkIdle_o : in std_logic) is
  begin
    -- CRC Err Detection Check --
    WaitForCase(0,  DivClk_i);
    ProcDone(ProcStatus) := '0';
    wait until SyncStatus_o = '1';
    while ProcDone(ProcOutput) = '0' loop
      wait until rising_edge(DivClk_i);
    end loop;
    wait until rising_edge(DivClk_i);
    Errors_Expect (CrcErrors=>1, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"CRC Err Check case ");
    ProcDone(ProcStatus) := '1';

    -- Frame Err Detection Check (double packet) --
    WaitForCase(1,  DivClk_i);
    ProcDone(ProcStatus) := '0';
    wait until SyncStatus_o = '1';
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(DivClk_i);
    end loop;
    wait until rising_edge(DivClk_i);
    Errors_Expect (FrameErrors=>1, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Err Check (double packte) case ");
    ProcDone(ProcStatus) := '1';

    -- Frame Err Detection Check (length missmatch) --
--    WaitForCase(2,  DivClk_i);
--    ProcDone(ProcStatus) := '0';
--    wait until SyncStatus_o = '1';
--    while ProcDone(ProcInput) = '0' loop
--      wait until rising_edge(DivClk_i);
--    end loop;
--    wait until rising_edge(DivClk_i);
--    Errors_Expect (FrameErrors=>1, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Err Check (length missmatch) case ");
--    ProcDone(ProcStatus) := '1';

    -- Buffer Overflow Err Detection Check --
    WaitForCase(3,  DivClk_i);
    ProcDone(ProcStatus) := '0';
    wait until SyncStatus_o = '1';
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(DivClk_i);
    end loop;
    wait until rising_edge(DivClk_i);
    Errors_Expect (FrameErrors=>1, Clk_i=>DivClk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Buffer Overflow Err Check case ");
    ProcDone(ProcStatus) := '1';

  end procedure;

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal Rst_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    constant Generics_c : Generics_t
  ) is
  begin
    Rst_RcvPktCount_i <= '0';
    wait until rising_edge(Clk_i);

    -- CRC Err Detection Check --
    WaitForCase(0,  Clk_i);
    wait until Rst_i = '0';
    PktCount_Expect(0, RcvPktCount_o, "CRC Err Check (pre) case " & integer'image(TestCase));
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(Clk_i);
    end loop;
    PktCount_Expect(1, RcvPktCount_o, "CRC Err Check (post) case " & integer'image(TestCase));
    ProcDone(ProcPktCount) := '1';

    -- Frame Err Detection Check (double packet) --
    WaitForCase(1,  Clk_i);
    wait until Rst_i = '0';
    PktCount_Expect(0, RcvPktCount_o, "Frame Err Check (double packte) (pre) case " & integer'image(TestCase));
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(Clk_i);
    end loop;
    PktCount_Expect(2, RcvPktCount_o, "Frame Err Check (double packte) (post) case " & integer'image(TestCase));
    ProcDone(ProcPktCount) := '1';

    -- Frame Err Detection Check (length missmatch) --
--    WaitForCase(2,  DivClk_i);
--    ...

    -- Buffer Overflow Err Detection Check --
    WaitForCase(3,  Clk_i);
    wait until Rst_i = '0';
    PktCount_Expect(0, RcvPktCount_o, "Buffer Overflow Err Check (pre) case " & integer'image(TestCase));
    while ProcDone(ProcInput) = '0' loop
      wait until rising_edge(Clk_i);
    end loop;
    PktCount_Expect(1, RcvPktCount_o, "Buffer Overflow Err Check (post) case " & integer'image(TestCase));
    ProcDone(ProcPktCount) := '1';

  end procedure;

end;
