------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_tb_pkg is

  -- *** Generics Record ***
  type Generics_t is record
    Dummy : boolean; -- required since empty records are not allowed
  end record;

  ------------------------------------------------------------
  -- Not exported Generics
  ------------------------------------------------------------
  constant IdlePattern_cgn  : std_logic_vector(7 downto 0) := x"5A";
  constant DlyRefClk_cgn    : real := 200.0;
  constant BitOrder_cgn     : string := "MSB first";
  constant SDataRate_cgn    : real := 640.0;

  constant EyeWidth_c       : time := 1.000 ns;
  constant BitPeriod_c      : time := 1.5625 ns;-- has to be precisely equal to the generated bit clock

  shared variable TestCase  : integer  := -1;
  shared variable ProcDone  : std_logic_vector(0 to 4) := (others=>'0');
  constant AllProcDone      : std_logic_vector(ProcDone'range)  := (others => '1');
  constant ProcControl      : integer := 0;
  constant ProcInput        : integer := 1;
  constant ProcOutput       : integer := 2;
  constant ProcStatus       : integer := 3;
  constant ProcPktCount     : integer := 4;

  ------------------------------------------------------------
  -- Test Case Control
  ------------------------------------------------------------
  procedure InitTestCase;

  procedure ApplyReset( signal  Clk_i : in  std_logic;
                        signal  Rst_o : out std_logic);

  procedure FinishTestCase;

  procedure ControlWaitCompl( signal Clk_i : in  std_logic);

  procedure WaitForCase(         SubCase : in  integer;
                         signal  Clk_i   : in  std_logic);

  ------------------------------------------------------------
  -- Test Procedures
  ------------------------------------------------------------
  procedure SerialInputGen (
         Data             : std_logic_vector(7 downto 0);
         BitPeriod        : time := BitPeriod_c;
         EyeWidth         : time := EyeWidth_c;
         BitOrder         : string := BitOrder_cgn; -- "MSB first", "LSB first"
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic
  );

  procedure DesDataGen_Stable (
           NrOfBytes        : integer := 1;
           Data             : std_logic_vector(7 downto 0) := IdlePattern_cgn;
           BitPeriod        : time := BitPeriod_c;
           EyeWidth         : time := EyeWidth_c;
           BitOrder         : string := BitOrder_cgn; -- "MSB first", "LSB first"
      signal SerialData_i_p : inout std_logic;
      signal SerialData_i_n : inout std_logic
  );

  procedure DataGen_Frame (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
             InjLenErr    : boolean := false;
             InjCrcErr    : boolean := false;
    variable Data         : out t_aslv8
  );

  procedure DesDataGen_Frame (
        Data              : t_aslv8;
        BitPeriod         : time := BitPeriod_c;
        EyeWidth          : time := EyeWidth_c;
        BitOrder          : string := BitOrder_cgn; -- "MSB first", "LSB first"
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic
  );

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_tb_pkg is

  ------------------------------------------------------------
  -- Test Case Control
  ------------------------------------------------------------
  procedure InitTestCase is
    begin
      ProcDone     := (others => '0');
      TestCase     := -1;
    end procedure;

    procedure ApplyReset( signal  Clk_i : in  std_logic;
                          signal  Rst_o : out std_logic) is
    begin
      wait until rising_edge(Clk_i);
      Rst_o <= '1';
      wait until rising_edge(Clk_i);
      wait until rising_edge(Clk_i);
      Rst_o <= '0';
      wait until rising_edge(Clk_i);
    end procedure;

    procedure FinishTestCase is
    begin
      TestCase := -1;
      wait for 1 us;
    end procedure;

    procedure ControlWaitCompl( signal Clk_i : in  std_logic) is
    begin
      while ProcDone /= AllProcDone loop
        wait until rising_edge(Clk_i);
      end loop;
      ProcDone := (others => '0');
    end procedure;

    procedure WaitForCase(        SubCase : in  integer;
                           signal Clk_i   : in  std_logic) is
    begin
      while TestCase /= SubCase loop
        wait until rising_edge(Clk_i);
      end loop;
    end procedure;

  ------------------------------------------------------------
  -- Test Procedures
  ------------------------------------------------------------
  procedure SerialInputGen (
           Data             : std_logic_vector(7 downto 0);
           BitPeriod        : time := BitPeriod_c;
           EyeWidth         : time := EyeWidth_c;
           BitOrder         : string := BitOrder_cgn; -- "MSB first", "LSB first"
      signal SerialData_i_p : inout std_logic;
      signal SerialData_i_n : inout std_logic
  ) is
    constant DataValid_Time  : time := EyeWidth;
    constant PreStable_Time  : time := (BitPeriod - EyeWidth)/2;
    constant PostStable_Time : time := BitPeriod - PreStable_Time - DataValid_Time;
  begin
    if BitOrder = "MSB first" then
      for i in 7 downto 0 loop
        -------------------------------------------------------------
        SerialData_i_p <= rand_sl;     -- Data_p not valid here
        SerialData_i_n <= not rand_sl; -- Data_n not valid here
        wait for PreStable_Time;
        SerialData_i_p <= Data(i);     -- Data_p valid
        SerialData_i_n <= not Data(i); -- Data_n valid
        wait for DataValid_Time;
        SerialData_i_p <= rand_sl;     -- Data_p not valid here
        SerialData_i_n <= not rand_sl; -- Data_n not valid here
        wait for PostStable_Time;
        -------------------------------------------------------------
      end loop;
    else
      for i in 0 to 7 loop
        -------------------------------------------------------------
        SerialData_i_p <= rand_sl;     -- Data_p not valid here
        SerialData_i_n <= not rand_sl; -- Data_n not valid here
        wait for PreStable_Time;
        SerialData_i_p <= Data(i);     -- Data_p valid
        SerialData_i_n <= not Data(i); -- Data_n valid
        wait for DataValid_Time;
        SerialData_i_p <= rand_sl;     -- Data_p not valid here
        SerialData_i_n <= not rand_sl; -- Data_n not valid here
        wait for PostStable_Time;
        -------------------------------------------------------------
      end loop;
    end if;
  end procedure;

  procedure DesDataGen_Stable (
        NrOfBytes         : integer := 1;
        Data              : std_logic_vector(7 downto 0) := IdlePattern_cgn;
        BitPeriod         : time := BitPeriod_c;
        EyeWidth          : time := EyeWidth_c;
        BitOrder          : string := BitOrder_cgn; -- "MSB first", "LSB first"
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic
  ) is
  begin
    for i in 0 to NrOfBytes-1 loop
      SerialInputGen(Data, BitPeriod, EyeWidth, BitOrder, SerialData_i_p, SerialData_i_n);
    end loop;
  end procedure;

  procedure DataGen_Frame (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
             InjLenErr    : boolean := false;
             InjCrcErr    : boolean := false;
    variable Data         : out t_aslv8
  ) is
    constant SOF      : t_aslv8(0 to 1) := (SofByte0_cpk, SofByte1_cpk);
    variable FrameLen : t_aslv8(0 to 1);
    variable header   : t_aslv8(0 to TbHdrBytes_cpk-1);
    variable payload  : t_aslv8(0 to PayloadLen-1);
    variable CRC      : t_aslv8(0 to 3);
  begin
    DataGen_FrameLen(PayloadLen+TbHdrBytes_cpk, InjLenErr, FrameLen);
    DataGen_Header(PayloadLen, EventNr, FlagSOE, FlagEOE, FlagCrcErr, FlagDtgrmErr, FlagOvflErr, header);
    DataGen_Payload(PayloadLen, payload);
    DataGen_CRC(header & payload, InjCrcErr, CRC);
    Data := SOF & FrameLen & header & payload & CRC;
  end procedure;

  procedure DesDataGen_Frame (
        Data              : t_aslv8;
        BitPeriod         : time := BitPeriod_c;
        EyeWidth          : time := EyeWidth_c;
        BitOrder          : string := BitOrder_cgn; -- "MSB first", "LSB first"
    signal SerialData_i_p : inout std_logic;
    signal SerialData_i_n : inout std_logic
  ) is
  begin
    for i in Data'range loop
      SerialInputGen(Data(i), BitPeriod, EyeWidth, BitOrder, SerialData_i_p, SerialData_i_n);
    end loop;
  end procedure;

end;
