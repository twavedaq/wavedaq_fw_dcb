------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.math_real.all;

library work;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
--  use work.psi_tb_activity_pkg.all;
  use work.crc_tb_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_tb_common_pkg is

  ------------------------------------------------------------
  -- Handwritten constants and variables
  ------------------------------------------------------------
  constant DesDataIdleByte_cpk    : std_logic_vector(7 downto 0) := x"5A";
  constant InterFrameGap_cpk      : integer := 12;

  constant FifoDepth_cgn    : positive := 20;

  constant TbHdrBytes_cpk               : integer := 24;
  constant TbHdrWdaqFlagWord_cpk        : integer :=  9;
  constant TbHdrWdaqEoeFlagBit_cpk      : integer :=  0;
  constant TbHdrWdaqSoeFlagBit_cpk      : integer :=  1;
  constant TbHdrWdaqCrcErrFlagBit_cpk   : integer :=  4;
  constant TbHdrWdaqDtgrmErrFlagBit_cpk : integer :=  5;
  constant TbHdrWdaqBufOvflFlagBit_cpk  : integer :=  6;
  constant TbHdrEventNrMsbWord_cpk      : integer := 16;
  constant TbHdrPldSizeMsbWord_cpk      : integer := 20;

  constant ErrCountAllZero  : std_logic_vector(ErrCountBits_cpk-1 downto 0) := (others=>'0');
  constant BytesPerWord_c   : integer := 8;
  constant LeftToRight_c    : boolean := false; -- word byte order

  constant FlagWord_c       : integer := TbHdrWdaqFlagWord_cpk  /  BytesPerWord_c;
  constant FlagByteOffs_c   : integer := TbHdrWdaqFlagWord_cpk mod BytesPerWord_c;

  shared variable Seed1 : integer := 999;
  shared variable Seed2 : integer := 999;

  ------------------------------------------------------------
  -- Helper Functions
  ------------------------------------------------------------
  impure function rand_time(min_val, max_val : time; unit : time := ns) return time;
  impure function rand_sl return std_logic;

  ------------------------------------------------------------
  -- Test Functions
  ------------------------------------------------------------
  procedure DataGen_FrameLen (
             FrameLen : natural := 128;
             InjErr   : boolean := false;
    variable Data     : out t_aslv8(0 to 1)
  );

  procedure DataGen_Header (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
    variable header       : out t_aslv8(0 to TbHdrBytes_cpk-1)
  );

  procedure DataGen_Payload (
           PayloadLen : natural := 32;
    variable OutData  : out t_aslv8
  );

  procedure DataGen_CRC (
             Data         : t_aslv8;
             InjectCrcErr : boolean := false;
    variable CRC          : out t_aslv8
  );

  procedure OutDataGen_Packet (
              PayloadLen   : natural := 32;
              EventNr      : natural :=  0;
              FlagSOE      : boolean := false;
              FlagEOE      : boolean := false;
              FlagCrcErr   : boolean := false;
              FlagDtgrmErr : boolean := false;
              FlagOvflErr  : boolean := false;
     variable OutData      : out t_aslv8
  );

  procedure OutData_Expect (
            OutData     : t_aslv8;
            Words       : natural := 0;
            WordOffset  : natural := 0;
    signal Clk_i        : in  std_logic;
    signal Axis_Data_o  : in  Output_Data_t;
    signal Axis_Vld_o   : in  std_logic;
    signal Axis_Rdy_i   : out std_logic;
    signal PktParam_o   : in  Pkt_Param_t;
            Msg         : in  string := ""
  );

  procedure Errors_Expect (
          CrcErrors      : integer := 0;
          FrameErrors    : integer := 0;
          DatagramErrors : integer := 0;
          SyncErrors     : integer := 0;
  signal Clk_i           : in    std_logic;
  signal Error_Flags_o   : in    Error_t;
          Msg            : in    string := ""
  );

  procedure PktStart_Wait (
    signal Clk_i        : in std_logic;
    signal Rst_i        : in std_logic;
    signal DesData_i    : in std_logic_vector(7 downto 0);
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic
  );

  procedure PktCount_Expect(
           ExpectedCount : integer := 0;
    signal RcvPktCount_o : in std_logic_vector;
           Msg           : in string := ""
  );

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_tb_common_pkg is

  ------------------------------------------------------------
  -- Helper Functions
  ------------------------------------------------------------
  impure function rand_time(min_val, max_val : time; unit : time := ns)
    return time is
    variable r, r_scaled, min_real, max_real : real;
  begin
    uniform(Seed1, Seed2, r);
    min_real := real(min_val / unit);
    max_real := real(max_val / unit);
    r_scaled := r * (max_real - min_real) + min_real;
    return real(r_scaled) * unit;
  end function;

  impure function rand_sl return std_logic is
    variable r  : real;
    variable sl : std_logic;
  begin
    uniform(seed1, seed2, r);
    sl := '1' when r > 0.5 else '0';
    return sl;
  end function;

  ------------------------------------------------------------
  -- Test Functions
  ------------------------------------------------------------
  procedure DataGen_FrameLen (
             FrameLen : natural := 128;
             InjErr   : boolean := false;
    variable Data     : out t_aslv8(0 to 1)
  ) is
    variable Len      : std_logic_vector(15 downto 0) := (others=>'0');
  begin
    Len := std_logic_vector(to_unsigned(FrameLen, Len'length));
    if InjErr then
      Len := not Len;
    end if;
    Data := (Len(15 downto 8), Len( 7 downto 0));
  end procedure;

  procedure DataGen_Header (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
    variable header     : out t_aslv8(0 to TbHdrBytes_cpk-1)
  ) is
    constant EventNr_slv    : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(EventNr, 32));
    constant PayloadLen_slv : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(PayloadLen, 16));
  begin
    for i in 0 to TbHdrBytes_cpk-1 loop
      header(i) := std_logic_vector(to_unsigned(i, header(i)'length));
    end loop;
    -- -- -- populate specific values -- -- --
    -- flags
    header(TbHdrWdaqFlagWord_cpk) := x"00";
    if FlagSOE then
      header(TbHdrWdaqFlagWord_cpk)(TbHdrWdaqSoeFlagBit_cpk) := '1';
    end if;
    if FlagEOE then
      header(TbHdrWdaqFlagWord_cpk)(TbHdrWdaqEoeFlagBit_cpk) := '1';
    end if;
    if FlagCrcErr then
      header(TbHdrWdaqFlagWord_cpk)(TbHdrWdaqCrcErrFlagBit_cpk) := '1';
    end if;
    if FlagDtgrmErr then
      header(TbHdrWdaqFlagWord_cpk)(TbHdrWdaqDtgrmErrFlagBit_cpk) := '1';
    end if;
    if FlagOvflErr then
      header(TbHdrWdaqFlagWord_cpk)(TbHdrWdaqBufOvflFlagBit_cpk) := '1';
    end if;
    -- event number
    header(TbHdrEventNrMsbWord_cpk)   := EventNr_slv(31 downto 24);
    header(TbHdrEventNrMsbWord_cpk+1) := EventNr_slv(23 downto 16);
    header(TbHdrEventNrMsbWord_cpk+2) := EventNr_slv(15 downto  8);
    header(TbHdrEventNrMsbWord_cpk+3) := EventNr_slv( 7 downto  0);
    -- payload length
    header(TbHdrPldSizeMsbWord_cpk)   := PayloadLen_slv(15 downto 8);
    header(TbHdrPldSizeMsbWord_cpk+1) := PayloadLen_slv( 7 downto 0);
    -- -- -- -- -- -- -- -- -- -- --  -- -- --
  end procedure;

  procedure DataGen_Payload (
           PayloadLen   : natural := 32;
    variable OutData    : out t_aslv8
  ) is
  begin
    for i in 0 to PayloadLen-1 loop
      OutData(i) := std_logic_vector(to_unsigned(i, OutData(i)'length));
    end loop;
  end procedure;

  procedure DataGen_CRC (
             Data         : t_aslv8;
             InjectCrcErr : boolean := false;
    variable CRC          : out t_aslv8
  ) is
    variable Checksum : std_logic_vector(31 downto 0) := x"deadbeef";
  begin
    crc32_gen(Data, Checksum);
    for i in 0 to 3 loop
      if InjectCrcErr then
        CRC(i) := not(Checksum((i+1)*8-1 downto i*8));
      else
        CRC(i) := Checksum((i+1)*8-1 downto i*8);
      end if;
    end loop;
  end procedure;

  procedure OutDataGen_Packet (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
    variable OutData      : out t_aslv8
  ) is
    variable TargetHeader_v   : t_aslv8(0 to TbHdrBytes_cpk-1);
    variable TargetPayload_v  : t_aslv8(0 to PayloadLen-1);
    variable PayloadLenCorr_v : natural := PayloadLen;
  begin
    -- Resize data if larger than buffer
    if PayloadLen > (FifoDepth_cgn * BytesPerWord_c) - TbHdrBytes_cpk then
      PayloadLenCorr_v := (FifoDepth_cgn * BytesPerWord_c) - TbHdrBytes_cpk;
    end if;
    DataGen_Header(PayloadLen, EventNr, FlagSOE, FlagEOE, FlagCrcErr, FlagDtgrmErr, FlagOvflErr, TargetHeader_v);
    DataGen_Payload(PayloadLenCorr_v, TargetPayload_v);
    OutData := TargetHeader_v & TargetPayload_v;
  end procedure;

  procedure OutData_Expect (
           OutData     : t_aslv8;
           Words       : natural := 0;
           WordOffset  : natural := 0;
    signal Clk_i       : in  std_logic;
    signal Axis_Data_o : in  Output_Data_t;
    signal Axis_Vld_o  : in  std_logic;
    signal Axis_Rdy_i  : out std_logic;
    signal PktParam_o  : in  Pkt_Param_t;
           Msg         : in  string := ""
  ) is
    variable TargetWord_v    : std_logic_vector(BytesPerWord_c*8-1 downto 0);
    variable ActualWord_v    : std_logic_vector(BytesPerWord_c*8-1 downto 0);
    variable OutWords        : natural := 0;
    variable BytesRcvd       : natural := WordOffset*BytesPerWord_c;
    variable BytesInWord     : natural;
    variable CrcErrFlagBit   : natural;
    variable DtgrmErrFlagBit : natural;
    variable OvflErrFlagBit  : natural;
    variable word            : integer;
    variable PktTerminated   : boolean := false;
  begin
    if LeftToRight_c then
      CrcErrFlagBit   := TbHdrWdaqCrcErrFlagBit_cpk   + ((BytesPerWord_c - 1) - FlagByteOffs_c) * 8;
      DtgrmErrFlagBit := TbHdrWdaqDtgrmErrFlagBit_cpk + ((BytesPerWord_c - 1) - FlagByteOffs_c) * 8;
      OvflErrFlagBit  := TbHdrWdaqBufOvflFlagBit_cpk  + ((BytesPerWord_c - 1) - FlagByteOffs_c) * 8;
    else
      CrcErrFlagBit   := TbHdrWdaqCrcErrFlagBit_cpk   + FlagByteOffs_c * 8;
      DtgrmErrFlagBit := TbHdrWdaqDtgrmErrFlagBit_cpk + FlagByteOffs_c * 8;
      OvflErrFlagBit  := TbHdrWdaqBufOvflFlagBit_cpk  + FlagByteOffs_c * 8;
    end if;
    wait until rising_edge(Clk_i) and PktParam_o.Valid = '1';
    OutWords := (OutData'length+BytesPerWord_c-1)/BytesPerWord_c - WordOffset;
    if Words /= 0 and Words<OutWords then
      OutWords := Words;
    end if;
    word := WordOffset;
    while word < WordOffset+OutWords and not(PktTerminated) loop
--      report "word = " & integer'image(word) & ", WordOffset = " & integer'image(WordOffset) & ", OutWords = " & integer'image(OutWords);
      TargetWord_v := (others=>'0');
      for byte in 0 to BytesPerWord_c-1 loop
        if OutData'length <= byte+word*BytesPerWord_c then
          TargetWord_v((byte+1)*8-1 downto byte*8) := (others=>'0');
        else
          BytesRcvd := BytesRcvd + 1;
          if LeftToRight_c then
            TargetWord_v((byte+1)*8-1 downto byte*8) := OutData((BytesPerWord_c-byte-1)+word*BytesPerWord_c);
          else
            TargetWord_v((byte+1)*8-1 downto byte*8) := OutData(word*BytesPerWord_c+byte);
          end if;
        end if;
      end loop;
      Axis_Rdy_i <= '1';
      wait until rising_edge(Clk_i) and PktParam_o.Valid = '1' and Axis_Vld_o = '1';
      ActualWord_v := Axis_Data_o.Data;
      -- Verify data word
      StdlvCompareStdlv(TargetWord_v, ActualWord_v, Msg & ", Word " & integer'image(word), "###ERROR###: ");
      -- Check number of bytes in word
      BytesInWord := BytesRcvd mod BytesPerWord_c;
      if BytesInWord = 0 then
        BytesInWord := 8;
      end if;
      IntCompare(BytesInWord, to_integer(unsigned(Axis_Data_o.Bytes)), Msg & " word " & integer'image(word) & " wrong number of bytes in word");
      -- Check number of bytes in packet and "Last" signal
      if BytesRcvd = OutData'length or BytesRcvd = FifoDepth_cgn * BytesPerWord_c then
        if Axis_Data_o.Last = '0' then
          report "###ERROR###: Last = '0' at end of packet";
          IntCompare(OutData'length, to_integer(unsigned(PktParam_o.Bytes)), Msg & " reportet packet length wrong");
        end if;
      end if;
      -- Handle packet termination
      if Axis_Data_o.Last = '1' then
        PktTerminated := true;
        if OutData'length > FifoDepth_cgn * BytesPerWord_c then
          -- buffer overflow, only buffer size packet
          IntCompare(FifoDepth_cgn * BytesPerWord_c, BytesRcvd, Msg & " packet length mismatch (should have buffer size)");
          IntCompare(FifoDepth_cgn * BytesPerWord_c, to_integer(unsigned(PktParam_o.Bytes)), Msg & " reportet packet length wrong (should be buffer size)");
        else
          -- normal packet
          IntCompare(OutData'length, BytesRcvd, Msg & " packet length mismatch ");
          IntCompare(OutData'length, to_integer(unsigned(PktParam_o.Bytes)), Msg & " reportet packet length wrong");
        end if;
      end if;
      -- Check error flags
      if word = FlagWord_c then
        StdlCompare(TargetWord_v(CrcErrFlagBit),   ActualWord_v(CrcErrFlagBit),   Msg & " Header CRC Error Flag (word " & integer'image(FlagWord_c) & ", index " & integer'image(CrcErrFlagBit) & ")");
        StdlCompare(TargetWord_v(DtgrmErrFlagBit), ActualWord_v(DtgrmErrFlagBit), Msg & " Header Datagram Error Flag (word " & integer'image(FlagWord_c) & ", index " & integer'image(DtgrmErrFlagBit) & ")");
        StdlCompare(TargetWord_v(OvflErrFlagBit),  ActualWord_v(OvflErrFlagBit),  Msg & " Header Overflow Error Flag (word " & integer'image(FlagWord_c) & ", index " & integer'image(OvflErrFlagBit) & ")");
      end if;
      word := word + 1;
    end loop;
    -- Report packet reception
    if BytesRcvd = OutData'length then
      print("<<< Packet reception completed (" & integer'image(BytesRcvd) & " bytes received)");
    elsif BytesRcvd = FifoDepth_cgn * BytesPerWord_c then
      print("<<< Buffer overflow! Truncated packet reception completed (" & integer'image(BytesRcvd) & " of " & integer'image(OutData'length) & " bytes received)");
    else
      print("=== Packet reception pending (" & integer'image(BytesRcvd) & " of " & integer'image(OutData'length) & " bytes received)");
    end if;
    Axis_Rdy_i <= '0';
    wait until rising_edge(Clk_i);
  end procedure;

  procedure Errors_Expect (
           CrcErrors      : integer := 0;
           FrameErrors    : integer := 0;
           DatagramErrors : integer := 0;
           SyncErrors     : integer := 0;
    signal Clk_i          : in    std_logic;
    signal Error_Flags_o  : in    Error_t;
           Msg            : in    string := ""
  ) is
  begin
    wait until rising_edge(Clk_i);
    wait until rising_edge(Clk_i);
    -- Report CRC Error check
    IntCompare(CrcErrors,   to_integer(unsigned(Error_Flags_o.CRC_ErrCount)),         Msg & " CRC error count mismatch ");
    if (Error_Flags_o.CRC_ErrCount  = ErrCountAllZero and Error_Flags_o.CRC_Err = '1') or
       (Error_Flags_o.CRC_ErrCount /= ErrCountAllZero and Error_Flags_o.CRC_Err = '0') then
      report "###ERROR###: mismatch in CRC Error Count " & integer'image(to_integer(unsigned(Error_Flags_o.CRC_ErrCount))) & " and Flag " & str(Error_Flags_o.CRC_Err);
    end if;
      -- Report Frame Error check
    IntCompare(FrameErrors, to_integer(unsigned(Error_Flags_o.Frame_ErrCount)),       Msg & " Frame error count mismatch ");
    if (Error_Flags_o.Frame_ErrCount  = ErrCountAllZero and Error_Flags_o.Frame_Err = '1') or
       (Error_Flags_o.Frame_ErrCount /= ErrCountAllZero and Error_Flags_o.Frame_Err = '0') then
      report "###ERROR###: mismatch in Frame Error Count " & integer'image(to_integer(unsigned(Error_Flags_o.Frame_ErrCount))) & " and Flag " & str(Error_Flags_o.Frame_Err);
      end if;
      -- Report Datagram Error check
    IntCompare(DatagramErrors, to_integer(unsigned(Error_Flags_o.Datagram_ErrCount)), Msg & " Datagram error count mismatch ");
    if (Error_Flags_o.Datagram_ErrCount  = ErrCountAllZero and Error_Flags_o.Datagram_Err = '1') or
       (Error_Flags_o.Datagram_ErrCount /= ErrCountAllZero and Error_Flags_o.Datagram_Err = '0') then
      report "###ERROR###: mismatch in Datagram Error Count " & integer'image(to_integer(unsigned(Error_Flags_o.Datagram_ErrCount))) & " and Flag " & str(Error_Flags_o.Datagram_Err);
    end if;
      -- Report Sync Error check
    IntCompare(SyncErrors, to_integer(unsigned(Error_Flags_o.Sync_ErrCount)),         Msg & " Sync error count mismatch ");
    if (Error_Flags_o.Sync_ErrCount  = ErrCountAllZero and Error_Flags_o.Sync_Err = '1') or
       (Error_Flags_o.Sync_ErrCount /= ErrCountAllZero and Error_Flags_o.Sync_Err = '0') then
      report "###ERROR###: mismatch in Sync Error Count " & integer'image(to_integer(unsigned(Error_Flags_o.Sync_ErrCount))) & " and Flag " & str(Error_Flags_o.Sync_Err);
    end if;
  end procedure;

  procedure PktStart_Wait (
            signal Clk_i        : in std_logic;
            signal Rst_i        : in std_logic;
            signal DesData_i    : in std_logic_vector(7 downto 0);
            signal ReadyToRcv_o : in std_logic;
            signal SyncStatus_i : in std_logic
  ) is
    variable SofStartReceived : boolean := false;
    variable FullSofReceived  : boolean := false;
  begin
    while not(FullSofReceived) loop
      wait until rising_edge(Clk_i);
      if Rst_i = '0' and ReadyToRcv_o = '1' and SyncStatus_i = '1' then
        if SofStartReceived = true and DesData_i = SofByte1_cpk then
          FullSofReceived  := true;
        else
          FullSofReceived  := false;
        end if;
        if DesData_i = SofByte0_cpk then
          SofStartReceived := true;
        else
          SofStartReceived := false;
        end if;
      end if;
    end loop;
  end procedure;

  procedure PktCount_Expect(
                   ExpectedCount : integer := 0;
            signal RcvPktCount_o : in std_logic_vector;
                   Msg           : in string := ""
  ) is
  begin
    -- Report mismatching count
    IntCompare(ExpectedCount, to_integer(unsigned(RcvPktCount_o)), Msg & " Packet count mismatch ");
  end procedure;

end;
