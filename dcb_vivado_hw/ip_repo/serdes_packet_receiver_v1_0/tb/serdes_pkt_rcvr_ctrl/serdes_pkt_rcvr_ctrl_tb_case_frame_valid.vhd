------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.serdes_pkt_rcvr_ctrl_tb_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_ctrl_tb_case_frame_valid is

  procedure deserializer (
    signal Clk_i : in std_logic;
    signal Rst_o : inout std_logic;
    signal DesData_i : inout std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : inout std_logic;
    constant Generics_c : Generics_t);

  procedure output (
    signal Clk_i       : in  std_logic;
    signal Axis_Data_o : in  Output_Data_t;
    signal Axis_Vld_o  : in  std_logic;
    signal Axis_Rdy_i  : out std_logic;
    signal PktParam_o  : in  Pkt_Param_t;
    signal PktLast_o   : in  std_logic;
    constant Generics_c : Generics_t);

  procedure error_out (
    signal Clk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal Rst_Errors_i : inout std_logic;
    constant Generics_c : Generics_t);

  procedure pkt_count (
      signal Clk_i             : in std_logic;
      signal Rst_i             : in std_logic;
      signal RcvPktCount_o     : in std_logic_vector;
      signal Rst_RcvPktCount_i : inout std_logic;
      signal DesData_i         : in std_logic_vector;
      signal ReadyToRcv_o      : in std_logic;
      signal SyncStatus_i      : in std_logic;
      constant Generics_c : Generics_t);

  procedure rdy_to_rcv (
    signal Clk_i : in std_logic;
    signal Rst_i : in std_logic;
    signal DesData_i : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_ctrl_tb_case_frame_valid is
  procedure deserializer (
    signal Clk_i        : in std_logic;
    signal Rst_o        : inout std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : inout std_logic;
    constant Generics_c : Generics_t) is
  begin

    print(">>> -- simple packet aligned --");
    InitTestCase(Clk_i, Rst_o);
    wait for 100 ns;
    TestCase := 0;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Idle(12, true, true, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Packet(96, 16#0fdababe#, true, true, false, false, x"e541393D", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- simple packet unaligned --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 1;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(58, 16#0fdababe#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- simple packet unaligned  (by FCS length) --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 2;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(60, 16#0fdababe#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- simple packet - interrupted by backpressure --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 3;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(58, 16#0fdababe#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    FinishTestCase;

  end procedure;

  procedure output (
    signal Clk_i        : in  std_logic;
    signal Axis_Data_o  : in  Output_Data_t;
    signal Axis_Vld_o   : in  std_logic;
    signal Axis_Rdy_i   : out std_logic;
    signal PktParam_o   : in  Pkt_Param_t;
    signal PktLast_o    : in  std_logic;
    constant Generics_c : Generics_t) is
    constant PayloadLen0 : integer := 96;
    variable OutData0    : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen0-1);
    constant PayloadLen1 : integer := 58;
    variable OutData1    : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen1-1);
    constant PayloadLen2 : integer := 60;
    variable OutData2    : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen2-1);
    constant PayloadLen3 : integer := 58;
    variable OutData3    : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen3-1);
  begin

    -- simple packet aligned
    WaitForCase(0,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#0fdababe#, true, true, false, false, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- simple packet unaligned
    WaitForCase(1,  Clk_i);
    OutDataGen_Packet(PayloadLen1, 16#0fdababe#, true, true, false, false, false, OutData1);
    OutData_Expect(OutData=>OutData1, Words=>0, WordOffset=>0,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- simple packet unaligned (by FCS length)
    WaitForCase(2,  Clk_i);
    OutDataGen_Packet(PayloadLen2, 16#0fdababe#, true, true, false, false, false, OutData2);
    OutData_Expect(OutData=>OutData2, Words=>0, WordOffset=>0,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- simple packet - interrupted by backpressure
    WaitForCase(3,  Clk_i);
    OutDataGen_Packet(PayloadLen3, 16#0fdababe#, true, true, false, false, false, OutData3);
    OutData_Expect(OutData=>OutData3, Words=>2, WordOffset=>0,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Frame Valid case " & integer'image(TestCase) & ".1");
    wait for 400 ns;
    wait until rising_edge(Clk_i);
    OutData_Expect(OutData=>OutData3, Words=>0, WordOffset=>2,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Frame Valid case " & integer'image(TestCase) & ".2");
    ProcDone(ProcOutput) := '1';

  end procedure;

  procedure error_out (
  signal Clk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal Rst_Errors_i : inout std_logic;
    constant Generics_c : Generics_t) is
  begin

    -- simple packet aligned
    WaitForCase(0,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Valid case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- simple packet unaligned
    WaitForCase(1,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Valid case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- simple packet unaligned (by FCS length)
    WaitForCase(2,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Valid case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- simple packet - interrupted by backpressure
    WaitForCase(3,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Frame Valid case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

  end procedure;

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal Rst_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    signal DesData_i         : in std_logic_vector;
    signal ReadyToRcv_o      : in std_logic;
    signal SyncStatus_i      : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    Rst_RcvPktCount_i <= '0';
    -- simple packet aligned
    -- simple packet unaligned
    -- simple packet unaligned (by FCS length)
    -- simple packet - interrupted by backpressure
    for CurrentCase in 0 to 3 loop
      WaitForCase(CurrentCase,  Clk_i);
      wait until rising_edge(Clk_i);
      PktCount_Expect(0, RcvPktCount_o, "Frame Valid case " & integer'image(TestCase));
      PktStart_Wait(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i);
      -- Wait until count is updated
      wait until rising_edge(Clk_i);
      wait until rising_edge(Clk_i);
      PktCount_Expect(1, RcvPktCount_o, "Frame Valid case " & integer'image(TestCase));
      ProcDone(ProcPktCount) := '1';
      end loop;

  end procedure;

  procedure rdy_to_rcv (
    signal Clk_i : in std_logic;
    signal Rst_i : in std_logic;
    signal DesData_i : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
    constant Generics_c : Generics_t) is
  begin

    -- simple packet aligned
    WaitForCase(0,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- simple packet unaligned
    WaitForCase(1,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- simple packet unaligned (by FCS length)
    WaitForCase(2,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- simple packet - interrupted by backpressure
    WaitForCase(3,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Frame Valid case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

  end procedure;

end;
