------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
use work.serdes_pkt_rcvr_tb_common_pkg.all;
use work.serdes_pkt_rcvr_ctrl_tb_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_ctrl_tb_case_errors is

  procedure deserializer (
    signal Clk_i        : in std_logic;
    signal Rst_o        : inout std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : inout std_logic;
    constant Generics_c : Generics_t);

  procedure output (
    signal Clk_i        : in  std_logic;
    signal Axis_Data_o  : in  Output_Data_t;
    signal Axis_Vld_o   : in  std_logic;
    signal Axis_Rdy_i   : out std_logic;
    signal PktParam_o   : in  Pkt_Param_t;
    signal PktLast_o    : in  std_logic;
    constant Generics_c : Generics_t);

  procedure error_out (
    signal Clk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal Rst_Errors_i : inout std_logic;
    constant Generics_c : Generics_t);

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal Rst_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    signal DesData_i         : in std_logic_vector;
    signal ReadyToRcv_o      : in std_logic;
    signal SyncStatus_i      : in std_logic;
    constant Generics_c : Generics_t);

  procedure rdy_to_rcv (
    signal Clk_i : in std_logic;
    signal Rst_i : in std_logic;
    signal DesData_i : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_ctrl_tb_case_errors is
  procedure deserializer (
    signal Clk_i        : in std_logic;
    signal Rst_o        : inout std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : inout std_logic;
    constant Generics_c : Generics_t) is
  begin

    print(">>> -- Checking CRC Err detection --");
    InitTestCase(Clk_i, Rst_o);
    wait for 100 ns;
    TestCase := 0;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Idle(12, true, true, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Packet(64, 16#0000000f#, true, true, false, true, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- Checking Frame Err detection: incoming packet while buffer not empty --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 1;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#000000ff#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Packet(64, 16#00000100#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

--    print(">>> -- Checking Frame Err detection: frame length to payload plus header length missmatch --");
--    InitTestCase(Clk_i, Rst_o);
--    wait for 500 ns;
--    TestCase := 2;
--    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
--    DesDataGen_Packet(64, 16#000000ff#, true, true, true, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
--    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
--    ProcDone(ProcDeserializer) := '1';
--    ControlWaitCompl(Clk_i);

    print(">>> -- Checking Datagram Err detection: missing SOE flag on event number change --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 3;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#00000fff#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    wait for 200 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#00001000#, false, false, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- Checking Datagram Err detection: acitve SOE flag without event number change --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 4;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#0000ffff#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    wait for 200 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#0000ffff#, true, false, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- Checking Datagram Err detection: missing SOE flag after active EOE Flag --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 5;
    wait for 100 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#000fffff#, true, true, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    wait for 200 ns;
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Packet(64, 16#000fffff#, false, false, false, false, x"deadbeef", Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(InterFrameGap_cpk, true, true, Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    print(">>> -- Checking Sync Err detection: no sync word in idle state --");
    InitTestCase(Clk_i, Rst_o);
    wait for 500 ns;
    TestCase := 6;
    DesDataGen_Idle(30, true, true,  Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle( 1, true, false, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(20, true, true,  Clk_i, DesData_i, SyncStatus_i);
    wait until rising_edge(Clk_i) and ReadyToRcv_o = '1';
    DesDataGen_Idle(20, true, true,  Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle( 1, true, false, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(30, true, true,  Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle( 2, true, false, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(30, true, true,  Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(10, true, false, Clk_i, DesData_i, SyncStatus_i);
    DesDataGen_Idle(90, true, true,  Clk_i, DesData_i, SyncStatus_i);
    ProcDone(ProcDeserializer) := '1';
    ControlWaitCompl(Clk_i);

    FinishTestCase;

  end procedure;

  procedure output (
    signal Clk_i        : in  std_logic;
    signal Axis_Data_o  : in  Output_Data_t;
    signal Axis_Vld_o   : in  std_logic;
    signal Axis_Rdy_i   : out std_logic;
    signal PktParam_o   : in  Pkt_Param_t;
    signal PktLast_o    : in  std_logic;
    constant Generics_c : Generics_t) is
    constant PayloadLen0 : integer := 64;
    variable OutData0    : t_aslv8(0 to TbHdrBytes_cpk+PayloadLen0-1);
  begin
    -- Checking CRC Error detection
    WaitForCase(0,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#0000000f#, true, true, true, false, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Err Check case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';
--    PktParam_o=>PktParam_o, PktLast_o=>PktLast_o,

    -- Checking Frame Error detection: incoming packet while buffer not empty
    WaitForCase(1,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#000000ff#, true, true, false, false, false, OutData0);
--    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
--                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
--                   PktParam_o=>PktParam_o, PktLast_o=>PktLast_o,
--                   Msg=>"Err Check case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Checking Frame Error detection: frame length to payload plus header length missmatch
--    WaitForCase(2,  Clk_i);
--    OutDataGen_Packet(PayloadLen0, 16#000000ff#, true, true, false, false, false, OutData0);
--    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
--                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
--                   PktParam_o=>PktParam_o, PktLast_o=>PktLast_o,
--                   Msg=>"Err Check case " & integer'image(TestCase));
--    ProcDone(ProcOutput) := '1';

    -- Checking Datagram Err: missing SOE flag on event number change
    WaitForCase(3,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#00000fff#, true, true, false, false, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Err Check case " & integer'image(TestCase));
    OutDataGen_Packet(PayloadLen0, 16#00001000#, false, false, false, true, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Err Check case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Checking Datagram Err: acitve SOE flag without event number change
    WaitForCase(4,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#0000ffff#, true, true, false, false, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Err Check case " & integer'image(TestCase));
    OutDataGen_Packet(PayloadLen0, 16#0000ffff#, true, false, false, true, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                   Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                   PktParam_o=>PktParam_o,
                   Msg=>"Err Check case " & integer'image(TestCase));
    ProcDone(ProcOutput) := '1';

    -- Checking Datagram Err: missing SOE flag after active EOE Flag
    WaitForCase(5,  Clk_i);
    OutDataGen_Packet(PayloadLen0, 16#000fffff#, true, true, false, false, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Err Check case " & integer'image(TestCase) & ".1");
    OutDataGen_Packet(PayloadLen0, 16#000fffff#, false, false, false, true, false, OutData0);
    OutData_Expect(OutData=>OutData0, Words=>0, WordOffset=>0,
                  Clk_i=>Clk_i, Axis_Data_o=>Axis_Data_o, Axis_Vld_o=>Axis_Vld_o, Axis_Rdy_i=>Axis_Rdy_i,
                  PktParam_o=>PktParam_o,
                  Msg=>"Err Check case " & integer'image(TestCase) & ".2");
    ProcDone(ProcOutput) := '1';

    -- Checking Sync Err: no sync word in idle state
    WaitForCase(6,  Clk_i);
    -- Nothing to be done at output
    ProcDone(ProcOutput) := '1';

  end procedure;

  procedure error_out (
    signal Clk_i : in std_logic;
    signal Error_Flags_o : in Error_t;
    signal Rst_Errors_i : inout std_logic;
    constant Generics_c : Generics_t) is
  begin
    -- Checking CRC Error detection
    WaitForCase(0,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (CRCErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- Checking Frame Error detection: incoming packet while buffer not empty
    WaitForCase(1,  Clk_i);
    while ProcDone(ProcDeserializer) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (FrameErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- Checking Frame Error detection: frame length to payload plus header length missmatch
--    WaitForCase(2,  Clk_i);
--    while ProcDone(ProcOutput) /= '1' loop
--      wait until rising_edge(Clk_i);
--    end loop;
--    Errors_Expect (FrameErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
--    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
--    ProcDone(ProcErrors) := '1';

    -- Checking Datagram Err: missing SOE flag on event number change
    WaitForCase(3,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (DatagramErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- Checking Datagram Err: acitve SOE flag without event number change
    WaitForCase(4,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (DatagramErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- Checking Datagram Err: missing SOE flag after active EOE Flag
    WaitForCase(5,  Clk_i);
    while ProcDone(ProcOutput) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (DatagramErrors=>1, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    ProcDone(ProcErrors) := '1';

    -- Checking Sync Err: no sync word in idle state
    WaitForCase(6,  Clk_i);
    while ProcDone(ProcDeserializer) /= '1' loop
      wait until rising_edge(Clk_i);
    end loop;
    Errors_Expect (SyncErrors=>4, Clk_i=>Clk_i, Error_Flags_o=>Error_Flags_o, Msg=>"Err Check case " & integer'image(TestCase));
    Reset_Errors (Clk_i=>Clk_i, Rst_Errors_i=>Rst_Errors_i);
    print("<<< -- Checking Sync Err completed --");
    ProcDone(ProcErrors) := '1';

  end procedure;

  procedure pkt_count (
    signal Clk_i             : in std_logic;
    signal Rst_i             : in std_logic;
    signal RcvPktCount_o     : in std_logic_vector;
    signal Rst_RcvPktCount_i : inout std_logic;
    signal DesData_i         : in std_logic_vector;
    signal ReadyToRcv_o      : in std_logic;
    signal SyncStatus_i      : in std_logic;
    constant Generics_c : Generics_t) is
    variable CurrentCase : integer := 0;
  begin
    Rst_RcvPktCount_i <= '0';
    -- Checking CRC Error detection
    -- Checking Frame Error detection: incoming packet while buffer not empty
    -- Checking Datagram Err: missing SOE flag on event number change
    -- Checking Datagram Err: acitve SOE flag without event number change
    -- Checking Datagram Err: missing SOE flag after active EOE Flag
    -- Checking Sync Err: no sync word in idle state
    while CurrentCase <= 5 loop
      -- skip case 2
      if CurrentCase = 2 then
        CurrentCase := CurrentCase + 1;
      end if;
      WaitForCase(CurrentCase,  Clk_i);
      wait until rising_edge(Clk_i);
      PktCount_Expect(0, RcvPktCount_o, "Frame Valid case " & integer'image(TestCase));
      PktStart_Wait(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i);
      -- Wait until count is updated
      wait until rising_edge(Clk_i);
      wait until rising_edge(Clk_i);
      PktCount_Expect(1, RcvPktCount_o, "Frame Valid case " & integer'image(TestCase));
      ProcDone(ProcPktCount) := '1';
      CurrentCase := CurrentCase + 1;
    end loop;

  end procedure;

  procedure rdy_to_rcv (
    signal Clk_i : in std_logic;
    signal Rst_i : in std_logic;
    signal DesData_i : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
    constant Generics_c : Generics_t) is
  begin

    -- Checking CRC Error detection
    WaitForCase(0,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Frame Error detection: incoming packet while buffer not empty
    WaitForCase(1,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Frame Error detection: frame length to payload plus header length missmatch
--    WaitForCase(2,  Clk_i);
--    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
--    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Datagram Err: missing SOE flag on event number change
    WaitForCase(3,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Datagram Err: acitve SOE flag without event number change
    WaitForCase(4,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Datagram Err: missing SOE flag after active EOE Flag
    WaitForCase(5,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

    -- Checking Sync Err: no sync word in idle state
    WaitForCase(6,  Clk_i);
    RdyToRcv_Expect(Clk_i, Rst_i, DesData_i, ReadyToRcv_o, SyncStatus_i, BufferFull_i, "Err Check case " & integer'image(TestCase));
    ProcDone(ProcRdyToRcv) := '1';

  end procedure;

end;
