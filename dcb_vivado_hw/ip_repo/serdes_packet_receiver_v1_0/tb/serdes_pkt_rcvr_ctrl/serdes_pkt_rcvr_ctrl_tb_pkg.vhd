------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;
  use work.crc_tb_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package serdes_pkt_rcvr_ctrl_tb_pkg is

  -- *** Generics Record ***
  type Generics_t is record
    Dummy : boolean; -- required since empty records are not allowed
  end record;

  ------------------------------------------------------------
  -- Not exported Generics
  ------------------------------------------------------------



  ------------------------------------------------------------
  -- Handwritten constants and variables
  ------------------------------------------------------------
  constant ErrCountAllZero     : std_logic_vector(ErrCountBits_cpk-1 downto 0) := (others=>'0');

  constant BytesPerWord_c          : integer := 8;
  constant LeftToRight_c           : boolean := false; -- word byte order

  shared variable TestCase  : integer  := -1;
  shared variable ProcDone  : std_logic_vector(0 to 4) := (others=>'0');
  constant AllProcDone      : std_logic_vector(ProcDone'range)  := (others => '1');
  constant ProcDeserializer : integer := 0;
  constant ProcOutput       : integer := 1;
  constant ProcErrors       : integer := 2;
  constant ProcRdyToRcv     : integer := 3;
  constant ProcPktCount     : integer := 4;

  ------------------------------------------------------------
  -- Test Case Control
  ------------------------------------------------------------
  procedure InitTestCase( signal Clk_i : in  std_logic;
                          signal Rst_o : out std_logic);

  procedure FinishTestCase;

  procedure ControlWaitCompl( signal Clk_i : in  std_logic);

  procedure WaitForCase(         SubCase : in  integer;
                         signal  Clk_i   : in  std_logic);

  ------------------------------------------------------------
  -- Test Functions
  ------------------------------------------------------------
  procedure DesDataGen_Idle (
           NrOfBytes    : integer := 1;
           InSync       : boolean := true;
           IdleWord     : boolean := true;
    signal Clk_i        : in  std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal SyncStatus_i : inout std_logic
  );

  procedure DesDataGen_Packet (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             InjectLenErr : boolean := false;
             InjectCrcErr : boolean := false;
             Checksum     : std_logic_vector(31 downto 0) := x"deadbeef";
      signal Clk_i        : in  std_logic;
      signal DesData_i    : inout std_logic_vector;
      signal SyncStatus_i : inout std_logic
    );

  procedure Reset_Errors (
    signal Clk_i          : in    std_logic;
    signal Rst_Errors_i   : inout std_logic
  );

  procedure RdyToRcv_Expect (
    signal Clk_i        : in std_logic;
    signal Rst_i        : in std_logic;
    signal DesData_i    : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
           Msg          : in    string := ""
  );

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body serdes_pkt_rcvr_ctrl_tb_pkg is

  procedure InitTestCase( signal  Clk_i : in  std_logic;
                          signal  Rst_o : out std_logic) is
  begin
    ProcDone     := (others => '0');
    TestCase     := -1;
    wait until rising_edge(Clk_i);
    Rst_o <= '1';
    wait until rising_edge(Clk_i);
    wait until rising_edge(Clk_i);
    Rst_o <= '0';
    wait until rising_edge(Clk_i);
  end procedure;

  procedure FinishTestCase is
  begin
    TestCase := -1;
    wait for 1 us;
  end procedure;

  procedure ControlWaitCompl( signal Clk_i : in  std_logic) is
  begin
    while ProcDone /= AllProcDone loop
      wait until rising_edge(Clk_i);
    end loop;
    ProcDone := (others => '0');
  end procedure;

  procedure WaitForCase(        SubCase : in  integer;
                         signal Clk_i   : in  std_logic) is
  begin
    while TestCase /= SubCase loop
      wait until rising_edge(Clk_i);
    end loop;
  end procedure;

  procedure DesDataGen_Idle (
           NrOfBytes    : integer := 1;
           InSync       : boolean := true;
           IdleWord     : boolean := true;
    signal Clk_i        : in  std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal SyncStatus_i : inout std_logic
  ) is
    variable Data : std_logic_vector(7 downto 0);
  begin
    if InSync then
      SyncStatus_i <= '1';
    else
      SyncStatus_i <= '0';
    end if;
    if IdleWord then
      Data := DesDataIdleByte_cpk;
    else
      Data := not DesDataIdleByte_cpk;
    end if;
    for i in 0 to NrOfBytes-1 loop
      DesData_i <= Data;
      wait until Clk_i = '1';
    end loop;
  end procedure;

  procedure DesDataGen_Packet (
           PayloadLen   : natural := 32;
           EventNr      : natural :=  0;
           FlagSOE      : boolean := false;
           FlagEOE      : boolean := false;
           InjectLenErr : boolean := false;
           InjectCrcErr : boolean := false;
           Checksum     : std_logic_vector(31 downto 0) := x"deadbeef";
    signal Clk_i        : in  std_logic;
    signal DesData_i    : inout std_logic_vector;
    signal SyncStatus_i : inout std_logic
  ) is
    constant SOF      : t_aslv8(0 to 1) := (SofByte0_cpk, SofByte1_cpk);
    variable FrameLen : t_aslv8(0 to 1);
    variable header   : t_aslv8(0 to TbHdrBytes_cpk-1);
    variable payload  : t_aslv8(0 to PayloadLen-1);
    variable CRC      : t_aslv8(0 to 3);-- := (x"de", x"ad", x"be", x"ef");
    variable Frame    : t_aslv8(0 to 2+2+TbHdrBytes_cpk+PayloadLen+4-1);
  begin
    DataGen_FrameLen(PayloadLen+TbHdrBytes_cpk, InjectLenErr, FrameLen);
    DataGen_Header(PayloadLen, EventNr, FlagSOE, FlagEOE, false, false, false, header);
    DataGen_Payload(PayloadLen, payload);
    DataGen_CRC(header & payload, InjectCrcErr, CRC);
    Frame := SOF & FrameLen & header & payload & CRC;
    SyncStatus_i <= '1';
    for i in 0 to Frame'length-1 loop
      DesData_i <= Frame(i);
      wait until rising_edge(Clk_i);
    end loop;
  end procedure;

  procedure Reset_Errors (
    signal Clk_i          : in    std_logic;
    signal Rst_Errors_i   : inout std_logic
  ) is
  begin
    wait until rising_edge(Clk_i);
    Rst_Errors_i <= '1';
    wait until rising_edge(Clk_i);
    wait until rising_edge(Clk_i);
    Rst_Errors_i <= '0';
    wait until rising_edge(Clk_i);
  end procedure;

  procedure RdyToRcv_Expect (
    signal Clk_i : in std_logic;
    signal Rst_i : in std_logic;
    signal DesData_i : in std_logic_vector;
    signal ReadyToRcv_o : in std_logic;
    signal SyncStatus_i : in std_logic;
    signal BufferFull_i : in std_logic;
           Msg            : in    string := ""
    ) is
    type R2R_State_t is (Idle_s, SOF_s, Rcv_s, HoldData_s, ChkRdyToRcv_s, EnterWaitIdle_s, WaitIdle_s, Done_s);
    variable State, State_next : R2R_State_t := Idle_s;
    variable CycleDelay : integer := 0;
  begin
    while State /= Done_s  and ProcDone(ProcDeserializer) = '0' loop
      wait until rising_edge(Clk_i);
      State := State_next;
      case State is
        when Idle_s =>
          StdlCompare(SyncStatus_i, ReadyToRcv_o, Msg & " while idle: Ready-to-Receive ");
          if DesData_i = SofByte0_cpk then
            State_next := SOF_s;
          elsif DesData_i /= IdleByte_cpk then
            State_next := EnterWaitIdle_s;
          end if;
        when SOF_s =>
          StdlCompare(SyncStatus_i, ReadyToRcv_o, Msg & " while SOF: Ready-to-Receive ");
          if DesData_i = SofByte1_cpk then
            State_next := Rcv_s;
          else
            State_next := EnterWaitIdle_s;
          end if;
        when Rcv_s =>
          StdlCompare(0, ReadyToRcv_o, Msg & " while receiving: Ready-to-Receive ");
          if BufferFull_i = '1' then
            State_next := HoldData_s;
          end if;
        when HoldData_s =>
          StdlCompare(0, ReadyToRcv_o, Msg & " while buffer full: Ready-to-Receive ");
          if BufferFull_i = '0' then
            CycleDelay := 1;
            State_next := ChkRdyToRcv_s;
          end if;
        when ChkRdyToRcv_s =>
          if CycleDelay = 0 then
            StdlCompare(SyncStatus_i, ReadyToRcv_o, Msg & " after readout: Ready-to-Receive ");
            State_next := Done_s;
          end if;
          CycleDelay := CycleDelay - 1;
        when EnterWaitIdle_s =>
          StdlCompare(SyncStatus_i, ReadyToRcv_o, Msg & " wait idle: Ready-to-Receive ");
          State_next := WaitIdle_s;
        when WaitIdle_s =>
          StdlCompare(0, ReadyToRcv_o, Msg & " wait idle: Ready-to-Receive ");
          if DesData_i = IdleByte_cpk then
            State_next := ChkRdyToRcv_s;
          end if;
        when Done_s => null;
        when others => null;
      end case;
      -- Break upon reset
      if Rst_i = '1' then
        State_next := Done_s;
      end if;
    end loop;
  end procedure;

end;
