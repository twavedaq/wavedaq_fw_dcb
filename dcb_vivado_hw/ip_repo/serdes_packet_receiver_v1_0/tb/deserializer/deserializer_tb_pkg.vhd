------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.vcomponents.all;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package deserializer_tb_pkg is

  -- *** Generics Record ***
  type Generics_t is record
    Dummy : boolean; -- required since empty records are not allowed
  end record;

  ------------------------------------------------------------
  -- Not exported Generics
  ------------------------------------------------------------
  constant IdlePattern_cpk     : std_logic_vector(7 downto 0) := x"5A";
  constant DlyRefClk_cpk       : real := 200.0;
  constant BitOrder_cpk        : string := "MSB first";
  constant SDataRate_cpk       : real := 640.0;

  constant EyeWidth_c          : time := 1.000 ns;
  constant BitPeriod_c         : time := 1.5625 ns;-- has to be precisely equal to the generated bit clock

  constant FramePayloadLen_cpk : integer := 500;

  shared variable TestCase     : integer  := -1;
  shared variable ProcDone     : std_logic_vector(0 to 2) := (others=>'0');
  constant AllProcDone         : std_logic_vector(ProcDone'range)  := (others => '1');
  constant ProcCtrlAndStat     : integer := 0;
  constant ProcSerialData      : integer := 1;
  constant ProcParallelData    : integer := 2;

  ------------------------------------------------------------
  -- Test Case Control
  ------------------------------------------------------------
  procedure InitTestCase;

  procedure ApplyReset( signal  Clk_i : in  std_logic;
                        signal  Rst_o : out std_logic);

  procedure FinishTestCase;

  procedure ControlWaitCompl( signal Clk_i : in  std_logic);

  procedure WaitForCase(         SubCase : in  integer;
                         signal  Clk_i   : in  std_logic);

  ------------------------------------------------------------
  -- Test Procedures
  ------------------------------------------------------------
  procedure SerialInputGen (
         Data       : std_logic_vector(7 downto 0);
         BitPeriod  : time := BitPeriod_c;
         EyeWidth   : time := EyeWidth_c;
         BitOrder   : string := BitOrder_cpk; -- "MSB first", "LSB first"
  signal SerialData : inout std_logic
  );

  procedure DesDataGen_Stable (
           NrOfBytes      : integer := 1;
           Data           : std_logic_vector(7 downto 0) := IdlePattern_cpk;
           BitPeriod      : time := BitPeriod_c;
           EyeWidth       : time := EyeWidth_c;
           BitOrder       : string := BitOrder_cpk; -- "MSB first", "LSB first"
      signal SerialData_i : inout std_logic
  );

  procedure DataGen_Frame (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
    variable Data         : out t_aslv8
  );

  procedure DesDataGen_Frame (
        Data            : t_aslv8;
        BitPeriod       : time := BitPeriod_c;
        EyeWidth        : time := EyeWidth_c;
        BitOrder        : string := BitOrder_cpk; -- "MSB first", "LSB first"
    signal SerialData_i : inout std_logic
  );

  procedure WaitSync (
             SyncStartTime : time;
      signal SyncStatus    : in std_logic;
      signal DelayOk       : in std_logic;
      signal BitslipOk     : in std_logic;
             Msg           : in string := ""
  );

  procedure ExpectSync (
             TargetSync      : std_logic := '1';
             TargetDelayOk   : std_logic := '1';
             TargetBitslipOk : std_logic := '1';
      signal Clk             : in std_logic;
      signal SyncStatus      : in std_logic;
      signal DelayOk         : in std_logic;
      signal BitslipOk       : in std_logic;
             Msg             : in string := ""
  );

  procedure ExpectPData (
           TargetData     : t_aslv8;
    signal Clk            : in std_logic;
    signal ParallelData_o : in std_logic_vector;
           Msg            : in string := ""
  );

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body deserializer_tb_pkg is

  procedure InitTestCase is
  begin
    ProcDone     := (others => '0');
    TestCase     := -1;
  end procedure;

  procedure ApplyReset( signal  Clk_i : in  std_logic;
                        signal  Rst_o : out std_logic) is
  begin
    wait until rising_edge(Clk_i);
    Rst_o <= '1';
    wait until rising_edge(Clk_i);
    wait until rising_edge(Clk_i);
    Rst_o <= '0';
    wait until rising_edge(Clk_i);
  end procedure;

  procedure FinishTestCase is
  begin
    TestCase := -1;
    wait for 1 us;
  end procedure;

  procedure ControlWaitCompl( signal Clk_i : in  std_logic) is
  begin
    while ProcDone /= AllProcDone loop
      wait until rising_edge(Clk_i);
    end loop;
    ProcDone := (others => '0');
  end procedure;

  procedure WaitForCase(        SubCase : in  integer;
                         signal Clk_i   : in  std_logic) is
  begin
    while TestCase /= SubCase loop
      wait until rising_edge(Clk_i);
    end loop;
  end procedure;

  procedure SerialInputGen (
           Data       : std_logic_vector(7 downto 0);
           BitPeriod  : time := BitPeriod_c;
           EyeWidth   : time := EyeWidth_c;
           BitOrder   : string := BitOrder_cpk; -- "MSB first", "LSB first"
    signal SerialData : inout std_logic
  ) is
    constant DataValid_Time  : time := EyeWidth;
    constant PreStable_Time  : time := (BitPeriod - EyeWidth)/2;
    constant PostStable_Time : time := BitPeriod - PreStable_Time - DataValid_Time;
  begin
    if BitOrder = "MSB first" then
      for i in 7 downto 0 loop
        -------------------------------------------------------------
        SerialData <= rand_sl; -- Data not valid here
        wait for PreStable_Time;
        SerialData <= Data(i); -- Data valid
        wait for DataValid_Time;
        SerialData <= rand_sl; -- Data not valid here
        wait for PostStable_Time;
        -------------------------------------------------------------
      end loop;
    else
      for i in 0 to 7 loop
        -------------------------------------------------------------
        SerialData <= rand_sl; -- Data not valid here
        wait for PreStable_Time;
        SerialData <= Data(i); -- Data valid
        wait for DataValid_Time;
        SerialData <= rand_sl; -- Data not valid here
        wait for PostStable_Time;
        -------------------------------------------------------------
      end loop;
    end if;
  end procedure;

  procedure DesDataGen_Stable (
        NrOfBytes       : integer := 1;
        Data            : std_logic_vector(7 downto 0) := IdlePattern_cpk;
        BitPeriod       : time := BitPeriod_c;
        EyeWidth        : time := EyeWidth_c;
        BitOrder        : string := BitOrder_cpk; -- "MSB first", "LSB first"
    signal SerialData_i : inout std_logic
    ) is
  begin
    for i in 0 to NrOfBytes-1 loop
      SerialInputGen(Data, BitPeriod, EyeWidth, BitOrder, SerialData_i);
    end loop;
  end procedure;

  procedure DataGen_Frame (
             PayloadLen   : natural := 32;
             EventNr      : natural :=  0;
             FlagSOE      : boolean := false;
             FlagEOE      : boolean := false;
             FlagCrcErr   : boolean := false;
             FlagDtgrmErr : boolean := false;
             FlagOvflErr  : boolean := false;
    variable Data         : out t_aslv8
  ) is
    constant SOF     : t_aslv8(0 to 1) := (SofByte0_cpk, SofByte1_cpk);
    variable header  : t_aslv8(0 to TbHdrBytes_cpk-1);
    variable payload : t_aslv8(0 to PayloadLen-1);
    variable CRC     : t_aslv8(0 to 3);
    --variable Frame   : t_aslv8(0 to 2+TbHdrBytes_cpk+PayloadLen+4-1);
  begin
    DataGen_Header(PayloadLen, EventNr, FlagSOE, FlagEOE, FlagCrcErr, FlagDtgrmErr, FlagOvflErr, header);
    DataGen_Payload(PayloadLen, payload);
    DataGen_CRC(header & payload, false, CRC);
    Data := SOF & header & payload & CRC;
  end procedure;

  procedure DesDataGen_Frame (
        Data            : t_aslv8;
        BitPeriod       : time := BitPeriod_c;
        EyeWidth        : time := EyeWidth_c;
        BitOrder        : string := BitOrder_cpk; -- "MSB first", "LSB first"
    signal SerialData_i : inout std_logic
    ) is
  begin
    for i in Data'range loop
      SerialInputGen(Data(i), BitPeriod, EyeWidth, BitOrder, SerialData_i);
    end loop;
  end procedure;

  procedure WaitSync (
             SyncStartTime : time;
      signal SyncStatus    : in std_logic;
      signal DelayOk       : in std_logic;
      signal BitslipOk     : in std_logic;
             Msg           : in string := ""
  ) is
  begin
    wait until SyncStatus = '1' or ProcDone(ProcSerialData) = '1';
    if SyncStatus = '1' then
      print("> '---> Sync Time " & time'image(now-SyncStartTime));
      if BitslipOk = '1' then
        StdlCompare('1', DelayOk, Msg & " Bitslip OK without valid Delay ");
      end if;
    elsif ProcDone(ProcSerialData) = '1' then
      report "###ERROR###: Sync failed";
    end if;
  end procedure;

  procedure ExpectSync (
             TargetSync      : std_logic := '1';
             TargetDelayOk   : std_logic := '1';
             TargetBitslipOk : std_logic := '1';
      signal Clk             : in std_logic;
      signal SyncStatus      : in std_logic;
      signal DelayOk         : in std_logic;
      signal BitslipOk       : in std_logic;
             Msg             : in string := ""
  ) is
  begin
    wait until rising_edge(Clk);
    StdlCompare(TargetSync, SyncStatus, Msg & " unexpected Sync Status ");
    StdlCompare(TargetDelayOk, DelayOk, Msg & " unexpected Sync Delay Status");
    StdlCompare(TargetBitslipOk, BitslipOk, Msg & " unexpected Sync Bitslip Status");
  end procedure;

  procedure ExpectPData (
           TargetData     : t_aslv8;
    signal Clk            : in std_logic;
    signal ParallelData_o : in std_logic_vector;
           Msg            : in string := ""
  ) is
    variable DataReceived : boolean := false;
    variable i            : integer := 0;
  begin
    while i < TargetData'length and ProcDone(ProcSerialData) = '0' loop
      wait until rising_edge(Clk);
      StdlvCompareStdlv(TargetData(i), ParallelData_o, Msg & " Expect Parallel Data ");
      i := i + 1;
    end loop;
    wait until rising_edge(Clk);
    IntCompare(TargetData'length, i, Msg & " Data Incomplete ");
  end procedure;

end;
