------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.vcomponents.all;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_common_math_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.deserializer_tb_pkg.all;

library work;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package deserializer_tb_case_link_idle is

  procedure serial_data (
    signal Clk_i : in std_logic;
    signal SerialData_i : inout std_logic);

  procedure ctrl_and_stat (
    signal DivClk_i : in std_logic;
    signal Rst_o : inout std_logic;
    signal Resync_i : inout std_logic;
    signal SyncDone_o : in std_logic;
    signal DelayOk_o : in std_logic;
    signal BitslipOk_o : in std_logic;
    signal LinkIdle_o : in std_logic);

  procedure parallel_data (
    signal DivClk_i : in std_logic;
    signal Rst_o : in std_logic;
    signal Resync_i : in std_logic;
    signal SyncDone_o : in std_logic;
    signal ParallelData_o : in std_logic_vector);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body deserializer_tb_case_link_idle is

  procedure serial_data (
    signal Clk_i : in  std_logic;
    signal SerialData_i : inout std_logic) is
    constant TimeOffsetStep : time := 190 ps;
    constant EyeWidthStep   : time := 200 ps;
  begin
    print(">>> -- Stable Valid Idle Pattern --");
    InitTestCase;
    TestCase := 0;
    for eye in 1 to 7 loop
      for offs in 0 to 7 loop
        print("> Case " & integer'image(TestCase) & ": Eye Width " & time'image(eye*EyeWidthStep) & " / Offset " & time'image(offs*TimeOffsetStep));
        wait for offs*TimeOffsetStep;
        DesDataGen_Stable (NrOfBytes=>2000, EyeWidth=>eye*EyeWidthStep, SerialData_i=>SerialData_i);
        ProcDone(ProcSerialData) := '1';
        ControlWaitCompl(Clk_i);
        TestCase := TestCase + 1;
      end loop;
    end loop;
    print(">>> Stable Valid Idle Pattern done <<<");
    print("");

    print(">>> -- Stable Valid Idle Pattern (Resync) --");
    InitTestCase;
    TestCase := 56;
    DesDataGen_Stable(NrOfBytes=>4000, EyeWidth=>1.000 ns, SerialData_i=>SerialData_i);
    ProcDone(ProcSerialData) := '1';
    ControlWaitCompl(Clk_i);
    print(">>> Stable Valid Idle Pattern (Resync) done <<<");
    print("");

    print(">>> -- Stable Invalid Idle Pattern --");
    InitTestCase;
    TestCase := 57;
    DesDataGen_Stable(NrOfBytes=>2000, Data=>x"F0", EyeWidth=>1.000 ns, SerialData_i=>SerialData_i);
    ProcDone(ProcSerialData) := '1';
    ControlWaitCompl(Clk_i);
    print(">>> Stable Invalid Idle Pattern done <<<");
    print("");

    FinishTestCase;

  end procedure;

  procedure ctrl_and_stat (
    signal DivClk_i : in std_logic;
    signal Rst_o : inout std_logic;
    signal Resync_i : inout std_logic;
    signal SyncDone_o : in std_logic;
    signal DelayOk_o : in std_logic;
    signal BitslipOk_o : in std_logic;
    signal LinkIdle_o : in std_logic) is
    variable CurrentTestCase : integer := 0;
  begin
    -- Stable Idle Pattern --
    for eye in 1 to 7 loop
      for offs in 0 to 7 loop
        WaitForCase(CurrentTestCase, DivClk_i);
        ApplyReset(DivClk_i, Rst_o);
        WaitSync(now, SyncDone_o, DelayOk_o, BitslipOk_o, "Stable Idle Pattern: when waiting for Sync ");
        ExpectSync('1', '1', '1', DivClk_i, SyncDone_o, DelayOk_o, BitslipOk_o, " after successful Sync ");
        ProcDone(ProcCtrlAndStat) := '1';
        CurrentTestCase := CurrentTestCase + 1;
      end loop;
    end loop;

    -- Stable Idle Pattern (Resync) --
    WaitForCase(56, DivClk_i);
    ApplyReset(DivClk_i, Rst_o);
    WaitSync(now, SyncDone_o, DelayOk_o, BitslipOk_o, " Resync: when waiting for first Sync ");
    StdlCompare('1', SyncDone_o, "Resync: first Sync failed ");
    if SyncDone_o = '1' then
      Resync_i <= '1';
      wait until rising_edge(DivClk_i);
      Resync_i <= '0';
      wait until rising_edge(DivClk_i);
    end if;
    WaitSync(now, SyncDone_o, DelayOk_o, BitslipOk_o, " Resync: when waiting for second Sync ");
    ExpectSync('1', '1', '1', DivClk_i, SyncDone_o, DelayOk_o, BitslipOk_o, "Resync: after successful Sync ");
    ProcDone(ProcCtrlAndStat) := '1';

      -- Stable Invalid Idle Pattern --
    WaitForCase(57, DivClk_i);
    ApplyReset(DivClk_i, Rst_o);
    WaitSync(now, SyncDone_o, DelayOk_o, BitslipOk_o, " Stable Invalid Idle Pattern: when waiting for Sync ");
    ExpectSync('1', '0', '0', DivClk_i, SyncDone_o, DelayOk_o, BitslipOk_o, "Stable Invalid Idle Pattern: ");
    ProcDone(ProcCtrlAndStat) := '1';

  end procedure;

  procedure parallel_data (
    signal DivClk_i : in std_logic;
    signal Rst_o : in std_logic;
    signal Resync_i : in std_logic;
    signal SyncDone_o : in std_logic;
    signal ParallelData_o : in std_logic_vector) is
    variable CurrentTestCase : integer := 0;
    variable EmptyData       : t_aslv8(0 downto 1);
  begin
    -- Stable Idle Pattern --
    for eye in 1 to 7 loop
      for offs in 0 to 7 loop
        WaitForCase(CurrentTestCase, DivClk_i);
        wait until SyncDone_o = '0';
        wait until SyncDone_o = '1';
        while ProcDone(ProcSerialData) = '0' loop
          StdlvCompareStdlv(IdlePattern_cpk, ParallelData_o, "Stable Idle Pattern (Case " & integer'image(CurrentTestCase) & "): Idle Data ");
          wait until rising_edge(DivClk_i);
        end loop;
        ProcDone(ProcParallelData) := '1';
        CurrentTestCase := CurrentTestCase + 1;
      end loop;
    end loop;

    -- Stable Idle Pattern (Resync) --
    WaitForCase(56, DivClk_i);
    wait until SyncDone_o = '0';
    wait until SyncDone_o = '1';
    while ProcDone(ProcSerialData) = '0' and Resync_i = '0' loop
      StdlvCompareStdlv(IdlePattern_cpk, ParallelData_o, "Resync: Idle Data ");
      wait until rising_edge(DivClk_i);
    end loop;
    wait until SyncDone_o = '1';
    while ProcDone(ProcSerialData) = '0' loop
      StdlvCompareStdlv(IdlePattern_cpk, ParallelData_o, "Resync: Idle Data ");
      wait until rising_edge(DivClk_i);
    end loop;
    ProcDone(ProcParallelData) := '1';

    -- Stable Invalid Idle Pattern --
    WaitForCase(57, DivClk_i);
    -- No data checks since sync is not expected to succeed
    ProcDone(ProcParallelData) := '1';
  end procedure;

end;
