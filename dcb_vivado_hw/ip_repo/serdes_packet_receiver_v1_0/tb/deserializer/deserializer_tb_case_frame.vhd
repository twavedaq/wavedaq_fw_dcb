------------------------------------------------------------
-- Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.vcomponents.all;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;

library work;
  use work.serdes_pkt_rcvr_tb_common_pkg.all;
  use work.deserializer_tb_pkg.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;
  use work.psi_tb_activity_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package deserializer_tb_case_frame is

  procedure serial_data (
    signal Clk_i : in std_logic;
    signal SerialData_i : inout std_logic);

  procedure ctrl_and_stat (
    signal DivClk_i : in std_logic;
    signal Rst_o : inout std_logic;
    signal Resync_i : inout std_logic;
    signal SyncDone_o : in std_logic;
    signal DelayOk_o : in std_logic;
    signal BitslipOk_o : in std_logic;
    signal LinkIdle_o : in std_logic);

  procedure parallel_data (
    signal DivClk_i : in std_logic;
    signal Rst_o : in std_logic;
    signal Resync_i : in std_logic;
    signal SyncDone_o : in std_logic;
    signal ParallelData_o : in std_logic_vector);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body deserializer_tb_case_frame is

  procedure serial_data (
    signal Clk_i : in std_logic;
    signal SerialData_i : inout std_logic) is
    constant FrameSize : integer := 2+24+FramePayloadLen_cpk+4; -- SOF+header+payload+CRC
    variable FrameData : t_aslv8(0 to FrameSize-1);
  begin
    print(">>> -- Frame Reception --");
    InitTestCase;
    TestCase := 0;
    wait for 400 ps; -- time offset
    DesDataGen_Stable (NrOfBytes=>1200, EyeWidth=>400 ps, SerialData_i=>SerialData_i);
    DataGen_Frame(FramePayloadLen_cpk, 16#00C0FFEE#, false, false, false, false, false, FrameData);
    DesDataGen_Frame (Data=>FrameData, EyeWidth=>400 ps, SerialData_i=>SerialData_i);
    DesDataGen_Stable (NrOfBytes=>100, EyeWidth=>400 ps, SerialData_i=>SerialData_i);
    ProcDone(ProcSerialData) := '1';
    ControlWaitCompl(Clk_i);
    print(">>> Frame reception done <<<");
    print("");

    FinishTestCase;

  end procedure;

  procedure ctrl_and_stat (
    signal DivClk_i : in std_logic;
    signal Rst_o : inout std_logic;
    signal Resync_i : inout std_logic;
    signal SyncDone_o : in std_logic;
    signal DelayOk_o : in std_logic;
    signal BitslipOk_o : in std_logic;
    signal LinkIdle_o : in std_logic) is
  begin
    -- Frame Reception
    WaitForCase(0, DivClk_i);
    ApplyReset(DivClk_i, Rst_o);
    WaitSync(now, SyncDone_o, DelayOk_o, BitslipOk_o, "Frame Reception: when waiting for first Sync ");
    ExpectSync('1', '1', '1', DivClk_i, SyncDone_o, DelayOk_o, BitslipOk_o, " after Sync done (Frame Reception) ");
    ProcDone(ProcCtrlAndStat) := '1';
  end procedure;

  procedure parallel_data (
    signal DivClk_i : in std_logic;
    signal Rst_o : in std_logic;
    signal Resync_i : in std_logic;
    signal SyncDone_o : in std_logic;
    signal ParallelData_o : in std_logic_vector) is
--    constant FrameSize : integer := 2+24+32+4; -- SOF+header+payload+CRC
    constant FrameSize : integer := 2+24+FramePayloadLen_cpk+4; -- SOF+header+payload+CRC
    variable TargetData : t_aslv8(0 to FrameSize-1);
  begin
    -- Frame Reception
    WaitForCase(0, DivClk_i);
    wait until SyncDone_o = '1';
--    DataGen_Frame(32, 16#00C0FFEE#, false, false, false, false, false, TargetData);
    DataGen_Frame(FramePayloadLen_cpk, 16#00C0FFEE#, false, false, false, false, false, TargetData);
    -- wait for frame to start
    wait until ParallelData_o /= IdlePattern_cpk;
    print("> Checking received data...");
    ExpectPData(TargetData, DivClk_i, ParallelData_o, "Frame Reception: ");
    -- check if idle after frame
    StdlvCompareStdlv(IdlePattern_cpk, ParallelData_o, "Frame Reception: check idle after frame ");
    ProcDone(ProcParallelData) := '1';
  end procedure;

end;
