# General Information

## Maintainer
Elmar Schmid [elmar.schmid@psi.ch]

## License
TBD

## Detailed Documentation
TBD

# Description
This IP core is developed for the Data Concentrator Board (DCB) of the WaveDAQ used in the MEGII project.
It is used to receive data packets from WaveDream Boards (WDB) and Trigger Control Boards (TCB) in a WaveDAQ
crate with using ISERDES interfaces on standard FPGA IOs. The core handels the da stream synchronization by
adjusting the IDelay as well as the bitslip of the corresponding ISERDES when the link is idle. Further more
it checks the frame length and CRC of incomming packets and writes the packets to a FIFO to make it
available to a subsequent core via an AXI Stream interface. The data is handled on a packet by packet base.
Extra signals on top of the AXI Stream interface are used to provide parameters of a full packet.
Backpreassure handling is also packet related. A READY_TO_RECEIVE signal is used to tell the sender
(WDB or TCB) if a packet can be transmitted.

# Dependencies

PSI common Library:
* psi_common_sync_fifo
* psi_common_tdm_par
* psi_common_bit_cc

PSI 3205 Library:
* crc32_d8

# Simulations and Testbenches

A regression test script for Modelsim is present. To run the regression test, execute the following command
in modelsim from within the directory *sim*

```
source ./run.tcl
```
