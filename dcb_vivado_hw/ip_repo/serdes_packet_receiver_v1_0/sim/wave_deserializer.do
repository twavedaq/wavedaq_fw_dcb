onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Testbench /deserializer_tb/TbRunning
add wave -noupdate -expand -group Testbench /deserializer_tb/AllProcessesDone_c
add wave -noupdate -expand -group Testbench -expand /deserializer_tb/ProcessDone
add wave -noupdate -expand -group Testbench /deserializer_tb/BitClk_Serdes_i
add wave -noupdate -expand -group Testbench /deserializer_tb/SerialData_i
add wave -noupdate -expand -group Testbench /deserializer_tb/DivClk_i
add wave -noupdate -expand -group Testbench /deserializer_tb/DlySyncDone_o
add wave -noupdate -expand -group Testbench /deserializer_tb/SyncDone_o
add wave -noupdate -expand -group Testbench /deserializer_tb/InSync_o
add wave -noupdate -expand -group Testbench /deserializer_tb/LinkIdle_o
add wave -noupdate -expand -group Testbench /deserializer_tb/NextCase
add wave -noupdate -expand -group Testbench /deserializer_tb/ParallelData_o
add wave -noupdate -expand -group Testbench /deserializer_tb/Resync_i
add wave -noupdate -expand -group Testbench /deserializer_tb/Rst_i
add wave -noupdate -expand -group Testbench /deserializer_tb/TbProcNr_ctrl_and_stat_c
add wave -noupdate -expand -group Testbench /deserializer_tb/TbProcNr_parallel_data_c
add wave -noupdate -expand -group Testbench /deserializer_tb/TbProcNr_serial_data_c
add wave -noupdate -expand -group Testbench /deserializer_tb/i_dut/PatternDetect
add wave -noupdate -expand -group Testbench /deserializer_tb/i_dut/TestEye_o
add wave -noupdate -expand -group Testbench /deserializer_tb/i_dut/TestTap_o
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/Deser_PData
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/DlySyncDone_o
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/LinkIdle_o
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/ParallelData
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/ParallelData_o
add wave -noupdate -expand -group Deserializer -expand /deserializer_tb/i_dut/r
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/SerialDataDelayed
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/SerialDataIn
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/SyncDone_o
add wave -noupdate -expand -group Deserializer /deserializer_tb/i_dut/TargetTap
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/C
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/CE
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/CINVCTRL
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/CINVCTRL_SEL
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/CNTVALUEIN
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/CNTVALUEOUT
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/DATAIN
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/DATAOUT
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/IDATAIN
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/INC
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/LD
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/LDPIPEEN
add wave -noupdate -expand -group IDelay /deserializer_tb/i_dut/IDELAYE2_i/REGRST
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/BITSLIP
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CE1
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CE2
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CLK
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CLKB
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CLKDIV
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/CLKDIVP
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/D
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/DDLY
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/DYNCLKDIVSEL
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/DYNCLKSEL
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/O
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/OCLK
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/OCLKB
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/OFB
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q1
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q2
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q3
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q4
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q5
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q6
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q7
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/Q8
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/RST
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SHIFTIN1
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SHIFTIN2
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SHIFTOUT1
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SHIFTOUT2
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SRVAL_Q1
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SRVAL_Q2
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SRVAL_Q3
add wave -noupdate -expand -group ISERDES /deserializer_tb/i_dut/ISERDESE2_inst/SRVAL_Q4
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1486825000 ps} 0} {{Cursor 2} {4187500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1680 us}
