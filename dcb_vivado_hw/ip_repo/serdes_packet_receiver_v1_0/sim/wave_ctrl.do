onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/Clk_i
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/Rst_i
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group Testbench -divider {TB Control}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbRunning
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_deserializer_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_error_out_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_output_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ProcessDone
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/AllProcessesDone_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/NextCase
add wave -noupdate -expand -group Testbench -divider Input
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Clk_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/DesData_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ReadyToRcv_o
add wave -noupdate -expand -group Testbench -divider {Byte to Word Input}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ByteToWord_Rst
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din_Rdy
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din_Vld
add wave -noupdate -expand -group Testbench -divider {Byte to Word - Fifo}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64_Rdy
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64_Vld
add wave -noupdate -expand -group Testbench -divider Fifo
add wave -noupdate -expand -group Testbench -divider {Output Interface}
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_ctrl_tb/Axis_Data_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Axis_Rdy_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Axis_Vld_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_ctrl_tb/PktParam_o
add wave -noupdate -expand -group Testbench -divider {Other Controls}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/SyncStatus_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/RcvPktCount_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_RcvPktCount_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_Errors_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_CRC_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/CRC_Ok_i
add wave -noupdate -expand -group Testbench -childformat {{/serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.CRC_ErrCount -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Frame_ErrCount -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Datagram_ErrCount -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Sync_ErrCount -radix unsigned}} -expand -subitemconfig {/serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.CRC_ErrCount {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Frame_ErrCount {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Datagram_ErrCount {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/Error_Flags_o.Sync_ErrCount {-height 16 -radix unsigned}} /serdes_pkt_rcvr_ctrl_tb/Error_Flags_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrLock
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrLock_B2W
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrLock
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Last_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Rdy_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Vld_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Buffer_Rst_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/BufferFull_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/ByteToWord_Rst_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Clk_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_Err
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_Ok_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_Vld_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_Err
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/DesData_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/DesDataS
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Error_Flags_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/FlagsReady_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/FlagsValid_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_Err
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrLock_B2W
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/HdrCountSize_c
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/PktCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/RcvPktCount_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/PktErrorFlags_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/PktParam_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/r
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/ReadyToRcv_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Rst_CRC_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Rst_Errors_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Rst_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Rst_RcvPktCount_i
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/SetPktErrCrc
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/SetPktErrDatagram
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Sync_Err
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Sync_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/SyncDone_i
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/i_dut/r.EventNrChange
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/i_dut/r.EventNrChange
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/Clk_i
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/Rst_i
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/FlagsReady_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/InData_i
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/OutBytes_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/OutData_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/OutLast_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/OutPktParam_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/OutValid_o
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/PktValid
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/r
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/r_next
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/RamRdData
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/RamWr
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/RamWrAddr
add wave -noupdate -expand -group {packet buffer} /serdes_pkt_rcvr_ctrl_tb/pkt_buffer_i/RamWrData
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/Tdm
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Vld_o
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Rdy_i
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Last_o
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/Parallel
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/ParallelVld
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/ParallelLast
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/Data64_Rdy
add wave -noupdate -expand -group {byte to word64 buffer} -expand /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/r
add wave -noupdate -expand -group {byte to word64 buffer} -expand /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/r_next
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group crc32 /serdes_pkt_rcvr_ctrl_tb/i_crc32/CRC_O
add wave -noupdate -expand -group crc32 /serdes_pkt_rcvr_ctrl_tb/i_crc32/CRC_VALID_O
add wave -noupdate -expand -group crc32 /serdes_pkt_rcvr_ctrl_tb/i_crc32/crc
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {362500 ps} 0} {{Cursor 2} {159825395 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {204712 ps} {665313 ps}
