onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/Clk_i
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/Rst_i
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group Testbench -divider {TB Control}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbRunning
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_deserializer_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_error_out_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/TbProcNr_output_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ProcessDone
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/AllProcessesDone_c
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/NextCase
add wave -noupdate -expand -group Testbench -divider Input
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Clk_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/DesData_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din_Sel
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ReadyToRcv_o
add wave -noupdate -expand -group Testbench -divider {Byte to Word Input}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/ByteToWord_Rst
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din_Rdy
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/B2W_Din_Vld
add wave -noupdate -expand -group Testbench -divider {Byte to Word - Fifo}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64_Rdy
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Data64_Vld
add wave -noupdate -expand -group Testbench -divider Fifo
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Fifo_Rst
add wave -noupdate -expand -group Testbench -divider {Output Interface}
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_ctrl_tb/Axis_Data_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Axis_Rdy_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Axis_Vld_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_ctrl_tb/PktParam_o
add wave -noupdate -expand -group Testbench -divider {Other Controls}
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/Rst_Errors_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/CRC_Error_i
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_ctrl_tb/Error_Flags_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrLock
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrLock
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrLock
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/i_dut/Sync_ErrLock
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_ctrl_tb/SyncStatus_i
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/ReadyToRcv_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Sel_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/B2W_Din_Vld_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Fifo_Rst_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/ByteToWord_Rst_o
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/PktBytes
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/PktWords
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Frame_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Datagram_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/Sync_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/Sync_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -radix unsigned /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrCount
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl /serdes_pkt_rcvr_ctrl_tb/i_dut/CRC_ErrLock
add wave -noupdate -expand -group serdes_pkt_rcvr_ctrl -childformat {{/serdes_pkt_rcvr_ctrl_tb/i_dut/r.SampleCount -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/i_dut/r.FillerCount -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/i_dut/r.PayloadLen -radix unsigned} {/serdes_pkt_rcvr_ctrl_tb/i_dut/r.SampleBytes -radix unsigned}} -expand -subitemconfig {/serdes_pkt_rcvr_ctrl_tb/i_dut/r.SampleCount {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/i_dut/r.FillerCount {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/i_dut/r.PayloadLen {-height 16 -radix unsigned} /serdes_pkt_rcvr_ctrl_tb/i_dut/r.SampleBytes {-height 16 -radix unsigned}} /serdes_pkt_rcvr_ctrl_tb/i_dut/r
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/i_dut/r.EventNrChange
add wave -noupdate /serdes_pkt_rcvr_ctrl_tb/i_dut/r.EventNrChange
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/InData
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/InRdy
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/Full
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/AlmFull
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/InLevel
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/Empty
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/AlmEmpty
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/OutLevel
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/r
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/r_next
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/RamWr
add wave -noupdate -expand -group {packet fifo} /serdes_pkt_rcvr_ctrl_tb/i_pkt_fifo/RamRdAddr
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/Tdm
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/TdmRdy
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/Parallel
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/ParallelVld
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/r
add wave -noupdate -expand -group {byte to word64 buffer} /serdes_pkt_rcvr_ctrl_tb/i_ByteToWord64/r_next
add wave -noupdate -divider <NULL>
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2206250 ps} 0} {{Cursor 2} {159825395 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2157026 ps} {2349945 ps}
