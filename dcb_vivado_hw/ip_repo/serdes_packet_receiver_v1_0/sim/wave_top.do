onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/TbRunning
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ProcessDone
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/NextCase
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Rst_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/BitClk_Serdes_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/DivClk_Serdes_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/DivClk_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/SerialData_i_n
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/SerialData_i_p
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ReadyToRcv_o_n
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ReadyToRcv_o_p
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/LinkIdle_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_tb/Axis_Data_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Axis_Rdy_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Axis_Vld_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_tb/Error_Flags_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/PktParam_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Resync_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Rst_Errors_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/TbRunning
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ProcessDone
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/NextCase
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Rst_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/BitClk_Serdes_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/DivClk_Serdes_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/DivClk_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/SerialData_i_n
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/SerialData_i_p
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ReadyToRcv_o_n
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/ReadyToRcv_o_p
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/LinkIdle_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_tb/Axis_Data_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Axis_Rdy_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Axis_Vld_o
add wave -noupdate -expand -group Testbench -expand /serdes_pkt_rcvr_tb/Error_Flags_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/PktParam_o
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Resync_i
add wave -noupdate -expand -group Testbench /serdes_pkt_rcvr_tb/Rst_Errors_i
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SyncDone_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Data_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Rdy_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Vld_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Rdy
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Rst
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/CRC_Ok
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/CRC_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data8
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64_Rdy
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/BitClk_Serdes_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/DivClk_Serdes_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/DivClk_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/LinkIdle_o
add wave -noupdate -expand -group DUT -radix unsigned /serdes_pkt_rcvr_tb/i_dut/RcvPktCount_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_RcvPktCount_i
add wave -noupdate -expand -group DUT -expand /serdes_pkt_rcvr_tb/i_dut/Error_Flags_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/PktParam_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/ReadyToRcv_o_n
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/ReadyToRcv_o_p
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SyncOk
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/BitslipOk
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/DelayOk
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Resync_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_CRC
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_Errors_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SerialData_i_n
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SerialData_i_p
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SyncDone_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Data_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Rdy_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Axis_Vld_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Rdy
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/B2W_Rst
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/CRC_Ok
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/CRC_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data8
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64_Rdy
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Data64_Vld
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/BitClk_Serdes_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/DivClk_Serdes_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/DivClk_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/LinkIdle_o
add wave -noupdate -expand -group DUT -expand /serdes_pkt_rcvr_tb/i_dut/Error_Flags_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/PktParam_o
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/ReadyToRcv_o_n
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/ReadyToRcv_o_p
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Resync_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_CRC
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_Errors_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/Rst_i
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SerialData_i_n
add wave -noupdate -expand -group DUT /serdes_pkt_rcvr_tb/i_dut/SerialData_i_p
add wave -noupdate -divider <NULL>
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/B2W_Din_Vld_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/ByteToWord_Rst_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_ErrLock
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_Vld_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_ErrLock
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/DesData_i
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/DesDataS
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_ErrLock_B2W
add wave -noupdate -group Control -expand /serdes_pkt_rcvr_tb/i_dut/control_i/r
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/ReadyToRcv_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Rst_CRC_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Sync_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Sync_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/B2W_Din_Vld_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/ByteToWord_Rst_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_ErrLock
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/CRC_Vld_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Datagram_ErrLock
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/DesData_i
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/DesDataS
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Frame_ErrLock_B2W
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/ReadyToRcv_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Rst_CRC_o
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Sync_Err
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/control_i/Sync_ErrCount
add wave -noupdate -group Control /serdes_pkt_rcvr_tb/i_dut/Rst_RcvPktCount_i
add wave -noupdate -group Control -radix unsigned /serdes_pkt_rcvr_tb/i_dut/RcvPktCount_o
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Deser_PData
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DivClk_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DlyRefClk_cgn
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DlyWaitTicks_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/LinkIdle_o
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/MaxTaps_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/MaxTapsU_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/ParallelData
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/ParallelData_o
add wave -noupdate -expand -group Deserializer -expand /serdes_pkt_rcvr_tb/i_dut/deserializer_i/r
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Resync_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Rst_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialData_i_n
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialData_i_p
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialDataDelayed
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialDataIn
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SyncDone_o
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/TargetTap
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Deser_PData
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DivClk_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DlyRefClk_cgn
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/DlyWaitTicks_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/LinkIdle_o
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/MaxTaps_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/MaxTapsU_c
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/ParallelData
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/ParallelData_o
add wave -noupdate -expand -group Deserializer -expand /serdes_pkt_rcvr_tb/i_dut/deserializer_i/r
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Resync_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/Rst_i
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialData_i_n
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialData_i_p
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialDataDelayed
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SerialDataIn
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/SyncDone_o
add wave -noupdate -expand -group Deserializer /serdes_pkt_rcvr_tb/i_dut/deserializer_i/TargetTap
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/Tdm
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Vld
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/TdmRdy
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/B2W_Din_Last
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/Parallel
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/ParallelVld
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/Data64_Rdy
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/ParallelLast
add wave -noupdate -expand -group {Byte to Word} -expand /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/r
add wave -noupdate -expand -group {Byte to Word} /serdes_pkt_rcvr_tb/i_dut/ByteToWord64_i/r_next
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/BufferFull_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/BufferOverflow_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/Clk_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/FlagsReady_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/FlagsValid_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/InData_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/InLast_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/InPktParam_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/InValid_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutBytes_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutData_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutLast_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutPktParam_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutReady_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/OutValid_o
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/PktErrorFlags_i
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/PktValid
add wave -noupdate -expand -group {Packet Buffer} -expand /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/r
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/r_next
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamRdAddr
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamRdData
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamWr
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamWrAddr
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamWrData
add wave -noupdate -expand -group {Packet Buffer} /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/Rst_i
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/Behavior_g
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/Depth_g
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/IsAsync_g
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/RamStyle_g
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/Width_g
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/RdAddr
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/RdData
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/WrAddr
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/i_ram/WrData
add wave -noupdate -expand -group RAM /serdes_pkt_rcvr_tb/i_dut/pkt_buffer_i/RamWr
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/crc
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/CRC_O
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/CRC_VALID_O
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/DATA_I
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/crc
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/CRC_O
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/CRC_VALID_O
add wave -noupdate -expand -group CRC /serdes_pkt_rcvr_tb/i_dut/i_crc32/DATA_I
add wave -noupdate -divider <NULL>
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21205905 ps} 0} {{Cursor 2} {5300000 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {173250 ns}
