##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

#Constants
set LibPath "../../../../library/VHDL"

#Import psi::sim
namespace import psi::sim::*

#Set library
add_library serdes_pkt_rcvr

#suppress messages
compile_suppress 135,1236,1073,1246
run_suppress 8684,3479,3813,8009,3812

# Library
add_sources $LibPath {
	psi_tb/hdl/psi_tb_txt_util.vhd \
	psi_tb/hdl/psi_tb_compare_pkg.vhd \
	psi_tb/hdl/psi_tb_activity_pkg.vhd \
	psi_common/hdl/psi_common_array_pkg.vhd \
	psi_common/hdl/psi_common_math_pkg.vhd \
	psi_common/hdl/psi_common_logic_pkg.vhd \
	psi_common/hdl/psi_common_sdp_ram.vhd \
	psi_common/hdl/psi_common_sync_fifo.vhd \
	psi_common/hdl/psi_common_tdp_ram.vhd \
	psi_3205_v1_0/crc32_d8.vhd \
} -tag lib

# project sources
add_sources "../hdl" {
	tdm_par_fill.vhd \
	serdes_pkt_rcvr_pkg.vhd \
	serdes_pkt_buffer.vhd \
	serdes_pkt_rcvr_ctrl.vhd \
	deserializer.vhd
	serdes_pkt_rcvr.vhd \
} -tag src

# testbenches
add_sources "../tb" {
	serdes_pkt_rcvr_ctrl/crc_tb_pkg.vhd \
	serdes_pkt_rcvr_tb_common_pkg.vhd \
	serdes_pkt_rcvr_ctrl/serdes_pkt_rcvr_ctrl_tb_pkg.vhd \
	serdes_pkt_rcvr_ctrl/serdes_pkt_rcvr_ctrl_tb_case_frame_valid.vhd \
	serdes_pkt_rcvr_ctrl/serdes_pkt_rcvr_ctrl_tb_case_errors.vhd \
	serdes_pkt_rcvr_ctrl/serdes_pkt_rcvr_ctrl_tb.vhd \
	deserializer/deserializer_tb_pkg.vhd \
	deserializer/deserializer_tb_case_link_idle.vhd \
	deserializer/deserializer_tb_case_frame.vhd \
	deserializer/deserializer_tb.vhd \
	serdes_pkt_rcvr/serdes_pkt_rcvr_tb_pkg.vhd \
	serdes_pkt_rcvr/serdes_pkt_rcvr_tb_case_valid_data.vhd \
	serdes_pkt_rcvr/serdes_pkt_rcvr_tb_case_errors.vhd \
	serdes_pkt_rcvr/serdes_pkt_rcvr_tb.vhd \
} -tag tb

#TB Runs
create_tb_run "serdes_pkt_rcvr_ctrl_tb"
add_tb_run

create_tb_run "deserializer_tb"
add_tb_run

create_tb_run "serdes_pkt_rcvr_tb"
add_tb_run
