------------------------------------------------------------------------------
--  Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Elmar Schmid
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Description
------------------------------------------------------------------------------
-- This component receives a WDAQ packet via a serdes links and stores it in
-- a fifo buffer where it can be fetched for further prcessing or transmission.
-- The flag handling at the output is packet based, i.e. the valid flag goes
-- high after a full packet is stored.
-- If there is a CRC error during reception, an error flag is set, an error
-- counter is incremented and the buffer is cleared.

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

Library UNISIM;
  use UNISIM.vcomponents.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
-- $$ testcases=valid_data, errors $$
-- $$ processes=control,input,output,status $$
-- $$ tbpkg=work.psi_tb_txt_util,work.psi_tb_compare_pkg,work.psi_tb_activity_pkg $$
entity serdes_pkt_rcvr is
  generic (
    BitOrder_cgn       : string := "MSB first";                   -- "MSB first", "LSB first" $$ constant="MSB first" $$
    IdlePattern_cgn    : std_logic_vector(7 downto 0) := x"5A";   -- $$ constant=x"5A" $$
    SDataRate_cgn      : real := 640.0;                           -- Serial Data Rate in [MB/s]   $$ constant=640.0 $$
    DlyRefClk_cgn      : real := 200.0;                           -- IDelay Reference Clock [MHz] $$ constant=200.0 $$
    FifoDepth_cgn      : positive := MaxPktWords_cpk              -- $$ constant=1122 $$
  );
  port (
    TestEye_o          : out std_logic_vector(19 downto 0);
    TestTap_o          : out std_logic_vector( 4 downto 0);
    -- Control signals
    BitClk_Serdes_i    : in  std_logic;       -- $$ type=clk; freq=640e6; proc=input $$
    DivClk_Serdes_i    : in  std_logic;       -- $$ type=clk; freq=80e6;  proc=input $$
    DivClk_i           : in  std_logic;       -- $$ type=clk; freq=80e6;  proc=control,output,status $$
    Rst_i              : in  std_logic;       -- $$ type=rst; clk=DivClk_i; proc=control $$
    Resync_i           : in  std_logic;       -- $$ proc=control $$

    -- Status
    Error_Flags_o      : out Error_t;         -- $$ proc=status $$
    Rst_Errors_i       : in std_logic;        -- $$ proc=control $$
    RcvPktCount_o      : out std_logic_vector(PktCountBits_cpk-1 downto 0); -- $$ proc=pkt_count_out $$
    Rst_RcvPktCount_i  : in  std_logic;       -- $$ proc=pkt_count_out $$
    SyncDone_o         : out std_logic;       -- $$ proc=control, status $$
    BitslipOk_o        : out std_logic;       -- $$ proc=status $$
    DelayOk_o          : out std_logic;       -- $$ proc=status $$
    LinkIdle_o         : out std_logic;       -- $$ proc=status $$

    -- AXI Stream Data output
    Axis_Data_o        : out Output_Data_t;   -- $$ proc=output $$
    Axis_Vld_o         : out std_logic;       -- $$ proc=output $$
    Axis_Rdy_i         : in  std_logic;       -- $$ proc=output $$
    -- Output packet parameters
    PktParam_o         : out Pkt_Param_t;     -- $$ proc=output $$

    -- Debug Signals
    ParallelData_Dbg_o : out std_logic_vector(7 downto 0);
    CrcOk_Dbg_o        : out std_logic;

    -- SERDES
    SerialData_i_p     : in  std_logic;       -- $$ proc=input $$
    SerialData_i_n     : in  std_logic;       -- $$ proc=input $$
    ReadyToRcv_o_p     : out std_logic;       -- $$ proc=input $$
    ReadyToRcv_o_n     : out std_logic        -- $$ proc=input $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of serdes_pkt_rcvr is

  signal Data8          : std_logic_vector( 7 downto 0) := (others=>'0');
  signal Data64         : std_logic_vector(63 downto 0) := (others=>'0');
  signal Data64_Vld     : std_logic := '0';
  signal Data64_Rdy     : std_logic := '0';
  signal Data64_Last    : std_logic := '0';
  signal PktParam       : Pkt_Param_t;
  signal BufferFull     : std_logic := '0';
  signal BufferOverflow : std_logic := '0';
  signal OutPktParam    : Pkt_Param_t;
--  signal Axis_Vld       : std_logic := '0';

  signal B2W_Din_Vld    : std_logic := '0';
  signal B2W_Din_Rdy    : std_logic := '0';
  signal B2W_Din_Last   : std_logic := '0';
  signal B2W_Rst        : std_logic := '0';
  signal BufferRst      : std_logic := '0';

  signal PktErrorFlags  : PktError_t;
  signal FlagsValid     : std_logic := '0';
  signal FlagsReady     : std_logic := '0';

  signal Rst_CRC        : std_logic := '0';
  signal CRC_Vld        : std_logic := '0';
  signal CRC_Ok         : std_logic := '0';

  signal SyncDone       : std_logic := '0';
  signal DelayOk        : std_logic := '0';
  signal BitslipOk      : std_logic := '0';
  signal SyncOk         : std_logic := '0';
  signal ReadyToRcvCtrl : std_logic := '0';
  signal ReadyToRcv     : std_logic := '0';

begin
  ParallelData_Dbg_o <= Data8;
  CrcOk_Dbg_o        <= CRC_Ok;

  SyncDone_o  <= SyncDone;
  DelayOk_o   <= DelayOk;
  BitslipOk_o <= BitslipOk;
  SyncOk      <= SyncDone and DelayOk and BitslipOk;

  deserializer_i : entity work.deserializer
  generic map (
    BitOrder_cgn    => BitOrder_cgn,
    IdlePattern_cgn => IdlePattern_cgn,
    SDataRate_cgn   => SDataRate_cgn,
    DlyRefClk_cgn   => DlyRefClk_cgn
  )
  port map (
    TestEye_o       => TestEye_o,
    TestTap_o       => TestTap_o,
    BitClk_Serdes_i => BitClk_Serdes_i,
    DivClk_Serdes_i => DivClk_Serdes_i,
    DivClk_i        => DivClk_i,
    Rst_i           => Rst_i,
    SerialData_i_p  => SerialData_i_p,
    SerialData_i_n  => SerialData_i_n,
    ParallelData_o  => Data8,
    Resync_i        => Resync_i,
    SyncDone_o      => SyncDone,
    DelayOk_o       => DelayOk,
    BitslipOk_o     => BitslipOk,
    LinkIdle_o      => LinkIdle_o
  );

  pkt_buffer_i : entity work.serdes_pkt_buffer
    generic map (
      Width_g    => 64,
      Depth_g    => FifoDepth_cgn,
      RamStyle_g => "block"
    )
    port map (
      Clk_i            => DivClk_i,
      Rst_i            => BufferRst,
      InData_i         => Data64,
      InValid_i        => Data64_Vld,
      InReady_o        => Data64_Rdy,
      InLast_i         => Data64_Last,
      InPktParam_i     => PktParam,
      BufferFull_o     => BufferFull,
      BufferOverflow_o => BufferOverflow,
      PktErrorFlags_i  => PktErrorFlags,
      FlagsValid_i     => FlagsValid,
      FlagsReady_o     => FlagsReady,
      OutData_o        => Axis_Data_o.Data,
      OutValid_o       => Axis_Vld_o,
      OutReady_i       => Axis_Rdy_i,
      OutLast_o        => Axis_Data_o.Last,
      OutBytes_o       => Axis_Data_o.Bytes,
      OutPktParam_o    => OutPktParam
    );
  PktParam_o <= OutPktParam;

  ByteToWord64_i : entity work.tdm_par_fill
    generic map (
      ChannelCount_g => 8,
      ChannelWidth_g => 8
    )
    port map (
      Clk          => DivClk_i,
      Rst          => B2W_Rst,
      Tdm          => Data8,
      TdmVld       => B2W_Din_Vld,
      TdmRdy       => B2W_Din_Rdy,
      TdmLast      => B2W_Din_Last,
      Parallel     => Data64,
      ParallelVld  => Data64_Vld,
      ParallelRdy  => Data64_Rdy,
      ParallelKeep => open,
      ParallelLast => Data64_Last
    );

  obufds_i : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",
    SLEW       => "SLOW")
  port map (
    O  => ReadyToRcv_o_p,
    OB => ReadyToRcv_o_n,
    I  => ReadyToRcv
  );

  ReadyToRcv <= ReadyToRcvCtrl and SyncOk;

  control_i : entity work.serdes_pkt_rcvr_ctrl
    port map (
      Clk_i             => DivClk_i,
      Rst_i             => Rst_i,
      DesData_i         => Data8,
      ReadyToRcv_o      => ReadyToRcvCtrl,
      SyncDone_i        => SyncOk,
      B2W_Din_Vld_o     => B2W_Din_Vld,
      B2W_Din_Rdy_i     => B2W_Din_Rdy,
      B2W_Din_Last_o    => B2W_Din_Last,
      BufferFull_i      => BufferFull,
      BufferOverflow_i  => BufferOverflow,
      Buffer_Rst_o      => BufferRst,
      ByteToWord_Rst_o  => B2W_Rst,
      PktErrorFlags_o   => PktErrorFlags,
      FlagsValid_o      => FlagsValid,
      FlagsReady_i      => FlagsReady,
      Rst_CRC_o         => Rst_CRC,
      CRC_Vld_o         => CRC_Vld,
      CRC_Ok_i          => CRC_Ok,
      PktParam_o        => PktParam,
      RcvPktCount_o     => RcvPktCount_o,
      Rst_RcvPktCount_i => Rst_RcvPktCount_i,
      Error_Flags_o     => Error_Flags_o,
      Rst_Errors_i      => Rst_Errors_i
    );

  i_crc32 : entity work.crc32_d8
    port map (
      CLK_I        => DivClk_i,
      RESET_I      => Rst_CRC,
      DATA_I       => Data8,
      DATA_VALID_I => CRC_Vld,
      CRC_O        => open,
      CRC_VALID_O  => CRC_Ok
    );

end;
