------------------------------------------------------------------------------
--  Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Elmar Schmid
--  Created: 08.09.2020 09:11:54
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.math_real.all;

library work;
  use work.psi_common_math_pkg.all;

library UNISIM;
  use UNISIM.vcomponents.all;

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
-- $$ testcases=link_idle, frame $$
-- $$ processes=ctrl_and_stat,serial_data, parallel_data $$
-- $$ tbpkg=work.psi_tb_txt_util,work.psi_tb_compare_pkg,work.psi_tb_activity_pkg $$
entity deserializer is
  generic (
    BitOrder_cgn     : string := "MSB first";                    -- "MSB first", "LSB first" $$ constant="MSB first" $$
    IdlePattern_cgn  : std_logic_vector(7 downto 0) := x"5A";   -- $$ constant=x"5A" $$
    SDataRate_cgn    : real := 640.0;                           -- Serial Data Rate in [MB/s]   $$ constant=640.0 $$
    DlyRefClk_cgn    : real := 200.0                            -- IDelay Reference Clock [MHz] $$ constant=200.0 $$
  );
  port (
    TestEye_o       : out std_logic_vector(19 downto 0);
    TestTap_o       : out std_logic_vector( 4 downto 0);
    -- Clock and Reset
    BitClk_Serdes_i : in  std_logic;   -- $$ type=clk; freq=640e6; $$
    DivClk_Serdes_i : in  std_logic;   -- $$ type=clk; freq=80e6; proc=serial_data $$
    DivClk_i        : in  std_logic;   -- $$ type=clk; freq=80e6; proc=parallel_data,ctrl_and_stat $$
    Rst_i           : in  std_logic;   -- $$ type=rst; clk=DivClk_i; proc=ctrl_and_stat $$

    -- Data
    SerialData_i_p  : in  std_logic;                      -- $$ proc=serial_data $$
    SerialData_i_n  : in  std_logic;                      -- $$ proc=serial_data $$
    ParallelData_o  : out std_logic_vector(7 downto 0);   -- $$ proc=parallel_data $$

    -- Control & Status
    Resync_i        : in  std_logic;                      -- $$ proc=ctrl_and_stat $$
    SyncDone_o      : out std_logic;                      -- $$ proc=ctrl_and_stat $$
    InSync_o        : out std_logic;                      -- $$ proc=ctrl_and_stat $$
    DlySyncDone_o   : out std_logic;                      -- $$ proc=ctrl_and_stat $$
    LinkIdle_o      : out std_logic                       -- $$ proc=ctrl_and_stat $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of deserializer is

  constant MaxTaps_c          : integer := work.psi_common_math_pkg.min( 32, integer(ceil(32.0*2.0*DlyRefClk_cgn/SDataRate_cgn)) ); -- Delay taps to apply to cover one bit period of serial data
  constant MaxTapsU_c         : unsigned(4 downto 0) := to_unsigned(MaxTaps_c, 5);
  constant DlyWaitTicks_c     : natural := 10;
  constant DlyStableTicks_c   : natural := 32;
  constant BslipWaitTicks_c   : natural :=  4;
  constant BslipStableTicks_c : natural :=  4;
  constant NrOfBitslips_c     : natural :=  8;
  constant TickCountWidth_c   : natural := log2ceil(DlyStableTicks_c);
  constant BitslpCountWidth_c : natural := log2ceil(NrOfBitslips_c);
  constant DoublePattern_c    : std_logic_vector(15 downto 0) := IdlePattern_cgn & IdlePattern_cgn;

  signal SerialDataIn      : std_logic := '0';
  signal SerialDataDelayed : std_logic := '0';
  signal Deser_PData       : std_logic_vector(7 downto 0) := (others => '0');
  signal ParallelData      : std_logic_vector(7 downto 0) := (others => '0');

  signal TargetTap         : unsigned(5 downto 0) := (others=>'0');

  signal PatternDetect     : unsigned(7 downto 0) := (others => '0');

  signal TestEye           : std_logic_vector(MaxTaps_c-1 downto 0) := (others => '0');

  -- Types
  type State_t is (Idle_s, Init_s, WaitStableDly_s, ValidateDly_s, SetDly_s, WaitStableBslip_s, ValidateBslip_s);

  type one_process_r is record
    State : State_t;
    SyncDone     : std_logic;
    SyncSuccess  : std_logic;
    DlySyncDone  : std_logic;
    DelayTap     : unsigned(4 downto 0);
    DelayLd      : std_logic;
    DelayRst     : std_logic;
    DesBitslip   : std_logic;
    DesRst       : std_logic;
    PrevData     : std_logic_vector(7 downto 0);
    PrevValid    : std_logic;
    EyeCount     : unsigned(4 downto 0);
    RisEdgeTap   : unsigned(4 downto 0);
    BitslipCount : unsigned(BitslpCountWidth_c-1 downto 0);
    TickCount    : unsigned(TickCountWidth_c-1 downto 0);
  end record;
  signal r : one_process_r;

  attribute fsm_encoding : string;
--  attribute fsm_encoding of r.State : signal is "one-hot";

begin

  -- Outputs
  TestEye_o     <= TestEye;
  TestTap_o     <= std_logic_vector(r.DelayTap);
  SyncDone_o    <= r.SyncDone;
  DlySyncDone_o <= r.DlySyncDone;
  InSync_o      <= r.SyncSuccess;
  LinkIdle_o    <= '1' when (r.State = Idle_s and ParallelData = IdlePattern_cgn) else '0';

  TargetTapCalc_i : process(DivClk_i, r)
    variable TTap : unsigned(5 downto 0);
  begin
    TTap := ('0' & r.RisEdgeTap) + ("00" & r.EyeCount(4 downto 1));
    if TTap >= ('0' & MaxTapsU_c) then
      TTap := TTap - MaxTapsU_c;
    end if;
    if rising_edge(DivClk_i) then
      TargetTap <= TTap;
    end if;
  end process;

  PATTERNCOMP_I : process(DivClk_i)
  begin
    if rising_edge(DivClk_i) then
      for i in 0 to 7 loop
        if ParallelData = DoublePattern_c(i+7 downto i) then
          PatternDetect(i) <= '1';
        else
          PatternDetect(i) <= '0';
        end if;
      end loop;
    end if;
  end process;

  -- FSM
  SyncCtrl_i : process(DivClk_i)
  begin
    if rising_edge(DivClk_i) then
      if Rst_i = '1' then
        r.SyncDone    <= '0';
        r.SyncSuccess <= '0';
        r.State <= Init_s;
      else
        -- Defaults
        r.SyncDone   <= '0';
        r.DelayLd    <= '0';
        r.DesBitslip <= '0';
        r.DelayRst   <= '0';
        r.DesRst     <= '0';
        -- Store input data
        r.PrevData   <= ParallelData;
        -- Tick Counter
        if r.TickCount /= 0 then
          r.TickCount <= r.TickCount - 1;
        end if;
        -- State logic
        case r.State is
          when Init_s =>
            r.SyncSuccess  <= '0';
            r.DlySyncDone  <= '0';
            r.PrevValid    <= '0';
            r.DelayLd      <= '1';
            r.DelayRst     <= '1';
            r.DesRst       <= '1';
            r.RisEdgeTap   <= (others=>'0');
            r.EyeCount     <= (others=>'0');
            r.DelayTap     <= (others=>'0');
            r.BitslipCount <= to_unsigned(NrOfBitslips_c-1, r.BitslipCount'length);
            r.TickCount    <= to_unsigned(DlyWaitTicks_c-1, r.TickCount'length);
            TestEye        <= (others=>'0');
            r.State <= WaitStableDly_s;
          -- Delay Calibration
          when WaitStableDly_s =>
            if r.TickCount = 0 then
              r.TickCount <= to_unsigned(DlyStableTicks_c-1, r.TickCount'length);
              r.State <= ValidateDly_s;
            end if;
          when ValidateDly_s =>
            if ParallelData /= r.PrevData or PatternDetect = 0 or r.TickCount = 0 then
              r.TickCount <= to_unsigned(DlyWaitTicks_c-1, r.TickCount'length);
              r.DelayLd   <= '1';
              r.PrevValid <= '0';
              if r.TickCount = 0 then
                if r.PrevValid = '0' then
                  r.RisEdgeTap <= r.DelayTap;
                end if;
                r.EyeCount  <= r.EyeCount + 1;
                r.PrevValid <= '1';
                TestEye(to_integer(unsigned(r.DelayTap))) <= '1';
              end if;
              if r.DelayTap < MaxTapsU_c-1 then
                r.DelayTap <= r.DelayTap + 1;
                r.State <= WaitStableDly_s;
              else
                r.DelayTap <= TargetTap(4 downto 0);
                r.State <= SetDly_s;
              end if;
            end if;
          -- End of Delay Calibration
          when SetDly_s =>
            r.DlySyncDone <= '1';
            if r.TickCount = 0 then
              -- already delayed for the tap value
              -- so go directly to check
              r.BitslipCount <= to_unsigned(NrOfBitslips_c-1, r.BitslipCount'length);
              r.TickCount    <= to_unsigned(BslipStableTicks_c-1, r.TickCount'length);
              r.State <= ValidateBslip_s;
            end if;
        -- Bitslip Calibration
          when WaitStableBslip_s =>
            if r.TickCount = 0 then
              r.TickCount <= to_unsigned(BslipStableTicks_c-1, r.TickCount'length);
              r.State <= ValidateBslip_s;
            end if;
          when ValidateBslip_s =>
--            if ParallelData /= IdlePattern_cgn then
            if PatternDetect(0) = '0' then
                if r.BitslipCount = 0 then
                r.State <= Idle_s;
              else
                r.TickCount  <= to_unsigned(BslipWaitTicks_c-1, r.TickCount'length);
                r.DesBitslip <= '1';
                r.BitslipCount <= r.BitslipCount - 1;
                r.State <= WaitStableBslip_s;
              end if;
            elsif r.TickCount = 0 then
              r.SyncSuccess <= '1';
              r.State <= Idle_s;
            end if;
          -- End of Bitslip Calibration
          when Idle_s =>
            r.SyncDone <= '1';
            if Resync_i = '1' then
              r.State <= Init_s;
            end if;
          when others =>
            r.State <= Init_s;
        end case;
      end if;
    end if;
  end process;

  IBUFDS_i : IBUFDS
  generic map (
    DIFF_TERM    => TRUE,
    IBUF_LOW_PWR => TRUE,
    IOSTANDARD   => "DEFAULT")
  port map (
    O  => SerialDataIn,
    I  => SerialData_i_p,
    IB => SerialData_i_n
  );

  BitOrder_i : process(Deser_PData)
  begin
    if BitOrder_cgn = "MSB first" then
      ParallelData <= Deser_PData;
    else
      for i in 7 downto 0 loop
        ParallelData(i) <= Deser_PData(7-i);
      end loop;
    end if;
  end process;
  ParallelData_o <= ParallelData;

  IDELAYE2_i : IDELAYE2
  generic map (
    IDELAY_TYPE           => "VAR_LOAD",
    DELAY_SRC             => "IDATAIN",
    IDELAY_VALUE          => 0,
    HIGH_PERFORMANCE_MODE => "FALSE",
    SIGNAL_PATTERN        => "DATA",
    REFCLK_FREQUENCY      => DlyRefClk_cgn,
    CINVCTRL_SEL          => "TRUE",
    PIPE_SEL              => "FALSE"
  )
  port map (
    CNTVALUEOUT => open,
    DATAOUT     => SerialDataDelayed,
    C           => DivClk_i,
    CE          => '0',
    CINVCTRL    => '1',
    CNTVALUEIN  => std_logic_vector(r.DelayTap),
    DATAIN      => '0',
    IDATAIN     => SerialDataIn,
    INC         => '0',
    LD          => r.DelayLd,
    LDPIPEEN    => '0',
    REGRST      => r.DelayRst
  );

  ISERDESE2_inst : ISERDESE2
  generic map (
    DATA_RATE         => "DDR",
    DATA_WIDTH        => 8,
    DYN_CLKDIV_INV_EN => "FALSE",
    DYN_CLK_INV_EN    => "FALSE",
    INTERFACE_TYPE    => "NETWORKING",
    IOBDELAY          => "IFD",
    NUM_CE            => 1,
    OFB_USED          => "FALSE",
    SERDES_MODE       => "MASTER"
  )
  port map (
    O            => open,
    Q1           => Deser_PData(0),
    Q2           => Deser_PData(1),
    Q3           => Deser_PData(2),
    Q4           => Deser_PData(3),
    Q5           => Deser_PData(4),
    Q6           => Deser_PData(5),
    Q7           => Deser_PData(6),
    Q8           => Deser_PData(7),
    SHIFTOUT1    => open,
    SHIFTOUT2    => open,
    BITSLIP      => r.DesBitslip,
    CE1          => '1',
    CE2          => '0',
    CLKDIVP      => '0',
    CLK          => BitClk_Serdes_i,
    CLKB         => not BitClk_Serdes_i,
    CLKDIV       => DivClk_Serdes_i,
    OCLK         => '0',
    DYNCLKDIVSEL => '0',
    DYNCLKSEL    => '0',
    D            => '0',
    DDLY         => SerialDataDelayed,
    OFB          => '0',
    OCLKB        => '0',
    RST          => r.DesRst,
    SHIFTIN1     => '0',
    SHIFTIN2     => '0'
  );

end;
