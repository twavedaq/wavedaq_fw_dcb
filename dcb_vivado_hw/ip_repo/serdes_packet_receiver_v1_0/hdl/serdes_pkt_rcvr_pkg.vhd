------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Elmar Schmid
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;

------------------------------------------------------------------------------
-- Package Header
------------------------------------------------------------------------------
package serdes_pkt_rcvr_pkg is

  constant HdrBytes_cpk          : integer   := 24;
  --constant HdrCountSize_cpk      : integer   := log2ceil(HdrBytes_cpk-1);

  constant MaxPktBytes_cpk       : integer   := 8972; -- Jumbo frame: 9000 - 20(IP header) - 8(UDP header) = 8972
--  constant MaxPktBytesSize_cpk   : integer   := log2ceil(MaxPktBytes_cpk);
  constant MaxPktWords_cpk       : integer   := (MaxPktBytes_cpk+7)/8;
--  constant MaxPktWordsSize_cpk   : integer   := log2ceil(MaxPktWords_cpk);
  constant MaxPktBytesSize_cpk   : integer   := 16;  -- Payload length in header is 16 bit
  constant MaxPktWordsSize_cpk   : integer   := 14;  -- Due to Payload length restriction and (..+7)/8

  constant HdrWdaqFlagByte_cpk        : integer :=  9;
  constant HdrWdaqEoeFlagBit_cpk      : integer :=  0;
  constant HdrWdaqSoeFlagBit_cpk      : integer :=  1;
  constant HdrWdaqCrcErrFlagBit_cpk   : integer :=  4;--12;
  constant HdrWdaqDtgrmErrFlagBit_cpk : integer :=  5;--13;
  constant HdrWdaqBufOvflFlagBit_cpk  : integer :=  6;--14;
  constant HdrEventNrMsbByte_cpk      : integer := 16;
  constant HdrPldSizeMsbByte_cpk      : integer := 20;

  constant DesOutOfSync_cpk      : std_logic := '0';
  constant DesInSync_cpk         : std_logic := '1';
  constant SofByte0_cpk          : std_logic_vector(7 downto 0) := x"F0";
  constant SofByte1_cpk          : std_logic_vector(7 downto 0) := x"0D";
  constant IdleByte_cpk          : std_logic_vector(7 downto 0) := x"5A";

  constant ErrCountBits_cpk      : integer  := 8;
  constant MaxErrors_cpk         : unsigned(ErrCountBits_cpk-1 downto 0) := (others=>'1');
  constant PktCountBits_cpk      : integer  := 32;
  constant MaxPktCount_cpk       : unsigned(PktCountBits_cpk-1 downto 0) := (others=>'1');

  type Output_Data_t is record
    Last   : std_logic;
    Data   : std_logic_vector(63 downto 0);
    Bytes  : std_logic_vector(3 downto 0);
  end record;
  type Output_Data_a is array (natural range <>) of Output_Data_t;

  type Pkt_Param_t is record
    EOE   : std_logic;
    Bytes : std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    Valid : std_logic;
  end record;
  type Pkt_Param_a is array (natural range <>) of Pkt_Param_t;

  type Error_t is record
    CRC_ErrCount      : std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Frame_ErrCount    : std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Datagram_ErrCount : std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Sync_ErrCount     : std_logic_vector(ErrCountBits_cpk-1 downto 0);
    CRC_Err           : std_logic;
    Frame_Err         : std_logic;
    Datagram_Err      : std_logic;
    Sync_Err          : std_logic;
  end record;
  type Error_a is array (natural range <>) of Error_t;

  type PktError_t is record
    CRC_Err      : std_logic;
    Datagram_Err : std_logic;
  end record;
  type PktError_a is array (natural range <>) of PktError_t;

end serdes_pkt_rcvr_pkg;

------------------------------------------------------------------------------
-- Package Body
------------------------------------------------------------------------------
package body serdes_pkt_rcvr_pkg is

end serdes_pkt_rcvr_pkg;
