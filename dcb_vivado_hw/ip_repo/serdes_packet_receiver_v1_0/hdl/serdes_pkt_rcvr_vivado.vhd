------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_array_pkg.all;
  use work.psi_common_logic_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

Library UNISIM;
  use UNISIM.vcomponents.all;

------------------------------------------------------------------------------
-- Entity
------------------------------------------------------------------------------
entity serdes_pkt_rcvr_vivado is
  generic (
    Streams_cgn     : positive range 1 to 17 := 1;
    IdelayCtrls_cgn : positive range 1 to  8 := 1; -- Must equal the number of clock regions covered by the IDELAY/ISERDES inputs
    BitOrder_cgn    : string := "MSB first";                   -- "MSB first", "LSB first"
    IdlePattern_cgn : std_logic_vector(7 downto 0) := x"5A";
    SDataRate_cgn   : positive := 640;                         -- Serial Data Rate in [MB/s]
    DlyRefClk_cgn   : positive := 200;                         -- IDelay Reference Clock [MHz]
    FifoSize_cgn    : positive := 4096
  );
  port (
    -- Idelay control signals
    IdelayCtrl_Refclk_i      : in  std_logic;
    IdelayCtrl_Rst_i         : in  std_logic;
    IdelayCtrl_Rdy_o         : out std_logic;
    Idelay_Refclk_Lock_i     : in  std_logic;

    -- Control
    Rst_i                    : in  std_logic;
    Rst_Errors_i             : in std_logic;
    Resync_i                 : in  std_logic;
    Rst_RcvPktCount_i        : in  std_logic;

    -- Debug Signals
    ParallelData_Dbg_o       : out std_logic_vector(Streams_cgn*8-1 downto 0);
    CrcOk_Dbg_o              : out std_logic_vector(Streams_cgn-1 downto 0);

    -- Serial IF
    SerialData_i_p           : in  std_logic_vector(Streams_cgn-1 downto 0);
    SerialData_i_n           : in  std_logic_vector(Streams_cgn-1 downto 0);
    ReadyToRcv_o_p           : out std_logic_vector(Streams_cgn-1 downto 0);
    ReadyToRcv_o_n           : out std_logic_vector(Streams_cgn-1 downto 0);

    -- Clock Status
    SerdesClk_Lock_i         : in  std_logic;

    -- Clocks
    -- Global
    DivClk_Global_i          : in  std_logic;
    -- Serdes
    Slot00_BitClk_Serdes_i   : in  std_logic;
    Slot00_DivClk_Serdes_i   : in  std_logic;
    Slot01_BitClk_Serdes_i   : in  std_logic;
    Slot01_DivClk_Serdes_i   : in  std_logic;
    Slot02_BitClk_Serdes_i   : in  std_logic;
    Slot02_DivClk_Serdes_i   : in  std_logic;
    Slot03_BitClk_Serdes_i   : in  std_logic;
    Slot03_DivClk_Serdes_i   : in  std_logic;
    Slot04_BitClk_Serdes_i   : in  std_logic;
    Slot04_DivClk_Serdes_i   : in  std_logic;
    Slot05_BitClk_Serdes_i   : in  std_logic;
    Slot05_DivClk_Serdes_i   : in  std_logic;
    Slot06_BitClk_Serdes_i   : in  std_logic;
    Slot06_DivClk_Serdes_i   : in  std_logic;
    Slot07_BitClk_Serdes_i   : in  std_logic;
    Slot07_DivClk_Serdes_i   : in  std_logic;
    Slot08_BitClk_Serdes_i   : in  std_logic;
    Slot08_DivClk_Serdes_i   : in  std_logic;
    Slot09_BitClk_Serdes_i   : in  std_logic;
    Slot09_DivClk_Serdes_i   : in  std_logic;
    Slot10_BitClk_Serdes_i   : in  std_logic;
    Slot10_DivClk_Serdes_i   : in  std_logic;
    Slot11_BitClk_Serdes_i   : in  std_logic;
    Slot11_DivClk_Serdes_i   : in  std_logic;
    Slot12_BitClk_Serdes_i   : in  std_logic;
    Slot12_DivClk_Serdes_i   : in  std_logic;
    Slot13_BitClk_Serdes_i   : in  std_logic;
    Slot13_DivClk_Serdes_i   : in  std_logic;
    Slot14_BitClk_Serdes_i   : in  std_logic;
    Slot14_DivClk_Serdes_i   : in  std_logic;
    Slot15_BitClk_Serdes_i   : in  std_logic;
    Slot15_DivClk_Serdes_i   : in  std_logic;
    Slot16_BitClk_Serdes_i   : in  std_logic;
    Slot16_DivClk_Serdes_i   : in  std_logic;

    -- Streams 0 IO
    Slot00_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot00_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot00_TData             : out std_logic_vector(63 downto 0);
    Slot00_TValid            : out std_logic;
    Slot00_TReady            : in  std_logic;
    Slot00_TLast             : out std_logic;
    Slot00_Clk               : out std_logic;
    Slot00_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot00_PktValid_o        : out std_logic;
    Slot00_EOE_o             : out std_logic;
    Slot00_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot00_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot00_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot00_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot00_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot00_CrcError_o        : out std_logic;
    Slot00_FrameError_o      : out std_logic;
    Slot00_DatagramError_o   : out std_logic;
    Slot00_SyncError_o       : out std_logic;
    Slot00_SyncDone_o        : out std_logic;
    Slot00_DelayOk_o         : out std_logic;
    Slot00_BitslipOk_o       : out std_logic;
    Slot00_LinkIdle_o        : out std_logic;
    Slot00_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 1 IO
    Slot01_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot01_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot01_TData             : out std_logic_vector(63 downto 0);
    Slot01_TValid            : out std_logic;
    Slot01_TReady            : in  std_logic;
    Slot01_TLast             : out std_logic;
    Slot01_Clk               : out std_logic;
    Slot01_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot01_PktValid_o        : out std_logic;
    Slot01_EOE_o             : out std_logic;
    Slot01_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot01_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot01_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot01_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot01_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot01_CrcError_o        : out std_logic;
    Slot01_FrameError_o      : out std_logic;
    Slot01_DatagramError_o   : out std_logic;
    Slot01_SyncError_o       : out std_logic;
    Slot01_SyncDone_o        : out std_logic;
    Slot01_DelayOk_o         : out std_logic;
    Slot01_BitslipOk_o       : out std_logic;
    Slot01_LinkIdle_o        : out std_logic;
    Slot01_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 2 IO
    Slot02_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot02_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot02_TData             : out std_logic_vector(63 downto 0);
    Slot02_TValid            : out std_logic;
    Slot02_TReady            : in  std_logic;
    Slot02_TLast             : out std_logic;
    Slot02_Clk               : out std_logic;
    Slot02_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot02_PktValid_o        : out std_logic;
    Slot02_EOE_o             : out std_logic;
    Slot02_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot02_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot02_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot02_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot02_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot02_CrcError_o        : out std_logic;
    Slot02_FrameError_o      : out std_logic;
    Slot02_DatagramError_o   : out std_logic;
    Slot02_SyncError_o       : out std_logic;
    Slot02_SyncDone_o        : out std_logic;
    Slot02_DelayOk_o         : out std_logic;
    Slot02_BitslipOk_o       : out std_logic;
    Slot02_LinkIdle_o        : out std_logic;
    Slot02_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 3 IO
    Slot03_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot03_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot03_TData             : out std_logic_vector(63 downto 0);
    Slot03_TValid            : out std_logic;
    Slot03_TReady            : in  std_logic;
    Slot03_TLast             : out std_logic;
    Slot03_Clk               : out std_logic;
    Slot03_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot03_PktValid_o        : out std_logic;
    Slot03_EOE_o             : out std_logic;
    Slot03_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot03_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot03_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot03_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot03_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot03_CrcError_o        : out std_logic;
    Slot03_FrameError_o      : out std_logic;
    Slot03_DatagramError_o   : out std_logic;
    Slot03_SyncError_o       : out std_logic;
    Slot03_SyncDone_o        : out std_logic;
    Slot03_DelayOk_o         : out std_logic;
    Slot03_BitslipOk_o       : out std_logic;
    Slot03_LinkIdle_o        : out std_logic;
    Slot03_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 4 IO
    Slot04_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot04_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot04_TData             : out std_logic_vector(63 downto 0);
    Slot04_TValid            : out std_logic;
    Slot04_TReady            : in  std_logic;
    Slot04_TLast             : out std_logic;
    Slot04_Clk               : out std_logic;
    Slot04_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot04_PktValid_o        : out std_logic;
    Slot04_EOE_o             : out std_logic;
    Slot04_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot04_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot04_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot04_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot04_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot04_CrcError_o        : out std_logic;
    Slot04_FrameError_o      : out std_logic;
    Slot04_DatagramError_o   : out std_logic;
    Slot04_SyncError_o       : out std_logic;
    Slot04_SyncDone_o        : out std_logic;
    Slot04_DelayOk_o         : out std_logic;
    Slot04_BitslipOk_o       : out std_logic;
    Slot04_LinkIdle_o        : out std_logic;
    Slot04_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 5 IO
    Slot05_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot05_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot05_TData             : out std_logic_vector(63 downto 0);
    Slot05_TValid            : out std_logic;
    Slot05_TReady            : in  std_logic;
    Slot05_TLast             : out std_logic;
    Slot05_Clk               : out std_logic;
    Slot05_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot05_PktValid_o        : out std_logic;
    Slot05_EOE_o             : out std_logic;
    Slot05_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot05_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot05_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot05_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot05_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot05_CrcError_o        : out std_logic;
    Slot05_FrameError_o      : out std_logic;
    Slot05_DatagramError_o   : out std_logic;
    Slot05_SyncError_o       : out std_logic;
    Slot05_SyncDone_o        : out std_logic;
    Slot05_DelayOk_o         : out std_logic;
    Slot05_BitslipOk_o       : out std_logic;
    Slot05_LinkIdle_o        : out std_logic;
    Slot05_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 6 IO
    Slot06_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot06_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot06_TData             : out std_logic_vector(63 downto 0);
    Slot06_TValid            : out std_logic;
    Slot06_TReady            : in  std_logic;
    Slot06_TLast             : out std_logic;
    Slot06_Clk               : out std_logic;
    Slot06_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot06_PktValid_o        : out std_logic;
    Slot06_EOE_o             : out std_logic;
    Slot06_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot06_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot06_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot06_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot06_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot06_CrcError_o        : out std_logic;
    Slot06_FrameError_o      : out std_logic;
    Slot06_DatagramError_o   : out std_logic;
    Slot06_SyncError_o       : out std_logic;
    Slot06_SyncDone_o        : out std_logic;
    Slot06_DelayOk_o         : out std_logic;
    Slot06_BitslipOk_o       : out std_logic;
    Slot06_LinkIdle_o        : out std_logic;
    Slot06_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 7 IO
    Slot07_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot07_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot07_TData             : out std_logic_vector(63 downto 0);
    Slot07_TValid            : out std_logic;
    Slot07_TReady            : in  std_logic;
    Slot07_TLast             : out std_logic;
    Slot07_Clk               : out std_logic;
    Slot07_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot07_PktValid_o        : out std_logic;
    Slot07_EOE_o             : out std_logic;
    Slot07_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot07_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot07_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot07_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot07_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot07_CrcError_o        : out std_logic;
    Slot07_FrameError_o      : out std_logic;
    Slot07_DatagramError_o   : out std_logic;
    Slot07_SyncError_o       : out std_logic;
    Slot07_SyncDone_o        : out std_logic;
    Slot07_DelayOk_o         : out std_logic;
    Slot07_BitslipOk_o       : out std_logic;
    Slot07_LinkIdle_o        : out std_logic;
    Slot07_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 8 IO
    Slot08_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot08_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot08_TData             : out std_logic_vector(63 downto 0);
    Slot08_TValid            : out std_logic;
    Slot08_TReady            : in  std_logic;
    Slot08_TLast             : out std_logic;
    Slot08_Clk               : out std_logic;
    Slot08_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot08_PktValid_o        : out std_logic;
    Slot08_EOE_o             : out std_logic;
    Slot08_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot08_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot08_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot08_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot08_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot08_CrcError_o        : out std_logic;
    Slot08_FrameError_o      : out std_logic;
    Slot08_DatagramError_o   : out std_logic;
    Slot08_SyncError_o       : out std_logic;
    Slot08_SyncDone_o        : out std_logic;
    Slot08_DelayOk_o         : out std_logic;
    Slot08_BitslipOk_o       : out std_logic;
    Slot08_LinkIdle_o        : out std_logic;
    Slot08_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 9 IO
    Slot09_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot09_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot09_TData             : out std_logic_vector(63 downto 0);
    Slot09_TValid            : out std_logic;
    Slot09_TReady            : in  std_logic;
    Slot09_TLast             : out std_logic;
    Slot09_Clk               : out std_logic;
    Slot09_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot09_PktValid_o        : out std_logic;
    Slot09_EOE_o             : out std_logic;
    Slot09_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot09_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot09_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot09_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot09_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot09_CrcError_o        : out std_logic;
    Slot09_FrameError_o      : out std_logic;
    Slot09_DatagramError_o   : out std_logic;
    Slot09_SyncError_o       : out std_logic;
    Slot09_SyncDone_o        : out std_logic;
    Slot09_DelayOk_o         : out std_logic;
    Slot09_BitslipOk_o       : out std_logic;
    Slot09_LinkIdle_o        : out std_logic;
    Slot09_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 10 IO
    Slot10_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot10_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot10_TData             : out std_logic_vector(63 downto 0);
    Slot10_TValid            : out std_logic;
    Slot10_TReady            : in  std_logic;
    Slot10_TLast             : out std_logic;
    Slot10_Clk               : out std_logic;
    Slot10_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot10_PktValid_o        : out std_logic;
    Slot10_EOE_o             : out std_logic;
    Slot10_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot10_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot10_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot10_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot10_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot10_CrcError_o        : out std_logic;
    Slot10_FrameError_o      : out std_logic;
    Slot10_DatagramError_o   : out std_logic;
    Slot10_SyncError_o       : out std_logic;
    Slot10_SyncDone_o        : out std_logic;
    Slot10_DelayOk_o         : out std_logic;
    Slot10_BitslipOk_o       : out std_logic;
    Slot10_LinkIdle_o        : out std_logic;
    Slot10_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 11 IO
    Slot11_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot11_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot11_TData             : out std_logic_vector(63 downto 0);
    Slot11_TValid            : out std_logic;
    Slot11_TReady            : in  std_logic;
    Slot11_TLast             : out std_logic;
    Slot11_Clk               : out std_logic;
    Slot11_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot11_PktValid_o        : out std_logic;
    Slot11_EOE_o             : out std_logic;
    Slot11_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot11_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot11_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot11_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot11_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot11_CrcError_o        : out std_logic;
    Slot11_FrameError_o      : out std_logic;
    Slot11_DatagramError_o   : out std_logic;
    Slot11_SyncError_o       : out std_logic;
    Slot11_SyncDone_o        : out std_logic;
    Slot11_DelayOk_o         : out std_logic;
    Slot11_BitslipOk_o       : out std_logic;
    Slot11_LinkIdle_o        : out std_logic;
    Slot11_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 12 IO
    Slot12_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot12_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot12_TData             : out std_logic_vector(63 downto 0);
    Slot12_TValid            : out std_logic;
    Slot12_TReady            : in  std_logic;
    Slot12_TLast             : out std_logic;
    Slot12_Clk               : out std_logic;
    Slot12_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot12_PktValid_o        : out std_logic;
    Slot12_EOE_o             : out std_logic;
    Slot12_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot12_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot12_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot12_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot12_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot12_CrcError_o        : out std_logic;
    Slot12_FrameError_o      : out std_logic;
    Slot12_DatagramError_o   : out std_logic;
    Slot12_SyncError_o       : out std_logic;
    Slot12_SyncDone_o        : out std_logic;
    Slot12_DelayOk_o         : out std_logic;
    Slot12_BitslipOk_o       : out std_logic;
    Slot12_LinkIdle_o        : out std_logic;
    Slot12_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 13 IO
    Slot13_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot13_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot13_TData             : out std_logic_vector(63 downto 0);
    Slot13_TValid            : out std_logic;
    Slot13_TReady            : in  std_logic;
    Slot13_TLast             : out std_logic;
    Slot13_Clk               : out std_logic;
    Slot13_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot13_PktValid_o        : out std_logic;
    Slot13_EOE_o             : out std_logic;
    Slot13_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot13_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot13_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot13_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot13_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot13_CrcError_o        : out std_logic;
    Slot13_FrameError_o      : out std_logic;
    Slot13_DatagramError_o   : out std_logic;
    Slot13_SyncError_o       : out std_logic;
    Slot13_SyncDone_o        : out std_logic;
    Slot13_DelayOk_o         : out std_logic;
    Slot13_BitslipOk_o       : out std_logic;
    Slot13_LinkIdle_o        : out std_logic;
    Slot13_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams14 IO
    Slot14_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot14_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot14_TData             : out std_logic_vector(63 downto 0);
    Slot14_TValid            : out std_logic;
    Slot14_TReady            : in  std_logic;
    Slot14_TLast             : out std_logic;
    Slot14_Clk               : out std_logic;
    Slot14_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot14_PktValid_o        : out std_logic;
    Slot14_EOE_o             : out std_logic;
    Slot14_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot14_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot14_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot14_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot14_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot14_CrcError_o        : out std_logic;
    Slot14_FrameError_o      : out std_logic;
    Slot14_DatagramError_o   : out std_logic;
    Slot14_SyncError_o       : out std_logic;
    Slot14_SyncDone_o        : out std_logic;
    Slot14_DelayOk_o         : out std_logic;
    Slot14_BitslipOk_o       : out std_logic;
    Slot14_LinkIdle_o        : out std_logic;
    Slot14_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 15 IO
    Slot15_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot15_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot15_TData             : out std_logic_vector(63 downto 0);
    Slot15_TValid            : out std_logic;
    Slot15_TReady            : in  std_logic;
    Slot15_TLast             : out std_logic;
    Slot15_Clk               : out std_logic;
    Slot15_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot15_PktValid_o        : out std_logic;
    Slot15_EOE_o             : out std_logic;
    Slot15_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot15_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot15_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot15_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot15_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot15_CrcError_o        : out std_logic;
    Slot15_FrameError_o      : out std_logic;
    Slot15_DatagramError_o   : out std_logic;
    Slot15_SyncError_o       : out std_logic;
    Slot15_SyncDone_o        : out std_logic;
    Slot15_DelayOk_o         : out std_logic;
    Slot15_BitslipOk_o       : out std_logic;
    Slot15_LinkIdle_o        : out std_logic;
    Slot15_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0);

    -- Streams 16 IO
    Slot16_TestEye_o         : out std_logic_vector(19 downto 0);
    Slot16_TestTap_o         : out std_logic_vector( 4 downto 0);
    -- Parallel IF
    Slot16_TData             : out std_logic_vector(63 downto 0);
    Slot16_TValid            : out std_logic;
    Slot16_TReady            : in  std_logic;
    Slot16_TLast             : out std_logic;
    Slot16_Clk               : out std_logic;
    Slot16_Bytes_o           : out std_logic_vector(3 downto 0);
    Slot16_PktValid_o        : out std_logic;
    Slot16_EOE_o             : out std_logic;
    Slot16_PktBytes_o        : out std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    -- Status
    Slot16_CrcErrors_o       : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot16_FrameErrors_o     : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot16_DatagramErrors_o  : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot16_SyncErrors_o      : out std_logic_vector(ErrCountBits_cpk-1 downto 0);
    Slot16_CrcError_o        : out std_logic;
    Slot16_FrameError_o      : out std_logic;
    Slot16_DatagramError_o   : out std_logic;
    Slot16_SyncError_o       : out std_logic;
    Slot16_SyncDone_o        : out std_logic;
    Slot16_DelayOk_o         : out std_logic;
    Slot16_BitslipOk_o       : out std_logic;
    Slot16_LinkIdle_o        : out std_logic;
    Slot16_RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0)
  );
end entity serdes_pkt_rcvr_vivado;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture rtl of serdes_pkt_rcvr_vivado is

  constant FifoDepth_c         : integer := FifoSize_cgn/8;
  constant SDataRate_c         : real := Real(SDataRate_cgn);
  constant DlyRefClk_c         : real := Real(DlyRefClk_cgn);

  signal All_BitClk_Serdes     : std_logic_vector(0 to 16) := (others=>'0');
  signal All_DivClk_Serdes     : std_logic_vector(0 to 16) := (others=>'0');

  signal ClkMgrLock            : std_logic := '0';
  signal Rst                   : std_logic := '0';
  signal Rst_In                : std_logic := '0';
  signal Rst_Errors            : std_logic := '0';
  signal Resync                : std_logic := '0';
  signal Rst_RcvPktCount       : std_logic := '0';

  signal LockDly_Count         : unsigned(3 downto 0) := (others=>'0');
  signal LockDly_Rst           : std_logic := '0';
  signal LockDly_Rst_s         : std_logic := '0';

  type t_RcvPktCount is array (natural range <>) of std_logic_vector(PktCountBits_cpk-1 downto 0);
  signal All_RcvPktCount       : t_RcvPktCount(0 to 16)    := (others=>(others=>'0'));
  signal All_Error_Flags       : Error_a(0 to 16);
  signal All_Axis_Data         : Output_Data_a(0 to 16);
  signal All_PktParam          : Pkt_Param_a(0 to 16);
  signal All_SyncDone          : std_logic_vector(0 to 16) := (others=>'0');
  signal All_DelayOk           : std_logic_vector(0 to 16) := (others=>'0');
  signal All_BitslipOk         : std_logic_vector(0 to 16) := (others=>'0');
  signal All_LinkIdle          : std_logic_vector(0 to 16) := (others=>'0');
  signal All_Axis_Vld          : std_logic_vector(0 to 16) := (others=>'0');
  signal All_Axis_Rdy          : std_logic_vector(0 to 16) := (others=>'0');

  signal All_IdelayCtrl_Rdy    : std_logic_vector(0 to IdelayCtrls_cgn-1) := (others=>'0');

  signal All_TestEye_o         : t_aslv20(16 downto 0);
  signal All_TestTap_o         : t_aslv5(16 downto 0);

begin

  Slot00_TestEye_o <= All_TestEye_o(0);
  Slot00_TestTap_o <= All_TestTap_o(0);
  Slot01_TestEye_o <= All_TestEye_o(1);
  Slot01_TestTap_o <= All_TestTap_o(1);
  Slot02_TestEye_o <= All_TestEye_o(2);
  Slot02_TestTap_o <= All_TestTap_o(2);
  Slot03_TestEye_o <= All_TestEye_o(3);
  Slot03_TestTap_o <= All_TestTap_o(3);
  Slot04_TestEye_o <= All_TestEye_o(4);
  Slot04_TestTap_o <= All_TestTap_o(4);
  Slot05_TestEye_o <= All_TestEye_o(5);
  Slot05_TestTap_o <= All_TestTap_o(5);
  Slot06_TestEye_o <= All_TestEye_o(6);
  Slot06_TestTap_o <= All_TestTap_o(6);
  Slot07_TestEye_o <= All_TestEye_o(7);
  Slot07_TestTap_o <= All_TestTap_o(7);
  Slot08_TestEye_o <= All_TestEye_o(8);
  Slot08_TestTap_o <= All_TestTap_o(8);
  Slot09_TestEye_o <= All_TestEye_o(9);
  Slot09_TestTap_o <= All_TestTap_o(9);
  Slot10_TestEye_o <= All_TestEye_o(10);
  Slot10_TestTap_o <= All_TestTap_o(10);
  Slot11_TestEye_o <= All_TestEye_o(11);
  Slot11_TestTap_o <= All_TestTap_o(11);
  Slot12_TestEye_o <= All_TestEye_o(12);
  Slot12_TestTap_o <= All_TestTap_o(12);
  Slot13_TestEye_o <= All_TestEye_o(13);
  Slot13_TestTap_o <= All_TestTap_o(13);
  Slot14_TestEye_o <= All_TestEye_o(14);
  Slot14_TestTap_o <= All_TestTap_o(14);
  Slot15_TestEye_o <= All_TestEye_o(15);
  Slot15_TestTap_o <= All_TestTap_o(15);
  Slot16_TestEye_o <= All_TestEye_o(16);
  Slot16_TestTap_o <= All_TestTap_o(16);

  ClkMgrLock <= Idelay_Refclk_Lock_i and SerdesClk_Lock_i;
  clk_mgr_lock_rst : process(DivClk_Global_i, ClkMgrLock)
  begin
    if ClkMgrLock = '0' then
      LockDly_Count <= (others=>'1');
    elsif rising_edge(DivClk_Global_i) then
      if LockDly_Count /= 0 then
        LockDly_Count <= LockDly_Count - 1;
        LockDly_Rst_s <= '1';
      else
        LockDly_Rst_s <= '0';
      end if;
      LockDly_Rst <= LockDly_Rst_s;
    end if;
  end process;

  rst_cc_i : entity work.psi_common_bit_cc
  generic map(
    NumBits_g => 4
  )
  port map(
    BitsA(0) => Rst_i,
    BitsA(1) => Rst_Errors_i,
    BitsA(2) => Resync_i,
    BitsA(3) => Rst_RcvPktCount_i,
    ClkB     => DivClk_Global_i,
    BitsB(0) => Rst_In,
    BitsB(1) => Rst_Errors,
    BitsB(2) => Resync,
    BitsB(3) => Rst_RcvPktCount
  );
  Rst <= Rst_In or LockDly_Rst;

  ---------------------------------------
  -- Clocking                          --
  ---------------------------------------

  All_BitClk_Serdes(0)  <= Slot00_BitClk_Serdes_i;
  All_DivClk_Serdes(0)  <= Slot00_DivClk_Serdes_i;
  All_BitClk_Serdes(1)  <= Slot01_BitClk_Serdes_i;
  All_DivClk_Serdes(1)  <= Slot01_DivClk_Serdes_i;
  All_BitClk_Serdes(2)  <= Slot02_BitClk_Serdes_i;
  All_DivClk_Serdes(2)  <= Slot02_DivClk_Serdes_i;
  All_BitClk_Serdes(3)  <= Slot03_BitClk_Serdes_i;
  All_DivClk_Serdes(3)  <= Slot03_DivClk_Serdes_i;
  All_BitClk_Serdes(4)  <= Slot04_BitClk_Serdes_i;
  All_DivClk_Serdes(4)  <= Slot04_DivClk_Serdes_i;
  All_BitClk_Serdes(5)  <= Slot05_BitClk_Serdes_i;
  All_DivClk_Serdes(5)  <= Slot05_DivClk_Serdes_i;
  All_BitClk_Serdes(6)  <= Slot06_BitClk_Serdes_i;
  All_DivClk_Serdes(6)  <= Slot06_DivClk_Serdes_i;
  All_BitClk_Serdes(7)  <= Slot07_BitClk_Serdes_i;
  All_DivClk_Serdes(7)  <= Slot07_DivClk_Serdes_i;
  All_BitClk_Serdes(8)  <= Slot08_BitClk_Serdes_i;
  All_DivClk_Serdes(8)  <= Slot08_DivClk_Serdes_i;
  All_BitClk_Serdes(9)  <= Slot09_BitClk_Serdes_i;
  All_DivClk_Serdes(9)  <= Slot09_DivClk_Serdes_i;
  All_BitClk_Serdes(10) <= Slot10_BitClk_Serdes_i;
  All_DivClk_Serdes(10) <= Slot10_DivClk_Serdes_i;
  All_BitClk_Serdes(11) <= Slot11_BitClk_Serdes_i;
  All_DivClk_Serdes(11) <= Slot11_DivClk_Serdes_i;
  All_BitClk_Serdes(12) <= Slot12_BitClk_Serdes_i;
  All_DivClk_Serdes(12) <= Slot12_DivClk_Serdes_i;
  All_BitClk_Serdes(13) <= Slot13_BitClk_Serdes_i;
  All_DivClk_Serdes(13) <= Slot13_DivClk_Serdes_i;
  All_BitClk_Serdes(14) <= Slot14_BitClk_Serdes_i;
  All_DivClk_Serdes(14) <= Slot14_DivClk_Serdes_i;
  All_BitClk_Serdes(15) <= Slot15_BitClk_Serdes_i;
  All_DivClk_Serdes(15) <= Slot15_DivClk_Serdes_i;
  All_BitClk_Serdes(16) <= Slot16_BitClk_Serdes_i;
  All_DivClk_Serdes(16) <= Slot16_DivClk_Serdes_i;

  ---------------------------------------

  Slot00_TData             <= All_Axis_Data(0).Data;
  Slot00_TValid            <= All_Axis_Vld(0);
  All_Axis_Rdy(0)          <= Slot00_TReady;
  Slot00_TLast             <= All_Axis_Data(0).Last;
  Slot00_Clk               <= DivClk_Global_i;
  Slot00_Bytes_o           <= All_Axis_Data(0).Bytes;
  Slot00_PktValid_o        <= All_PktParam(0).Valid;
  Slot00_EOE_o             <= All_PktParam(0).EOE;
  Slot00_PktBytes_o        <= All_PktParam(0).Bytes;
  Slot00_CrcErrors_o       <= All_Error_Flags(0).CRC_ErrCount;
  Slot00_FrameErrors_o     <= All_Error_Flags(0).Frame_ErrCount;
  Slot00_DatagramErrors_o  <= All_Error_Flags(0).Datagram_ErrCount;
  Slot00_SyncErrors_o      <= All_Error_Flags(0).Sync_ErrCount;
  Slot00_CrcError_o        <= All_Error_Flags(0).CRC_Err;
  Slot00_FrameError_o      <= All_Error_Flags(0).Frame_Err;
  Slot00_DatagramError_o   <= All_Error_Flags(0).Datagram_Err;
  Slot00_SyncError_o       <= All_Error_Flags(0).Sync_Err;
  Slot00_SyncDone_o        <= All_SyncDone(0);
  Slot00_DelayOk_o         <= All_DelayOk(0);
  Slot00_BitslipOk_o       <= All_BitslipOk(0);
  Slot00_LinkIdle_o        <= All_LinkIdle(0);
  Slot00_RcvPktCount_o     <= All_RcvPktCount(0);

  Slot01_TData             <= All_Axis_Data(1).Data;
  Slot01_TValid            <= All_Axis_Vld(1);
  All_Axis_Rdy(1)          <= Slot01_TReady;
  Slot01_TLast             <= All_Axis_Data(1).Last;
  Slot01_Clk               <= DivClk_Global_i;
  Slot01_Bytes_o           <= All_Axis_Data(1).Bytes;
  Slot01_PktValid_o        <= All_PktParam(1).Valid;
  Slot01_EOE_o             <= All_PktParam(1).EOE;
  Slot01_PktBytes_o        <= All_PktParam(1).Bytes;
  Slot01_CrcErrors_o       <= All_Error_Flags(1).CRC_ErrCount;
  Slot01_FrameErrors_o     <= All_Error_Flags(1).Frame_ErrCount;
  Slot01_DatagramErrors_o  <= All_Error_Flags(1).Datagram_ErrCount;
  Slot01_SyncErrors_o      <= All_Error_Flags(1).Sync_ErrCount;
  Slot01_CrcError_o        <= All_Error_Flags(1).CRC_Err;
  Slot01_FrameError_o      <= All_Error_Flags(1).Frame_Err;
  Slot01_DatagramError_o   <= All_Error_Flags(1).Datagram_Err;
  Slot01_SyncError_o       <= All_Error_Flags(1).Sync_Err;
  Slot01_SyncDone_o        <= All_SyncDone(1);
  Slot01_DelayOk_o         <= All_DelayOk(1);
  Slot01_BitslipOk_o       <= All_BitslipOk(1);
  Slot01_LinkIdle_o        <= All_LinkIdle(1);
  Slot01_RcvPktCount_o     <= All_RcvPktCount(1);

  Slot02_TData             <= All_Axis_Data(2).Data;
  Slot02_TValid            <= All_Axis_Vld(2);
  All_Axis_Rdy(2)          <= Slot02_TReady;
  Slot02_TLast             <= All_Axis_Data(2).Last;
  Slot02_Clk               <= DivClk_Global_i;
  Slot02_Bytes_o           <= All_Axis_Data(2).Bytes;
  Slot02_PktValid_o        <= All_PktParam(2).Valid;
  Slot02_EOE_o             <= All_PktParam(2).EOE;
  Slot02_PktBytes_o        <= All_PktParam(2).Bytes;
  Slot02_CrcErrors_o       <= All_Error_Flags(2).CRC_ErrCount;
  Slot02_FrameErrors_o     <= All_Error_Flags(2).Frame_ErrCount;
  Slot02_DatagramErrors_o  <= All_Error_Flags(2).Datagram_ErrCount;
  Slot02_SyncErrors_o      <= All_Error_Flags(2).Sync_ErrCount;
  Slot02_CrcError_o        <= All_Error_Flags(2).CRC_Err;
  Slot02_FrameError_o      <= All_Error_Flags(2).Frame_Err;
  Slot02_DatagramError_o   <= All_Error_Flags(2).Datagram_Err;
  Slot02_SyncError_o       <= All_Error_Flags(2).Sync_Err;
  Slot02_SyncDone_o        <= All_SyncDone(2);
  Slot02_DelayOk_o         <= All_DelayOk(2);
  Slot02_BitslipOk_o       <= All_BitslipOk(2);
  Slot02_LinkIdle_o        <= All_LinkIdle(2);
  Slot02_RcvPktCount_o     <= All_RcvPktCount(2);

  Slot03_TData             <= All_Axis_Data(3).Data;
  Slot03_TValid            <= All_Axis_Vld(3);
  All_Axis_Rdy(3)          <= Slot03_TReady;
  Slot03_TLast             <= All_Axis_Data(3).Last;
  Slot03_Clk               <= DivClk_Global_i;
  Slot03_Bytes_o           <= All_Axis_Data(3).Bytes;
  Slot03_PktValid_o        <= All_PktParam(3).Valid;
  Slot03_EOE_o             <= All_PktParam(3).EOE;
  Slot03_PktBytes_o        <= All_PktParam(3).Bytes;
  Slot03_CrcErrors_o       <= All_Error_Flags(3).CRC_ErrCount;
  Slot03_FrameErrors_o     <= All_Error_Flags(3).Frame_ErrCount;
  Slot03_DatagramErrors_o  <= All_Error_Flags(3).Datagram_ErrCount;
  Slot03_SyncErrors_o      <= All_Error_Flags(3).Sync_ErrCount;
  Slot03_CrcError_o        <= All_Error_Flags(3).CRC_Err;
  Slot03_FrameError_o      <= All_Error_Flags(3).Frame_Err;
  Slot03_DatagramError_o   <= All_Error_Flags(3).Datagram_Err;
  Slot03_SyncError_o       <= All_Error_Flags(3).Sync_Err;
  Slot03_SyncDone_o        <= All_SyncDone(3);
  Slot03_DelayOk_o         <= All_DelayOk(3);
  Slot03_BitslipOk_o       <= All_BitslipOk(3);
  Slot03_LinkIdle_o        <= All_LinkIdle(3);
  Slot03_RcvPktCount_o     <= All_RcvPktCount(3);

  Slot04_TData             <= All_Axis_Data(4).Data;
  Slot04_TValid            <= All_Axis_Vld(4);
  All_Axis_Rdy(4)          <= Slot04_TReady;
  Slot04_TLast             <= All_Axis_Data(4).Last;
  Slot04_Clk               <= DivClk_Global_i;
  Slot04_Bytes_o           <= All_Axis_Data(4).Bytes;
  Slot04_PktValid_o        <= All_PktParam(4).Valid;
  Slot04_EOE_o             <= All_PktParam(4).EOE;
  Slot04_PktBytes_o        <= All_PktParam(4).Bytes;
  Slot04_CrcErrors_o       <= All_Error_Flags(4).CRC_ErrCount;
  Slot04_FrameErrors_o     <= All_Error_Flags(4).Frame_ErrCount;
  Slot04_DatagramErrors_o  <= All_Error_Flags(4).Datagram_ErrCount;
  Slot04_SyncErrors_o      <= All_Error_Flags(4).Sync_ErrCount;
  Slot04_CrcError_o        <= All_Error_Flags(4).CRC_Err;
  Slot04_FrameError_o      <= All_Error_Flags(4).Frame_Err;
  Slot04_DatagramError_o   <= All_Error_Flags(4).Datagram_Err;
  Slot04_SyncError_o       <= All_Error_Flags(4).Sync_Err;
  Slot04_SyncDone_o        <= All_SyncDone(4);
  Slot04_DelayOk_o         <= All_DelayOk(4);
  Slot04_BitslipOk_o       <= All_BitslipOk(4);
  Slot04_LinkIdle_o        <= All_LinkIdle(4);
  Slot04_RcvPktCount_o     <= All_RcvPktCount(4);

  Slot05_TData             <= All_Axis_Data(5).Data;
  Slot05_TValid            <= All_Axis_Vld(5);
  All_Axis_Rdy(5)          <= Slot05_TReady;
  Slot05_TLast             <= All_Axis_Data(5).Last;
  Slot05_Clk               <= DivClk_Global_i;
  Slot05_Bytes_o           <= All_Axis_Data(5).Bytes;
  Slot05_PktValid_o        <= All_PktParam(5).Valid;
  Slot05_EOE_o             <= All_PktParam(5).EOE;
  Slot05_PktBytes_o        <= All_PktParam(5).Bytes;
  Slot05_CrcErrors_o       <= All_Error_Flags(5).CRC_ErrCount;
  Slot05_FrameErrors_o     <= All_Error_Flags(5).Frame_ErrCount;
  Slot05_DatagramErrors_o  <= All_Error_Flags(5).Datagram_ErrCount;
  Slot05_SyncErrors_o      <= All_Error_Flags(5).Sync_ErrCount;
  Slot05_CrcError_o        <= All_Error_Flags(5).CRC_Err;
  Slot05_FrameError_o      <= All_Error_Flags(5).Frame_Err;
  Slot05_DatagramError_o   <= All_Error_Flags(5).Datagram_Err;
  Slot05_SyncError_o       <= All_Error_Flags(5).Sync_Err;
  Slot05_SyncDone_o        <= All_SyncDone(5);
  Slot05_DelayOk_o         <= All_DelayOk(5);
  Slot05_BitslipOk_o       <= All_BitslipOk(5);
  Slot05_LinkIdle_o        <= All_LinkIdle(5);
  Slot05_RcvPktCount_o     <= All_RcvPktCount(5);

  Slot06_TData             <= All_Axis_Data(6).Data;
  Slot06_TValid            <= All_Axis_Vld(6);
  All_Axis_Rdy(6)          <= Slot06_TReady;
  Slot06_TLast             <= All_Axis_Data(6).Last;
  Slot06_Clk               <= DivClk_Global_i;
  Slot06_Bytes_o           <= All_Axis_Data(6).Bytes;
  Slot06_PktValid_o        <= All_PktParam(6).Valid;
  Slot06_EOE_o             <= All_PktParam(6).EOE;
  Slot06_PktBytes_o        <= All_PktParam(6).Bytes;
  Slot06_CrcErrors_o       <= All_Error_Flags(6).CRC_ErrCount;
  Slot06_FrameErrors_o     <= All_Error_Flags(6).Frame_ErrCount;
  Slot06_DatagramErrors_o  <= All_Error_Flags(6).Datagram_ErrCount;
  Slot06_SyncErrors_o      <= All_Error_Flags(6).Sync_ErrCount;
  Slot06_CrcError_o        <= All_Error_Flags(6).CRC_Err;
  Slot06_FrameError_o      <= All_Error_Flags(6).Frame_Err;
  Slot06_DatagramError_o   <= All_Error_Flags(6).Datagram_Err;
  Slot06_SyncError_o       <= All_Error_Flags(6).Sync_Err;
  Slot06_SyncDone_o        <= All_SyncDone(6);
  Slot06_DelayOk_o         <= All_DelayOk(6);
  Slot06_BitslipOk_o       <= All_BitslipOk(6);
  Slot06_LinkIdle_o        <= All_LinkIdle(6);
  Slot06_RcvPktCount_o     <= All_RcvPktCount(6);

  Slot07_TData             <= All_Axis_Data(7).Data;
  Slot07_TValid            <= All_Axis_Vld(7);
  All_Axis_Rdy(7)          <= Slot07_TReady;
  Slot07_TLast             <= All_Axis_Data(7).Last;
  Slot07_Clk               <= DivClk_Global_i;
  Slot07_Bytes_o           <= All_Axis_Data(7).Bytes;
  Slot07_PktValid_o        <= All_PktParam(7).Valid;
  Slot07_EOE_o             <= All_PktParam(7).EOE;
  Slot07_PktBytes_o        <= All_PktParam(7).Bytes;
  Slot07_CrcErrors_o       <= All_Error_Flags(7).CRC_ErrCount;
  Slot07_FrameErrors_o     <= All_Error_Flags(7).Frame_ErrCount;
  Slot07_DatagramErrors_o  <= All_Error_Flags(7).Datagram_ErrCount;
  Slot07_SyncErrors_o      <= All_Error_Flags(7).Sync_ErrCount;
  Slot07_CrcError_o        <= All_Error_Flags(7).CRC_Err;
  Slot07_FrameError_o      <= All_Error_Flags(7).Frame_Err;
  Slot07_DatagramError_o   <= All_Error_Flags(7).Datagram_Err;
  Slot07_SyncError_o       <= All_Error_Flags(7).Sync_Err;
  Slot07_SyncDone_o        <= All_SyncDone(7);
  Slot07_DelayOk_o         <= All_DelayOk(7);
  Slot07_BitslipOk_o       <= All_BitslipOk(7);
  Slot07_LinkIdle_o        <= All_LinkIdle(7);
  Slot07_RcvPktCount_o     <= All_RcvPktCount(7);

  Slot08_TData             <= All_Axis_Data(8).Data;
  Slot08_TValid            <= All_Axis_Vld(8);
  All_Axis_Rdy(8)          <= Slot08_TReady;
  Slot08_TLast             <= All_Axis_Data(8).Last;
  Slot08_Clk               <= DivClk_Global_i;
  Slot08_Bytes_o           <= All_Axis_Data(8).Bytes;
  Slot08_PktValid_o        <= All_PktParam(8).Valid;
  Slot08_EOE_o             <= All_PktParam(8).EOE;
  Slot08_PktBytes_o        <= All_PktParam(8).Bytes;
  Slot08_CrcErrors_o       <= All_Error_Flags(8).CRC_ErrCount;
  Slot08_FrameErrors_o     <= All_Error_Flags(8).Frame_ErrCount;
  Slot08_DatagramErrors_o  <= All_Error_Flags(8).Datagram_ErrCount;
  Slot08_SyncErrors_o      <= All_Error_Flags(8).Sync_ErrCount;
  Slot08_CrcError_o        <= All_Error_Flags(8).CRC_Err;
  Slot08_FrameError_o      <= All_Error_Flags(8).Frame_Err;
  Slot08_DatagramError_o   <= All_Error_Flags(8).Datagram_Err;
  Slot08_SyncError_o       <= All_Error_Flags(8).Sync_Err;
  Slot08_SyncDone_o        <= All_SyncDone(8);
  Slot08_DelayOk_o         <= All_DelayOk(8);
  Slot08_BitslipOk_o       <= All_BitslipOk(8);
  Slot08_LinkIdle_o        <= All_LinkIdle(8);
  Slot08_RcvPktCount_o     <= All_RcvPktCount(8);

  Slot09_TData             <= All_Axis_Data(9).Data;
  Slot09_TValid            <= All_Axis_Vld(9);
  All_Axis_Rdy(9)          <= Slot09_TReady;
  Slot09_TLast             <= All_Axis_Data(9).Last;
  Slot09_Clk               <= DivClk_Global_i;
  Slot09_Bytes_o           <= All_Axis_Data(9).Bytes;
  Slot09_PktValid_o        <= All_PktParam(9).Valid;
  Slot09_EOE_o             <= All_PktParam(9).EOE;
  Slot09_PktBytes_o        <= All_PktParam(9).Bytes;
  Slot09_CrcErrors_o       <= All_Error_Flags(9).CRC_ErrCount;
  Slot09_FrameErrors_o     <= All_Error_Flags(9).Frame_ErrCount;
  Slot09_DatagramErrors_o  <= All_Error_Flags(9).Datagram_ErrCount;
  Slot09_SyncErrors_o      <= All_Error_Flags(9).Sync_ErrCount;
  Slot09_CrcError_o        <= All_Error_Flags(9).CRC_Err;
  Slot09_FrameError_o      <= All_Error_Flags(9).Frame_Err;
  Slot09_DatagramError_o   <= All_Error_Flags(9).Datagram_Err;
  Slot09_SyncError_o       <= All_Error_Flags(9).Sync_Err;
  Slot09_SyncDone_o        <= All_SyncDone(9);
  Slot09_DelayOk_o         <= All_DelayOk(9);
  Slot09_BitslipOk_o       <= All_BitslipOk(9);
  Slot09_LinkIdle_o        <= All_LinkIdle(9);
  Slot09_RcvPktCount_o     <= All_RcvPktCount(9);

  Slot10_TData             <= All_Axis_Data(10).Data;
  Slot10_TValid            <= All_Axis_Vld(10);
  All_Axis_Rdy(10)         <= Slot10_TReady;
  Slot10_TLast             <= All_Axis_Data(10).Last;
  Slot10_Clk               <= DivClk_Global_i;
  Slot10_Bytes_o           <= All_Axis_Data(10).Bytes;
  Slot10_PktValid_o        <= All_PktParam(10).Valid;
  Slot10_EOE_o             <= All_PktParam(10).EOE;
  Slot10_PktBytes_o        <= All_PktParam(10).Bytes;
  Slot10_CrcErrors_o       <= All_Error_Flags(10).CRC_ErrCount;
  Slot10_FrameErrors_o     <= All_Error_Flags(10).Frame_ErrCount;
  Slot10_DatagramErrors_o  <= All_Error_Flags(10).Datagram_ErrCount;
  Slot10_SyncErrors_o      <= All_Error_Flags(10).Sync_ErrCount;
  Slot10_CrcError_o        <= All_Error_Flags(10).CRC_Err;
  Slot10_FrameError_o      <= All_Error_Flags(10).Frame_Err;
  Slot10_DatagramError_o   <= All_Error_Flags(10).Datagram_Err;
  Slot10_SyncError_o       <= All_Error_Flags(10).Sync_Err;
  Slot10_SyncDone_o        <= All_SyncDone(10);
  Slot10_DelayOk_o         <= All_DelayOk(10);
  Slot10_BitslipOk_o       <= All_BitslipOk(10);
  Slot10_LinkIdle_o        <= All_LinkIdle(10);
  Slot10_RcvPktCount_o     <= All_RcvPktCount(10);

  Slot11_TData             <= All_Axis_Data(11).Data;
  Slot11_TValid            <= All_Axis_Vld(11);
  All_Axis_Rdy(11)         <= Slot11_TReady;
  Slot11_TLast             <= All_Axis_Data(11).Last;
  Slot11_Clk               <= DivClk_Global_i;
  Slot11_Bytes_o           <= All_Axis_Data(11).Bytes;
  Slot11_PktValid_o        <= All_PktParam(11).Valid;
  Slot11_EOE_o             <= All_PktParam(11).EOE;
  Slot11_PktBytes_o        <= All_PktParam(11).Bytes;
  Slot11_CrcErrors_o       <= All_Error_Flags(11).CRC_ErrCount;
  Slot11_FrameErrors_o     <= All_Error_Flags(11).Frame_ErrCount;
  Slot11_DatagramErrors_o  <= All_Error_Flags(11).Datagram_ErrCount;
  Slot11_SyncErrors_o      <= All_Error_Flags(11).Sync_ErrCount;
  Slot11_CrcError_o        <= All_Error_Flags(11).CRC_Err;
  Slot11_FrameError_o      <= All_Error_Flags(11).Frame_Err;
  Slot11_DatagramError_o   <= All_Error_Flags(11).Datagram_Err;
  Slot11_SyncError_o       <= All_Error_Flags(11).Sync_Err;
  Slot11_SyncDone_o        <= All_SyncDone(11);
  Slot11_DelayOk_o         <= All_DelayOk(11);
  Slot11_BitslipOk_o       <= All_BitslipOk(11);
  Slot11_LinkIdle_o        <= All_LinkIdle(11);
  Slot11_RcvPktCount_o     <= All_RcvPktCount(11);

  Slot12_TData             <= All_Axis_Data(12).Data;
  Slot12_TValid            <= All_Axis_Vld(12);
  All_Axis_Rdy(12)         <= Slot12_TReady;
  Slot12_TLast             <= All_Axis_Data(12).Last;
  Slot12_Clk               <= DivClk_Global_i;
  Slot12_Bytes_o           <= All_Axis_Data(12).Bytes;
  Slot12_PktValid_o        <= All_PktParam(12).Valid;
  Slot12_EOE_o             <= All_PktParam(12).EOE;
  Slot12_PktBytes_o        <= All_PktParam(12).Bytes;
  Slot12_CrcErrors_o       <= All_Error_Flags(12).CRC_ErrCount;
  Slot12_FrameErrors_o     <= All_Error_Flags(12).Frame_ErrCount;
  Slot12_DatagramErrors_o  <= All_Error_Flags(12).Datagram_ErrCount;
  Slot12_SyncErrors_o      <= All_Error_Flags(12).Sync_ErrCount;
  Slot12_CrcError_o        <= All_Error_Flags(12).CRC_Err;
  Slot12_FrameError_o      <= All_Error_Flags(12).Frame_Err;
  Slot12_DatagramError_o   <= All_Error_Flags(12).Datagram_Err;
  Slot12_SyncError_o       <= All_Error_Flags(12).Sync_Err;
  Slot12_SyncDone_o        <= All_SyncDone(12);
  Slot12_DelayOk_o         <= All_DelayOk(12);
  Slot12_BitslipOk_o       <= All_BitslipOk(12);
  Slot12_LinkIdle_o        <= All_LinkIdle(12);
  Slot12_RcvPktCount_o     <= All_RcvPktCount(12);

  Slot13_TData             <= All_Axis_Data(13).Data;
  Slot13_TValid            <= All_Axis_Vld(13);
  All_Axis_Rdy(13)         <= Slot13_TReady;
  Slot13_TLast             <= All_Axis_Data(13).Last;
  Slot13_Clk               <= DivClk_Global_i;
  Slot13_Bytes_o           <= All_Axis_Data(13).Bytes;
  Slot13_PktValid_o        <= All_PktParam(13).Valid;
  Slot13_EOE_o             <= All_PktParam(13).EOE;
  Slot13_PktBytes_o        <= All_PktParam(13).Bytes;
  Slot13_CrcErrors_o       <= All_Error_Flags(13).CRC_ErrCount;
  Slot13_FrameErrors_o     <= All_Error_Flags(13).Frame_ErrCount;
  Slot13_DatagramErrors_o  <= All_Error_Flags(13).Datagram_ErrCount;
  Slot13_SyncErrors_o      <= All_Error_Flags(13).Sync_ErrCount;
  Slot13_CrcError_o        <= All_Error_Flags(13).CRC_Err;
  Slot13_FrameError_o      <= All_Error_Flags(13).Frame_Err;
  Slot13_DatagramError_o   <= All_Error_Flags(13).Datagram_Err;
  Slot13_SyncError_o       <= All_Error_Flags(13).Sync_Err;
  Slot13_SyncDone_o        <= All_SyncDone(13);
  Slot13_DelayOk_o         <= All_DelayOk(13);
  Slot13_BitslipOk_o       <= All_BitslipOk(13);
  Slot13_LinkIdle_o        <= All_LinkIdle(13);
  Slot13_RcvPktCount_o     <= All_RcvPktCount(13);

  Slot14_TData             <= All_Axis_Data(14).Data;
  Slot14_TValid            <= All_Axis_Vld(14);
  All_Axis_Rdy(14)         <= Slot14_TReady;
  Slot14_TLast             <= All_Axis_Data(14).Last;
  Slot14_Clk               <= DivClk_Global_i;
  Slot14_Bytes_o           <= All_Axis_Data(14).Bytes;
  Slot14_PktValid_o        <= All_PktParam(14).Valid;
  Slot14_EOE_o             <= All_PktParam(14).EOE;
  Slot14_PktBytes_o        <= All_PktParam(14).Bytes;
  Slot14_CrcErrors_o       <= All_Error_Flags(14).CRC_ErrCount;
  Slot14_FrameErrors_o     <= All_Error_Flags(14).Frame_ErrCount;
  Slot14_DatagramErrors_o  <= All_Error_Flags(14).Datagram_ErrCount;
  Slot14_SyncErrors_o      <= All_Error_Flags(14).Sync_ErrCount;
  Slot14_CrcError_o        <= All_Error_Flags(14).CRC_Err;
  Slot14_FrameError_o      <= All_Error_Flags(14).Frame_Err;
  Slot14_DatagramError_o   <= All_Error_Flags(14).Datagram_Err;
  Slot14_SyncError_o       <= All_Error_Flags(14).Sync_Err;
  Slot14_SyncDone_o        <= All_SyncDone(14);
  Slot14_DelayOk_o         <= All_DelayOk(14);
  Slot14_BitslipOk_o       <= All_BitslipOk(14);
  Slot14_LinkIdle_o        <= All_LinkIdle(14);
  Slot14_RcvPktCount_o     <= All_RcvPktCount(14);

  Slot15_TData             <= All_Axis_Data(15).Data;
  Slot15_TValid            <= All_Axis_Vld(15);
  All_Axis_Rdy(15)         <= Slot15_TReady;
  Slot15_TLast             <= All_Axis_Data(15).Last;
  Slot15_Clk               <= DivClk_Global_i;
  Slot15_Bytes_o           <= All_Axis_Data(15).Bytes;
  Slot15_PktValid_o        <= All_PktParam(15).Valid;
  Slot15_EOE_o             <= All_PktParam(15).EOE;
  Slot15_PktBytes_o        <= All_PktParam(15).Bytes;
  Slot15_CrcErrors_o       <= All_Error_Flags(15).CRC_ErrCount;
  Slot15_FrameErrors_o     <= All_Error_Flags(15).Frame_ErrCount;
  Slot15_DatagramErrors_o  <= All_Error_Flags(15).Datagram_ErrCount;
  Slot15_SyncErrors_o      <= All_Error_Flags(15).Sync_ErrCount;
  Slot15_CrcError_o        <= All_Error_Flags(15).CRC_Err;
  Slot15_FrameError_o      <= All_Error_Flags(15).Frame_Err;
  Slot15_DatagramError_o   <= All_Error_Flags(15).Datagram_Err;
  Slot15_SyncError_o       <= All_Error_Flags(15).Sync_Err;
  Slot15_SyncDone_o        <= All_SyncDone(15);
  Slot15_DelayOk_o         <= All_DelayOk(15);
  Slot15_BitslipOk_o       <= All_BitslipOk(15);
  Slot15_LinkIdle_o        <= All_LinkIdle(15);
  Slot15_RcvPktCount_o     <= All_RcvPktCount(15);

  Slot16_TData             <= All_Axis_Data(16).Data;
  Slot16_TValid            <= All_Axis_Vld(16);
  All_Axis_Rdy(16)         <= Slot16_TReady;
  Slot16_TLast             <= All_Axis_Data(16).Last;
  Slot16_Clk               <= DivClk_Global_i;
  Slot16_Bytes_o           <= All_Axis_Data(16).Bytes;
  Slot16_PktValid_o        <= All_PktParam(16).Valid;
  Slot16_EOE_o             <= All_PktParam(16).EOE;
  Slot16_PktBytes_o        <= All_PktParam(16).Bytes;
  Slot16_CrcErrors_o       <= All_Error_Flags(16).CRC_ErrCount;
  Slot16_FrameErrors_o     <= All_Error_Flags(16).Frame_ErrCount;
  Slot16_DatagramErrors_o  <= All_Error_Flags(16).Datagram_ErrCount;
  Slot16_SyncErrors_o      <= All_Error_Flags(16).Sync_ErrCount;
  Slot16_CrcError_o        <= All_Error_Flags(16).CRC_Err;
  Slot16_FrameError_o      <= All_Error_Flags(16).Frame_Err;
  Slot16_DatagramError_o   <= All_Error_Flags(16).Datagram_Err;
  Slot16_SyncError_o       <= All_Error_Flags(16).Sync_Err;
  Slot16_SyncDone_o        <= All_SyncDone(16);
  Slot16_DelayOk_o         <= All_DelayOk(16);
  Slot16_BitslipOk_o       <= All_BitslipOk(16);
  Slot16_LinkIdle_o        <= All_LinkIdle(16);
  Slot16_RcvPktCount_o     <= All_RcvPktCount(16);

  IdelayCtrl_Rdy_o <= ReduceAnd(All_IdelayCtrl_Rdy);

  g_idelayctrl : for c in 0 to IdelayCtrls_cgn-1 generate
    IDELAYCTRL_inst : IDELAYCTRL
    port map (
      RDY    => All_IdelayCtrl_Rdy(c),
      REFCLK => IdelayCtrl_Refclk_i,
      RST    => IdelayCtrl_Rst_i
    );
  end generate;

  g_stream : for s in 0 to Streams_cgn-1 generate
    i_serdes_pkt_rcvr : entity work.serdes_pkt_rcvr
    generic map(
      BitOrder_cgn    => BitOrder_cgn,
      IdlePattern_cgn => IdlePattern_cgn,
      SDataRate_cgn   => SDataRate_c,
      DlyRefClk_cgn   => DlyRefClk_c,
      FifoDepth_cgn   => FifoDepth_c
    )
    port map(
      TestEye_o          => All_TestEye_o(s),
      TestTap_o          => All_TestTap_o(s),
      ParallelData_Dbg_o => ParallelData_Dbg_o((s+1)*8-1 downto s*8),
      CrcOk_Dbg_o        => CrcOk_Dbg_o(s),
      BitClk_Serdes_i    => All_BitClk_Serdes(s),
      DivClk_Serdes_i    => All_DivClk_Serdes(s),
      DivClk_i           => DivClk_Global_i,
      Rst_i              => Rst,
      Resync_i           => Resync,
      Error_Flags_o      => All_Error_Flags(s),
      Rst_Errors_i       => Rst_Errors,
      RcvPktCount_o      => All_RcvPktCount(s),
      Rst_RcvPktCount_i  => Rst_RcvPktCount,
      SyncDone_o         => All_SyncDone(s),
      BitslipOk_o        => All_BitslipOk(s),
      DelayOk_o          => All_DelayOk(s),
      LinkIdle_o         => All_LinkIdle(s),
      Axis_Data_o        => All_Axis_Data(s),
      Axis_Vld_o         => All_Axis_Vld(s),
      Axis_Rdy_i         => All_Axis_Rdy(s),
      PktParam_o         => All_PktParam(s),
      SerialData_i_p     => SerialData_i_p(s),
      SerialData_i_n     => SerialData_i_n(s),
      ReadyToRcv_o_p     => ReadyToRcv_o_p(s),
      ReadyToRcv_o_n     => ReadyToRcv_o_n(s)
    );
  end generate;

end rtl;
