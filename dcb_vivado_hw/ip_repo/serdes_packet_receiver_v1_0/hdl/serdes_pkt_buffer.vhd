------------------------------------------------------------------------------
--  Copyright (c) 2021 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Elmar Schmid
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Description
------------------------------------------------------------------------------
-- This buffer stores a WDAQ packet received via a serdes link to make it
-- available for further prcessing or transmission. After storing a full
-- packet some SERDES receiver error flags in the packet header are updated
-- to allow for upstream SERDES debugging.
-- The flag handling at the output is packet based, i.e. the valid flag goes
-- high after a full packet is stored.
-- The errors reported in the header are CRC Error if the checksum at the
-- SERDES receiver doesn't match the checksum in the packet and Datagram
-- Error if there is an inconsistency in SOF/EOF flags and event number.

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.psi_common_math_pkg.all;
use work.serdes_pkt_rcvr_pkg.all;

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
entity serdes_pkt_buffer is
  generic(
    Width_g       : positive  := 64; -- multiples of 8
    Depth_g       : positive  := 512;
    RamStyle_g    : string    := "auto"
  );
  port(
    -- Control Ports
    Clk_i            : in  std_logic;
    Rst_i            : in  std_logic;
    -- Input Interface
    InData_i         : in  std_logic_vector(Width_g - 1 downto 0);
    InValid_i        : in  std_logic;
    InReady_o        : out std_logic;           -- not full
    InLast_i         : in  std_logic;
    InPktParam_i     : in  Pkt_Param_t;
    BufferFull_o     : out std_logic;
    BufferOverflow_o : out std_logic;
    -- Input Flags
    PktErrorFlags_i  : in  PktError_t;
    FlagsValid_i     : in  std_logic;
    FlagsReady_o     : out std_logic;
    -- Output Interface
    OutData_o        : out std_logic_vector(Width_g - 1 downto 0);
    OutValid_o       : out std_logic;           -- not empty
    OutReady_i       : in  std_logic;
    OutLast_o        : out std_logic;
    OutBytes_o       : out std_logic_vector(log2ceil(Width_g/8) downto 0);
    OutPktParam_o    : out Pkt_Param_t
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of serdes_pkt_buffer is

  constant AddrWidth_c        : integer := log2ceil(Depth_g);
  constant FlagWord_c         : integer := HdrWdaqFlagByte_cpk / 8;
  constant FlagAddr_c         : std_logic_vector(AddrWidth_c-1 downto 0) := std_logic_vector(to_unsigned(FlagWord_c, AddrWidth_c));
  constant AddrMax_c          : std_logic_vector(AddrWidth_c-1 downto 0) := std_logic_vector(to_unsigned(Depth_g - 1, AddrWidth_c));
  constant FlagByteOffset_c   : integer := HdrWdaqFlagByte_cpk - (FlagWord_c * 8);
  constant CrcErrFlag_idx_c   : integer := FlagByteOffset_c * 8 + HdrWdaqCrcErrFlagBit_cpk;
  constant DtgrmErrFlag_idx_c : integer := FlagByteOffset_c * 8 + HdrWdaqDtgrmErrFlagBit_cpk;
  constant BufOvflFlag_idx_c  : integer := FlagByteOffset_c * 8 + HdrWdaqBufOvflFlagBit_cpk;

  signal RamWr     : std_logic := '0';
  signal RamWrData : std_logic_vector(Width_g - 1 downto 0)   := (others=>'0');
  signal RamWrAddr : std_logic_vector(AddrWidth_c-1 downto 0) := (others=>'1');
  signal RamRdData : std_logic_vector(Width_g - 1 downto 0)   := (others=>'0');
  signal RamRdAddr : std_logic_vector(AddrWidth_c-1 downto 0) := (others=>'1');
  signal PktValid  : std_logic := '0';

  -- Types
  type State_t is (DataIn_s, UpdateHeader_s, DataOut_s);

  type fsm_signals_r is record
    State       : State_t;
    WrAddr      : std_logic_vector(AddrWidth_c-1 downto 0);
    RdAddr      : std_logic_vector(AddrWidth_c-1 downto 0);
    RamFull     : std_logic;
    RamOverflow : std_logic;
    Ready       : std_logic;
    Valid       : std_logic;
    Last        : std_logic;
    OutBytes    : std_logic_vector(log2ceil(Width_g/8) downto 0);
    PktBytes    : std_logic_vector(MaxPktBytesSize_cpk-1 downto 0);
    BufferFull  : std_logic;
  end record;

  signal r, r_next : fsm_signals_r;

begin

  OutPktParam_o.EOE   <= InPktParam_i.EOE;
  OutPktParam_o.Bytes <= r.PktBytes;
  OutPktParam_o.Valid <= PktValid;
  OutLast_o           <= r.Last;
  OutBytes_o          <= r.OutBytes;
  OutData_o           <= RamRdData;
  InReady_o           <= r.Ready;
  BufferFull_o        <= r.BufferFull;
  BufferOverflow_o    <= r.RamOverflow;

  p_comb : process(r, InData_i, InValid_i, InLast_i, PktErrorFlags_i, FlagsValid_i, OutReady_i, InPktParam_i, RamRdData)
    variable v : fsm_signals_r;
  begin
    v := r;

    -- defaults
    v.Ready       := '0';
    RamWr         <= '0';
    RamWrAddr     <= r.WrAddr;
    RamWrData     <= InData_i;
    PktValid      <= '0';
    OutValid_o    <= r.Valid;
    FlagsReady_o  <= '0';
    v.Valid       := '0';
    v.Last        := '0';
    v.OutBytes    := "1000";

    case r.State is
      when DataIn_s =>
        v.Ready := '1';
        if InValid_i = '1' then
          -- handle flags and update write adress
          if r.WrAddr = AddrMax_c then
            v.RamFull := '1';
          elsif InLast_i = '0' then
            v.WrAddr := std_logic_vector(unsigned(r.WrAddr) + 1);
          end if;
          if r.RamFull = '1' then
            v.RamOverflow := '1';
            v.PktBytes    := std_logic_vector(to_unsigned(Depth_g * Width_g / 8, r.PktBytes'length));
          else
            v.PktBytes    := InPktParam_i.Bytes;
          end if;
          -- write enable
          if r.RamFull = '0' then
            RamWr <= '1';
          end if;
          -- update state and read address
          if InLast_i = '1' then
            v.Ready      := '0';
            v.BufferFull := '1';
            v.RdAddr     := FlagAddr_c;
            v.State      := UpdateHeader_s;
          end if;
        end if;
      when UpdateHeader_s =>
        if FlagsValid_i = '1' then
          FlagsReady_o <= '1';
          RamWr        <= '1';
          RamWrAddr    <= FlagAddr_c;
          RamWrData                     <= RamRdData;
          RamWrData(CrcErrFlag_idx_c)   <= PktErrorFlags_i.CRC_Err;
          RamWrData(DtgrmErrFlag_idx_c) <= PktErrorFlags_i.Datagram_Err;
          RamWrData(BufOvflFlag_idx_c)  <= r.RamOverflow;
          v.RdAddr  := (others=>'0');
          v.State   := DataOut_s;
        end if;
      when DataOut_s =>
        PktValid      <= '1'; -- always ready to read buffer
        v.Valid       := '1';
        v.Last        := r.Last;
        v.OutBytes    := r.OutBytes;
        if OutReady_i = '1' then
          -- update read address
          if r.RdAddr < AddrMax_c then
            v.RdAddr := std_logic_vector(unsigned(r.RdAddr) + 1);
          end if;
          if r.WrAddr = v.RdAddr then
            -- handle last signal and output bytes
            v.Last := '1';
            if unsigned(r.PktBytes(2 downto 0)) /= 0 then
              v.OutBytes := std_logic_vector('0' & r.PktBytes(2 downto 0));
            end if;
          end if;
          if r.Last = '1' then
            -- read completed -> reset and get ready to write again
            v.Valid       := '0';
            v.WrAddr      := (others=>'0');
            v.RamOverflow := '0';
            v.Last        := '0';
            v.BufferFull  := '0';
            v.State  := DataIn_s;
          end if;
        end if;
      when others =>
        v.State := DataIn_s;
    end case;

    RamRdAddr <= v.RdAddr; -- Read Address needs to be updated directly to latch new data on next clock
    r_next <= v;
  end process;

p_seq : process(Clk_i)
begin
  if rising_edge(Clk_i) then
    r <= r_next;
    if Rst_i = '1' then
      r.State       <= DataIn_s;
      r.WrAddr      <= (others=>'0');
      r.RdAddr      <= (others=>'0');
      r.RamFull     <= '0';
      r.RamOverflow <= '0';
      r.Last        <= '0';
      r.OutBytes    <= "1000";
      r.PktBytes    <= (others=>'0');
      r.BufferFull  <= '0';
    end if;
  end if;
end process;

  --------------------------------------------------------------------------
  -- Component Instantiations
  --------------------------------------------------------------------------
  i_ram : entity work.psi_common_sdp_ram
    generic map(
      Depth_g    => Depth_g,
      Width_g    => Width_g,
      RamStyle_g => RamStyle_g,
      Behavior_g => "RBW"
    )
    port map(
      Clk    => Clk_i,
      WrAddr => RamWrAddr,
      Wr     => RamWr,
      WrData => RamWrData,
      RdAddr => RamRdAddr,
      RdData => RamRdData
    );

end;
