------------------------------------------------------------------------------
--  Copyright (c) 2020 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Elmar Schmid
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.serdes_pkt_rcvr_pkg.all;

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
-- $$ testcases=frame_valid,errors $$
-- $$ processes=deserializer,b2w_buffer,fifo,output,error_out $$
-- $$ tbpkg=work.psi_tb_txt_util,work.psi_tb_compare_pkg,work.psi_tb_activity_pkg $$
entity serdes_pkt_rcvr_ctrl is
  port (
    -- Control signals
    Clk_i             : in  std_logic;   -- $$ type=clk; freq=80e6; proc=deserializer,b2w_buffer,fifo,output,error_out $$
    Rst_i             : in  std_logic;   -- $$ type=rst; clk=Clk_i; $$

    -- Deserializer
    DesData_i         : in  std_logic_vector(7 downto 0);   -- $$ proc=deserializer $$
    ReadyToRcv_o      : out std_logic;   -- $$ proc=deserializer $$
    SyncDone_i        : in  std_logic;   -- $$ proc=deserializer $$

    -- Buffer control
    B2W_Din_Vld_o     : out std_logic;   -- $$ proc=b2w_buffer $$
    B2W_Din_Rdy_i     : in  std_logic;   -- $$ proc=b2w_buffer $$
    B2W_Din_Last_o    : out std_logic;   -- $$ proc=b2w_buffer $$
    BufferFull_i      : in  std_logic;   -- $$ proc=b2w_buffer $$
    BufferOverflow_i  : in  std_logic;   -- $$ proc=b2w_buffer $$
    Buffer_Rst_o      : out std_logic;   -- $$ proc=fifo $$
    ByteToWord_Rst_o  : out std_logic;   -- $$ proc=b2w_buffer $$
    PktErrorFlags_o   : out PktError_t;  -- $$ proc=error_out $$
    FlagsValid_o      : out std_logic;   -- $$ proc=error_out $$
    FlagsReady_i      : in  std_logic;   -- $$ proc=error_out $$

    -- CRC checker input
    Rst_CRC_o         : out std_logic;   -- $$ proc=crc $$
    CRC_Vld_o         : out std_logic;   -- $$ proc=crc $$
    CRC_Ok_i          : in  std_logic;   -- $$ proc=crc $$

    -- Output control
    PktParam_o        : out Pkt_Param_t;   -- $$ proc=output $$

    -- Debug information
    RcvPktCount_o     : out std_logic_vector(PktCountBits_cpk-1 downto 0); -- $$ proc=pkt_count_out $$
    Rst_RcvPktCount_i : in  std_logic;     -- $$ proc=pkt_count_out $$
    Error_Flags_o     : out Error_t;       -- $$ proc=error_out $$
    Rst_Errors_i      : in  std_logic      -- $$ proc=error_out $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of serdes_pkt_rcvr_ctrl is

  constant HdrCountSize_c  : integer := log2ceil(HdrBytes_cpk-1);

  signal DesDataS          : std_logic_vector(7 downto 0) := (others=>'0');
  signal PktWords          : unsigned(MaxPktBytesSize_cpk-1 downto 0) := (others=>'0');
  signal Frame_ErrCount    : unsigned(ErrCountBits_cpk-1 downto 0) := (others=>'0');
  signal Frame_Err         : std_logic := '0';
  signal Frame_ErrLock_B2W : std_logic := '0';
  signal Datagram_ErrCount : unsigned(ErrCountBits_cpk-1 downto 0) := (others=>'0');
  signal Datagram_Err      : std_logic := '0';
  signal Datagram_ErrLock  : std_logic := '0';
  signal Sync_ErrCount     : unsigned(ErrCountBits_cpk-1 downto 0) := (others=>'0');
  signal Sync_Err          : std_logic := '0';
  signal CRC_ErrCount      : unsigned(ErrCountBits_cpk-1 downto 0) := (others=>'0');
  signal CRC_Err           : std_logic := '0';
  signal CRC_ErrLock       : std_logic := '0';
  signal PktCount          : unsigned(PktCountBits_cpk-1 downto 0) := (others=>'0');
  signal SetPktErrCrc      : std_logic := '0';
  signal SetPktErrDatagram : std_logic := '0';

  -- Types
  type State_t is (Idle_s, SOF_s, RcvFrameLen_s, RcvData_s, RcvFCS_s, WaitBuffer_s, SndData_s, WaitIdle_s);

  type one_process_r is record
    State         : State_t;
    ByteCount     : unsigned(15 downto 0);
    HeaderCount   : unsigned(HdrCountSize_c-1 downto 0);
    ReadyToRcv    : std_logic;
    FrameLen      : unsigned(15 downto 0);
    B2W_Din_Vld   : std_logic;
    B2W_Din_Last  : std_logic;
    FirstPkt      : std_logic;
    EoeFlag       : std_logic;
    SoeFlag       : std_logic;
    EventNr       : std_logic_vector(31 downto 0);
    EventNrChange : std_logic;
    ExpectSoe     : std_logic;
    CrcChk        : std_logic;
    CrcRst        : std_logic;
    CrcVld        : std_logic;
    SofFailed     : std_logic;
    SyncErr       : std_logic;
    ClrPktFlags   : std_logic;
    SetFlagsVld   : std_logic;
  end record;
  signal r : one_process_r;

  attribute fsm_encoding : string;
--  attribute fsm_encoding of r.State : signal is "one-hot";

begin

  Buffer_Rst_o     <= Rst_i; -- or RST upon Error
  ByteToWord_Rst_o <= Rst_i; -- or RST upon Error

  LengthReg_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      PktWords <= (r.FrameLen + 7) / 8;
    end if;
  end process;

  -------------------------------------------------------
  -- Output Assignments
  -------------------------------------------------------
  ReadyToRcv_o     <= r.ReadyToRcv;
  B2W_Din_Vld_o    <= r.B2W_Din_Vld;
  B2W_Din_Last_o   <= r.B2W_Din_Last;
  PktParam_o.Valid <= '0';
  PktParam_o.EOE   <= r.EoeFlag;
  PktParam_o.Bytes <= std_logic_vector(r.FrameLen);
  Rst_CRC_o        <= r.CrcRst;
  CRC_Vld_o        <= r.CrcVld;

  -------------------------------------------------------
  -- FSM
  -------------------------------------------------------
  PktCtrlFsm_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if Rst_i = '1' then
        r.State         <= Idle_s;
        r.ByteCount     <= (others=>'0');
        r.HeaderCount   <= (others=>'1');
        r.FrameLen      <= (others=>'0');
        r.B2W_Din_Vld   <= '0';
        r.B2W_Din_Last  <= '0';
        r.FirstPkt      <= '1';
        r.EoeFlag       <= '1';
        r.SoeFlag       <= '0';
        r.ExpectSoe     <= '0';
        r.EventNr       <= (others=>'0');
        r.EventNrChange <= '0';
        r.CrcVld        <= '0';
        r.CrcRst        <= '1';
      else
        -- defaults -----------------------
        r.ReadyToRcv    <= '0';
        r.CrcChk        <= '0';
        r.CrcRst        <= '0';
        r.SofFailed     <= '0';
        r.SyncErr       <= '0';
        r.ClrPktFlags   <= '0';
        r.B2W_Din_Last  <= '0';
        r.SetFlagsVld   <= '0';
        -----------------------------------
        case r.State is
          when Idle_s =>
            r.ReadyToRcv    <= '1';
            r.EventNrChange <= '0';
            r.ClrPktFlags   <= '1';
            if SyncDone_i = DesInSync_cpk then
              if DesData_i = SofByte0_cpk then
                r.State <= SOF_s;
              elsif DesData_i /= IdleByte_cpk then
                r.SyncErr <= '1';
                r.State <= WaitIdle_s;
              end if;
            end if;
          when SOF_s =>
            if DesData_i = SofByte1_cpk then
              r.ByteCount <= to_unsigned(2, r.ByteCount'length);
              r.State <= RcvFrameLen_s;
            else
              r.SofFailed <= '1';
              r.State <= WaitIdle_s;
            end if;
          when RcvFrameLen_s =>
            r.ByteCount <= r.ByteCount-1;
            if r.ByteCount(0) = '0' then
              r.FrameLen(15 downto 8) <= unsigned(DesData_i);
            else -- r.ByteCount(0) = '0'
              r.FrameLen(7 downto 0) <= unsigned(DesData_i);
              r.ByteCount <= r.FrameLen(15 downto 8) & unsigned(DesData_i);
              r.HeaderCount <= (others=>'0');
              r.B2W_Din_Vld <= '1';
              r.CrcVld      <= '1';
              r.State <= RcvData_s;
            end if;
          when RcvData_s =>
            -- cannot stop at B2W_Din_Rdy = '0' since SERDES keeps receiving
            r.ByteCount <= r.ByteCount-1;
            if r.ByteCount = 2 then
              r.B2W_Din_Last  <= '1';
            elsif r.ByteCount = 1 then
              r.ByteCount <= to_unsigned(4, r.ByteCount'length);
              r.B2W_Din_Vld <= '0';
              r.State <= RcvFCS_s;
            end if;
          -- store relevant header information
            if r.HeaderCount < to_unsigned(HdrBytes_cpk, r.HeaderCount'length) then
              r.HeaderCount <= r.HeaderCount+1;
            end if;
            case r.HeaderCount is
              -- Flags
              when to_unsigned(HdrWdaqFlagByte_cpk, HdrCountSize_c) =>
                r.EoeFlag   <= DesData_i(HdrWdaqEoeFlagBit_cpk);
                r.SoeFlag   <= DesData_i(HdrWdaqSoeFlagBit_cpk);
              -- Event Number
              when to_unsigned(HdrEventNrMsbByte_cpk, HdrCountSize_c) =>
                if r.EventNr(31 downto 24) /= DesData_i then
                  r.EventNrChange <= '1';
                end if;
                r.EventNr(31 downto 24) <= DesData_i;
              when to_unsigned(HdrEventNrMsbByte_cpk+1, HdrCountSize_c) =>
                if r.EventNr(23 downto 16) /= DesData_i then
                  r.EventNrChange <= '1';
                end if;
                r.EventNr(23 downto 16) <= DesData_i;
              when to_unsigned(HdrEventNrMsbByte_cpk+2, HdrCountSize_c) =>
                if r.EventNr(15 downto  8) /= DesData_i then
                  r.EventNrChange <= '1';
                end if;
                r.EventNr(15 downto  8) <= DesData_i;
              when to_unsigned(HdrEventNrMsbByte_cpk+3, HdrCountSize_c) =>
                if r.EventNr( 7 downto  0) /= DesData_i then
                  r.EventNrChange <= '1';
                end if;
                r.EventNr( 7 downto  0) <= DesData_i;
              when others => null;
            end case;
          when RcvFCS_s =>
            r.ByteCount <= r.ByteCount-1;
            if r.ByteCount = 1 then
              r.CrcVld <= '0';
              r.CrcChk <= '1';
              r.State <= WaitBuffer_s;
            end if;
          when WaitBuffer_s =>
            if BufferFull_i = '1' then
              r.SetFlagsVld <= '1';
              r.State <= SndData_s;
            end if;
          when SndData_s =>
            if BufferFull_i = '0' then
              r.ExpectSoe <= r.EoeFlag;
              r.EoeFlag   <= '0';
              r.SoeFlag   <= '0';
              r.CrcRst    <= '1';
              r.FirstPkt  <= '0';
              r.State <= Idle_s;
            end if;
          when WaitIdle_s =>
            if SyncDone_i = DesInSync_cpk and DesData_i = IdleByte_cpk then
              r.State <= Idle_s;
            end if;
          when others =>
            r.State <= Idle_s;
        end case;
      end if;
    end if;
  end process;

  -------------------------------------------------------
  -- Error Detection
  -------------------------------------------------------

  -- CRC Error
  CrcError_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      SetPktErrCrc <= '0';
      if Rst_i = '1' or Rst_Errors_i = '1' then
        CRC_Err      <= '0';
        CRC_ErrCount <= (others=>'0');
        CRC_ErrLock  <= '0';
      else
        if r.CrcChk = '1' and CRC_Ok_i = '0' then
          CRC_Err    <= '1';
          if CRC_ErrLock = '0' and CRC_ErrCount < MaxErrors_cpk then
            CRC_ErrCount <= CRC_ErrCount + 1;
            SetPktErrCrc <= '1';
            CRC_ErrLock  <= '1';
          end if;
        else
          CRC_ErrLock  <= '0';
        end if;
      end if;
    end if;
  end process;
  Error_Flags_o.CRC_ErrCount <= std_logic_vector(CRC_ErrCount);
  Error_Flags_o.CRC_Err      <= CRC_Err;

  -- Frame Error (Buffer overflow)
  FrameError_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if Rst_i = '1' or Rst_Errors_i = '1' then
        Frame_Err         <= '0';
        Frame_ErrCount    <= (others=>'0');
        Frame_ErrLock_B2W <= '0';
      else
        -- Case: data arrives while byte-to-word unit is not ready
        --       or there is a buffer overflow
        if (r.B2W_Din_Vld = '1' and B2W_Din_Rdy_i = '0') or BufferOverflow_i = '1' then
          Frame_Err <= '1';
          if Frame_ErrLock_B2W = '0' and Frame_ErrCount < MaxErrors_cpk then
            Frame_ErrCount <= Frame_ErrCount + 1;
            Frame_ErrLock_B2W  <= '1';
          end if;
        else
        Frame_ErrLock_B2W  <= '0';
        end if;
      end if;
      -- Case: new data arrives while buffer is not empty (do not check during checksum reception!)
      DesDataS <= DesData_i;
      if r.State /= RcvFCS_s and BufferFull_i = '1' and DesDataS = SofByte0_cpk and DesData_i = SofByte1_cpk then
        Frame_Err <= '1';
        if Frame_ErrCount < MaxErrors_cpk then
          Frame_ErrCount <= Frame_ErrCount + 1;
        end if;
      end if;
      -- Case: missing idle word or invalid SOF
      if r.SofFailed = '1' then
        Frame_Err <= '1';
        if Frame_ErrCount < MaxErrors_cpk then
          Frame_ErrCount <= Frame_ErrCount + 1;
        end if;
      end if;
    end if;
  end process;
  Error_Flags_o.Frame_ErrCount <= std_logic_vector(Frame_ErrCount);
  Error_Flags_o.Frame_Err      <= Frame_Err;

  -- Datagram Error (packet sequence problem)
  DatagramError_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      SetPktErrDatagram <= '0';
      if Rst_i = '1' or Rst_Errors_i = '1' then
        Datagram_Err      <= '0';
        Datagram_ErrCount <= (others=>'0');
        Datagram_ErrLock  <= '0';
      else
        if (BufferFull_i = '1' ) and (r.FirstPkt = '0') and
          ( (r.SoeFlag /= r.EventNrChange) or (r.SoeFlag = '0' and r.ExpectSoe = '1') ) then
          Datagram_Err <= '1';
          if Datagram_ErrLock = '0' and Datagram_ErrCount < MaxErrors_cpk then
            Datagram_ErrCount <= Datagram_ErrCount + 1;
            SetPktErrDatagram <= '1';
            Datagram_ErrLock  <= '1';
          end if;
        else
          Datagram_ErrLock  <= '0';
        end if;
      end if;
    end if;
  end process;
  Error_Flags_o.Datagram_ErrCount <= std_logic_vector(Datagram_ErrCount);
  Error_Flags_o.Datagram_Err      <= Datagram_Err;

  -- Sync Error (loss of sync)
  SyncError_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if Rst_i = '1' or Rst_Errors_i = '1' then
        Sync_Err      <= '0';
        Sync_ErrCount <= (others=>'0');
      else
        if r.SyncErr = '1' then
          Sync_Err <= '1';
          if Sync_ErrCount < MaxErrors_cpk then
            Sync_ErrCount <= Sync_ErrCount + 1;
          end if;
        end if;
      end if;
    end if;
  end process;
  Error_Flags_o.Sync_ErrCount <= std_logic_vector(Sync_ErrCount);
  Error_Flags_o.Sync_Err      <= Sync_Err;

  -------------------------------------------------------
  -- Packet Error Flags
  -------------------------------------------------------
  PktCrcErrFlag_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if r.ClrPktFlags = '1' then
        PktErrorFlags_o.CRC_Err <= '0';
      elsif SetPktErrCrc = '1' then
        PktErrorFlags_o.CRC_Err <= '1';
      end if;
    end if;
  end process;

  PktDatagramErrFlag_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if r.ClrPktFlags = '1' then
        PktErrorFlags_o.Datagram_Err <= '0';
      elsif SetPktErrDatagram = '1' then
        PktErrorFlags_o.Datagram_Err <= '1';
      end if;
    end if;
  end process;

  ErrFlagHandshake_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if Rst_i = '1' then
        FlagsValid_o <= '0';
      else
        if r.SetFlagsVld = '1' then
          FlagsValid_o <= '1';
        elsif FlagsReady_i = '1' then
          FlagsValid_o <= '0';
        end if;
      end if;
    end if;
  end process;

  -------------------------------------------------------
  -- Packet Count
  -------------------------------------------------------
  PacketCounter_i : process(Clk_i)
  begin
    if rising_edge(Clk_i) then
      if Rst_i = '1' or Rst_RcvPktCount_i = '1' then
        PktCount <= (others=>'0');
      elsif r.State /= RcvFrameLen_s and
            r.State /= RcvData_s     and
            r.State /= RcvFCS_s      and
            DesDataS = SofByte0_cpk  and
            DesData_i = SofByte1_cpk then
        PktCount <= PktCount + 1;
      end if;
    end if;
  end process;
  RcvPktCount_o <= std_logic_vector(PktCount);

end;
