##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME serdes_pkt_rcvr
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY PSI
set IP_DESCIRPTION "WaveDAQ slot packet deserializer/buffer"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/serdes_pkt_rcvr_pkg.vhd \
  ../hdl/tdm_par_fill.vhd \
  ../hdl/serdes_pkt_buffer.vhd \
  ../hdl/serdes_pkt_rcvr_ctrl.vhd \
  ../hdl/deserializer.vhd \
  ../hdl/serdes_pkt_rcvr.vhd \
  ../hdl/serdes_pkt_rcvr_vivado.vhd \
}

set_top_entity serdes_pkt_rcvr_vivado

#Relative Library Files
add_lib_relative \
  "../../../../library/" \
{ \
  VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_math_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_logic_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_sdp_ram.vhd \
  VHDL/psi_common/hdl/psi_common_bit_cc.vhd \
  VHDL/psi_3205_v1_0/crc32_d8.vhd \
}
#  VHDL/psi_common/hdl/psi_common_sync_fifo.vhd \
#  VHDL/psi_common/hdl/psi_common_tdm_par.vhd \
#  VHDL/psi_common/hdl/psi_common_tdp_ram.vhd \

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "General Configuration"

gui_create_parameter "Streams_cgn" "Number of Streams"
gui_parameter_set_range 1 17
gui_add_parameter

gui_create_parameter "IdelayCtrls_cgn" "Number of IDelay Controller"
gui_parameter_set_range 1 8
gui_add_parameter

gui_create_parameter "BitOrder_cgn" "Bit Order"
gui_parameter_set_widget_dropdown {"MSB first" "LSB first"}
gui_add_parameter

gui_create_parameter "IdlePattern_cgn" "Expected Idle Byte"
gui_parameter_set_range 0 255
gui_add_parameter

gui_create_parameter "SDataRate_cgn" "Data Rate \[MBit/s\] of Serial Stream"
gui_parameter_set_range 0 1000
gui_add_parameter

gui_create_parameter "DlyRefClk_cgn" "Reference Clock Frequency \[MHz\] of IDelay Refclk"
gui_parameter_set_widget_dropdown {200 300 400}
gui_add_parameter

gui_create_parameter "FifoSize_cgn" "FIFO Size"
gui_parameter_set_widget_dropdown {4096 8192 16384}
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

for {set i 0} {$i < 17} {incr i} {
  set i02 [format "%02d" $i]
  add_port_enablement_condition "Slot$i02\_BitClk_Serdes_i" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_DivClk_Serdes_i" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_TData" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_TValid" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_TReady" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_TLast" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_Clk" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_Bytes\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_PktValid\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_EOE\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_PktBytes\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_CrcErrors\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_FrameErrors\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_DatagramErrors\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_SyncErrors\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_CrcError\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_FrameError\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_DatagramError\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_SyncError\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_SyncDone\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_DelayOk\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_BitslipOk\_o" "\$Streams_cgn > $i"
  add_port_enablement_condition "Slot$i02\_LinkIdle\_o" "\$Streams_cgn > $i"
  add_interface_enablement_condition "Slot$i02*" "\$Streams_cgn > $i"
}

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
