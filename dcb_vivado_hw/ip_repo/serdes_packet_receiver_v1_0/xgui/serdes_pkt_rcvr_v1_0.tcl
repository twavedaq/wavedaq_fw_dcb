# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set General_Configuration [ipgui::add_page $IPINST -name "General Configuration"]
  ipgui::add_param $IPINST -name "Streams_cgn" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "IdelayCtrls_cgn" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "BitOrder_cgn" -parent ${General_Configuration} -widget comboBox
  ipgui::add_param $IPINST -name "IdlePattern_cgn" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "SDataRate_cgn" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "DlyRefClk_cgn" -parent ${General_Configuration} -widget comboBox
  ipgui::add_param $IPINST -name "FifoSize_cgn" -parent ${General_Configuration} -widget comboBox


}

proc update_PARAM_VALUE.BitOrder_cgn { PARAM_VALUE.BitOrder_cgn } {
	# Procedure called to update BitOrder_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BitOrder_cgn { PARAM_VALUE.BitOrder_cgn } {
	# Procedure called to validate BitOrder_cgn
	return true
}

proc update_PARAM_VALUE.DlyRefClk_cgn { PARAM_VALUE.DlyRefClk_cgn } {
	# Procedure called to update DlyRefClk_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DlyRefClk_cgn { PARAM_VALUE.DlyRefClk_cgn } {
	# Procedure called to validate DlyRefClk_cgn
	return true
}

proc update_PARAM_VALUE.FifoSize_cgn { PARAM_VALUE.FifoSize_cgn } {
	# Procedure called to update FifoSize_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.FifoSize_cgn { PARAM_VALUE.FifoSize_cgn } {
	# Procedure called to validate FifoSize_cgn
	return true
}

proc update_PARAM_VALUE.IdelayCtrls_cgn { PARAM_VALUE.IdelayCtrls_cgn } {
	# Procedure called to update IdelayCtrls_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IdelayCtrls_cgn { PARAM_VALUE.IdelayCtrls_cgn } {
	# Procedure called to validate IdelayCtrls_cgn
	return true
}

proc update_PARAM_VALUE.IdlePattern_cgn { PARAM_VALUE.IdlePattern_cgn } {
	# Procedure called to update IdlePattern_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IdlePattern_cgn { PARAM_VALUE.IdlePattern_cgn } {
	# Procedure called to validate IdlePattern_cgn
	return true
}

proc update_PARAM_VALUE.SDataRate_cgn { PARAM_VALUE.SDataRate_cgn } {
	# Procedure called to update SDataRate_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SDataRate_cgn { PARAM_VALUE.SDataRate_cgn } {
	# Procedure called to validate SDataRate_cgn
	return true
}

proc update_PARAM_VALUE.Streams_cgn { PARAM_VALUE.Streams_cgn } {
	# Procedure called to update Streams_cgn when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.Streams_cgn { PARAM_VALUE.Streams_cgn } {
	# Procedure called to validate Streams_cgn
	return true
}


proc update_MODELPARAM_VALUE.Streams_cgn { MODELPARAM_VALUE.Streams_cgn PARAM_VALUE.Streams_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.Streams_cgn}] ${MODELPARAM_VALUE.Streams_cgn}
}

proc update_MODELPARAM_VALUE.IdelayCtrls_cgn { MODELPARAM_VALUE.IdelayCtrls_cgn PARAM_VALUE.IdelayCtrls_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IdelayCtrls_cgn}] ${MODELPARAM_VALUE.IdelayCtrls_cgn}
}

proc update_MODELPARAM_VALUE.BitOrder_cgn { MODELPARAM_VALUE.BitOrder_cgn PARAM_VALUE.BitOrder_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BitOrder_cgn}] ${MODELPARAM_VALUE.BitOrder_cgn}
}

proc update_MODELPARAM_VALUE.IdlePattern_cgn { MODELPARAM_VALUE.IdlePattern_cgn PARAM_VALUE.IdlePattern_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IdlePattern_cgn}] ${MODELPARAM_VALUE.IdlePattern_cgn}
}

proc update_MODELPARAM_VALUE.SDataRate_cgn { MODELPARAM_VALUE.SDataRate_cgn PARAM_VALUE.SDataRate_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SDataRate_cgn}] ${MODELPARAM_VALUE.SDataRate_cgn}
}

proc update_MODELPARAM_VALUE.DlyRefClk_cgn { MODELPARAM_VALUE.DlyRefClk_cgn PARAM_VALUE.DlyRefClk_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DlyRefClk_cgn}] ${MODELPARAM_VALUE.DlyRefClk_cgn}
}

proc update_MODELPARAM_VALUE.FifoSize_cgn { MODELPARAM_VALUE.FifoSize_cgn PARAM_VALUE.FifoSize_cgn } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.FifoSize_cgn}] ${MODELPARAM_VALUE.FifoSize_cgn}
}

