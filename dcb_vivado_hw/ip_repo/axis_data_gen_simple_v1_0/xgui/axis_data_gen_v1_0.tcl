# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set General_Configuration [ipgui::add_page $IPINST -name "General Configuration"]
  ipgui::add_param $IPINST -name "CGN_STREAMS" -parent ${General_Configuration}
  ipgui::add_param $IPINST -name "CGN_BYTE_ORDER" -parent ${General_Configuration} -widget comboBox


}

proc update_PARAM_VALUE.CGN_BYTE_ORDER { PARAM_VALUE.CGN_BYTE_ORDER } {
	# Procedure called to update CGN_BYTE_ORDER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_BYTE_ORDER { PARAM_VALUE.CGN_BYTE_ORDER } {
	# Procedure called to validate CGN_BYTE_ORDER
	return true
}

proc update_PARAM_VALUE.CGN_STREAMS { PARAM_VALUE.CGN_STREAMS } {
	# Procedure called to update CGN_STREAMS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_STREAMS { PARAM_VALUE.CGN_STREAMS } {
	# Procedure called to validate CGN_STREAMS
	return true
}


proc update_MODELPARAM_VALUE.CGN_STREAMS { MODELPARAM_VALUE.CGN_STREAMS PARAM_VALUE.CGN_STREAMS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_STREAMS}] ${MODELPARAM_VALUE.CGN_STREAMS}
}

proc update_MODELPARAM_VALUE.CGN_BYTE_ORDER { MODELPARAM_VALUE.CGN_BYTE_ORDER PARAM_VALUE.CGN_BYTE_ORDER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_BYTE_ORDER}] ${MODELPARAM_VALUE.CGN_BYTE_ORDER}
}

