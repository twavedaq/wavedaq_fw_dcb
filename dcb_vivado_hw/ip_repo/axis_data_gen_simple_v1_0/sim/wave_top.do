onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Testbench
add wave -noupdate /axis_data_gen_simple_tb/TbRunning
add wave -noupdate /axis_data_gen_simple_tb/ProcessDone
add wave -noupdate /axis_data_gen_simple_tb/CLK_I
add wave -noupdate /axis_data_gen_simple_tb/RST_N_I
add wave -noupdate /axis_data_gen_simple_tb/TRIGGER_PACKET_I
add wave -noupdate /axis_data_gen_simple_tb/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/PKTS_PER_EVENT_I
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_TData
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_TValid
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_TReady
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_TLast
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_Bytes
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_PktValid
add wave -noupdate -group Slot00 /axis_data_gen_simple_tb/Slot00_EOE
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_TData
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_TValid
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_TReady
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_TLast
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_Bytes
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_PktValid
add wave -noupdate -group Slot01 /axis_data_gen_simple_tb/Slot01_EOE
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_TData
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_TValid
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_TReady
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_TLast
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_Bytes
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_PktValid
add wave -noupdate -group Slot02 /axis_data_gen_simple_tb/Slot02_EOE
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_TData
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_TValid
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_TReady
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_TLast
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_Bytes
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_PktValid
add wave -noupdate -group Slot03 /axis_data_gen_simple_tb/Slot03_EOE
add wave -noupdate -divider DUT
add wave -noupdate /axis_data_gen_simple_tb/i_dut/CLK_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/RST_N_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/TRIGGER_PACKET_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/PKTS_PER_EVENT_I
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_TData
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_TValid
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_TReady
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_TLast
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_Bytes
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_PktValid
add wave -noupdate -expand -group {DUT Slot00} /axis_data_gen_simple_tb/i_dut/Slot00_EOE
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_TData
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_TValid
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_TReady
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_TLast
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_Bytes
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_PktValid
add wave -noupdate -expand -group {DUT Slot01} /axis_data_gen_simple_tb/i_dut/Slot01_EOE
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_TData
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_TValid
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_TReady
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_TLast
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_Bytes
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_PktValid
add wave -noupdate -expand -group {DUT Slot02} /axis_data_gen_simple_tb/i_dut/Slot02_EOE
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_TData
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_TValid
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_TReady
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_TLast
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_Bytes
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_PktValid
add wave -noupdate -expand -group {DUT Slot03} /axis_data_gen_simple_tb/i_dut/Slot03_EOE
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_TData
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_TValid
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_TReady
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_TLast
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_Bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_PktValid
add wave -noupdate /axis_data_gen_simple_tb/i_dut/All_EOE
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_TData
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_TValid
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_TLast
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_Bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_PktValid
add wave -noupdate /axis_data_gen_simple_tb/i_dut/Str_EOE
add wave -noupdate -divider {Generator 0}
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/PKTS_PER_EVENT_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/last
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/Bytes_O
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/count
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/data_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/rem_no_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/event_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/total_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(0)/i_data_gen_single/first_word
add wave -noupdate -divider {Generator 1}
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/PKTS_PER_EVENT_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/last
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/Bytes_O
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/count
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/data_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/rem_no_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/event_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/total_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(1)/i_data_gen_single/first_word
add wave -noupdate -divider {Generator 2}
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/PKTS_PER_EVENT_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/last
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/Bytes_O
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/count
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/data_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/rem_no_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/event_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/total_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(2)/i_data_gen_single/first_word
add wave -noupdate -divider {Generator 3}
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/NR_OF_BYTES_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/PKTS_PER_EVENT_I
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/last
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/Bytes_O
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/count
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/data_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/rem_no_bytes
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/event_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/total_pkt_cnt
add wave -noupdate /axis_data_gen_simple_tb/i_dut/g_generators(3)/i_data_gen_single/first_word
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1423 ps} 0} {{Cursor 2} {8012500 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 227
configure wave -valuecolwidth 147
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {31500 ns}
