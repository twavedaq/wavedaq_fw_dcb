##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

#Constants
set LibPath "../../../../library/VHDL"

#Import psi::sim
namespace import psi::sim::*

#Set library
add_library axis_data_gen_simple

#suppress messages
compile_suppress 135,1236,1073,1246
run_suppress 8684,3479,3813,8009,3812

# Library
add_sources $LibPath {
  psi_common/hdl/psi_common_array_pkg.vhd \
  psi_common/hdl/psi_common_math_pkg.vhd \
  psi_tb/hdl/psi_tb_txt_util.vhd \
  psi_tb/hdl/psi_tb_compare_pkg.vhd \
} -tag lib

# project sources
add_sources "../hdl" {
  axis_data_gen_simple_single.vhd \
  axis_data_gen_simple.vhd \
} -tag src

# testbenches
add_sources "../tb" {
  axis_data_gen_simple_tb/axis_data_gen_simple_tb_pkg.vhd \
  axis_data_gen_simple_tb/axis_data_gen_simple_tb.vhd \
} -tag tb

#TB Runs
create_tb_run "axis_data_gen_simple_tb"
add_tb_run
