------------------------------------------------------------------------------
--  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
--  All rights reserved.
--  Authors: Oliver Bruendler
------------------------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_math_pkg.all;
  use work.psi_common_array_pkg.all;
  use work.psi_tb_txt_util.all;
  use work.psi_tb_compare_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package axis_data_gen_simple_tb_pkg is

  --------------------------------------------------------
  -- Global Stuff
  --------------------------------------------------------
  shared variable PacketCount_v   : t_ainteger(0 to 3) := (0, 0,  0, 0);
  shared variable EventPktCount_v : t_ainteger(0 to 3) := (0, 0, 0, 0);

  procedure Init;

  --------------------------------------------------------
  -- Data Checking
  --------------------------------------------------------
  procedure CheckFrame( Stream                : in  integer;
                        RcvBurstSize          : in  integer;
                        ReadyPreDelay         : in  integer;
                        ReadyPostDelay        : in  integer;
                        ByteOrder             : in  string;
                        EndOfEvent            : out boolean;
                        signal BytesPerPacket : in  integer;
                        signal PktsPerEvent   : in  integer;
                        signal Clk            : in  std_logic;
                        signal TData          : in  std_logic_vector(63 downto 0);
                        signal TValid         : in  std_logic;
                        signal TReady         : out std_logic;
                        signal TLast          : in  std_logic;
                        signal Bytes          : in  std_logic_vector(3 downto 0);
                        signal PktValid       : in  std_logic;
                        signal EOE            : in  std_logic);

  procedure CheckData( Stream       : in  integer;
                       ByteNumber   : in  integer;
                       PacketNr     : in  integer;
                       FirstWord    : in  boolean;
                       ByteOrder    : in  string;
                       signal TData : in  std_logic_vector(63 downto 0));

  procedure UpdateEventPacketCount( Stream       : in integer;
                                    PktsPerEvent : in integer;
                                    DoCount      : in boolean;
                                    EndOfEvent   : in boolean);

  --------------------------------------------------------
  -- Helper Procedures
  --------------------------------------------------------
  procedure print( str : in string;
                   ena : in boolean);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body axis_data_gen_simple_tb_pkg is

  procedure Init is
  begin
    PacketCount_v   := (others=>0);
    EventPktCount_v := (others=>0);
  end procedure;

  --------------------------------------------------------
  -- Data Checking
  --------------------------------------------------------
  procedure CheckFrame( Stream                : in  integer;
                        RcvBurstSize          : in  integer;
                        ReadyPreDelay         : in  integer;
                        ReadyPostDelay        : in  integer;
                        ByteOrder             : in  string;
                        EndOfEvent            : out boolean;
                        signal BytesPerPacket : in  integer;
                        signal PktsPerEvent   : in  integer;
                        signal Clk            : in  std_logic;
                        signal TData          : in  std_logic_vector(63 downto 0);
                        signal TValid         : in  std_logic;
                        signal TReady         : out std_logic;
                        signal TLast          : in  std_logic;
                        signal Bytes          : in  std_logic_vector(3 downto 0);
                        signal PktValid       : in  std_logic;
                        signal EOE            : in  std_logic) is
    variable ByteCount_v  : integer := 0;
    variable FrameDone_v  : boolean := false;
    variable FirstWord_v  : boolean := true;
    variable ValidCount_v : integer := 0;
    variable BurstCount_v : integer := RcvBurstSize-1;
  begin
    -- Wait ReadyPreDelay clock cycles with TValid active before setting TReady
    while ValidCount_v < ReadyPreDelay loop
      wait until rising_edge(Clk);
      if TValid = '1' then
        ValidCount_v := ValidCount_v + 1;
      end if;
    end loop;
    wait until rising_edge(Clk);
    TReady <= '1';
    -- Receive and check frame
    while not(FrameDone_v) loop
      wait until rising_edge(Clk);
      if TValid = '1' and PktValid = '1' then
        if TReady = '1' then
          if FirstWord_v then
            ByteCount_v := BytesPerPacket - 1;
          end if;
          CheckData(Stream      => Stream,
                    ByteNumber  => ByteCount_v,
                    PacketNr    => PacketCount_v(Stream),
                    FirstWord   => FirstWord_v,
                    ByteOrder   => ByteOrder,
                    TData       => TData);
          FirstWord_v := false;
          ByteCount_v := ByteCount_v - to_integer(unsigned(Bytes));
          -- Validate byte count
          if ByteCount_v < -1 then
            print("###ERROR### Stream " & to_string(Stream) & ": Invalid Byte Count in Frame " & to_string(ByteCount_v) & " (Expected > 0)", true);
          end if;
          -- Handle last byte
          if TLast = '1' then
            FrameDone_v := true;
            if ByteCount_v /= -1 then
              print("###ERROR### Stream  " & to_string(Stream) & ": Invalid Byte Count at End of Frame " & to_string(ByteCount_v) & " (Expected 0)", true);
            end if;
            -- Signal End of Event reception for higher level checks
            if EOE = '1' then
              EndOfEvent := true;
            else
              EndOfEvent := false;
            end if;
            FirstWord_v := true;
          end if;
        end if;
        -- Generate backpressure by toggling TReady
        if RcvBurstSize /= 0 then
          BurstCount_v := BurstCount_v-1;
          if BurstCount_v = 0 then
            BurstCount_v := RcvBurstSize-1;
            TReady <= not TReady;
          end if;
        end if;
      end if;
    end loop;
    -- Count Packet
    PacketCount_v(Stream) := PacketCount_v(Stream) + 1;
    UpdateEventPacketCount(Stream=>Stream, PktsPerEvent=>PktsPerEvent, DoCount=>true, EndOfEvent=>EndOfEvent);
    -- Ready Post Delay
    for i in 1 to ReadyPostDelay loop
      wait until rising_edge(Clk);
    end loop;
    TReady <= '0';
    wait until rising_edge(Clk);
  end procedure;

  procedure CheckData( Stream       : in  integer;
                       ByteNumber   : in  integer;
                       PacketNr     : in  integer;
                       FirstWord    : in  boolean;
                       ByteOrder    : in  string;
                       signal TData : in  std_logic_vector(63 downto 0)) is
    variable TargetData : std_logic_vector(63 downto 0);
  begin
    for i in 7 downto 0 loop
      if ByteOrder = "up" then
        TargetData((i+1)*8-1 downto i*8) := std_logic_vector(to_signed(ByteNumber-i, 8));
      else
        TargetData((i+1)*8-1 downto i*8) := std_logic_vector(to_signed(ByteNumber-(7-i), 8));
      end if;
    end loop;
    -- Handle special info bytes (stream and packet number) in first word
    if FirstWord then
      if ByteOrder = "up" then
        TargetData( 7 downto 0) := std_logic_vector(to_unsigned(Stream, 8));
        TargetData(15 downto 8) := std_logic_vector(to_unsigned(PacketNr, 8));
      else
        TargetData(63 downto 56) := std_logic_vector(to_unsigned(Stream, 8));
        TargetData(55 downto 48) := std_logic_vector(to_unsigned(PacketNr, 8));
      end if;
    end if;
    StdlvCompareStdlv(Expected=>TargetData, Actual=>TData, Msg=>" Stream " & to_string(Stream) & " Packet " & to_string(PacketNr) & " Byte Nr " & to_string(ByteNumber));
  end procedure;

  procedure UpdateEventPacketCount( Stream       : in integer;
                                    PktsPerEvent : in integer;
                                    DoCount      : in boolean;
                                    EndOfEvent   : in boolean) is
  begin
    if DoCount then
      EventPktCount_v(Stream) := EventPktCount_v(Stream) + 1;
    end if;
    if EndOfEvent then
      IntCompare(Expected=>PktsPerEvent, Actual=>EventPktCount_v(Stream), Msg=>"Wrong End of Event: Stream " & to_string(Stream));
      EventPktCount_v(Stream) := 0;
    elsif EventPktCount_v(Stream) = PktsPerEvent then
      print("###ERROR###: Missing End of Event on Stream " & to_string(Stream) & " (Packets per Event " & to_string(PktsPerEvent) & ")" , true);
    end if;
  end procedure;

  --------------------------------------------------------
  -- Helper Procedures
  --------------------------------------------------------
  procedure print( str : in string;
                   ena : in boolean) is
  begin
    if ena then
      print(str);
    end if;
  end procedure;

end;
