---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  AXIS data generator simple
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  19.05.2020 15:09:38
--
--  Description :  Generates data packets with counter values to feed the DCB
--                 DMA packet scheduler for testing.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_array_pkg.all;

-- $$ processes=control $$
-- $$ tbpkg=work.axis_data_gen_simple_tb_pkg $$

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
entity axis_data_gen_simple is
  generic (
    CGN_STREAMS      : positive range 1 to 32 := 2;     -- $$ constant=4 $$
    CGN_BYTE_ORDER   : string                 := "down" -- $$ constant="up" $$ "up", "down"
  );
  port (
    -- Clock and Reset
    CLK_I            : in  std_logic;                     -- $$ type=clk; freq=125e6; proc=control,trigger,receive $$
    RST_N_I          : in  std_logic;                     -- $$ type=rst; clk=CLK_I; proc=control,trigger,receive $$
    -- Configuration
    TRIGGER_PACKET_I : in  std_logic;                     -- $$ proc=trigger $$
    NR_OF_BYTES_I    : in  std_logic_vector(15 downto 0); -- $$ proc=trigger $$
    PKTS_PER_EVENT_I : in  std_logic_vector( 7 downto 0); -- $$ proc=trigger $$
    -- Data Streams Input
    Slot00_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot00_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot00_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot00_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot00_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot00_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot00_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot01_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot01_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot01_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot01_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot01_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot01_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot01_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot02_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot02_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot02_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot02_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot02_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot02_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot02_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot03_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot03_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot03_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot03_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot03_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot03_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot03_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot04_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot04_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot04_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot04_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot04_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot04_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot04_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot05_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot05_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot05_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot05_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot05_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot05_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot05_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot06_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot06_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot06_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot06_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot06_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot06_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot06_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot07_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot07_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot07_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot07_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot07_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot07_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot07_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot08_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot08_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot08_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot08_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot08_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot08_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot08_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot09_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot09_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot09_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot09_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot09_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot09_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot09_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot10_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot10_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot10_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot10_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot10_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot10_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot10_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot11_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot11_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot11_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot11_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot11_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot11_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot11_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot12_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot12_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot12_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot12_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot12_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot12_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot12_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot13_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot13_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot13_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot13_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot13_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot13_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot13_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot14_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot14_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot14_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot14_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot14_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot14_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot14_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot15_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot15_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot15_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot15_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot15_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot15_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot15_EOE       : out std_logic;                     -- $$ proc=receive $$

    Slot16_TData     : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    Slot16_TValid    : out std_logic;                     -- $$ proc=receive $$
    Slot16_TReady    : in  std_logic;                     -- $$ proc=receive $$
    Slot16_TLast     : out std_logic;                     -- $$ proc=receive $$
    Slot16_Bytes     : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    Slot16_PktValid  : out std_logic;                     -- $$ proc=receive $$
    Slot16_EOE       : out std_logic                      -- $$ proc=receive $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of axis_data_gen_simple is

  signal All_TData    : t_aslv64(0 to 16)          := (others => (others => '0'));
  signal All_TValid   : std_logic_vector(0 to 16)  := (others => '0');
  signal All_TReady   : std_logic_vector(0 to 16)  := (others => '0');
  signal All_TLast    : std_logic_vector(0 to 16)  := (others => '0');
  signal All_Bytes    : t_aslv4(0 to 16)           := (others => (others => '0'));
  signal All_PktValid : std_logic_vector(0 to 16)  := (others => '0');
  signal All_EOE      : std_logic_vector(0 to 16)  := (others => '0');
  signal Str_TData    : t_aslv64(CGN_STREAMS-1 downto 0);
  signal Str_TValid   : std_logic_vector(CGN_STREAMS-1 downto 0);
  signal Str_TReady   : std_logic_vector(CGN_STREAMS-1 downto 0);
  signal Str_TLast    : std_logic_vector(CGN_STREAMS-1 downto 0);
  signal Str_Bytes    : t_aslv4(CGN_STREAMS-1 downto 0);
  signal Str_PktValid : std_logic_vector(CGN_STREAMS-1 downto 0);
  signal Str_EOE      : std_logic_vector(CGN_STREAMS-1 downto 0);

begin

  Slot00_TData    <= All_TData(0);
  Slot00_TValid   <= All_TValid(0);
  Slot00_TLast    <= All_TLast(0);
  Slot00_Bytes    <= All_Bytes(0);
  Slot00_PktValid <= All_PktValid(0);
  Slot00_EOE      <= All_EOE(0);
  All_TReady(0)   <= Slot00_TReady;

  Slot01_TData    <= All_TData(1);
  Slot01_TValid   <= All_TValid(1);
  Slot01_TLast    <= All_TLast(1);
  Slot01_Bytes    <= All_Bytes(1);
  Slot01_PktValid <= All_PktValid(1);
  Slot01_EOE      <= All_EOE(1);
  All_TReady(1)   <= Slot01_TReady;

  Slot02_TData    <= All_TData(2);
  Slot02_TValid   <= All_TValid(2);
  Slot02_TLast    <= All_TLast(2);
  Slot02_Bytes    <= All_Bytes(2);
  Slot02_PktValid <= All_PktValid(2);
  Slot02_EOE      <= All_EOE(2);
  All_TReady(2)   <= Slot02_TReady;

  Slot03_TData    <= All_TData(3);
  Slot03_TValid   <= All_TValid(3);
  Slot03_TLast    <= All_TLast(3);
  Slot03_Bytes    <= All_Bytes(3);
  Slot03_PktValid <= All_PktValid(3);
  Slot03_EOE      <= All_EOE(3);
  All_TReady(3)   <= Slot03_TReady;

  Slot04_TData    <= All_TData(4);
  Slot04_TValid   <= All_TValid(4);
  Slot04_TLast    <= All_TLast(4);
  Slot04_Bytes    <= All_Bytes(4);
  Slot04_PktValid <= All_PktValid(4);
  Slot04_EOE      <= All_EOE(4);
  All_TReady(4)   <= Slot04_TReady;

  Slot05_TData    <= All_TData(5);
  Slot05_TValid   <= All_TValid(5);
  Slot05_TLast    <= All_TLast(5);
  Slot05_Bytes    <= All_Bytes(5);
  Slot05_PktValid <= All_PktValid(5);
  Slot05_EOE      <= All_EOE(5);
  All_TReady(5)   <= Slot05_TReady;

  Slot06_TData    <= All_TData(6);
  Slot06_TValid   <= All_TValid(6);
  Slot06_TLast    <= All_TLast(6);
  Slot06_Bytes    <= All_Bytes(6);
  Slot06_PktValid <= All_PktValid(6);
  Slot06_EOE      <= All_EOE(6);
  All_TReady(6)   <= Slot06_TReady;

  Slot07_TData    <= All_TData(7);
  Slot07_TValid   <= All_TValid(7);
  Slot07_TLast    <= All_TLast(7);
  Slot07_Bytes    <= All_Bytes(7);
  Slot07_PktValid <= All_PktValid(7);
  Slot07_EOE      <= All_EOE(7);
  All_TReady(7)   <= Slot07_TReady;

  Slot08_TData    <= All_TData(8);
  Slot08_TValid   <= All_TValid(8);
  Slot08_TLast    <= All_TLast(8);
  Slot08_Bytes    <= All_Bytes(8);
  Slot08_PktValid <= All_PktValid(8);
  Slot08_EOE      <= All_EOE(8);
  All_TReady(8)   <= Slot08_TReady;

  Slot09_TData    <= All_TData(9);
  Slot09_TValid   <= All_TValid(9);
  Slot09_TLast    <= All_TLast(9);
  Slot09_Bytes    <= All_Bytes(9);
  Slot09_PktValid <= All_PktValid(9);
  Slot09_EOE      <= All_EOE(9);
  All_TReady(9)   <= Slot09_TReady;

  Slot10_TData    <= All_TData(10);
  Slot10_TValid   <= All_TValid(10);
  Slot10_TLast    <= All_TLast(10);
  Slot10_Bytes    <= All_Bytes(10);
  Slot10_PktValid <= All_PktValid(10);
  Slot10_EOE      <= All_EOE(10);
  All_TReady(10)  <= Slot10_TReady;

  Slot11_TData    <= All_TData(11);
  Slot11_TValid   <= All_TValid(11);
  Slot11_TLast    <= All_TLast(11);
  Slot11_Bytes    <= All_Bytes(11);
  Slot11_PktValid <= All_PktValid(11);
  Slot11_EOE      <= All_EOE(11);
  All_TReady(11)  <= Slot11_TReady;

  Slot12_TData    <= All_TData(12);
  Slot12_TValid   <= All_TValid(12);
  Slot12_TLast    <= All_TLast(12);
  Slot12_Bytes    <= All_Bytes(12);
  Slot12_PktValid <= All_PktValid(12);
  Slot12_EOE      <= All_EOE(12);
  All_TReady(12)  <= Slot12_TReady;

  Slot13_TData    <= All_TData(13);
  Slot13_TValid   <= All_TValid(13);
  Slot13_TLast    <= All_TLast(13);
  Slot13_Bytes    <= All_Bytes(13);
  Slot13_PktValid <= All_PktValid(13);
  Slot13_EOE      <= All_EOE(13);
  All_TReady(13)  <= Slot13_TReady;

  Slot14_TData    <= All_TData(14);
  Slot14_TValid   <= All_TValid(14);
  Slot14_TLast    <= All_TLast(14);
  Slot14_Bytes    <= All_Bytes(14);
  Slot14_PktValid <= All_PktValid(14);
  Slot14_EOE      <= All_EOE(14);
  All_TReady(14)  <= Slot14_TReady;

  Slot15_TData    <= All_TData(15);
  Slot15_TValid   <= All_TValid(15);
  Slot15_TLast    <= All_TLast(15);
  Slot15_Bytes    <= All_Bytes(15);
  Slot15_PktValid <= All_PktValid(15);
  Slot15_EOE      <= All_EOE(15);
  All_TReady(15)  <= Slot15_TReady;

  Slot16_TData    <= All_TData(16);
  Slot16_TValid   <= All_TValid(16);
  Slot16_TLast    <= All_TLast(16);
  Slot16_Bytes    <= All_Bytes(16);
  Slot16_PktValid <= All_PktValid(16);
  Slot16_EOE      <= All_EOE(16);
  All_TReady(16)  <= Slot16_TReady;

  g_stream : for s in 0 to CGN_STREAMS-1 generate
    All_TData(s)    <= Str_TData(s);
    All_TValid(s)   <= Str_TValid(s);
    All_TLast(s)    <= Str_TLast(s);
    All_Bytes(s)    <= Str_Bytes(s);
    All_PktValid(s) <= Str_PktValid(s);
    All_EOE(s)      <= Str_EOE(s);
    Str_TReady(s)   <= All_TReady(s);
  end generate;

  g_generators : for i in 0 to CGN_STREAMS-1 generate
    i_data_gen_single : entity work.axis_data_gen_simple_single
      generic map(
        CGN_BYTE_ORDER   => CGN_BYTE_ORDER,
        CGN_STREAM_NR    => i
      )
      port map(
        CLK_I            => CLK_I,
        RST_N_I          => RST_N_I,
        TRIGGER_PACKET_I => TRIGGER_PACKET_I,
        NR_OF_BYTES_I    => NR_OF_BYTES_I,
        PKTS_PER_EVENT_I => PKTS_PER_EVENT_I,
        TData_O          => Str_TData(i),
        TValid_O         => Str_TValid(i),
        TReady_I         => Str_TReady(i),
        TLast_O          => Str_TLast(i),
        Bytes_O          => Str_Bytes(i),
        PktValid_O       => Str_PktValid(i),
        EOE_O            => Str_EOE(i)
      );
  end generate;
end;
