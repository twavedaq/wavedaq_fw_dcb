---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  AXIS data generator simple
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  19.05.2020 15:42:43
--
--  Description :  Generates data packets with counter values to feed the DCB
--                 DMA packet scheduler for testing.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library work;
  use work.psi_common_array_pkg.all;

-- $$ processes=control,trigger,receive $$

------------------------------------------------------------------------------
-- Entity Declaration
------------------------------------------------------------------------------
entity axis_data_gen_simple_single is
  generic (
    CGN_BYTE_ORDER   : string                 := "down"; -- $$ constant="up" $$ "up", "down"
    CGN_STREAM_NR    : natural range 0 to 255 := 0       -- $$ constant=0 $$
  );
  port (
    -- Clock and Reset
    CLK_I            : in  std_logic; -- $$ type=clk; freq=125e6; proc=control,trigger,receive $$
    RST_N_I          : in  std_logic; -- $$ type=rst; clk=CLK_I; lowactive=true; proc=control,trigger,receive $$
    -- Configuration
    TRIGGER_PACKET_I : in  std_logic; -- $$ proc=trigger $$
    NR_OF_BYTES_I    : in  std_logic_vector(15 downto 0); -- $$ proc=trigger $$
    PKTS_PER_EVENT_I : in  std_logic_vector( 7 downto 0); -- $$ proc=trigger $$
    -- Data Interface
    TData_O          : out std_logic_vector(63 downto 0); -- $$ proc=receive $$
    TValid_O         : out std_logic;                     -- $$ proc=receive $$
    TReady_I         : in  std_logic;                     -- $$ proc=receive $$
    TLast_O          : out std_logic;                     -- $$ proc=receive $$
    Bytes_O          : out std_logic_vector(3 downto 0);  -- $$ proc=receive $$
    PktValid_O       : out std_logic;                     -- $$ proc=receive $$
    EOE_O            : out std_logic                      -- $$ proc=receive $$
  );
end entity;

------------------------------------------------------------------------------
-- Architecture Declaration
------------------------------------------------------------------------------
architecture rtl of axis_data_gen_simple_single is

  signal count         : signed(16 downto 0) := (others=>'0');
  signal data_bytes    : t_aslv8(7 downto 0) := (others=>(others=>'0'));
  signal word          : std_logic_vector(63 downto 0) := (others=>'0');
  signal rem_no_bytes  : std_logic_vector(3 downto 0)  := (others=>'0');
  signal event_pkt_cnt : unsigned(7 downto 0) := (others=>'0');
  signal total_pkt_cnt : unsigned(7 downto 0) := (others=>'0');
  signal first_word    : std_logic := '0';
  signal last          : std_logic := '0';

begin

  event_pkt_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_N_I = '0' then
        event_pkt_cnt <= (others=>'0');
      elsif last = '1' and TReady_I = '1' then
        if event_pkt_cnt = (unsigned(PKTS_PER_EVENT_I)-1) then
          event_pkt_cnt <= (others=>'0');
        else
          event_pkt_cnt <= event_pkt_cnt + 1;
        end if;
      end if;
    end if;
  end process;

  total_pkt_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_N_I = '0' then
        total_pkt_cnt <= (others=>'0');
      elsif last = '1' and TReady_I = '1' then
        total_pkt_cnt <= total_pkt_cnt + 1;
      end if;
    end if;
  end process;

  rem_gen: process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if count <= 0 and TRIGGER_PACKET_I = '1' then
        if NR_OF_BYTES_I(3 downto 0) = "0000" then
          rem_no_bytes <= "1000";
        else
          rem_no_bytes <= NR_OF_BYTES_I(3 downto 0);
        end if;
      end if;
    end if;
  end process;

  first_word_gen: process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if count <= 0 and last = '0' and TRIGGER_PACKET_I = '1' then
        first_word <= '1';
      else
        first_word <= '0';
      end if;
    end if;
  end process;

  byte_gen_comb: process(count)
  begin
    for i in 0 to 7 loop
      data_bytes(i) <= std_logic_vector(count(7 downto 0) - i);
    end loop;
  end process;

  counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if count > 0 then
        if TReady_I = '1' or first_word = '1' then
          if count > 7 then
            count <= count - 8;
          else
            count <= (others=>'0');
          end if;
        end if;
      elsif TRIGGER_PACKET_I = '1' and last = '0' then
        count <= '0' & (signed(NR_OF_BYTES_I) - 1);
      end if;
    end if;
  end process;

  TLast_O <= last;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_N_I = '0' then
        TData_O    <= (others=>'0');
        TValid_O   <= '0';
        PktValid_O <= '0';
        last       <= '0';
        Bytes_O    <= x"8";
        EOE_O      <= '0';
      else
        -- Data
        if TReady_I = '1' or first_word = '1' then
          for i in 7 downto 0 loop
            if CGN_BYTE_ORDER = "up" then
              TData_O((i+1)*8-1 downto i*8) <= data_bytes(i);
            else
              TData_O((i+1)*8-1 downto i*8) <= data_bytes(7-i);
            end if;
          end loop;
          if first_word = '1' then
            TValid_O   <= '1';
            PktValid_O <= '1';
            if CGN_BYTE_ORDER = "up" then
                -- first bytes is stream number
              TData_O( 7 downto  0) <= std_logic_vector(to_unsigned(CGN_STREAM_NR, 8));
              -- second bytes is packet number
              TData_O(15 downto  8) <= std_logic_vector(total_pkt_cnt);
            else
              -- first bytes is stream number
              TData_O(63 downto 56) <= std_logic_vector(to_unsigned(CGN_STREAM_NR, 8));
              -- second bytes is packet number
              TData_O(55 downto 48) <= std_logic_vector(total_pkt_cnt);
            end if;
          end if;
        end if;

        -- Valid and Packet Valid
        if count <= 0 then
          if TReady_I = '1' then
            TValid_O   <= '0';
            PktValid_O <= '0';
          end if;
        end if;

        -- Last and Bytes
        if TReady_I = '1' or first_word = '1' then
          if count <= 0 then
            last    <= '0';
            Bytes_O <= x"8";
          elsif count < 8 then
            last    <= '1';
            Bytes_O <= rem_no_bytes;
          end if;
        end if;

        -- End of Event
        if event_pkt_cnt = (unsigned(PKTS_PER_EVENT_I)-1) then
          EOE_O <= '1';
        else
          EOE_O <= '0';
        end if;
      end if;
    end if;

  end process;

end;
