##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Elmar Schmid
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME axis_dps_data_gen
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY PSI
set IP_DESCIRPTION "AXI Stream Counter Data Generator for DMA Packet Scheduler"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/axis_data_gen_simple.vhd \
  ../hdl/axis_data_gen_simple_single.vhd \
}

set_top_entity axis_data_gen_simple

#Relative Library Files
add_lib_relative \
  "../../../../library/" \
  { \
    VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
  }

# ###############################################################
# # Driver Files
# ###############################################################
#
# #WARNING! Driver files are stored with the VHDL code. If they are modified,
# #... the modifications need to be made there. The local files are overwritten
# #... automatically during packaging.
#
# #Copy files
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.c ../drivers/psi_ms_daq_axi/src/psi_ms_daq.c
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.h ../drivers/psi_ms_daq_axi/src/psi_ms_daq.h
# #Package
# add_drivers_relative ../drivers/psi_ms_daq_axi { \
#   src/psi_ms_daq.c \
#   src/psi_ms_daq.h \
# }

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "General Configuration"

gui_create_parameter "CGN_STREAMS" "Number of Streams"
gui_parameter_set_range 1 17
gui_add_parameter

gui_create_parameter "CGN_BYTE_ORDER" "Byte Order in 64 bit Word"
gui_parameter_set_widget_dropdown {"up" "down"}
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

for {set i 0} {$i < 17} {incr i} {
  set i02 [format "%02d" $i]
  add_port_enablement_condition "Slot$i02\_TData" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_TValid" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_TReady" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_TLast" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_Bytes" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_PktValid" "\$CGN_STREAMS > $i"
  add_port_enablement_condition "Slot$i02\_EOE" "\$CGN_STREAMS > $i"
  add_interface_enablement_condition "Slot$i02" "\$CGN_STREAMS > $i"
}

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
