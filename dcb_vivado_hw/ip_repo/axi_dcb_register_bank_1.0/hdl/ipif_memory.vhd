--------------------------------------------------------------------------------
--  Paul Scherrer Institut (PSI)
--------------------------------------------------------------------------------
--
--  Generic IPIF register file
--
--  Author  :  tg32
--  Created :  2018-02-02
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Library section
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;

library psi_3205_v1_00_a;
--use psi_3205_v1_00_a.all;
use psi_3205_v1_00_a.ram_dp;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------


entity ipif_memory is
  generic
  (
    CGN_USE_MEM0_SWAP         : boolean  := false;    --  true;  --  false; --
    CGN_USE_MEM1_SWAP         : boolean  := false     --  true   --  false  --
  );
  port
  (
    ------------------------------------------------------------------------
    -- user ports
    ------------------------------------------------------------------------
    USER_TO_MEM_I      : in  user_to_mem_type;
    MEM_TO_USER_O      : out mem_to_user_type;

    ------------------------------------------------------------------------
    -- IPIF ports
    ------------------------------------------------------------------------
    Bus2IP_Clk         : in  std_logic;
    Bus2IP_Reset       : in  std_logic;
    Bus2IP_Data        : in  std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    Bus2IP_BE          : in  std_logic_vector(C_SLV_DWIDTH/8-1 downto 0);
    Bus2IP_Addr        : in  std_logic_vector(C_SLV_AWIDTH-1   downto 0);
    Bus2IP_CS          : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW         : in  std_logic;
    IP2Bus_Data        : out std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    IP2Bus_RdAck       : out std_logic;
    IP2Bus_WrAck       : out std_logic;
    IP2Bus_Error       : out std_logic
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity ipif_memory;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ipif_memory is

  ------------------------------------------
  -- Signals for user logic memory space example
  ------------------------------------------

  signal mem_select            : std_logic_vector(0 to 1);
  signal mem_read_enable       : std_logic;
  signal mem_read_enable_dly1  : std_logic;

  signal mem_ip2bus_data       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_read_ack_dly1     : std_logic;
  signal mem_read_ack_dly2     : std_logic;
  signal mem_read_ack          : std_logic;
  signal mem_write_ack         : std_logic;


  signal mem0_a_enable  : std_logic;
  signal mem0_a_addr    : std_logic_vector(C_MEM0_DEPTH-1 downto 0);
  signal mem0_a_wr_en   : std_logic;
  signal mem0_a_wr_data : std_logic_vector(C_MEM0_WIDTH-1 downto 0);
  signal mem0_a_rd_data : std_logic_vector(C_MEM0_WIDTH-1 downto 0);
  signal mem0_a_swap    : std_logic_vector(1 downto 0) := "00";
  signal mem0_a_rd_cmd  : std_logic;

  signal mem0_b_enable  : std_logic;
  signal mem0_b_addr    : std_logic_vector(C_MEM0_DEPTH-1 downto 0);
  signal mem0_b_wr_en   : std_logic;
  signal mem0_b_wr_data : std_logic_vector(C_MEM0_WIDTH-1 downto 0);
  signal mem0_b_rd_data : std_logic_vector(C_MEM0_WIDTH-1 downto 0);

  signal mem1_a_enable  : std_logic;
  signal mem1_a_addr    : std_logic_vector(C_MEM1_DEPTH-1 downto 0);
  signal mem1_a_wr_en   : std_logic;
  signal mem1_a_wr_data : std_logic_vector(C_MEM1_WIDTH-1 downto 0);
  signal mem1_a_rd_data : std_logic_vector(C_MEM1_WIDTH-1 downto 0);
  signal mem1_a_swap    : std_logic_vector(1 downto 0) := "00";

  signal mem1_b_enable  : std_logic;
  signal mem1_b_addr    : std_logic_vector(C_MEM1_DEPTH-1 downto 0);
  signal mem1_b_wr_en   : std_logic;
  signal mem1_b_wr_data : std_logic_vector(C_MEM1_WIDTH-1 downto 0);
  signal mem1_b_rd_data : std_logic_vector(C_MEM1_WIDTH-1 downto 0);


  function swap_data(
    swap  :  in std_logic_vector( 1 downto 0);
    data  :  in std_logic_vector(31 downto 0)
  ) return std_logic_vector is
  begin
    case swap is
      when "01"    => return data(15 downto  8) & data( 7 downto  0) & data(31 downto 24) & data(23 downto 16); -- swap 16 bit words
      when "10"    => return data(23 downto 16) & data(31 downto 24) & data( 7 downto  0) & data(15 downto  8); -- swap bytes in 16 bit words
      when "11"    => return data( 7 downto  0) & data(15 downto  8) & data(23 downto 16) & data(31 downto 24); -- swap bytes in 32 bit word
      when others  => return data(31 downto 24) & data(23 downto 16) & data(15 downto  8) & data( 7 downto  0); -- do not swap, i.e. return data
    end case;
  end;

begin

  mem_select      <= Bus2IP_CS(0 to 1);
  mem_read_enable <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and Bus2IP_RNW;
  mem_read_ack    <= mem_read_ack_dly2;
  mem_write_ack   <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and not(Bus2IP_RNW);


  -- generate read acknowledge 1 or 2 clocks after read enable
  -- use mem_read_ack_dly1 if CGN_USE_OUTREG_A  = FALSE
  -- use mem_read_ack_dly2 if CGN_USE_OUTREG_A  = TRUE

  BRAM_RD_ACK_PROC : process( Bus2IP_Clk ) is
  begin

    if rising_edge(Bus2IP_Clk) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_enable_dly1 <= '0';
        mem_read_ack_dly1    <= '0';
        mem_read_ack_dly2    <= '0';
      else
        mem_read_enable_dly1 <= mem_read_enable;
        mem_read_ack_dly1    <= mem_read_enable and not(mem_read_enable_dly1);
        mem_read_ack_dly2    <= mem_read_ack_dly1;
      end if;
    end if;

  end process BRAM_RD_ACK_PROC;



--------------------------------------------------------------------------------
-- Memory 0
--------------------------------------------------------------------------------
-- Byte Enable ignored, just do 32bit access
  mem0_a_addr    <= Bus2IP_Addr(C_MEM0_DEPTH+7 downto 8);

  mem0_a_wr_en   <= not(Bus2IP_RNW) and Bus2IP_CS(0);

  gen_mem0_swap: if CGN_USE_MEM0_SWAP generate
    mem0_a_swap    <= Bus2IP_Addr(3 downto 2);
  end generate;

  mem0_a_wr_data <= Bus2IP_Addr(7 downto 4) & swap_data(mem0_a_swap, Bus2IP_Data);
  mem0_a_rd_cmd  <= Bus2IP_Addr(4);


  mem0_b_enable  <= USER_TO_MEM_I.mem0_enable;
  mem0_b_addr    <= USER_TO_MEM_I.mem0_addr;
  mem0_b_wr_en   <= USER_TO_MEM_I.mem0_wr_en;
  mem0_b_wr_data <= USER_TO_MEM_I.mem0_wr_data;

  MEM_TO_USER_O.mem0_rd_data <= mem0_b_rd_data;


  ram_dp_0_inst :entity psi_3205_v1_00_a.ram_dp
  generic map
  (
    CGN_DATA_WIDTH_A  => C_MEM0_WIDTH,
    CGN_ADDR_WIDTH_A  => C_MEM0_DEPTH,
    CGN_READ_PORT_A   => true,
    CGN_WRITE_PORT_A  => true,
    CGN_READ_FIRST_A  => false,
    CGN_USE_OUTREG_A  => true,

    CGN_DATA_WIDTH_B  => C_MEM0_WIDTH,
    CGN_ADDR_WIDTH_B  => C_MEM0_DEPTH,
    CGN_READ_PORT_B   => true,
    CGN_WRITE_PORT_B  => false,
    CGN_READ_FIRST_B  => true,
    CGN_USE_OUTREG_B  => false,

    CGN_RAM_STYLE     => "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
  )
  port map
  (
    PA_CLK_I    => Bus2IP_Clk,
    PA_ADDR_I   => mem0_a_addr,
    PA_EN_I     => '1',
    PA_WR_EN_I  => mem0_a_wr_en,
    PA_DATA_I   => mem0_a_wr_data,
    PA_DATA_O   => mem0_a_rd_data,

    PB_CLK_I    => Bus2IP_Clk,
    PB_ADDR_I   => mem0_b_addr,
    PB_EN_I     => mem0_b_enable,
    PB_WR_EN_I  => mem0_b_wr_en,
    PB_DATA_I   => mem0_b_wr_data,
    PB_DATA_O   => mem0_b_rd_data
  );




--------------------------------------------------------------------------------
-- Memory 1
--------------------------------------------------------------------------------
-- Byte Enable ignored, just do 32bit access

  mem1_a_addr    <= Bus2IP_Addr(C_MEM1_DEPTH+3 downto 4);
  mem1_a_wr_en   <= not(Bus2IP_RNW) and Bus2IP_CS(1);

  gen_mem1_swap: if CGN_USE_MEM1_SWAP generate
    mem1_a_swap    <= Bus2IP_Addr(3 downto 2);
  end generate;

  mem1_a_wr_data <= swap_data(mem1_a_swap, Bus2IP_Data);

  mem1_b_enable  <= USER_TO_MEM_I.mem1_enable;
  mem1_b_addr    <= USER_TO_MEM_I.mem1_addr;
  mem1_b_wr_en   <= USER_TO_MEM_I.mem1_wr_en;
  mem1_b_wr_data <= USER_TO_MEM_I.mem1_wr_data;

  MEM_TO_USER_O.mem1_rd_data <= mem1_b_rd_data;



  ram_dp_1_inst :entity psi_3205_v1_00_a.ram_dp
  generic map
  (
    CGN_DATA_WIDTH_A  => C_MEM1_WIDTH,
    CGN_ADDR_WIDTH_A  => C_MEM1_DEPTH,
    CGN_READ_PORT_A   => true,
    CGN_WRITE_PORT_A  => true,
    CGN_READ_FIRST_A  => false,
    CGN_USE_OUTREG_A  => true,

    CGN_DATA_WIDTH_B  => C_MEM1_WIDTH,
    CGN_ADDR_WIDTH_B  => C_MEM1_DEPTH,
    CGN_READ_PORT_B   => true,
    CGN_WRITE_PORT_B  => true,
    CGN_READ_FIRST_B  => true,
    CGN_USE_OUTREG_B  => false,

    CGN_RAM_STYLE     => "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
  )
  port map
  (
    PA_CLK_I    => Bus2IP_Clk,
    PA_ADDR_I   => mem1_a_addr,
    PA_EN_I     => '1',
    PA_WR_EN_I  => mem1_a_wr_en,
    PA_DATA_I   => mem1_a_wr_data,
    PA_DATA_O   => mem1_a_rd_data,

    PB_CLK_I    => Bus2IP_Clk,
    PB_ADDR_I   => mem1_b_addr,
    PB_EN_I     => mem1_b_enable,
    PB_WR_EN_I  => mem1_b_wr_en,
    PB_DATA_I   => mem1_b_wr_data,
    PB_DATA_O   => mem1_b_rd_data
  );

-----------------------------------------------------------------------------------
-- implement Block RAM read mux
-----------------------------------------------------------------------------------

  MEM_IP2BUS_DATA_PROC : process(mem_select, mem0_a_rd_data, mem1_a_rd_data, mem0_a_swap, mem1_a_swap, mem0_a_rd_cmd) is
  begin

    case (mem_select) is
      when "10" =>  if ( mem0_a_rd_cmd = '0') then
                      mem_ip2bus_data <= swap_data(mem0_a_swap, mem0_a_rd_data(31 downto  0));
                    else
                      mem_ip2bus_data <= x"0000000" & mem0_a_rd_data(35 downto 32);
                    end if;

      when "01" => mem_ip2bus_data <= swap_data(mem1_a_swap, mem1_a_rd_data(31 downto  0));
      when others => mem_ip2bus_data <= (others => '0');
    end case;

  end process MEM_IP2BUS_DATA_PROC;

  ---------------

  IP2Bus_Data    <= mem_ip2bus_data;
  IP2Bus_RdAck   <= mem_read_ack;
  IP2Bus_WrAck   <= mem_write_ack;
  IP2Bus_Error   <= '0';


end behavioral;
