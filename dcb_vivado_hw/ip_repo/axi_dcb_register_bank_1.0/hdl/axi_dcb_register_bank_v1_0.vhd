---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  axi_dcb_register_bank_v1_0.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)
--  Created :  01.04.2022 10:50:56
--
--  Description :  Toplevel of the MEGII DCB register bank.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;
use axi_dcb_reg_bank_v1_0.ipif_axi;

entity axi_dcb_register_bank_v1_0 is
  generic (
    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH  : integer           := 32;
    C_S00_AXI_ADDR_WIDTH  : integer           := 32;
    C_S00_AXI_BASEADDR    : std_logic_vector := X"FFFFFFFF";
    C_S00_AXI_HIGHADDR    : std_logic_vector := X"00000000";
    C_S00_AXI_MIN_SIZE    : std_logic_vector := X"FFFFFFFF";
    -- Users parameters
    C_USE_WSTRB           : integer          := 0;
    C_DPHASE_TIMEOUT      : integer          := 8;
    C_FAMILY              : string           := "virtex6"
    --C_MEM0_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM0_HIGHADDR       : std_logic_vector := X"00000000";
    --C_MEM1_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM1_HIGHADDR       : std_logic_vector := X"00000000"
  );
  port (
    -- -------------------------------------------
    -- User Ports                               --
    -- -------------------------------------------
    -- Register 0 [0x0000]: HW_VER
    HW_VER_REG_RD_STROBE_O                    : out std_logic;
    HW_VER_REG_WR_STROBE_O                    : out std_logic;

    -- Register 1 [0x0004]: REG_LAYOUT_VER
    REG_LAYOUT_VER_REG_RD_STROBE_O            : out std_logic;
    REG_LAYOUT_VER_REG_WR_STROBE_O            : out std_logic;

    -- Register 2 [0x0008]: FW_BUILD_DATE
    FW_BUILD_DATE_REG_RD_STROBE_O             : out std_logic;
    FW_BUILD_DATE_REG_WR_STROBE_O             : out std_logic;
    FW_BUILD_YEAR_I                           : in  std_logic_vector(15 downto 0);    
    FW_BUILD_MONTH_I                          : in  std_logic_vector(7 downto 0);     
    FW_BUILD_DAY_I                            : in  std_logic_vector(7 downto 0);     

    -- Register 3 [0x000C]: FW_BUILD_TIME
    FW_BUILD_TIME_REG_RD_STROBE_O             : out std_logic;
    FW_BUILD_TIME_REG_WR_STROBE_O             : out std_logic;
    FW_BUILD_HOUR_I                           : in  std_logic_vector(7 downto 0);     
    FW_BUILD_MINUTE_I                         : in  std_logic_vector(7 downto 0);     
    FW_BUILD_SECOND_I                         : in  std_logic_vector(7 downto 0);     

    -- Register 4 [0x0010]: SW_BUILD_DATE
    SW_BUILD_DATE_REG_RD_STROBE_O             : out std_logic;
    SW_BUILD_DATE_REG_WR_STROBE_O             : out std_logic;
    SW_BUILD_YEAR_O                           : out std_logic_vector(15 downto 0);    
    SW_BUILD_MONTH_O                          : out std_logic_vector(7 downto 0);     
    SW_BUILD_DAY_O                            : out std_logic_vector(7 downto 0);     

    -- Register 5 [0x0014]: SW_BUILD_TIME
    SW_BUILD_TIME_REG_RD_STROBE_O             : out std_logic;
    SW_BUILD_TIME_REG_WR_STROBE_O             : out std_logic;
    SW_BUILD_HOUR_O                           : out std_logic_vector(7 downto 0);     
    SW_BUILD_MINUTE_O                         : out std_logic_vector(7 downto 0);     
    SW_BUILD_SECOND_O                         : out std_logic_vector(7 downto 0);     

    -- Register 6 [0x0018]: FW_GIT_HASH_TAG
    FW_GIT_HASH_TAG_REG_RD_STROBE_O           : out std_logic;
    FW_GIT_HASH_TAG_REG_WR_STROBE_O           : out std_logic;
    FW_GIT_HASH_TAG_I                         : in  std_logic_vector(31 downto 0);    

    -- Register 7 [0x001C]: SW_GIT_HASH_TAG
    SW_GIT_HASH_TAG_REG_RD_STROBE_O           : out std_logic;
    SW_GIT_HASH_TAG_REG_WR_STROBE_O           : out std_logic;
    SW_GIT_HASH_TAG_O                         : out std_logic_vector(31 downto 0);    

    -- Register 8 [0x0020]: PROT_VER
    PROT_VER_REG_RD_STROBE_O                  : out std_logic;
    PROT_VER_REG_WR_STROBE_O                  : out std_logic;
    PROTOCOL_VERSION_O                        : out std_logic_vector(7 downto 0);     

    -- Register 9 [0x0024]: SN
    SN_REG_RD_STROBE_O                        : out std_logic;
    SN_REG_WR_STROBE_O                        : out std_logic;
    SERIAL_NUMBER_O                           : out std_logic_vector(15 downto 0);    

    -- Register 10 [0x0028]: STATUS
    STATUS_REG_RD_STROBE_O                    : out std_logic;
    STATUS_REG_WR_STROBE_O                    : out std_logic;
    FLASH_SEL_I                               : in  std_logic;                        
    BOARD_SEL_I                               : in  std_logic;                        
    SERIAL_BUSY_I                             : in  std_logic;                        
    DCB_BUSY_I                                : in  std_logic;                        
    SYS_BUSY_I                                : in  std_logic;                        

    -- Register 11 [0x002C]: TEMP
    TEMP_REG_RD_STROBE_O                      : out std_logic;
    TEMP_REG_WR_STROBE_O                      : out std_logic;
    TEMPERATURE_O                             : out std_logic_vector(15 downto 0);    

    -- Register 12 [0x0030]: PLL_LOCK
    PLL_LOCK_REG_RD_STROBE_O                  : out std_logic;
    PLL_LOCK_REG_WR_STROBE_O                  : out std_logic;
    SERDES_CLK_MGR_LOCK_I                     : in  std_logic;                        
    WDB_CLK_MGR_LOCK_I                        : in  std_logic;                        
    SYS_DCM_LOCK_I                            : in  std_logic;                        
    LMK_PLL_LOCK_I                            : in  std_logic;                        

    -- Register 13 [0x0034]: DCB_LOC
    DCB_LOC_REG_RD_STROBE_O                   : out std_logic;
    DCB_LOC_REG_WR_STROBE_O                   : out std_logic;
    CRATE_ID_O                                : out std_logic_vector(7 downto 0);     
    SLOT_ID_O                                 : out std_logic_vector(7 downto 0);     

    -- Register 14 [0x0038]: CTRL
    CTRL_REG_RD_STROBE_O                      : out std_logic;
    CTRL_REG_WR_STROBE_O                      : out std_logic;
    SYNC_DELAY_O                              : out std_logic_vector(4 downto 0);     
    DAQ_SOFT_TRIGGER_O                        : out std_logic;                        
    DAQ_AUTO_O                                : out std_logic;                        
    DAQ_NORMAL_O                              : out std_logic;                        
    DAQ_SINGLE_O                              : out std_logic;                        

    -- Register 15 [0x003C]: SET_CTRL
    SET_CTRL_REG_RD_STROBE_O                  : out std_logic;
    SET_CTRL_REG_WR_STROBE_O                  : out std_logic;
    SET_BIT_CTRL_O                            : out std_logic_vector(31 downto 0);    

    -- Register 16 [0x0040]: CLR_CTRL
    CLR_CTRL_REG_RD_STROBE_O                  : out std_logic;
    CLR_CTRL_REG_WR_STROBE_O                  : out std_logic;
    CLR_BIT_CTRL_O                            : out std_logic_vector(31 downto 0);    

    -- Register 17 [0x0044]: CLK_CTRL
    CLK_CTRL_REG_RD_STROBE_O                  : out std_logic;
    CLK_CTRL_REG_WR_STROBE_O                  : out std_logic;
    DISTRIBUTOR_CLK_OUT_EN_O                  : out std_logic_vector(19 downto 0);    
    DISTRIBUTOR_CLK_SRC_SEL_O                 : out std_logic;                        
    BUS_CLK_SRC_SEL_O                         : out std_logic;                        
    LMK_CLK_SRC_SEL_O                         : out std_logic;                        
    EXT_CLK_IN_SEL_O                          : out std_logic;                        

    -- Register 18 [0x0048]: SET_CLK_CTRL
    SET_CLK_CTRL_REG_RD_STROBE_O              : out std_logic;
    SET_CLK_CTRL_REG_WR_STROBE_O              : out std_logic;
    SET_BIT_CLK_CTRL_O                        : out std_logic_vector(31 downto 0);    

    -- Register 19 [0x004C]: CLR_CLK_CTRL
    CLR_CLK_CTRL_REG_RD_STROBE_O              : out std_logic;
    CLR_CLK_CTRL_REG_WR_STROBE_O              : out std_logic;
    CLR_BIT_CLK_CTRL_O                        : out std_logic_vector(31 downto 0);    

    -- Register 20 [0x0050]: COM_CTRL
    COM_CTRL_REG_RD_STROBE_O                  : out std_logic;
    COM_CTRL_REG_WR_STROBE_O                  : out std_logic;
    INTER_PKG_DELAY_O                         : out std_logic_vector(23 downto 0);    

    -- Register 21 [0x0054]: DPS_CTRL
    -- '--> Pure software register => no ports available

    -- Register 22 [0x0058]: RST
    RST_REG_RD_STROBE_O                       : out std_logic;
    RST_REG_WR_STROBE_O                       : out std_logic;
    ISERDES_RCVR_PACKET_COUNT_RST_O           : out std_logic;                        
    WDB_SERDES_CLK_MGR_RST_O                  : out std_logic;                        
    WDB_REFCLK_MGR_RST_O                      : out std_logic;                        
    TRIGGER_MGR_RST_O                         : out std_logic;                        
    TR_SYNC_BPL_O                             : out std_logic;                        
    LMK_SYNC_DCB_O                            : out std_logic;                        
    ISERDES_RCVR_ERROR_COUNT_RST_O            : out std_logic;                        
    ISERDES_RECEIVER_RESYNC_O                 : out std_logic;                        
    ISERDES_RECEIVER_RST_O                    : out std_logic;                        
    RECONFIGURE_FPGA_O                        : out std_logic;                        

    -- Register 23 [0x005C]: SERDES_STATUS_00_07
    SERDES_STATUS_00_07_REG_RD_STROBE_O       : out std_logic;
    SERDES_STATUS_00_07_REG_WR_STROBE_O       : out std_logic;
    IDLE_PATTERN_DETECT_07_I                  : in  std_logic;                        
    BITSLIP_OK_07_I                           : in  std_logic;                        
    DELAY_OK_07_I                             : in  std_logic;                        
    SYNC_DONE_07_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_06_I                  : in  std_logic;                        
    BITSLIP_OK_06_I                           : in  std_logic;                        
    DELAY_OK_06_I                             : in  std_logic;                        
    SYNC_DONE_06_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_05_I                  : in  std_logic;                        
    BITSLIP_OK_05_I                           : in  std_logic;                        
    DELAY_OK_05_I                             : in  std_logic;                        
    SYNC_DONE_05_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_04_I                  : in  std_logic;                        
    BITSLIP_OK_04_I                           : in  std_logic;                        
    DELAY_OK_04_I                             : in  std_logic;                        
    SYNC_DONE_04_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_03_I                  : in  std_logic;                        
    BITSLIP_OK_03_I                           : in  std_logic;                        
    DELAY_OK_03_I                             : in  std_logic;                        
    SYNC_DONE_03_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_02_I                  : in  std_logic;                        
    BITSLIP_OK_02_I                           : in  std_logic;                        
    DELAY_OK_02_I                             : in  std_logic;                        
    SYNC_DONE_02_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_01_I                  : in  std_logic;                        
    BITSLIP_OK_01_I                           : in  std_logic;                        
    DELAY_OK_01_I                             : in  std_logic;                        
    SYNC_DONE_01_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_00_I                  : in  std_logic;                        
    BITSLIP_OK_00_I                           : in  std_logic;                        
    DELAY_OK_00_I                             : in  std_logic;                        
    SYNC_DONE_00_I                            : in  std_logic;                        

    -- Register 24 [0x0060]: SERDES_STATUS_08_15
    SERDES_STATUS_08_15_REG_RD_STROBE_O       : out std_logic;
    SERDES_STATUS_08_15_REG_WR_STROBE_O       : out std_logic;
    IDLE_PATTERN_DETECT_15_I                  : in  std_logic;                        
    BITSLIP_OK_15_I                           : in  std_logic;                        
    DELAY_OK_15_I                             : in  std_logic;                        
    SYNC_DONE_15_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_14_I                  : in  std_logic;                        
    BITSLIP_OK_14_I                           : in  std_logic;                        
    DELAY_OK_14_I                             : in  std_logic;                        
    SYNC_DONE_14_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_13_I                  : in  std_logic;                        
    BITSLIP_OK_13_I                           : in  std_logic;                        
    DELAY_OK_13_I                             : in  std_logic;                        
    SYNC_DONE_13_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_12_I                  : in  std_logic;                        
    BITSLIP_OK_12_I                           : in  std_logic;                        
    DELAY_OK_12_I                             : in  std_logic;                        
    SYNC_DONE_12_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_11_I                  : in  std_logic;                        
    BITSLIP_OK_11_I                           : in  std_logic;                        
    DELAY_OK_11_I                             : in  std_logic;                        
    SYNC_DONE_11_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_10_I                  : in  std_logic;                        
    BITSLIP_OK_10_I                           : in  std_logic;                        
    DELAY_OK_10_I                             : in  std_logic;                        
    SYNC_DONE_10_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_09_I                  : in  std_logic;                        
    BITSLIP_OK_09_I                           : in  std_logic;                        
    DELAY_OK_09_I                             : in  std_logic;                        
    SYNC_DONE_09_I                            : in  std_logic;                        
    IDLE_PATTERN_DETECT_08_I                  : in  std_logic;                        
    BITSLIP_OK_08_I                           : in  std_logic;                        
    DELAY_OK_08_I                             : in  std_logic;                        
    SYNC_DONE_08_I                            : in  std_logic;                        

    -- Register 25 [0x0064]: SERDES_STATUS_17
    SERDES_STATUS_17_REG_RD_STROBE_O          : out std_logic;
    SERDES_STATUS_17_REG_WR_STROBE_O          : out std_logic;
    IDLE_PATTERN_DETECT_17_I                  : in  std_logic;                        
    BITSLIP_OK_17_I                           : in  std_logic;                        
    DELAY_OK_17_I                             : in  std_logic;                        
    SYNC_DONE_17_I                            : in  std_logic;                        

    -- Register 26 [0x0068]: SERDES_ERR_CNT_00
    SERDES_ERR_CNT_00_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_00_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_00_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_00_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_00_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_00_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 27 [0x006C]: SERDES_ERR_CNT_01
    SERDES_ERR_CNT_01_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_01_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_01_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_01_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_01_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_01_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 28 [0x0070]: SERDES_ERR_CNT_02
    SERDES_ERR_CNT_02_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_02_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_02_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_02_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_02_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_02_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 29 [0x0074]: SERDES_ERR_CNT_03
    SERDES_ERR_CNT_03_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_03_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_03_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_03_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_03_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_03_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 30 [0x0078]: SERDES_ERR_CNT_04
    SERDES_ERR_CNT_04_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_04_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_04_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_04_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_04_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_04_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 31 [0x007C]: SERDES_ERR_CNT_05
    SERDES_ERR_CNT_05_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_05_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_05_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_05_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_05_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_05_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 32 [0x0080]: SERDES_ERR_CNT_06
    SERDES_ERR_CNT_06_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_06_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_06_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_06_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_06_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_06_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 33 [0x0084]: SERDES_ERR_CNT_07
    SERDES_ERR_CNT_07_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_07_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_07_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_07_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_07_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_07_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 34 [0x0088]: SERDES_ERR_CNT_08
    SERDES_ERR_CNT_08_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_08_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_08_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_08_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_08_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_08_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 35 [0x008C]: SERDES_ERR_CNT_09
    SERDES_ERR_CNT_09_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_09_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_09_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_09_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_09_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_09_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 36 [0x0090]: SERDES_ERR_CNT_10
    SERDES_ERR_CNT_10_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_10_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_10_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_10_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_10_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_10_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 37 [0x0094]: SERDES_ERR_CNT_11
    SERDES_ERR_CNT_11_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_11_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_11_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_11_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_11_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_11_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 38 [0x0098]: SERDES_ERR_CNT_12
    SERDES_ERR_CNT_12_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_12_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_12_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_12_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_12_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_12_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 39 [0x009C]: SERDES_ERR_CNT_13
    SERDES_ERR_CNT_13_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_13_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_13_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_13_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_13_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_13_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 40 [0x00A0]: SERDES_ERR_CNT_14
    SERDES_ERR_CNT_14_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_14_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_14_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_14_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_14_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_14_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 41 [0x00A4]: SERDES_ERR_CNT_15
    SERDES_ERR_CNT_15_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_15_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_15_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_15_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_15_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_15_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 42 [0x00A8]: SERDES_ERR_CNT_17
    SERDES_ERR_CNT_17_REG_RD_STROBE_O         : out std_logic;
    SERDES_ERR_CNT_17_REG_WR_STROBE_O         : out std_logic;
    CRC_ERRORS_17_I                           : in  std_logic_vector(7 downto 0);     
    FRAME_ERRORS_17_I                         : in  std_logic_vector(7 downto 0);     
    DATAGRAM_ERRORS_17_I                      : in  std_logic_vector(7 downto 0);     
    SYNC_ERRORS_17_I                          : in  std_logic_vector(7 downto 0);     

    -- Register 43 [0x00AC]: APLY_CFG
    APLY_CFG_REG_RD_STROBE_O                  : out std_logic;
    APLY_CFG_REG_WR_STROBE_O                  : out std_logic;
    APPLY_SETTINGS_LMK_O                      : out std_logic;                        

    -- Register 44 [0x00B0]: LMK_0
    LMK_0_REG_RD_STROBE_O                     : out std_logic;
    LMK_0_REG_WR_STROBE_O                     : out std_logic;
    LMK0_RESET_O                              : out std_logic;                        
    LMK0_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK0_CLKOUT_EN_O                          : out std_logic;                        
    LMK0_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK0_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 45 [0x00B4]: LMK_1
    LMK_1_REG_RD_STROBE_O                     : out std_logic;
    LMK_1_REG_WR_STROBE_O                     : out std_logic;
    LMK1_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK1_CLKOUT_EN_O                          : out std_logic;                        
    LMK1_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK1_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 46 [0x00B8]: LMK_2
    LMK_2_REG_RD_STROBE_O                     : out std_logic;
    LMK_2_REG_WR_STROBE_O                     : out std_logic;
    LMK2_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK2_CLKOUT_EN_O                          : out std_logic;                        
    LMK2_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK2_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 47 [0x00BC]: LMK_3
    LMK_3_REG_RD_STROBE_O                     : out std_logic;
    LMK_3_REG_WR_STROBE_O                     : out std_logic;
    LMK3_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK3_CLKOUT_EN_O                          : out std_logic;                        
    LMK3_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK3_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 48 [0x00C0]: LMK_4
    LMK_4_REG_RD_STROBE_O                     : out std_logic;
    LMK_4_REG_WR_STROBE_O                     : out std_logic;
    LMK4_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK4_CLKOUT_EN_O                          : out std_logic;                        
    LMK4_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK4_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 49 [0x00C4]: LMK_5
    LMK_5_REG_RD_STROBE_O                     : out std_logic;
    LMK_5_REG_WR_STROBE_O                     : out std_logic;
    LMK5_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK5_CLKOUT_EN_O                          : out std_logic;                        
    LMK5_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK5_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 50 [0x00C8]: LMK_6
    LMK_6_REG_RD_STROBE_O                     : out std_logic;
    LMK_6_REG_WR_STROBE_O                     : out std_logic;
    LMK6_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK6_CLKOUT_EN_O                          : out std_logic;                        
    LMK6_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK6_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 51 [0x00CC]: LMK_7
    LMK_7_REG_RD_STROBE_O                     : out std_logic;
    LMK_7_REG_WR_STROBE_O                     : out std_logic;
    LMK7_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK7_CLKOUT_EN_O                          : out std_logic;                        
    LMK7_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK7_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     

    -- Register 52 [0x00D0]: LMK_8
    LMK_8_REG_RD_STROBE_O                     : out std_logic;
    LMK_8_REG_WR_STROBE_O                     : out std_logic;
    LMK8_PHASE_NOISE_OPT_O                    : out std_logic_vector(27 downto 0);    

    -- Register 53 [0x00D4]: LMK_9
    LMK_9_REG_RD_STROBE_O                     : out std_logic;
    LMK_9_REG_WR_STROBE_O                     : out std_logic;
    LMK9_VBOOST_O                             : out std_logic;                        

    -- Register 54 [0x00D8]: LMK_11
    LMK_11_REG_RD_STROBE_O                    : out std_logic;
    LMK_11_REG_WR_STROBE_O                    : out std_logic;
    LMK11_DIV4_O                              : out std_logic;                        

    -- Register 55 [0x00DC]: LMK_13
    LMK_13_REG_RD_STROBE_O                    : out std_logic;
    LMK_13_REG_WR_STROBE_O                    : out std_logic;
    LMK13_OSCIN_FREQ_O                        : out std_logic_vector(7 downto 0);     
    LMK13_VCO_R4_LF_O                         : out std_logic_vector(2 downto 0);     
    LMK13_VCO_R3_LF_O                         : out std_logic_vector(2 downto 0);     
    LMK13_VCO_C3_C4_LF_O                      : out std_logic_vector(3 downto 0);     

    -- Register 56 [0x00E0]: LMK_14
    LMK_14_REG_RD_STROBE_O                    : out std_logic;
    LMK_14_REG_WR_STROBE_O                    : out std_logic;
    LMK14_EN_FOUT_O                           : out std_logic;                        
    LMK14_EN_CLKOUT_GLOBAL_O                  : out std_logic;                        
    LMK14_POWERDOWN_O                         : out std_logic;                        
    LMK14_PLL_MUX_O                           : out std_logic_vector(3 downto 0);     
    LMK14_PLL_R_O                             : out std_logic_vector(11 downto 0);    

    -- Register 57 [0x00E4]: LMK_15
    LMK_15_REG_RD_STROBE_O                    : out std_logic;
    LMK_15_REG_WR_STROBE_O                    : out std_logic;
    LMK15_PLL_CP_GAIN_O                       : out std_logic_vector(1 downto 0);     
    LMK15_VCO_DIV_O                           : out std_logic_vector(3 downto 0);     
    LMK15_PLL_N_O                             : out std_logic_vector(17 downto 0);    

    -- Register 58 [0x00E8]: TIME_LSB
    TIME_LSB_REG_RD_STROBE_O                  : out std_logic;
    TIME_LSB_REG_WR_STROBE_O                  : out std_logic;
    TIME_LSB_I                                : in  std_logic_vector(31 downto 0);    

    -- Register 59 [0x00EC]: TIME_MSB
    TIME_MSB_REG_RD_STROBE_O                  : out std_logic;
    TIME_MSB_REG_WR_STROBE_O                  : out std_logic;
    TIME_MSB_I                                : in  std_logic_vector(31 downto 0);    

    -- Register 60 [0x00F0]: TIME_LSB_SET
    TIME_LSB_SET_REG_RD_STROBE_O              : out std_logic;
    TIME_LSB_SET_REG_WR_STROBE_O              : out std_logic;
    TIME_LSB_SET_O                            : out std_logic_vector(31 downto 0);    

    -- Register 61 [0x00F4]: TIME_MSB_SET
    TIME_MSB_SET_REG_RD_STROBE_O              : out std_logic;
    TIME_MSB_SET_REG_WR_STROBE_O              : out std_logic;
    TIME_MSB_SET_O                            : out std_logic_vector(31 downto 0);    

    -- Register 62 [0x00F8]: EVENT_TX_RATE
    EVENT_TX_RATE_REG_RD_STROBE_O             : out std_logic;
    EVENT_TX_RATE_REG_WR_STROBE_O             : out std_logic;
    EVENT_TX_RATE_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 63 [0x00FC]: EVENT_NR
    EVENT_NR_REG_RD_STROBE_O                  : out std_logic;
    EVENT_NR_REG_WR_STROBE_O                  : out std_logic;
    EVENT_NUMBER_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 64 [0x0100]: TRG_CFG
    TRG_CFG_REG_RD_STROBE_O                   : out std_logic;
    TRG_CFG_REG_WR_STROBE_O                   : out std_logic;
    EXT_TRIGGER_OUT_ENABLE_O                  : out std_logic;                        

    -- Register 65 [0x0104]: SET_TRG_CFG
    SET_TRG_CFG_REG_RD_STROBE_O               : out std_logic;
    SET_TRG_CFG_REG_WR_STROBE_O               : out std_logic;
    SET_BIT_TRG_CFG_O                         : out std_logic_vector(31 downto 0);    

    -- Register 66 [0x0108]: CLR_TRG_CFG
    CLR_TRG_CFG_REG_RD_STROBE_O               : out std_logic;
    CLR_TRG_CFG_REG_WR_STROBE_O               : out std_logic;
    CLR_BIT_TRG_CFG_O                         : out std_logic_vector(31 downto 0);    

    -- Register 67 [0x010C]: TRG_AUTO_PERIOD
    TRG_AUTO_PERIOD_REG_RD_STROBE_O           : out std_logic;
    TRG_AUTO_PERIOD_REG_WR_STROBE_O           : out std_logic;
    AUTO_TRIGGER_PERIOD_O                     : out std_logic_vector(31 downto 0);    

    -- Register 68 [0x0110]: TRB_INFO_STAT
    TRB_INFO_STAT_REG_RD_STROBE_O             : out std_logic;
    TRB_INFO_STAT_REG_WR_STROBE_O             : out std_logic;
    TRB_FLAG_NEW_I                            : in  std_logic;                        
    TRB_FLAG_PARITY_ERROR_I                   : in  std_logic;                        
    TRB_PARITY_ERROR_COUNT_I                  : in  std_logic_vector(15 downto 0);    

    -- Register 69 [0x0114]: TRB_INFO_LSB
    TRB_INFO_LSB_REG_RD_STROBE_O              : out std_logic;
    TRB_INFO_LSB_REG_WR_STROBE_O              : out std_logic;
    TRB_INFO_LSB_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 70 [0x0118]: TRB_INFO_MSB
    TRB_INFO_MSB_REG_RD_STROBE_O              : out std_logic;
    TRB_INFO_MSB_REG_WR_STROBE_O              : out std_logic;
    TRB_INFO_MSB_I                            : in  std_logic_vector(15 downto 0);    

    -- Register 71 [0x011C]: LMK_MOD_FLAG
    LMK_MOD_FLAG_REG_RD_STROBE_O              : out std_logic;
    LMK_MOD_FLAG_REG_WR_STROBE_O              : out std_logic;
    LMK_7_MOD_I                               : in  std_logic;                        
    LMK_6_MOD_I                               : in  std_logic;                        
    LMK_5_MOD_I                               : in  std_logic;                        
    LMK_4_MOD_I                               : in  std_logic;                        
    LMK_3_MOD_I                               : in  std_logic;                        
    LMK_2_MOD_I                               : in  std_logic;                        
    LMK_1_MOD_I                               : in  std_logic;                        
    LMK_0_MOD_I                               : in  std_logic;                        

    -- Register 72 [0x0120]: CMB_MSCB_ADR
    -- '--> Pure software register => no ports available

    -- Register 73 [0x0124]: SD_PKT_CNT_0
    SD_PKT_CNT_0_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_0_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_0_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 74 [0x0128]: SD_PKT_CNT_1
    SD_PKT_CNT_1_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_1_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_1_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 75 [0x012C]: SD_PKT_CNT_2
    SD_PKT_CNT_2_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_2_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_2_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 76 [0x0130]: SD_PKT_CNT_3
    SD_PKT_CNT_3_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_3_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_3_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 77 [0x0134]: SD_PKT_CNT_4
    SD_PKT_CNT_4_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_4_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_4_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 78 [0x0138]: SD_PKT_CNT_5
    SD_PKT_CNT_5_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_5_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_5_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 79 [0x013C]: SD_PKT_CNT_6
    SD_PKT_CNT_6_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_6_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_6_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 80 [0x0140]: SD_PKT_CNT_7
    SD_PKT_CNT_7_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_7_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_7_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 81 [0x0144]: SD_PKT_CNT_8
    SD_PKT_CNT_8_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_8_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_8_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 82 [0x0148]: SD_PKT_CNT_9
    SD_PKT_CNT_9_REG_RD_STROBE_O              : out std_logic;
    SD_PKT_CNT_9_REG_WR_STROBE_O              : out std_logic;
    SD_PKT_CNT_9_I                            : in  std_logic_vector(31 downto 0);    

    -- Register 83 [0x014C]: SD_PKT_CNT_10
    SD_PKT_CNT_10_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_10_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_10_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 84 [0x0150]: SD_PKT_CNT_11
    SD_PKT_CNT_11_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_11_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_11_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 85 [0x0154]: SD_PKT_CNT_12
    SD_PKT_CNT_12_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_12_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_12_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 86 [0x0158]: SD_PKT_CNT_13
    SD_PKT_CNT_13_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_13_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_13_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 87 [0x015C]: SD_PKT_CNT_14
    SD_PKT_CNT_14_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_14_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_14_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 88 [0x0160]: SD_PKT_CNT_15
    SD_PKT_CNT_15_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_15_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_15_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 89 [0x0164]: SD_PKT_CNT_17
    SD_PKT_CNT_17_REG_RD_STROBE_O             : out std_logic;
    SD_PKT_CNT_17_REG_WR_STROBE_O             : out std_logic;
    SD_PKT_CNT_17_I                           : in  std_logic_vector(31 downto 0);    

    -- Register 90 [0x0168]: SD_EYE_STATUS_0
    SD_EYE_STATUS_0_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_0_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_0_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_0_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 91 [0x016C]: SD_EYE_STATUS_1
    SD_EYE_STATUS_1_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_1_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_1_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_1_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 92 [0x0170]: SD_EYE_STATUS_2
    SD_EYE_STATUS_2_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_2_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_2_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_2_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 93 [0x0174]: SD_EYE_STATUS_3
    SD_EYE_STATUS_3_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_3_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_3_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_3_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 94 [0x0178]: SD_EYE_STATUS_4
    SD_EYE_STATUS_4_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_4_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_4_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_4_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 95 [0x017C]: SD_EYE_STATUS_5
    SD_EYE_STATUS_5_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_5_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_5_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_5_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 96 [0x0180]: SD_EYE_STATUS_6
    SD_EYE_STATUS_6_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_6_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_6_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_6_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 97 [0x0184]: SD_EYE_STATUS_7
    SD_EYE_STATUS_7_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_7_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_7_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_7_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 98 [0x0188]: SD_EYE_STATUS_8
    SD_EYE_STATUS_8_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_8_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_8_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_8_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 99 [0x018C]: SD_EYE_STATUS_9
    SD_EYE_STATUS_9_REG_RD_STROBE_O           : out std_logic;
    SD_EYE_STATUS_9_REG_WR_STROBE_O           : out std_logic;
    SD_TAP_9_I                                : in  std_logic_vector(4 downto 0);     
    SD_EYE_9_I                                : in  std_logic_vector(19 downto 0);    

    -- Register 100 [0x0190]: SD_EYE_STATUS_10
    SD_EYE_STATUS_10_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_10_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_10_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_10_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 101 [0x0194]: SD_EYE_STATUS_11
    SD_EYE_STATUS_11_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_11_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_11_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_11_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 102 [0x0198]: SD_EYE_STATUS_12
    SD_EYE_STATUS_12_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_12_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_12_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_12_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 103 [0x019C]: SD_EYE_STATUS_13
    SD_EYE_STATUS_13_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_13_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_13_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_13_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 104 [0x01A0]: SD_EYE_STATUS_14
    SD_EYE_STATUS_14_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_14_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_14_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_14_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 105 [0x01A4]: SD_EYE_STATUS_15
    SD_EYE_STATUS_15_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_15_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_15_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_15_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 106 [0x01A8]: SD_EYE_STATUS_17
    SD_EYE_STATUS_17_REG_RD_STROBE_O          : out std_logic;
    SD_EYE_STATUS_17_REG_WR_STROBE_O          : out std_logic;
    SD_TAP_17_I                               : in  std_logic_vector(4 downto 0);     
    SD_EYE_17_I                               : in  std_logic_vector(19 downto 0);    

    -- Register 107 [0x01AC]: CRC32_REG_BANK
    CRC32_REG_BANK_REG_RD_STROBE_O            : out std_logic;
    CRC32_REG_BANK_REG_WR_STROBE_O            : out std_logic;
    CRC32_REG_BANK_O                          : out std_logic_vector(31 downto 0);    

    -- -------------------------------------------
    -- Ports of Axi Slave Bus Interface S00_AXI --
    -- -------------------------------------------
    s00_axi_aclk     : in std_logic;
    s00_axi_aresetn  : in std_logic;
    s00_axi_awaddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot   : in std_logic_vector(2 downto 0);
    s00_axi_awvalid  : in std_logic;
    s00_axi_awready  : out std_logic;
    s00_axi_wdata    : in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb    : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid   : in std_logic;
    s00_axi_wready   : out std_logic;
    s00_axi_bresp    : out std_logic_vector(1 downto 0);
    s00_axi_bvalid   : out std_logic;
    s00_axi_bready   : in std_logic;
    s00_axi_araddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot   : in std_logic_vector(2 downto 0);
    s00_axi_arvalid  : in std_logic;
    s00_axi_arready  : out std_logic;
    s00_axi_rdata    : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp    : out std_logic_vector(1 downto 0);
    s00_axi_rvalid   : out std_logic;
    s00_axi_rready   : in std_logic
  );
end axi_dcb_register_bank_v1_0;

architecture arch_imp of axi_dcb_register_bank_v1_0 is

  ---------------------------------------------------------------------------
  -- interface signal definitions
  ---------------------------------------------------------------------------
  signal user_to_ipif     : user_to_ipif_type;
  signal ipif_to_user     : ipif_to_user_type;

begin

  ------------------------------------------------------
  -- register definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  -- Register Bit <-> I/O Mapping

  -- Register 0 [0x0000]: HW_VER
  HW_VER_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_HW_VER);
  HW_VER_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_HW_VER);
  user_to_ipif.reg.data(C_REG_HW_VER)(31 downto 24)                    <= x"AC";
  user_to_ipif.reg.data(C_REG_HW_VER)(23 downto 16)                    <= x"01";
  user_to_ipif.reg.data(C_REG_HW_VER)(15 downto 8)                     <= x"03";
  user_to_ipif.reg.data(C_REG_HW_VER)(7 downto 2)                      <= "000001";
  user_to_ipif.reg.data(C_REG_HW_VER)(1 downto 0)                      <= "11";

  -- Register 1 [0x0004]: REG_LAYOUT_VER
  REG_LAYOUT_VER_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_REG_LAYOUT_VER);
  REG_LAYOUT_VER_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_REG_LAYOUT_VER);
  user_to_ipif.reg.data(C_REG_REG_LAYOUT_VER)(31 downto 16)            <= x"0000";
  user_to_ipif.reg.data(C_REG_REG_LAYOUT_VER)(15 downto 0)             <= x"0000";

  -- Register 2 [0x0008]: FW_BUILD_DATE
  FW_BUILD_DATE_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_FW_BUILD_DATE);
  FW_BUILD_DATE_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_FW_BUILD_DATE);
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(31 downto 16)             <= FW_BUILD_YEAR_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(15 downto 8)              <= FW_BUILD_MONTH_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(7 downto 0)               <= FW_BUILD_DAY_I;

  -- Register 3 [0x000C]: FW_BUILD_TIME
  FW_BUILD_TIME_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_FW_BUILD_TIME);
  FW_BUILD_TIME_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_FW_BUILD_TIME);
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(31 downto 24)             <= x"00";
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(23 downto 16)             <= FW_BUILD_HOUR_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(15 downto 8)              <= FW_BUILD_MINUTE_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(7 downto 0)               <= FW_BUILD_SECOND_I;

  -- Register 4 [0x0010]: SW_BUILD_DATE
  SW_BUILD_DATE_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SW_BUILD_DATE);
  SW_BUILD_DATE_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SW_BUILD_DATE);
  SW_BUILD_YEAR_O                                                      <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(31 downto 16);
  SW_BUILD_MONTH_O                                                     <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(15 downto 8);
  SW_BUILD_DAY_O                                                       <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(7 downto 0);

  -- Register 5 [0x0014]: SW_BUILD_TIME
  SW_BUILD_TIME_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SW_BUILD_TIME);
  SW_BUILD_TIME_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SW_BUILD_TIME);
  SW_BUILD_HOUR_O                                                      <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(23 downto 16);
  SW_BUILD_MINUTE_O                                                    <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(15 downto 8);
  SW_BUILD_SECOND_O                                                    <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(7 downto 0);

  -- Register 6 [0x0018]: FW_GIT_HASH_TAG
  FW_GIT_HASH_TAG_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_FW_GIT_HASH_TAG);
  FW_GIT_HASH_TAG_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_FW_GIT_HASH_TAG);
  user_to_ipif.reg.data(C_REG_FW_GIT_HASH_TAG)                         <= FW_GIT_HASH_TAG_I;

  -- Register 7 [0x001C]: SW_GIT_HASH_TAG
  SW_GIT_HASH_TAG_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SW_GIT_HASH_TAG);
  SW_GIT_HASH_TAG_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SW_GIT_HASH_TAG);
  SW_GIT_HASH_TAG_O                                                    <= ipif_to_user.reg.data(C_REG_SW_GIT_HASH_TAG);

  -- Register 8 [0x0020]: PROT_VER
  PROT_VER_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_PROT_VER);
  PROT_VER_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_PROT_VER);
  PROTOCOL_VERSION_O                                                   <= ipif_to_user.reg.data(C_REG_PROT_VER)(7 downto 0);

  -- Register 9 [0x0024]: SN
  SN_REG_RD_STROBE_O                                                   <= ipif_to_user.reg.rd_strobe(C_REG_SN);
  SN_REG_WR_STROBE_O                                                   <= ipif_to_user.reg.wr_strobe(C_REG_SN);
  SERIAL_NUMBER_O                                                      <= ipif_to_user.reg.data(C_REG_SN)(15 downto 0);

  -- Register 10 [0x0028]: STATUS
  STATUS_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_STATUS);
  STATUS_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_STATUS);
  user_to_ipif.reg.data(C_REG_STATUS)(5)                               <= FLASH_SEL_I;
  user_to_ipif.reg.data(C_REG_STATUS)(4)                               <= BOARD_SEL_I;
  user_to_ipif.reg.data(C_REG_STATUS)(2)                               <= SERIAL_BUSY_I;
  user_to_ipif.reg.data(C_REG_STATUS)(1)                               <= DCB_BUSY_I;
  user_to_ipif.reg.data(C_REG_STATUS)(0)                               <= SYS_BUSY_I;

  -- Register 11 [0x002C]: TEMP
  TEMP_REG_RD_STROBE_O                                                 <= ipif_to_user.reg.rd_strobe(C_REG_TEMP);
  TEMP_REG_WR_STROBE_O                                                 <= ipif_to_user.reg.wr_strobe(C_REG_TEMP);
  TEMPERATURE_O                                                        <= ipif_to_user.reg.data(C_REG_TEMP)(15 downto 0);

  -- Register 12 [0x0030]: PLL_LOCK
  PLL_LOCK_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_PLL_LOCK);
  PLL_LOCK_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_PLL_LOCK);
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(3)                             <= SERDES_CLK_MGR_LOCK_I;
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(2)                             <= WDB_CLK_MGR_LOCK_I;
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(1)                             <= SYS_DCM_LOCK_I;
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(0)                             <= LMK_PLL_LOCK_I;

  -- Register 13 [0x0034]: DCB_LOC
  DCB_LOC_REG_RD_STROBE_O                                              <= ipif_to_user.reg.rd_strobe(C_REG_DCB_LOC);
  DCB_LOC_REG_WR_STROBE_O                                              <= ipif_to_user.reg.wr_strobe(C_REG_DCB_LOC);
  CRATE_ID_O                                                           <= ipif_to_user.reg.data(C_REG_DCB_LOC)(23 downto 16);
  SLOT_ID_O                                                            <= ipif_to_user.reg.data(C_REG_DCB_LOC)(7 downto 0);

  -- Register 14 [0x0038]: CTRL
  CTRL_REG_RD_STROBE_O                                                 <= ipif_to_user.reg.rd_strobe(C_REG_CTRL);
  CTRL_REG_WR_STROBE_O                                                 <= ipif_to_user.reg.wr_strobe(C_REG_CTRL);
  SYNC_DELAY_O                                                         <= ipif_to_user.reg.data(C_REG_CTRL)(20 downto 16);
  DAQ_SOFT_TRIGGER_O                                                   <= ipif_to_user.reg.data(C_REG_CTRL)(5);
  DAQ_AUTO_O                                                           <= ipif_to_user.reg.data(C_REG_CTRL)(2);
  DAQ_NORMAL_O                                                         <= ipif_to_user.reg.data(C_REG_CTRL)(1);
  DAQ_SINGLE_O                                                         <= ipif_to_user.reg.data(C_REG_CTRL)(0);

  -- Register 15 [0x003C]: SET_CTRL
  SET_CTRL_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_SET_CTRL);
  SET_CTRL_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_SET_CTRL);
  SET_BIT_CTRL_O                                                       <= ipif_to_user.reg.data(C_REG_SET_CTRL);

  -- Register 16 [0x0040]: CLR_CTRL
  CLR_CTRL_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_CLR_CTRL);
  CLR_CTRL_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_CLR_CTRL);
  CLR_BIT_CTRL_O                                                       <= ipif_to_user.reg.data(C_REG_CLR_CTRL);

  -- Register 17 [0x0044]: CLK_CTRL
  CLK_CTRL_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_CLK_CTRL);
  CLK_CTRL_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_CLK_CTRL);
  DISTRIBUTOR_CLK_OUT_EN_O                                             <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(31 downto 12);
  DISTRIBUTOR_CLK_SRC_SEL_O                                            <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(3);
  BUS_CLK_SRC_SEL_O                                                    <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(2);
  LMK_CLK_SRC_SEL_O                                                    <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(1);
  EXT_CLK_IN_SEL_O                                                     <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(0);

  -- Register 18 [0x0048]: SET_CLK_CTRL
  SET_CLK_CTRL_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SET_CLK_CTRL);
  SET_CLK_CTRL_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SET_CLK_CTRL);
  SET_BIT_CLK_CTRL_O                                                   <= ipif_to_user.reg.data(C_REG_SET_CLK_CTRL);

  -- Register 19 [0x004C]: CLR_CLK_CTRL
  CLR_CLK_CTRL_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_CLR_CLK_CTRL);
  CLR_CLK_CTRL_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_CLR_CLK_CTRL);
  CLR_BIT_CLK_CTRL_O                                                   <= ipif_to_user.reg.data(C_REG_CLR_CLK_CTRL);

  -- Register 20 [0x0050]: COM_CTRL
  COM_CTRL_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_COM_CTRL);
  COM_CTRL_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_COM_CTRL);
  INTER_PKG_DELAY_O                                                    <= ipif_to_user.reg.data(C_REG_COM_CTRL)(23 downto 0);

  -- Register 21 [0x0054]: DPS_CTRL
  -- '--> Pure software register => no ports assigned

  -- Register 22 [0x0058]: RST
  RST_REG_RD_STROBE_O                                                  <= ipif_to_user.reg.rd_strobe(C_REG_RST);
  RST_REG_WR_STROBE_O                                                  <= ipif_to_user.reg.wr_strobe(C_REG_RST);
  ISERDES_RCVR_PACKET_COUNT_RST_O                                      <= ipif_to_user.reg.data(C_REG_RST)(9);
  WDB_SERDES_CLK_MGR_RST_O                                             <= ipif_to_user.reg.data(C_REG_RST)(8);
  WDB_REFCLK_MGR_RST_O                                                 <= ipif_to_user.reg.data(C_REG_RST)(7);
  TRIGGER_MGR_RST_O                                                    <= ipif_to_user.reg.data(C_REG_RST)(6);
  TR_SYNC_BPL_O                                                        <= ipif_to_user.reg.data(C_REG_RST)(5);
  LMK_SYNC_DCB_O                                                       <= ipif_to_user.reg.data(C_REG_RST)(4);
  ISERDES_RCVR_ERROR_COUNT_RST_O                                       <= ipif_to_user.reg.data(C_REG_RST)(3);
  ISERDES_RECEIVER_RESYNC_O                                            <= ipif_to_user.reg.data(C_REG_RST)(2);
  ISERDES_RECEIVER_RST_O                                               <= ipif_to_user.reg.data(C_REG_RST)(1);
  RECONFIGURE_FPGA_O                                                   <= ipif_to_user.reg.data(C_REG_RST)(0);

  -- Register 23 [0x005C]: SERDES_STATUS_00_07
  SERDES_STATUS_00_07_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_STATUS_00_07);
  SERDES_STATUS_00_07_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_STATUS_00_07);
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(31)                 <= IDLE_PATTERN_DETECT_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(30)                 <= BITSLIP_OK_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(29)                 <= DELAY_OK_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(28)                 <= SYNC_DONE_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(27)                 <= IDLE_PATTERN_DETECT_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(26)                 <= BITSLIP_OK_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(25)                 <= DELAY_OK_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(24)                 <= SYNC_DONE_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(23)                 <= IDLE_PATTERN_DETECT_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(22)                 <= BITSLIP_OK_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(21)                 <= DELAY_OK_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(20)                 <= SYNC_DONE_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(19)                 <= IDLE_PATTERN_DETECT_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(18)                 <= BITSLIP_OK_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(17)                 <= DELAY_OK_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(16)                 <= SYNC_DONE_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(15)                 <= IDLE_PATTERN_DETECT_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(14)                 <= BITSLIP_OK_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(13)                 <= DELAY_OK_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(12)                 <= SYNC_DONE_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(11)                 <= IDLE_PATTERN_DETECT_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(10)                 <= BITSLIP_OK_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(9)                  <= DELAY_OK_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(8)                  <= SYNC_DONE_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(7)                  <= IDLE_PATTERN_DETECT_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(6)                  <= BITSLIP_OK_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(5)                  <= DELAY_OK_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(4)                  <= SYNC_DONE_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(3)                  <= IDLE_PATTERN_DETECT_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(2)                  <= BITSLIP_OK_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(1)                  <= DELAY_OK_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_00_07)(0)                  <= SYNC_DONE_00_I;

  -- Register 24 [0x0060]: SERDES_STATUS_08_15
  SERDES_STATUS_08_15_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_STATUS_08_15);
  SERDES_STATUS_08_15_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_STATUS_08_15);
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(31)                 <= IDLE_PATTERN_DETECT_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(30)                 <= BITSLIP_OK_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(29)                 <= DELAY_OK_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(28)                 <= SYNC_DONE_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(27)                 <= IDLE_PATTERN_DETECT_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(26)                 <= BITSLIP_OK_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(25)                 <= DELAY_OK_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(24)                 <= SYNC_DONE_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(23)                 <= IDLE_PATTERN_DETECT_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(22)                 <= BITSLIP_OK_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(21)                 <= DELAY_OK_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(20)                 <= SYNC_DONE_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(19)                 <= IDLE_PATTERN_DETECT_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(18)                 <= BITSLIP_OK_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(17)                 <= DELAY_OK_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(16)                 <= SYNC_DONE_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(15)                 <= IDLE_PATTERN_DETECT_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(14)                 <= BITSLIP_OK_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(13)                 <= DELAY_OK_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(12)                 <= SYNC_DONE_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(11)                 <= IDLE_PATTERN_DETECT_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(10)                 <= BITSLIP_OK_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(9)                  <= DELAY_OK_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(8)                  <= SYNC_DONE_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(7)                  <= IDLE_PATTERN_DETECT_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(6)                  <= BITSLIP_OK_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(5)                  <= DELAY_OK_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(4)                  <= SYNC_DONE_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(3)                  <= IDLE_PATTERN_DETECT_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(2)                  <= BITSLIP_OK_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(1)                  <= DELAY_OK_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_08_15)(0)                  <= SYNC_DONE_08_I;

  -- Register 25 [0x0064]: SERDES_STATUS_17
  SERDES_STATUS_17_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_STATUS_17);
  SERDES_STATUS_17_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_STATUS_17);
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_17)(3)                     <= IDLE_PATTERN_DETECT_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_17)(2)                     <= BITSLIP_OK_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_17)(1)                     <= DELAY_OK_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_STATUS_17)(0)                     <= SYNC_DONE_17_I;

  -- Register 26 [0x0068]: SERDES_ERR_CNT_00
  SERDES_ERR_CNT_00_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_00);
  SERDES_ERR_CNT_00_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_00);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_00)(31 downto 24)         <= CRC_ERRORS_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_00)(23 downto 16)         <= FRAME_ERRORS_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_00)(15 downto 8)          <= DATAGRAM_ERRORS_00_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_00)(7 downto 0)           <= SYNC_ERRORS_00_I;

  -- Register 27 [0x006C]: SERDES_ERR_CNT_01
  SERDES_ERR_CNT_01_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_01);
  SERDES_ERR_CNT_01_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_01);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_01)(31 downto 24)         <= CRC_ERRORS_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_01)(23 downto 16)         <= FRAME_ERRORS_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_01)(15 downto 8)          <= DATAGRAM_ERRORS_01_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_01)(7 downto 0)           <= SYNC_ERRORS_01_I;

  -- Register 28 [0x0070]: SERDES_ERR_CNT_02
  SERDES_ERR_CNT_02_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_02);
  SERDES_ERR_CNT_02_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_02);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_02)(31 downto 24)         <= CRC_ERRORS_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_02)(23 downto 16)         <= FRAME_ERRORS_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_02)(15 downto 8)          <= DATAGRAM_ERRORS_02_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_02)(7 downto 0)           <= SYNC_ERRORS_02_I;

  -- Register 29 [0x0074]: SERDES_ERR_CNT_03
  SERDES_ERR_CNT_03_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_03);
  SERDES_ERR_CNT_03_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_03);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_03)(31 downto 24)         <= CRC_ERRORS_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_03)(23 downto 16)         <= FRAME_ERRORS_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_03)(15 downto 8)          <= DATAGRAM_ERRORS_03_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_03)(7 downto 0)           <= SYNC_ERRORS_03_I;

  -- Register 30 [0x0078]: SERDES_ERR_CNT_04
  SERDES_ERR_CNT_04_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_04);
  SERDES_ERR_CNT_04_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_04);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_04)(31 downto 24)         <= CRC_ERRORS_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_04)(23 downto 16)         <= FRAME_ERRORS_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_04)(15 downto 8)          <= DATAGRAM_ERRORS_04_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_04)(7 downto 0)           <= SYNC_ERRORS_04_I;

  -- Register 31 [0x007C]: SERDES_ERR_CNT_05
  SERDES_ERR_CNT_05_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_05);
  SERDES_ERR_CNT_05_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_05);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_05)(31 downto 24)         <= CRC_ERRORS_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_05)(23 downto 16)         <= FRAME_ERRORS_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_05)(15 downto 8)          <= DATAGRAM_ERRORS_05_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_05)(7 downto 0)           <= SYNC_ERRORS_05_I;

  -- Register 32 [0x0080]: SERDES_ERR_CNT_06
  SERDES_ERR_CNT_06_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_06);
  SERDES_ERR_CNT_06_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_06);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_06)(31 downto 24)         <= CRC_ERRORS_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_06)(23 downto 16)         <= FRAME_ERRORS_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_06)(15 downto 8)          <= DATAGRAM_ERRORS_06_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_06)(7 downto 0)           <= SYNC_ERRORS_06_I;

  -- Register 33 [0x0084]: SERDES_ERR_CNT_07
  SERDES_ERR_CNT_07_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_07);
  SERDES_ERR_CNT_07_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_07);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_07)(31 downto 24)         <= CRC_ERRORS_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_07)(23 downto 16)         <= FRAME_ERRORS_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_07)(15 downto 8)          <= DATAGRAM_ERRORS_07_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_07)(7 downto 0)           <= SYNC_ERRORS_07_I;

  -- Register 34 [0x0088]: SERDES_ERR_CNT_08
  SERDES_ERR_CNT_08_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_08);
  SERDES_ERR_CNT_08_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_08);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_08)(31 downto 24)         <= CRC_ERRORS_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_08)(23 downto 16)         <= FRAME_ERRORS_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_08)(15 downto 8)          <= DATAGRAM_ERRORS_08_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_08)(7 downto 0)           <= SYNC_ERRORS_08_I;

  -- Register 35 [0x008C]: SERDES_ERR_CNT_09
  SERDES_ERR_CNT_09_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_09);
  SERDES_ERR_CNT_09_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_09);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_09)(31 downto 24)         <= CRC_ERRORS_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_09)(23 downto 16)         <= FRAME_ERRORS_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_09)(15 downto 8)          <= DATAGRAM_ERRORS_09_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_09)(7 downto 0)           <= SYNC_ERRORS_09_I;

  -- Register 36 [0x0090]: SERDES_ERR_CNT_10
  SERDES_ERR_CNT_10_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_10);
  SERDES_ERR_CNT_10_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_10);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_10)(31 downto 24)         <= CRC_ERRORS_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_10)(23 downto 16)         <= FRAME_ERRORS_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_10)(15 downto 8)          <= DATAGRAM_ERRORS_10_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_10)(7 downto 0)           <= SYNC_ERRORS_10_I;

  -- Register 37 [0x0094]: SERDES_ERR_CNT_11
  SERDES_ERR_CNT_11_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_11);
  SERDES_ERR_CNT_11_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_11);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_11)(31 downto 24)         <= CRC_ERRORS_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_11)(23 downto 16)         <= FRAME_ERRORS_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_11)(15 downto 8)          <= DATAGRAM_ERRORS_11_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_11)(7 downto 0)           <= SYNC_ERRORS_11_I;

  -- Register 38 [0x0098]: SERDES_ERR_CNT_12
  SERDES_ERR_CNT_12_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_12);
  SERDES_ERR_CNT_12_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_12);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_12)(31 downto 24)         <= CRC_ERRORS_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_12)(23 downto 16)         <= FRAME_ERRORS_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_12)(15 downto 8)          <= DATAGRAM_ERRORS_12_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_12)(7 downto 0)           <= SYNC_ERRORS_12_I;

  -- Register 39 [0x009C]: SERDES_ERR_CNT_13
  SERDES_ERR_CNT_13_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_13);
  SERDES_ERR_CNT_13_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_13);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_13)(31 downto 24)         <= CRC_ERRORS_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_13)(23 downto 16)         <= FRAME_ERRORS_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_13)(15 downto 8)          <= DATAGRAM_ERRORS_13_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_13)(7 downto 0)           <= SYNC_ERRORS_13_I;

  -- Register 40 [0x00A0]: SERDES_ERR_CNT_14
  SERDES_ERR_CNT_14_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_14);
  SERDES_ERR_CNT_14_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_14);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_14)(31 downto 24)         <= CRC_ERRORS_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_14)(23 downto 16)         <= FRAME_ERRORS_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_14)(15 downto 8)          <= DATAGRAM_ERRORS_14_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_14)(7 downto 0)           <= SYNC_ERRORS_14_I;

  -- Register 41 [0x00A4]: SERDES_ERR_CNT_15
  SERDES_ERR_CNT_15_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_15);
  SERDES_ERR_CNT_15_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_15);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_15)(31 downto 24)         <= CRC_ERRORS_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_15)(23 downto 16)         <= FRAME_ERRORS_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_15)(15 downto 8)          <= DATAGRAM_ERRORS_15_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_15)(7 downto 0)           <= SYNC_ERRORS_15_I;

  -- Register 42 [0x00A8]: SERDES_ERR_CNT_17
  SERDES_ERR_CNT_17_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SERDES_ERR_CNT_17);
  SERDES_ERR_CNT_17_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SERDES_ERR_CNT_17);
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_17)(31 downto 24)         <= CRC_ERRORS_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_17)(23 downto 16)         <= FRAME_ERRORS_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_17)(15 downto 8)          <= DATAGRAM_ERRORS_17_I;
  user_to_ipif.reg.data(C_REG_SERDES_ERR_CNT_17)(7 downto 0)           <= SYNC_ERRORS_17_I;

  -- Register 43 [0x00AC]: APLY_CFG
  APLY_CFG_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_APLY_CFG);
  APLY_CFG_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_APLY_CFG);
  APPLY_SETTINGS_LMK_O                                                 <= ipif_to_user.reg.data(C_REG_APLY_CFG)(0);

  -- Register 44 [0x00B0]: LMK_0
  LMK_0_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_0);
  LMK_0_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_0);
  LMK0_RESET_O                                                         <= ipif_to_user.reg.data(C_REG_LMK_0)(31);
  LMK0_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_0)(18 downto 17);
  LMK0_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_0)(16);
  LMK0_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_0)(15 downto 8);
  LMK0_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_0)(7 downto 4);

  -- Register 45 [0x00B4]: LMK_1
  LMK_1_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_1);
  LMK_1_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_1);
  LMK1_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_1)(18 downto 17);
  LMK1_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_1)(16);
  LMK1_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_1)(15 downto 8);
  LMK1_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_1)(7 downto 4);

  -- Register 46 [0x00B8]: LMK_2
  LMK_2_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_2);
  LMK_2_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_2);
  LMK2_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_2)(18 downto 17);
  LMK2_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_2)(16);
  LMK2_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_2)(15 downto 8);
  LMK2_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_2)(7 downto 4);

  -- Register 47 [0x00BC]: LMK_3
  LMK_3_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_3);
  LMK_3_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_3);
  LMK3_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_3)(18 downto 17);
  LMK3_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_3)(16);
  LMK3_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_3)(15 downto 8);
  LMK3_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_3)(7 downto 4);

  -- Register 48 [0x00C0]: LMK_4
  LMK_4_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_4);
  LMK_4_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_4);
  LMK4_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_4)(18 downto 17);
  LMK4_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_4)(16);
  LMK4_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_4)(15 downto 8);
  LMK4_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_4)(7 downto 4);

  -- Register 49 [0x00C4]: LMK_5
  LMK_5_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_5);
  LMK_5_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_5);
  LMK5_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_5)(18 downto 17);
  LMK5_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_5)(16);
  LMK5_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_5)(15 downto 8);
  LMK5_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_5)(7 downto 4);

  -- Register 50 [0x00C8]: LMK_6
  LMK_6_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_6);
  LMK_6_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_6);
  LMK6_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_6)(18 downto 17);
  LMK6_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_6)(16);
  LMK6_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_6)(15 downto 8);
  LMK6_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_6)(7 downto 4);

  -- Register 51 [0x00CC]: LMK_7
  LMK_7_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_7);
  LMK_7_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_7);
  LMK7_CLKOUT_MUX_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_7)(18 downto 17);
  LMK7_CLKOUT_EN_O                                                     <= ipif_to_user.reg.data(C_REG_LMK_7)(16);
  LMK7_CLKOUT_DIV_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_7)(15 downto 8);
  LMK7_CLKOUT_DLY_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_7)(7 downto 4);

  -- Register 52 [0x00D0]: LMK_8
  LMK_8_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_8);
  LMK_8_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_8);
  LMK8_PHASE_NOISE_OPT_O                                               <= ipif_to_user.reg.data(C_REG_LMK_8)(31 downto 4);

  -- Register 53 [0x00D4]: LMK_9
  LMK_9_REG_RD_STROBE_O                                                <= ipif_to_user.reg.rd_strobe(C_REG_LMK_9);
  LMK_9_REG_WR_STROBE_O                                                <= ipif_to_user.reg.wr_strobe(C_REG_LMK_9);
  LMK9_VBOOST_O                                                        <= ipif_to_user.reg.data(C_REG_LMK_9)(16);

  -- Register 54 [0x00D8]: LMK_11
  LMK_11_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_LMK_11);
  LMK_11_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_LMK_11);
  LMK11_DIV4_O                                                         <= ipif_to_user.reg.data(C_REG_LMK_11)(15);

  -- Register 55 [0x00DC]: LMK_13
  LMK_13_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_LMK_13);
  LMK_13_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_LMK_13);
  LMK13_OSCIN_FREQ_O                                                   <= ipif_to_user.reg.data(C_REG_LMK_13)(21 downto 14);
  LMK13_VCO_R4_LF_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_13)(13 downto 11);
  LMK13_VCO_R3_LF_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_13)(10 downto 8);
  LMK13_VCO_C3_C4_LF_O                                                 <= ipif_to_user.reg.data(C_REG_LMK_13)(7 downto 4);

  -- Register 56 [0x00E0]: LMK_14
  LMK_14_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_LMK_14);
  LMK_14_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_LMK_14);
  LMK14_EN_FOUT_O                                                      <= ipif_to_user.reg.data(C_REG_LMK_14)(28);
  LMK14_EN_CLKOUT_GLOBAL_O                                             <= ipif_to_user.reg.data(C_REG_LMK_14)(27);
  LMK14_POWERDOWN_O                                                    <= ipif_to_user.reg.data(C_REG_LMK_14)(26);
  LMK14_PLL_MUX_O                                                      <= ipif_to_user.reg.data(C_REG_LMK_14)(23 downto 20);
  LMK14_PLL_R_O                                                        <= ipif_to_user.reg.data(C_REG_LMK_14)(19 downto 8);

  -- Register 57 [0x00E4]: LMK_15
  LMK_15_REG_RD_STROBE_O                                               <= ipif_to_user.reg.rd_strobe(C_REG_LMK_15);
  LMK_15_REG_WR_STROBE_O                                               <= ipif_to_user.reg.wr_strobe(C_REG_LMK_15);
  LMK15_PLL_CP_GAIN_O                                                  <= ipif_to_user.reg.data(C_REG_LMK_15)(31 downto 30);
  LMK15_VCO_DIV_O                                                      <= ipif_to_user.reg.data(C_REG_LMK_15)(29 downto 26);
  LMK15_PLL_N_O                                                        <= ipif_to_user.reg.data(C_REG_LMK_15)(25 downto 8);

  -- Register 58 [0x00E8]: TIME_LSB
  TIME_LSB_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_TIME_LSB);
  TIME_LSB_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_TIME_LSB);
  user_to_ipif.reg.data(C_REG_TIME_LSB)                                <= TIME_LSB_I;

  -- Register 59 [0x00EC]: TIME_MSB
  TIME_MSB_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_TIME_MSB);
  TIME_MSB_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_TIME_MSB);
  user_to_ipif.reg.data(C_REG_TIME_MSB)                                <= TIME_MSB_I;

  -- Register 60 [0x00F0]: TIME_LSB_SET
  TIME_LSB_SET_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_TIME_LSB_SET);
  TIME_LSB_SET_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_TIME_LSB_SET);
  TIME_LSB_SET_O                                                       <= ipif_to_user.reg.data(C_REG_TIME_LSB_SET);

  -- Register 61 [0x00F4]: TIME_MSB_SET
  TIME_MSB_SET_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_TIME_MSB_SET);
  TIME_MSB_SET_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_TIME_MSB_SET);
  TIME_MSB_SET_O                                                       <= ipif_to_user.reg.data(C_REG_TIME_MSB_SET);

  -- Register 62 [0x00F8]: EVENT_TX_RATE
  EVENT_TX_RATE_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_EVENT_TX_RATE);
  EVENT_TX_RATE_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_EVENT_TX_RATE);
  user_to_ipif.reg.data(C_REG_EVENT_TX_RATE)                           <= EVENT_TX_RATE_I;

  -- Register 63 [0x00FC]: EVENT_NR
  EVENT_NR_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_EVENT_NR);
  EVENT_NR_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_EVENT_NR);
  user_to_ipif.reg.data(C_REG_EVENT_NR)                                <= EVENT_NUMBER_I;

  -- Register 64 [0x0100]: TRG_CFG
  TRG_CFG_REG_RD_STROBE_O                                              <= ipif_to_user.reg.rd_strobe(C_REG_TRG_CFG);
  TRG_CFG_REG_WR_STROBE_O                                              <= ipif_to_user.reg.wr_strobe(C_REG_TRG_CFG);
  EXT_TRIGGER_OUT_ENABLE_O                                             <= ipif_to_user.reg.data(C_REG_TRG_CFG)(0);

  -- Register 65 [0x0104]: SET_TRG_CFG
  SET_TRG_CFG_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_SET_TRG_CFG);
  SET_TRG_CFG_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_SET_TRG_CFG);
  SET_BIT_TRG_CFG_O                                                    <= ipif_to_user.reg.data(C_REG_SET_TRG_CFG);

  -- Register 66 [0x0108]: CLR_TRG_CFG
  CLR_TRG_CFG_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_CLR_TRG_CFG);
  CLR_TRG_CFG_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_CLR_TRG_CFG);
  CLR_BIT_TRG_CFG_O                                                    <= ipif_to_user.reg.data(C_REG_CLR_TRG_CFG);

  -- Register 67 [0x010C]: TRG_AUTO_PERIOD
  TRG_AUTO_PERIOD_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_TRG_AUTO_PERIOD);
  TRG_AUTO_PERIOD_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_TRG_AUTO_PERIOD);
  AUTO_TRIGGER_PERIOD_O                                                <= ipif_to_user.reg.data(C_REG_TRG_AUTO_PERIOD);

  -- Register 68 [0x0110]: TRB_INFO_STAT
  TRB_INFO_STAT_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_STAT);
  TRB_INFO_STAT_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_STAT);
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(31)                       <= TRB_FLAG_NEW_I;
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(30)                       <= TRB_FLAG_PARITY_ERROR_I;
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(15 downto 0)              <= TRB_PARITY_ERROR_COUNT_I;

  -- Register 69 [0x0114]: TRB_INFO_LSB
  TRB_INFO_LSB_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_LSB);
  TRB_INFO_LSB_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_LSB);
  user_to_ipif.reg.data(C_REG_TRB_INFO_LSB)                            <= TRB_INFO_LSB_I;

  -- Register 70 [0x0118]: TRB_INFO_MSB
  TRB_INFO_MSB_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_MSB);
  TRB_INFO_MSB_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_MSB);
  user_to_ipif.reg.data(C_REG_TRB_INFO_MSB)(15 downto 0)               <= TRB_INFO_MSB_I;

  -- Register 71 [0x011C]: LMK_MOD_FLAG
  LMK_MOD_FLAG_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_LMK_MOD_FLAG);
  LMK_MOD_FLAG_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_LMK_MOD_FLAG);
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(7)                         <= LMK_7_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(6)                         <= LMK_6_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(5)                         <= LMK_5_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(4)                         <= LMK_4_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(3)                         <= LMK_3_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(2)                         <= LMK_2_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(1)                         <= LMK_1_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(0)                         <= LMK_0_MOD_I;

  -- Register 72 [0x0120]: CMB_MSCB_ADR
  -- '--> Pure software register => no ports assigned

  -- Register 73 [0x0124]: SD_PKT_CNT_0
  SD_PKT_CNT_0_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_0);
  SD_PKT_CNT_0_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_0);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_0)                            <= SD_PKT_CNT_0_I;

  -- Register 74 [0x0128]: SD_PKT_CNT_1
  SD_PKT_CNT_1_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_1);
  SD_PKT_CNT_1_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_1);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_1)                            <= SD_PKT_CNT_1_I;

  -- Register 75 [0x012C]: SD_PKT_CNT_2
  SD_PKT_CNT_2_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_2);
  SD_PKT_CNT_2_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_2);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_2)                            <= SD_PKT_CNT_2_I;

  -- Register 76 [0x0130]: SD_PKT_CNT_3
  SD_PKT_CNT_3_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_3);
  SD_PKT_CNT_3_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_3);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_3)                            <= SD_PKT_CNT_3_I;

  -- Register 77 [0x0134]: SD_PKT_CNT_4
  SD_PKT_CNT_4_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_4);
  SD_PKT_CNT_4_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_4);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_4)                            <= SD_PKT_CNT_4_I;

  -- Register 78 [0x0138]: SD_PKT_CNT_5
  SD_PKT_CNT_5_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_5);
  SD_PKT_CNT_5_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_5);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_5)                            <= SD_PKT_CNT_5_I;

  -- Register 79 [0x013C]: SD_PKT_CNT_6
  SD_PKT_CNT_6_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_6);
  SD_PKT_CNT_6_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_6);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_6)                            <= SD_PKT_CNT_6_I;

  -- Register 80 [0x0140]: SD_PKT_CNT_7
  SD_PKT_CNT_7_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_7);
  SD_PKT_CNT_7_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_7);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_7)                            <= SD_PKT_CNT_7_I;

  -- Register 81 [0x0144]: SD_PKT_CNT_8
  SD_PKT_CNT_8_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_8);
  SD_PKT_CNT_8_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_8);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_8)                            <= SD_PKT_CNT_8_I;

  -- Register 82 [0x0148]: SD_PKT_CNT_9
  SD_PKT_CNT_9_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_9);
  SD_PKT_CNT_9_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_9);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_9)                            <= SD_PKT_CNT_9_I;

  -- Register 83 [0x014C]: SD_PKT_CNT_10
  SD_PKT_CNT_10_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_10);
  SD_PKT_CNT_10_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_10);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_10)                           <= SD_PKT_CNT_10_I;

  -- Register 84 [0x0150]: SD_PKT_CNT_11
  SD_PKT_CNT_11_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_11);
  SD_PKT_CNT_11_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_11);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_11)                           <= SD_PKT_CNT_11_I;

  -- Register 85 [0x0154]: SD_PKT_CNT_12
  SD_PKT_CNT_12_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_12);
  SD_PKT_CNT_12_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_12);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_12)                           <= SD_PKT_CNT_12_I;

  -- Register 86 [0x0158]: SD_PKT_CNT_13
  SD_PKT_CNT_13_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_13);
  SD_PKT_CNT_13_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_13);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_13)                           <= SD_PKT_CNT_13_I;

  -- Register 87 [0x015C]: SD_PKT_CNT_14
  SD_PKT_CNT_14_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_14);
  SD_PKT_CNT_14_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_14);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_14)                           <= SD_PKT_CNT_14_I;

  -- Register 88 [0x0160]: SD_PKT_CNT_15
  SD_PKT_CNT_15_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_15);
  SD_PKT_CNT_15_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_15);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_15)                           <= SD_PKT_CNT_15_I;

  -- Register 89 [0x0164]: SD_PKT_CNT_17
  SD_PKT_CNT_17_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_SD_PKT_CNT_17);
  SD_PKT_CNT_17_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_SD_PKT_CNT_17);
  user_to_ipif.reg.data(C_REG_SD_PKT_CNT_17)                           <= SD_PKT_CNT_17_I;

  -- Register 90 [0x0168]: SD_EYE_STATUS_0
  SD_EYE_STATUS_0_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_0);
  SD_EYE_STATUS_0_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_0);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_0)(28 downto 24)           <= SD_TAP_0_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_0)(19 downto 0)            <= SD_EYE_0_I;

  -- Register 91 [0x016C]: SD_EYE_STATUS_1
  SD_EYE_STATUS_1_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_1);
  SD_EYE_STATUS_1_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_1);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_1)(28 downto 24)           <= SD_TAP_1_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_1)(19 downto 0)            <= SD_EYE_1_I;

  -- Register 92 [0x0170]: SD_EYE_STATUS_2
  SD_EYE_STATUS_2_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_2);
  SD_EYE_STATUS_2_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_2);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_2)(28 downto 24)           <= SD_TAP_2_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_2)(19 downto 0)            <= SD_EYE_2_I;

  -- Register 93 [0x0174]: SD_EYE_STATUS_3
  SD_EYE_STATUS_3_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_3);
  SD_EYE_STATUS_3_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_3);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_3)(28 downto 24)           <= SD_TAP_3_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_3)(19 downto 0)            <= SD_EYE_3_I;

  -- Register 94 [0x0178]: SD_EYE_STATUS_4
  SD_EYE_STATUS_4_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_4);
  SD_EYE_STATUS_4_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_4);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_4)(28 downto 24)           <= SD_TAP_4_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_4)(19 downto 0)            <= SD_EYE_4_I;

  -- Register 95 [0x017C]: SD_EYE_STATUS_5
  SD_EYE_STATUS_5_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_5);
  SD_EYE_STATUS_5_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_5);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_5)(28 downto 24)           <= SD_TAP_5_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_5)(19 downto 0)            <= SD_EYE_5_I;

  -- Register 96 [0x0180]: SD_EYE_STATUS_6
  SD_EYE_STATUS_6_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_6);
  SD_EYE_STATUS_6_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_6);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_6)(28 downto 24)           <= SD_TAP_6_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_6)(19 downto 0)            <= SD_EYE_6_I;

  -- Register 97 [0x0184]: SD_EYE_STATUS_7
  SD_EYE_STATUS_7_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_7);
  SD_EYE_STATUS_7_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_7);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_7)(28 downto 24)           <= SD_TAP_7_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_7)(19 downto 0)            <= SD_EYE_7_I;

  -- Register 98 [0x0188]: SD_EYE_STATUS_8
  SD_EYE_STATUS_8_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_8);
  SD_EYE_STATUS_8_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_8);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_8)(28 downto 24)           <= SD_TAP_8_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_8)(19 downto 0)            <= SD_EYE_8_I;

  -- Register 99 [0x018C]: SD_EYE_STATUS_9
  SD_EYE_STATUS_9_REG_RD_STROBE_O                                      <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_9);
  SD_EYE_STATUS_9_REG_WR_STROBE_O                                      <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_9);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_9)(28 downto 24)           <= SD_TAP_9_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_9)(19 downto 0)            <= SD_EYE_9_I;

  -- Register 100 [0x0190]: SD_EYE_STATUS_10
  SD_EYE_STATUS_10_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_10);
  SD_EYE_STATUS_10_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_10);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_10)(28 downto 24)          <= SD_TAP_10_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_10)(19 downto 0)           <= SD_EYE_10_I;

  -- Register 101 [0x0194]: SD_EYE_STATUS_11
  SD_EYE_STATUS_11_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_11);
  SD_EYE_STATUS_11_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_11);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_11)(28 downto 24)          <= SD_TAP_11_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_11)(19 downto 0)           <= SD_EYE_11_I;

  -- Register 102 [0x0198]: SD_EYE_STATUS_12
  SD_EYE_STATUS_12_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_12);
  SD_EYE_STATUS_12_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_12);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_12)(28 downto 24)          <= SD_TAP_12_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_12)(19 downto 0)           <= SD_EYE_12_I;

  -- Register 103 [0x019C]: SD_EYE_STATUS_13
  SD_EYE_STATUS_13_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_13);
  SD_EYE_STATUS_13_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_13);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_13)(28 downto 24)          <= SD_TAP_13_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_13)(19 downto 0)           <= SD_EYE_13_I;

  -- Register 104 [0x01A0]: SD_EYE_STATUS_14
  SD_EYE_STATUS_14_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_14);
  SD_EYE_STATUS_14_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_14);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_14)(28 downto 24)          <= SD_TAP_14_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_14)(19 downto 0)           <= SD_EYE_14_I;

  -- Register 105 [0x01A4]: SD_EYE_STATUS_15
  SD_EYE_STATUS_15_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_15);
  SD_EYE_STATUS_15_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_15);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_15)(28 downto 24)          <= SD_TAP_15_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_15)(19 downto 0)           <= SD_EYE_15_I;

  -- Register 106 [0x01A8]: SD_EYE_STATUS_17
  SD_EYE_STATUS_17_REG_RD_STROBE_O                                     <= ipif_to_user.reg.rd_strobe(C_REG_SD_EYE_STATUS_17);
  SD_EYE_STATUS_17_REG_WR_STROBE_O                                     <= ipif_to_user.reg.wr_strobe(C_REG_SD_EYE_STATUS_17);
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_17)(28 downto 24)          <= SD_TAP_17_I;
  user_to_ipif.reg.data(C_REG_SD_EYE_STATUS_17)(19 downto 0)           <= SD_EYE_17_I;

  -- Register 107 [0x01AC]: CRC32_REG_BANK
  CRC32_REG_BANK_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_CRC32_REG_BANK);
  CRC32_REG_BANK_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_CRC32_REG_BANK);
  CRC32_REG_BANK_O                                                     <= ipif_to_user.reg.data(C_REG_CRC32_REG_BANK);

--  ------------------------------------------------------
--  -- memory definition is done in ipif_user_cfg.vhdl
--  ------------------------------------------------------
--
--  user_to_ipif.mem.addr   <= core_mem_address;
--  user_to_ipif.mem.enable <= '1';
--  user_to_ipif.mem.wr_en  <= '0';
--  user_to_ipif.mem.data   <= (others => '0');

  ------------------------------------------------------------------------------
  -- instantiate AXI IP Interface
  ------------------------------------------------------------------------------

  ipif_axi_inst : entity axi_dcb_reg_bank_v1_0.ipif_axi
  generic map
  (
    -- Bus protocol parameters, do not add to or delete
    C_S_AXI_DATA_WIDTH  => C_S00_AXI_DATA_WIDTH,
    C_S_AXI_ADDR_WIDTH  => C_S00_AXI_ADDR_WIDTH,
    C_S_AXI_MIN_SIZE    => C_S00_AXI_MIN_SIZE,
    C_USE_WSTRB         => C_USE_WSTRB,
    C_BASEADDR          => C_S00_AXI_BASEADDR,
    C_HIGHADDR          => C_S00_AXI_HIGHADDR,
    C_SLV_AWIDTH        => C_S00_AXI_ADDR_WIDTH,
    C_SLV_DWIDTH        => C_S00_AXI_DATA_WIDTH,
    C_DPHASE_TIMEOUT    => C_DPHASE_TIMEOUT,
    C_FAMILY            => C_FAMILY
    --C_MEM0_BASEADDR     => C_MEM0_BASEADDR,
    --C_MEM0_HIGHADDR     => C_MEM0_HIGHADDR,
    --C_MEM1_BASEADDR     => C_MEM1_BASEADDR,
    --C_MEM1_HIGHADDR     => C_MEM1_HIGHADDR
  )
  port map
  (
    -- user ports for ip interface
    USER_TO_IPIF_I        => user_to_ipif,
    IPIF_TO_USER_O        => ipif_to_user,
    -- Bus protocol ports, do not add to or delete
    S_AXI_ACLK            => s00_axi_aclk,
    S_AXI_ARESETN         => s00_axi_aresetn,
    S_AXI_AWADDR          => s00_axi_awaddr,
    S_AXI_AWVALID         => s00_axi_awvalid,
    S_AXI_WDATA           => s00_axi_wdata,
    S_AXI_WSTRB           => s00_axi_wstrb,
    S_AXI_WVALID          => s00_axi_wvalid,
    S_AXI_BREADY          => s00_axi_bready,
    S_AXI_ARADDR          => s00_axi_araddr,
    S_AXI_ARVALID         => s00_axi_arvalid,
    S_AXI_RREADY          => s00_axi_rready,
    S_AXI_ARREADY         => s00_axi_arready,
    S_AXI_RDATA           => s00_axi_rdata,
    S_AXI_RRESP           => s00_axi_rresp,
    S_AXI_RVALID          => s00_axi_rvalid,
    S_AXI_WREADY          => s00_axi_wready,
    S_AXI_BRESP           => s00_axi_bresp,
    S_AXI_BVALID          => s00_axi_bvalid,
    S_AXI_AWREADY         => s00_axi_awready
  );

  -- Check if necessary:
  -- S_AXI_AWPROT  => s00_axi_awprot,
  -- S_AXI_ARPROT  => s00_axi_arprot,

end arch_imp;
