---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  ipif_user_cfg.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)
--  Created :  01.04.2022 10:50:56
--
--  Description :  Mapping of the register content of WaveDream2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package ipif_user_cfg is

  ---------------------------------------------------------------------------
  -- General Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_WIDTH         : integer := 32;
  constant  C_SLV_AWIDTH        : integer := 32;
  constant  C_SLV_DWIDTH        : integer := 32;

  ---------------------------------------------------------------------------
  -- Register Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_UNDEFINED     : std_logic_vector(C_REG_WIDTH-1 downto 0) := X"DEADBEEF";
  constant  C_DEFAULT_ZERO      : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_BIT_ALL        : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '1');
  constant  C_WR_BIT_NONE       : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_PULSE_NONE     : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');

  constant  C_WR_EXT_NO         : integer := 0;
  constant  C_WR_EXT_YES        : integer := 1;

  constant  C_REG_SELF          : integer := -1;
  constant  C_REGISTER_REG_READ : boolean := false;


  ---------------------------------------------------------------------------
  -- register description
  ---------------------------------------------------------------------------

  -- Define special register types, default is C_REG_RW, possible types:
  -- C_REG_RW  : Read / Write register
  -- C_REG_R_W : Seperate Read (from Input) and Write (to output)
  -- C_REG_RO  : Read Only register
  -- C_REG_WR  : WRite register,               reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_SET : SET bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_CLR : CLR bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- set as record (<reg_type>, <reg_num>, <default>, <writable mask>),
  --   e.g. C_REG_CTRL_SET => (C_BIT_SET, C_REG_CTRL)
  --   <reg_num> only used for C_BIT_SET and C_BIT_CLR, otherwise set to C_REG_SELF

  type  reg_type is (C_REG_RW, C_REG_R_W, C_REG_RO, C_REG_WR, C_BIT_SET, C_BIT_CLR);

  type  reg_descr_record is
        record
          typ     : reg_type;
          reg     : integer;
          def     : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_msk  : std_logic_vector(C_REG_WIDTH-1 downto 0);
          pls_msk : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_ext  : integer;
        end record;

  type  reg_descr_type is array (natural range <>) of reg_descr_record;

  constant  C_REG_DESCR_DEFAULT : reg_descr_type(0 to 1) :=
  (        --  type         set/clr num       default           writable mask    write pulse only   external overwrite
    others => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,   C_WR_EXT_NO)
  );

  ---------------------------------------------------------------------------
  -- User register definition
  ---------------------------------------------------------------------------


  -- register numbers
  constant C_REG_HW_VER              : integer :=   0;
  constant C_REG_REG_LAYOUT_VER      : integer :=   1;
  constant C_REG_FW_BUILD_DATE       : integer :=   2;
  constant C_REG_FW_BUILD_TIME       : integer :=   3;
  constant C_REG_SW_BUILD_DATE       : integer :=   4;
  constant C_REG_SW_BUILD_TIME       : integer :=   5;
  constant C_REG_FW_GIT_HASH_TAG     : integer :=   6;
  constant C_REG_SW_GIT_HASH_TAG     : integer :=   7;
  constant C_REG_PROT_VER            : integer :=   8;
  constant C_REG_SN                  : integer :=   9;
  constant C_REG_STATUS              : integer :=  10;
  constant C_REG_TEMP                : integer :=  11;
  constant C_REG_PLL_LOCK            : integer :=  12;
  constant C_REG_DCB_LOC             : integer :=  13;
  constant C_REG_CTRL                : integer :=  14;
  constant C_REG_SET_CTRL            : integer :=  15;
  constant C_REG_CLR_CTRL            : integer :=  16;
  constant C_REG_CLK_CTRL            : integer :=  17;
  constant C_REG_SET_CLK_CTRL        : integer :=  18;
  constant C_REG_CLR_CLK_CTRL        : integer :=  19;
  constant C_REG_COM_CTRL            : integer :=  20;
  constant C_REG_DPS_CTRL            : integer :=  21;
  constant C_REG_RST                 : integer :=  22;
  constant C_REG_SERDES_STATUS_00_07 : integer :=  23;
  constant C_REG_SERDES_STATUS_08_15 : integer :=  24;
  constant C_REG_SERDES_STATUS_17    : integer :=  25;
  constant C_REG_SERDES_ERR_CNT_00   : integer :=  26;
  constant C_REG_SERDES_ERR_CNT_01   : integer :=  27;
  constant C_REG_SERDES_ERR_CNT_02   : integer :=  28;
  constant C_REG_SERDES_ERR_CNT_03   : integer :=  29;
  constant C_REG_SERDES_ERR_CNT_04   : integer :=  30;
  constant C_REG_SERDES_ERR_CNT_05   : integer :=  31;
  constant C_REG_SERDES_ERR_CNT_06   : integer :=  32;
  constant C_REG_SERDES_ERR_CNT_07   : integer :=  33;
  constant C_REG_SERDES_ERR_CNT_08   : integer :=  34;
  constant C_REG_SERDES_ERR_CNT_09   : integer :=  35;
  constant C_REG_SERDES_ERR_CNT_10   : integer :=  36;
  constant C_REG_SERDES_ERR_CNT_11   : integer :=  37;
  constant C_REG_SERDES_ERR_CNT_12   : integer :=  38;
  constant C_REG_SERDES_ERR_CNT_13   : integer :=  39;
  constant C_REG_SERDES_ERR_CNT_14   : integer :=  40;
  constant C_REG_SERDES_ERR_CNT_15   : integer :=  41;
  constant C_REG_SERDES_ERR_CNT_17   : integer :=  42;
  constant C_REG_APLY_CFG            : integer :=  43;
  constant C_REG_LMK_0               : integer :=  44;
  constant C_REG_LMK_1               : integer :=  45;
  constant C_REG_LMK_2               : integer :=  46;
  constant C_REG_LMK_3               : integer :=  47;
  constant C_REG_LMK_4               : integer :=  48;
  constant C_REG_LMK_5               : integer :=  49;
  constant C_REG_LMK_6               : integer :=  50;
  constant C_REG_LMK_7               : integer :=  51;
  constant C_REG_LMK_8               : integer :=  52;
  constant C_REG_LMK_9               : integer :=  53;
  constant C_REG_LMK_11              : integer :=  54;
  constant C_REG_LMK_13              : integer :=  55;
  constant C_REG_LMK_14              : integer :=  56;
  constant C_REG_LMK_15              : integer :=  57;
  constant C_REG_TIME_LSB            : integer :=  58;
  constant C_REG_TIME_MSB            : integer :=  59;
  constant C_REG_TIME_LSB_SET        : integer :=  60;
  constant C_REG_TIME_MSB_SET        : integer :=  61;
  constant C_REG_EVENT_TX_RATE       : integer :=  62;
  constant C_REG_EVENT_NR            : integer :=  63;
  constant C_REG_TRG_CFG             : integer :=  64;
  constant C_REG_SET_TRG_CFG         : integer :=  65;
  constant C_REG_CLR_TRG_CFG         : integer :=  66;
  constant C_REG_TRG_AUTO_PERIOD     : integer :=  67;
  constant C_REG_TRB_INFO_STAT       : integer :=  68;
  constant C_REG_TRB_INFO_LSB        : integer :=  69;
  constant C_REG_TRB_INFO_MSB        : integer :=  70;
  constant C_REG_LMK_MOD_FLAG        : integer :=  71;
  constant C_REG_CMB_MSCB_ADR        : integer :=  72;
  constant C_REG_SD_PKT_CNT_0        : integer :=  73;
  constant C_REG_SD_PKT_CNT_1        : integer :=  74;
  constant C_REG_SD_PKT_CNT_2        : integer :=  75;
  constant C_REG_SD_PKT_CNT_3        : integer :=  76;
  constant C_REG_SD_PKT_CNT_4        : integer :=  77;
  constant C_REG_SD_PKT_CNT_5        : integer :=  78;
  constant C_REG_SD_PKT_CNT_6        : integer :=  79;
  constant C_REG_SD_PKT_CNT_7        : integer :=  80;
  constant C_REG_SD_PKT_CNT_8        : integer :=  81;
  constant C_REG_SD_PKT_CNT_9        : integer :=  82;
  constant C_REG_SD_PKT_CNT_10       : integer :=  83;
  constant C_REG_SD_PKT_CNT_11       : integer :=  84;
  constant C_REG_SD_PKT_CNT_12       : integer :=  85;
  constant C_REG_SD_PKT_CNT_13       : integer :=  86;
  constant C_REG_SD_PKT_CNT_14       : integer :=  87;
  constant C_REG_SD_PKT_CNT_15       : integer :=  88;
  constant C_REG_SD_PKT_CNT_17       : integer :=  89;
  constant C_REG_SD_EYE_STATUS_0     : integer :=  90;
  constant C_REG_SD_EYE_STATUS_1     : integer :=  91;
  constant C_REG_SD_EYE_STATUS_2     : integer :=  92;
  constant C_REG_SD_EYE_STATUS_3     : integer :=  93;
  constant C_REG_SD_EYE_STATUS_4     : integer :=  94;
  constant C_REG_SD_EYE_STATUS_5     : integer :=  95;
  constant C_REG_SD_EYE_STATUS_6     : integer :=  96;
  constant C_REG_SD_EYE_STATUS_7     : integer :=  97;
  constant C_REG_SD_EYE_STATUS_8     : integer :=  98;
  constant C_REG_SD_EYE_STATUS_9     : integer :=  99;
  constant C_REG_SD_EYE_STATUS_10    : integer := 100;
  constant C_REG_SD_EYE_STATUS_11    : integer := 101;
  constant C_REG_SD_EYE_STATUS_12    : integer := 102;
  constant C_REG_SD_EYE_STATUS_13    : integer := 103;
  constant C_REG_SD_EYE_STATUS_14    : integer := 104;
  constant C_REG_SD_EYE_STATUS_15    : integer := 105;
  constant C_REG_SD_EYE_STATUS_17    : integer := 106;
  constant C_REG_CRC32_REG_BANK      : integer := 107;

  constant C_NUM_REG                 : integer := 108;

  -- register description
  constant  C_REG_DESCR     : reg_descr_type(0 to C_NUM_REG-1) :=
  (                         --  type          set/clr num               default           writable mask    write pulse only   external write
    C_REG_HW_VER              => (C_REG_RO,     C_REG_SELF,               X"AC010307",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_REG_LAYOUT_VER      => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_FW_BUILD_DATE       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_FW_BUILD_TIME       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SW_BUILD_DATE       => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SW_BUILD_TIME       => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_FW_GIT_HASH_TAG     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SW_GIT_HASH_TAG     => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_PROT_VER            => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"000000FF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SN                  => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"0000FFFF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_STATUS              => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TEMP                => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"0000FFFF",     X"00000000",       C_WR_EXT_NO ),
    C_REG_PLL_LOCK            => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_DCB_LOC             => (C_REG_RW,     C_REG_SELF,               X"FFFFFFFF",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_CTRL                => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"001F0027",     X"00000020",       C_WR_EXT_YES),
    C_REG_SET_CTRL            => (C_BIT_SET,    C_REG_CTRL,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_CLR_CTRL            => (C_BIT_CLR,    C_REG_CTRL,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_CLK_CTRL            => (C_REG_RW,     C_REG_SELF,               X"FFFFC00E",      X"FFFFF00F",     X"00000000",       C_WR_EXT_YES),
    C_REG_SET_CLK_CTRL        => (C_BIT_SET,    C_REG_CLK_CTRL,           X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_CLR_CLK_CTRL        => (C_BIT_CLR,    C_REG_CLK_CTRL,           X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_COM_CTRL            => (C_REG_RW,     C_REG_SELF,               X"00000753",      X"00FFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_DPS_CTRL            => (C_REG_RO,     C_REG_SELF,               X"DEADBEEF",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_RST                 => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"000003FF",     X"000003FF",       C_WR_EXT_YES),
    C_REG_SERDES_STATUS_00_07 => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_STATUS_08_15 => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_STATUS_17    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_00   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_01   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_02   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_03   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_04   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_05   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_06   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_07   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_08   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_09   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_10   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_11   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_12   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_13   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_14   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_15   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SERDES_ERR_CNT_17   => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_APLY_CFG            => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"00000001",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_0               => (C_REG_RW,     C_REG_SELF,               X"00020100",      X"8007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_1               => (C_REG_RW,     C_REG_SELF,               X"00030101",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_2               => (C_REG_RW,     C_REG_SELF,               X"00020102",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_3               => (C_REG_RW,     C_REG_SELF,               X"00020103",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_4               => (C_REG_RW,     C_REG_SELF,               X"00000104",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_5               => (C_REG_RW,     C_REG_SELF,               X"00000105",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_6               => (C_REG_RW,     C_REG_SELF,               X"00000106",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_7               => (C_REG_RW,     C_REG_SELF,               X"00000107",      X"0007FFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_8               => (C_REG_RW,     C_REG_SELF,               X"10000908",      X"FFFFFFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_9               => (C_REG_RW,     C_REG_SELF,               X"A0022A09",      X"00010000",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_11              => (C_REG_RW,     C_REG_SELF,               X"0082000B",      X"00008000",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_13              => (C_REG_RW,     C_REG_SELF,               X"029400AD",      X"003FFFF0",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_14              => (C_REG_RW,     C_REG_SELF,               X"0830140E",      X"1CFFFF00",     X"00000000",       C_WR_EXT_YES),
    C_REG_LMK_15              => (C_REG_RW,     C_REG_SELF,               X"E000280F",      X"FFFFFF00",     X"00000000",       C_WR_EXT_YES),
    C_REG_TIME_LSB            => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TIME_MSB            => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TIME_LSB_SET        => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_TIME_MSB_SET        => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_EVENT_TX_RATE       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_EVENT_NR            => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TRG_CFG             => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"00000001",     X"00000001",       C_WR_EXT_YES),
    C_REG_SET_TRG_CFG         => (C_BIT_SET,    C_REG_TRG_CFG,            X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_CLR_TRG_CFG         => (C_BIT_CLR,    C_REG_TRG_CFG,            X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_TRG_AUTO_PERIOD     => (C_REG_RW,     C_REG_SELF,               X"04C4B400",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_YES),
    C_REG_TRB_INFO_STAT       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TRB_INFO_LSB        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_TRB_INFO_MSB        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_LMK_MOD_FLAG        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_CMB_MSCB_ADR        => (C_REG_RO,     C_REG_SELF,               X"DEADBEEF",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_0        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_1        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_2        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_3        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_4        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_5        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_6        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_7        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_8        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_9        => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_10       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_11       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_12       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_13       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_14       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_15       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_PKT_CNT_17       => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_0     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_1     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_2     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_3     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_4     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_5     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_6     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_7     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_8     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_9     => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_10    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_11    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_12    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_13    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_14    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_15    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_SD_EYE_STATUS_17    => (C_REG_RO,     C_REG_SELF,               X"00000000",      X"00000000",     X"00000000",       C_WR_EXT_NO ),
    C_REG_CRC32_REG_BANK      => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_NO ),
    others                    => (C_REG_RW,     C_REG_SELF,               X"00000000",      X"FFFFFFFF",     X"00000000",       C_WR_EXT_NO )
  );

  ---------------------------------------------------------------------------
  -- register user interface signals
  ---------------------------------------------------------------------------

  type  reg_array is array (natural range <>) of std_logic_vector(C_REG_WIDTH-1 downto 0);

  type  user_to_reg_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_ext           :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  type  reg_to_user_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
          rd_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  -------------------------------------------------------------------------------------------
  -- User memory definition
  -------------------------------------------------------------------------------------------

  constant  C_MEM_DEPTH                  : integer              := 9;
  constant  C_NUM_MEM                    : integer              := 0;


  type  user_to_mem_type is
        record
          enable  : std_logic;
          wr_en   : std_logic;
          addr    : std_logic_vector(8 downto 0);
          data    : std_logic_vector(31 downto 0);
        end record;


  type  mem_to_user_type is
        record
          data    : std_logic_vector(31 downto 0);
        end record;

  ---------------------------------------------------------------------------
  -- User interface signals to top level
  ---------------------------------------------------------------------------

  constant  C_NUM_CS : integer  := (1 + C_NUM_MEM);

  type  user_to_ipif_type is
        record
          reg    : user_to_reg_type;
          mem    : user_to_mem_type;
        end record;


  type  ipif_to_user_type is
        record
          rst    : std_logic;
          reg    : reg_to_user_type;
          mem    : mem_to_user_type;
        end record;


end;


--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body ipif_user_cfg is

end package body;
