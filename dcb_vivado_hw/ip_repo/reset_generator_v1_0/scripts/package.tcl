##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME reset_generator
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY psi_3205
set IP_DESCIRPTION "Mutli reset generator"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
set_vendor_short PSI
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/rst_gen.vhd \
  ../hdl/reset_generator.vhd \
}

#Relative Library Files
#add_lib_relative \
#  "../../../../library/" \
#  { \
#    VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
#  }

# ###############################################################
# # Driver Files
# ###############################################################
#
# #WARNING! Driver files are stored with the VHDL code. If they are modified,
# #... the modifications need to be made there. The local files are overwritten
# #... automatically during packaging.
#
# #Copy files
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.c ../drivers/psi_ms_daq_axi/src/psi_ms_daq.c
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.h ../drivers/psi_ms_daq_axi/src/psi_ms_daq.h
#
# #Package
# add_drivers_relative ../drivers/psi_ms_daq_axi { \
#   src/psi_ms_daq.c \
#   src/psi_ms_daq.h \
# }

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "General Configuration"

gui_create_parameter "CGN_INPUT_RESET_POLARITY" "Polarity of input reset (0 = active low, 1 = active high)"
gui_parameter_set_range 0 1
gui_add_parameter

gui_create_parameter "CGN_INPUT_CLK_PULSE_WIDTH" "Number of clocks for input pulse"
gui_parameter_set_range 1 32
gui_add_parameter

gui_create_parameter "CGN_NR_OF_BUS_RST" "Number of active high bus resets"
gui_parameter_set_range 1 16
gui_add_parameter

gui_create_parameter "CGN_NR_OF_BUS_RST_N" "Number of active low bus resets"
gui_parameter_set_range 1 16
gui_add_parameter

gui_create_parameter "CGN_NR_OF_PERIPHERAL_RST" "Number of active high peripheral resets"
gui_parameter_set_range 1 16
gui_add_parameter

gui_create_parameter "CGN_NR_OF_PERIPHERAL_RST_N" "Number of active low peripheral resets"
gui_parameter_set_range 1 16
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

for {set i 0} {$i < 16} {incr i} {
  set i02 [format "%02d" $i]
  add_port_enablement_condition "BUS\_RST\_$i02\_O" "\$CGN_NR_OF_BUS_RST > $i"
  add_port_enablement_condition "BUS\_RST\_$i02\_CLK\_I" "\$CGN_NR_OF_BUS_RST > $i"
  add_port_enablement_condition "BUS\_RST\_N\_$i02\_O" "\$CGN_NR_OF_BUS_RST_N > $i"
  add_port_enablement_condition "BUS\_RST\_N\_$i02\_CLK\_I" "\$CGN_NR_OF_BUS_RST_N > $i"
  add_port_enablement_condition "PERIPHERAL\_RST\_$i02\_O" "\$CGN_NR_OF_PERIPHERAL_RST > $i"
  add_port_enablement_condition "PERIPHERAL\_RST\_$i02\_CLK\_I" "\$CGN_NR_OF_PERIPHERAL_RST > $i"
  add_port_enablement_condition "PERIPHERAL\_RST\_N\_$i02\_O" "\$CGN_NR_OF_PERIPHERAL_RST_N > $i"
  add_port_enablement_condition "PERIPHERAL\_RST\_N\_$i02\_CLK\_I" "\$CGN_NR_OF_PERIPHERAL_RST_N > $i"
}

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
