---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Reset generator
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.04.2019 09:07:56
--
--  Description :  Generates resets triggered by an asynchronous reset input. The
--                 output resets are synchronized to the corresponding clock domain.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

Library work;

--- vvv Start of Testbench Generator Configuration vvv ---
--- See TbGenerator documentation for info on global
--- settings like processes, testcases, dutlib and tbpkg
--- ^^^  End of Testbench Generator Configuration  ^^^ ---

entity reset_generator is
  generic (
    CGN_INPUT_RESET_POLARITY   : integer := 1;                -- 0 = active low, 1 = active high $$ constant=1 $$
    CGN_INPUT_CLK_PULSE_WIDTH  : integer := 4;                -- $$ constant=4 $$
    CGN_NR_OF_BUS_RST          : positive range 1 to 16 := 1; -- $$ constant=1 $$
    CGN_NR_OF_BUS_RST_N        : positive range 1 to 16 := 1; -- $$ constant=1 $$
    CGN_NR_OF_PERIPHERAL_RST   : positive range 1 to 16 := 1; -- $$ constant=1 $$
    CGN_NR_OF_PERIPHERAL_RST_N : positive range 1 to 16 := 1  -- $$ constant=1 $$
  );
  port (
    RST_I                     : in  std_logic;
    REF_CLK_I                 : in  std_logic; -- $$ type=clk; freq=50e6 $$
    -- Reset Ports
    BUS_RST_00_O              : out std_logic;
    BUS_RST_00_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_00_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_00_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_00_O       : out std_logic;
    PERIPHERAL_RST_00_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_00_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_00_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_01_O              : out std_logic;
    BUS_RST_01_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_01_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_01_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_01_O       : out std_logic;
    PERIPHERAL_RST_01_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_01_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_01_CLK_I : in  std_logic;  -- $$ type=clk; freq=150e6 $$

    BUS_RST_02_O              : out std_logic;
    BUS_RST_02_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_02_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_02_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_02_O       : out std_logic;
    PERIPHERAL_RST_02_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_02_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_02_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_03_O              : out std_logic;
    BUS_RST_03_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_03_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_03_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_03_O       : out std_logic;
    PERIPHERAL_RST_03_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_03_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_03_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_04_O              : out std_logic;
    BUS_RST_04_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_04_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_04_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_04_O       : out std_logic;
    PERIPHERAL_RST_04_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_04_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_04_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_05_O              : out std_logic;
    BUS_RST_05_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_05_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_05_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_05_O       : out std_logic;
    PERIPHERAL_RST_05_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_05_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_05_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_06_O              : out std_logic;
    BUS_RST_06_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_06_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_06_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_06_O       : out std_logic;
    PERIPHERAL_RST_06_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_06_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_06_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_07_O              : out std_logic;
    BUS_RST_07_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_07_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_07_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_07_O       : out std_logic;
    PERIPHERAL_RST_07_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_07_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_07_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_08_O              : out std_logic;
    BUS_RST_08_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_08_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_08_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_08_O       : out std_logic;
    PERIPHERAL_RST_08_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_08_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_08_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_09_O              : out std_logic;
    BUS_RST_09_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_09_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_09_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_09_O       : out std_logic;
    PERIPHERAL_RST_09_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_09_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_09_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_10_O              : out std_logic;
    BUS_RST_10_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_10_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_10_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_10_O       : out std_logic;
    PERIPHERAL_RST_10_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_10_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_10_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_11_O              : out std_logic;
    BUS_RST_11_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_11_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_11_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_11_O       : out std_logic;
    PERIPHERAL_RST_11_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_11_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_11_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_12_O              : out std_logic;
    BUS_RST_12_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_12_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_12_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_12_O       : out std_logic;
    PERIPHERAL_RST_12_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_12_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_12_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_13_O              : out std_logic;
    BUS_RST_13_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_13_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_13_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_13_O       : out std_logic;
    PERIPHERAL_RST_13_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_13_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_13_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_14_O              : out std_logic;
    BUS_RST_14_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_14_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_14_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_14_O       : out std_logic;
    PERIPHERAL_RST_14_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_14_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_14_CLK_I : in  std_logic; -- $$ type=clk; freq=150e6 $$

    BUS_RST_15_O              : out std_logic;
    BUS_RST_15_CLK_I          : in  std_logic; -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_15_O            : out std_logic; -- $$ lowactive=true $$
    BUS_RST_N_15_CLK_I        : in  std_logic; -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_15_O       : out std_logic;
    PERIPHERAL_RST_15_CLK_I   : in  std_logic; -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_15_O     : out std_logic; -- $$ lowactive=true $$
    PERIPHERAL_RST_N_15_CLK_I : in  std_logic  -- $$ type=clk; freq=150e6 $$
  );
end reset_generator;

architecture behavioral of reset_generator is

  signal all_bus_rst              : std_logic_vector(15 downto 0) := (others=>'0');
  signal all_bus_rst_clk          : std_logic_vector(15 downto 0) := (others=>'0');
  signal all_bus_rst_n            : std_logic_vector(15 downto 0) := (others=>'1');
  signal all_bus_rst_n_clk        : std_logic_vector(15 downto 0) := (others=>'0');
  signal all_peripheral_rst       : std_logic_vector(15 downto 0) := (others=>'0');
  signal all_peripheral_rst_clk   : std_logic_vector(15 downto 0) := (others=>'0');
  signal all_peripheral_rst_n     : std_logic_vector(15 downto 0) := (others=>'1');
  signal all_peripheral_rst_n_clk : std_logic_vector(15 downto 0) := (others=>'0');

  signal bus_rst                  : std_logic_vector(CGN_NR_OF_BUS_RST-1 downto 0);
  signal bus_rst_clk              : std_logic_vector(CGN_NR_OF_BUS_RST-1 downto 0);
  signal bus_rst_n                : std_logic_vector(CGN_NR_OF_BUS_RST_N-1 downto 0);
  signal bus_rst_n_clk            : std_logic_vector(CGN_NR_OF_BUS_RST_N-1 downto 0);
  signal peripheral_rst           : std_logic_vector(CGN_NR_OF_PERIPHERAL_RST-1 downto 0);
  signal peripheral_rst_clk       : std_logic_vector(CGN_NR_OF_PERIPHERAL_RST-1 downto 0);
  signal peripheral_rst_n         : std_logic_vector(CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0);
  signal peripheral_rst_n_clk     : std_logic_vector(CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0);

begin

  BUS_RST_00_O                <= all_bus_rst(0);
  all_bus_rst_clk(0)          <= BUS_RST_00_CLK_I;
  BUS_RST_N_00_O              <= all_bus_rst_n(0);
  all_bus_rst_n_clk(0)        <= BUS_RST_N_00_CLK_I;
  PERIPHERAL_RST_00_O         <= all_peripheral_rst(0);
  all_peripheral_rst_clk(0)   <= PERIPHERAL_RST_00_CLK_I;
  PERIPHERAL_RST_N_00_O       <= all_peripheral_rst_n(0);
  all_peripheral_rst_n_clk(0) <= PERIPHERAL_RST_N_00_CLK_I;

  BUS_RST_01_O                <= all_bus_rst(1);
  all_bus_rst_clk(1)          <= BUS_RST_01_CLK_I;
  BUS_RST_N_01_O              <= all_bus_rst_n(1);
  all_bus_rst_n_clk(1)        <= BUS_RST_N_01_CLK_I;
  PERIPHERAL_RST_01_O         <= all_peripheral_rst(1);
  all_peripheral_rst_clk(1)   <= PERIPHERAL_RST_01_CLK_I;
  PERIPHERAL_RST_N_01_O       <= all_peripheral_rst_n(1);
  all_peripheral_rst_n_clk(1) <= PERIPHERAL_RST_N_01_CLK_I;

  BUS_RST_02_O                <= all_bus_rst(2);
  all_bus_rst_clk(2)          <= BUS_RST_02_CLK_I;
  BUS_RST_N_02_O              <= all_bus_rst_n(2);
  all_bus_rst_n_clk(2)        <= BUS_RST_N_02_CLK_I;
  PERIPHERAL_RST_02_O         <= all_peripheral_rst(2);
  all_peripheral_rst_clk(2)   <= PERIPHERAL_RST_02_CLK_I;
  PERIPHERAL_RST_N_02_O       <= all_peripheral_rst_n(2);
  all_peripheral_rst_n_clk(2) <= PERIPHERAL_RST_N_02_CLK_I;

  BUS_RST_03_O                <= all_bus_rst(3);
  all_bus_rst_clk(3)          <= BUS_RST_03_CLK_I;
  BUS_RST_N_03_O              <= all_bus_rst_n(3);
  all_bus_rst_n_clk(3)        <= BUS_RST_N_03_CLK_I;
  PERIPHERAL_RST_03_O         <= all_peripheral_rst(3);
  all_peripheral_rst_clk(3)   <= PERIPHERAL_RST_03_CLK_I;
  PERIPHERAL_RST_N_03_O       <= all_peripheral_rst_n(3);
  all_peripheral_rst_n_clk(3) <= PERIPHERAL_RST_N_03_CLK_I;

  BUS_RST_04_O                <= all_bus_rst(4);
  all_bus_rst_clk(4)          <= BUS_RST_04_CLK_I;
  BUS_RST_N_04_O              <= all_bus_rst_n(4);
  all_bus_rst_n_clk(4)        <= BUS_RST_N_04_CLK_I;
  PERIPHERAL_RST_04_O         <= all_peripheral_rst(4);
  all_peripheral_rst_clk(4)   <= PERIPHERAL_RST_04_CLK_I;
  PERIPHERAL_RST_N_04_O       <= all_peripheral_rst_n(4);
  all_peripheral_rst_n_clk(4) <= PERIPHERAL_RST_N_04_CLK_I;

  BUS_RST_05_O                <= all_bus_rst(5);
  all_bus_rst_clk(5)          <= BUS_RST_05_CLK_I;
  BUS_RST_N_05_O              <= all_bus_rst_n(5);
  all_bus_rst_n_clk(5)        <= BUS_RST_N_05_CLK_I;
  PERIPHERAL_RST_05_O         <= all_peripheral_rst(5);
  all_peripheral_rst_clk(5)   <= PERIPHERAL_RST_05_CLK_I;
  PERIPHERAL_RST_N_05_O       <= all_peripheral_rst_n(5);
  all_peripheral_rst_n_clk(5) <= PERIPHERAL_RST_N_05_CLK_I;

  BUS_RST_06_O                <= all_bus_rst(6);
  all_bus_rst_clk(6)          <= BUS_RST_06_CLK_I;
  BUS_RST_N_06_O              <= all_bus_rst_n(6);
  all_bus_rst_n_clk(6)        <= BUS_RST_N_06_CLK_I;
  PERIPHERAL_RST_06_O         <= all_peripheral_rst(6);
  all_peripheral_rst_clk(6)   <= PERIPHERAL_RST_06_CLK_I;
  PERIPHERAL_RST_N_06_O       <= all_peripheral_rst_n(6);
  all_peripheral_rst_n_clk(6) <= PERIPHERAL_RST_N_06_CLK_I;

  BUS_RST_07_O                <= all_bus_rst(7);
  all_bus_rst_clk(7)          <= BUS_RST_07_CLK_I;
  BUS_RST_N_07_O              <= all_bus_rst_n(7);
  all_bus_rst_n_clk(7)        <= BUS_RST_N_07_CLK_I;
  PERIPHERAL_RST_07_O         <= all_peripheral_rst(7);
  all_peripheral_rst_clk(7)   <= PERIPHERAL_RST_07_CLK_I;
  PERIPHERAL_RST_N_07_O       <= all_peripheral_rst_n(7);
  all_peripheral_rst_n_clk(7) <= PERIPHERAL_RST_N_07_CLK_I;

  BUS_RST_08_O                <= all_bus_rst(8);
  all_bus_rst_clk(8)          <= BUS_RST_08_CLK_I;
  BUS_RST_N_08_O              <= all_bus_rst_n(8);
  all_bus_rst_n_clk(8)        <= BUS_RST_N_08_CLK_I;
  PERIPHERAL_RST_08_O         <= all_peripheral_rst(8);
  all_peripheral_rst_clk(8)   <= PERIPHERAL_RST_08_CLK_I;
  PERIPHERAL_RST_N_08_O       <= all_peripheral_rst_n(8);
  all_peripheral_rst_n_clk(8) <= PERIPHERAL_RST_N_08_CLK_I;

  BUS_RST_09_O                <= all_bus_rst(9);
  all_bus_rst_clk(9)          <= BUS_RST_09_CLK_I;
  BUS_RST_N_09_O              <= all_bus_rst_n(9);
  all_bus_rst_n_clk(9)        <= BUS_RST_N_09_CLK_I;
  PERIPHERAL_RST_09_O         <= all_peripheral_rst(9);
  all_peripheral_rst_clk(9)   <= PERIPHERAL_RST_09_CLK_I;
  PERIPHERAL_RST_N_09_O       <= all_peripheral_rst_n(9);
  all_peripheral_rst_n_clk(9) <= PERIPHERAL_RST_N_09_CLK_I;

  BUS_RST_10_O                 <= all_bus_rst(10);
  all_bus_rst_clk(10)          <= BUS_RST_10_CLK_I;
  BUS_RST_N_10_O               <= all_bus_rst_n(10);
  all_bus_rst_n_clk(10)        <= BUS_RST_N_10_CLK_I;
  PERIPHERAL_RST_10_O          <= all_peripheral_rst(10);
  all_peripheral_rst_clk(10)   <= PERIPHERAL_RST_10_CLK_I;
  PERIPHERAL_RST_N_10_O        <= all_peripheral_rst_n(10);
  all_peripheral_rst_n_clk(10) <= PERIPHERAL_RST_N_10_CLK_I;

  BUS_RST_11_O                 <= all_bus_rst(11);
  all_bus_rst_clk(11)          <= BUS_RST_11_CLK_I;
  BUS_RST_N_11_O               <= all_bus_rst_n(11);
  all_bus_rst_n_clk(11)        <= BUS_RST_N_11_CLK_I;
  PERIPHERAL_RST_11_O          <= all_peripheral_rst(11);
  all_peripheral_rst_clk(11)   <= PERIPHERAL_RST_11_CLK_I;
  PERIPHERAL_RST_N_11_O        <= all_peripheral_rst_n(11);
  all_peripheral_rst_n_clk(11) <= PERIPHERAL_RST_N_11_CLK_I;

  BUS_RST_12_O                 <= all_bus_rst(12);
  all_bus_rst_clk(12)          <= BUS_RST_12_CLK_I;
  BUS_RST_N_12_O               <= all_bus_rst_n(12);
  all_bus_rst_n_clk(12)        <= BUS_RST_N_12_CLK_I;
  PERIPHERAL_RST_12_O          <= all_peripheral_rst(12);
  all_peripheral_rst_clk(12)   <= PERIPHERAL_RST_12_CLK_I;
  PERIPHERAL_RST_N_12_O        <= all_peripheral_rst_n(12);
  all_peripheral_rst_n_clk(12) <= PERIPHERAL_RST_N_12_CLK_I;

  BUS_RST_13_O                 <= all_bus_rst(13);
  all_bus_rst_clk(13)          <= BUS_RST_13_CLK_I;
  BUS_RST_N_13_O               <= all_bus_rst_n(13);
  all_bus_rst_n_clk(13)        <= BUS_RST_N_13_CLK_I;
  PERIPHERAL_RST_13_O          <= all_peripheral_rst(13);
  all_peripheral_rst_clk(13)   <= PERIPHERAL_RST_13_CLK_I;
  PERIPHERAL_RST_N_13_O        <= all_peripheral_rst_n(13);
  all_peripheral_rst_n_clk(13) <= PERIPHERAL_RST_N_13_CLK_I;

  BUS_RST_14_O                 <= all_bus_rst(14);
  all_bus_rst_clk(14)          <= BUS_RST_14_CLK_I;
  BUS_RST_N_14_O               <= all_bus_rst_n(14);
  all_bus_rst_n_clk(14)        <= BUS_RST_N_14_CLK_I;
  PERIPHERAL_RST_14_O          <= all_peripheral_rst(14);
  all_peripheral_rst_clk(14)   <= PERIPHERAL_RST_14_CLK_I;
  PERIPHERAL_RST_N_14_O        <= all_peripheral_rst_n(14);
  all_peripheral_rst_n_clk(14) <= PERIPHERAL_RST_N_14_CLK_I;

  BUS_RST_15_O                 <= all_bus_rst(15);
  all_bus_rst_clk(15)          <= BUS_RST_15_CLK_I;
  BUS_RST_N_15_O               <= all_bus_rst_n(15);
  all_bus_rst_n_clk(15)        <= BUS_RST_N_15_CLK_I;
  PERIPHERAL_RST_15_O          <= all_peripheral_rst(15);
  all_peripheral_rst_clk(15)   <= PERIPHERAL_RST_15_CLK_I;
  PERIPHERAL_RST_N_15_O        <= all_peripheral_rst_n(15);
  all_peripheral_rst_n_clk(15) <= PERIPHERAL_RST_N_15_CLK_I;

  g_bus_rst_assign : for i in 0 to CGN_NR_OF_BUS_RST-1 generate
    all_bus_rst(i) <= bus_rst(i);
    bus_rst_clk(i) <= all_bus_rst_clk(i);
  end generate;

  g_bus_rst_n_assign : for i in 0 to CGN_NR_OF_BUS_RST_N-1 generate
    all_bus_rst_n(i) <= bus_rst_n(i);
    bus_rst_n_clk(i) <= all_bus_rst_n_clk(i);
  end generate;

  g_peripheral_rst_assign : for i in 0 to CGN_NR_OF_PERIPHERAL_RST-1 generate
    all_peripheral_rst(i) <= peripheral_rst(i);
    peripheral_rst_clk(i) <= all_peripheral_rst_clk(i);
  end generate;

  g_peripheral_rst_n_assign : for i in 0 to CGN_NR_OF_PERIPHERAL_RST_N-1 generate
    all_peripheral_rst_n(i) <= peripheral_rst_n(i);
    peripheral_rst_n_clk(i) <= all_peripheral_rst_n_clk(i);
  end generate;

  i_impl : entity work.rst_gen
  generic map(
    CGN_INPUT_RESET_POLARITY   => CGN_INPUT_RESET_POLARITY,
    CGN_INPUT_CLK_PULSE_WIDTH  => CGN_INPUT_CLK_PULSE_WIDTH,
    CGN_NR_OF_BUS_RST          => CGN_NR_OF_BUS_RST,
    CGN_NR_OF_BUS_RST_N        => CGN_NR_OF_BUS_RST_N,
    CGN_NR_OF_PERIPHERAL_RST   => CGN_NR_OF_PERIPHERAL_RST,
    CGN_NR_OF_PERIPHERAL_RST_N => CGN_NR_OF_PERIPHERAL_RST_N
  )
  port map(
    RST_I                  => RST_I,
    REF_CLK_I              => REF_CLK_I,
    BUS_RST_O              => bus_rst,
    BUS_RST_CLK_I          => bus_rst_clk,
    BUS_RST_N_O            => bus_rst_n,
    BUS_RST_N_CLK_I        => bus_rst_n_clk,
    PERIPHERAL_RST_O       => peripheral_rst,
    PERIPHERAL_RST_CLK_I   => peripheral_rst_clk,
    PERIPHERAL_RST_N_O     => peripheral_rst_n,
    PERIPHERAL_RST_N_CLK_I => peripheral_rst_n_clk
  );

end architecture behavioral;