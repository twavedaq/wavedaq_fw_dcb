#Load dependencies
#source ../../../TCL/PsiSim/PsiSim.tcl
source ~/EE_Projects/psi_fpga_all/TCL/PsiSim/PsiSim.tcl

#Initialize Simulation
psi::sim::init

#Configure
source ./config.tcl

#Run Simulation
puts "------------------------------"
puts "-- Compile"
puts "------------------------------"
psi::sim::compile -all -clean
puts "------------------------------"
puts "-- Run"
puts "------------------------------"
psi::sim::run_tb -all
puts "------------------------------"
puts "-- Check"
puts "------------------------------"

psi::sim::run_check_errors "###ERROR###"
