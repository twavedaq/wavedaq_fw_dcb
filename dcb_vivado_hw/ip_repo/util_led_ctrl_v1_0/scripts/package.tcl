##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME util_led_ctrl
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY PSI
set IP_DESCIRPTION "LED controler"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
set_vendor_short PSI
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../src/led_ctrl.vhd \
}

#Relative Library Files
add_lib_relative \
  "../../../../library/" \
{ \
  VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_math_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_logic_pkg.vhd \
  VHDL/psi_common/hdl/psi_common_bit_cc.vhd \
}

# ###############################################################
# # Driver Files
# ###############################################################
#
# #WARNING! Driver files are stored with the VHDL code. If they are modified,
# #... the modifications need to be made there. The local files are overwritten
# #... automatically during packaging.
#
# #Copy files
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.c ../drivers/psi_ms_daq_axi/src/psi_ms_daq.c
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.h ../drivers/psi_ms_daq_axi/src/psi_ms_daq.h
#
# #Package
# add_drivers_relative ../drivers/psi_ms_daq_axi { \
#   src/psi_ms_daq.c \
#   src/psi_ms_daq.h \
# }

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "General Configuration"

gui_create_parameter "CGN_ACTIVE_HIGH_HW_ERRORS" "Number of active high hardware error input signals"
gui_parameter_set_range 1 32
gui_add_parameter

gui_create_parameter "CGN_ACTIVE_LOW_HW_ERRORS" "Number of active low hardware error input signals"
gui_parameter_set_range 1 32
gui_add_parameter

gui_create_parameter "CGN_CLK_PERIOD" "Clock period in ps"
gui_parameter_set_range 1 1000000
gui_add_parameter

gui_create_parameter "CGN_MIN_STEADY_TIME" "Minimum hold time for an LED state in ns"
gui_parameter_set_range 1 10000000000
gui_add_parameter

gui_create_parameter "CGN_BLINK_TIME" "LED blink time in ns"
gui_parameter_set_range 1 10000000000
gui_add_parameter

###############################################################
# Optional Ports
###############################################################


###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
