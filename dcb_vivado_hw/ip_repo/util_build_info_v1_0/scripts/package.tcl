##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME util_build_info
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY psi_3205
set IP_DESCIRPTION "Generates the firmware build date, time and GIT revision"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
set_vendor_short PSI
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/util_build_info.vhd \
}

#Relative Library Files
#add_lib_relative \
#  "../../../../library/" \
#  { \
#    VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
#  }

# ###############################################################
# # Driver Files
# ###############################################################
#
# #WARNING! Driver files are stored with the VHDL code. If they are modified,
# #... the modifications need to be made there. The local files are overwritten
# #... automatically during packaging.
#
# #Copy files
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.c ../drivers/psi_ms_daq_axi/src/psi_ms_daq.c
# file copy -force ../../../VHDL/psi_multi_stream_daq/driver/psi_ms_daq.h ../drivers/psi_ms_daq_axi/src/psi_ms_daq.h
#
# #Package
# add_drivers_relative ../drivers/psi_ms_daq_axi { \
#   src/psi_ms_daq.c \
#   src/psi_ms_daq.h \
# }

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "General Configuration"

gui_create_parameter "CGN_DATE_YEAR" "Default build year (will be replaced by actual build year)"
gui_add_parameter

gui_create_parameter "CGN_DATE_MONTH" "Default build month (will be replaced by actual build month)"
gui_parameter_set_range 1 12
gui_add_parameter

gui_create_parameter "CGN_DATE_DAY" "Default build day (will be replaced by actual build day)"
gui_parameter_set_range 1 31
gui_add_parameter

gui_create_parameter "CGN_TIME_HOUR" "Default build hour (will be replaced by actual build hour)"
gui_parameter_set_range 0 23
gui_add_parameter

gui_create_parameter "CGN_TIME_MINUTE" "Default build minute (will be replaced by actual build minute)"
gui_parameter_set_range 0 59
gui_add_parameter

gui_create_parameter "CGN_TIME_SECOND" "Default build second (will be replaced by actual build second)"
gui_parameter_set_range 0 59
gui_add_parameter

gui_create_parameter "CGN_BCD_FORMAT" "Show date as Binary Coded Decimal (BCD) format"
gui_add_parameter

gui_create_parameter "CGN_GIT_HASHTAG" "Hash tag of the current GIT revision"
gui_add_parameter

gui_create_parameter "CGN_USE_GENERICS" "If checked, use the generic values. If unchecked, generate values during build"
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

#None

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
