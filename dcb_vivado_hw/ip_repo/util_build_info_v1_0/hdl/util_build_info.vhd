---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Build Date and Time Unit
--
--  Project :  MEG
--
--  PCB  :  DCB
--  Part :  Xilinx ZYNQ 7000
--
--  Tool Version :  2019.1 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.01.2022 14:34:48
--
--  Description :  Provide build date and time to firmware.
--                 This IP is based on the fpga_base IP of Goran Marinkovic. The
--                 corresponding IP can be found in the PSI VHDL (VivadoIp) library.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity util_build_info is
  generic (
    CGN_DATE_YEAR     : integer := 0;
    CGN_DATE_MONTH    : integer := 1;
    CGN_DATE_DAY      : integer := 1;
    CGN_TIME_HOUR     : integer := 0;
    CGN_TIME_MINUTE   : integer := 0;
    CGN_TIME_SECOND   : integer := 0;
    CGN_BCD_FORMAT    : boolean := true;
    CGN_GIT_HASHTAG   : integer := 0;
    CGN_USE_GENERICS  : boolean := false
  );
  port (
    BUILD_YEAR_O   : out std_logic_vector(15 downto 0);
    BUILD_MONTH_O  : out std_logic_vector( 7 downto 0);
    BUILD_DAY_O    : out std_logic_vector( 7 downto 0);
    BUILD_HOUR_O   : out std_logic_vector( 7 downto 0);
    BUILD_MINUTE_O : out std_logic_vector( 7 downto 0);
    BUILD_SECOND_O : out std_logic_vector( 7 downto 0);
    GIT_HASHTAG_O  : out std_logic_vector(31 downto 0);
    CLK_I          : in  std_logic
  );
end util_build_info;

architecture structural of util_build_info is

  constant const_sl_0 : std_logic := '0';

  signal year     : std_logic_vector(15 downto 0);
  signal month    : std_logic_vector( 7 downto 0);
  signal day      : std_logic_vector( 7 downto 0);
  signal hour     : std_logic_vector( 7 downto 0);
  signal minute   : std_logic_vector( 7 downto 0);
  signal second   : std_logic_vector( 7 downto 0);
  signal git_hash : std_logic_vector(31 downto 0);

begin

  gen_year : for i in year'range generate
    attribute dont_touch : string;
    attribute dont_touch of year_dfpe_inst: label is "true";
  begin
    year_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => year(i)
    );
  end generate;

    gen_month : for i in month'range generate
    attribute dont_touch : string;
    attribute dont_touch of month_dfpe_inst: label is "true";
  begin
    month_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => month(i)
    );
  end generate;

  gen_day : for i in day'range generate
    attribute dont_touch : string;
    attribute dont_touch of day_dfpe_inst: label is "true";
  begin
    day_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => day(i)
    );
  end generate;

  gen_hour : for i in hour'range generate
    attribute dont_touch : string;
    attribute dont_touch of hour_dfpe_inst: label is "true";
  begin
    hour_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => hour(i)
    );
  end generate;

  gen_minute : for i in minute'range generate
    attribute dont_touch : string;
    attribute dont_touch of minute_dfpe_inst: label is "true";
  begin
    minute_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => minute(i)
    );
  end generate;

  gen_second : for i in second'range generate
    attribute dont_touch : string;
    attribute dont_touch of second_dfpe_inst: label is "true";
  begin
    second_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => second(i)
    );
  end generate;

  gen_git_hash : for i in git_hash'range generate
    attribute dont_touch : string;
    attribute dont_touch of git_hash_dfpe_inst: label is "true";
  begin
    git_hash_dfpe_inst: FDPE
    port map
    (
      PRE => const_sl_0,
      CE  => const_sl_0,
      C   => CLK_I,
      D   => const_sl_0,
      Q   => git_hash(i)
    );
  end generate;

  GENERIC_INFO : if CGN_USE_GENERICS generate
  begin
    BCD_FORMAT : if CGN_BCD_FORMAT generate
    begin
      BUILD_YEAR_O(15 downto 12)   <= std_logic_vector(to_unsigned( CGN_DATE_YEAR/1000,          4));
      BUILD_YEAR_O(11 downto  8)   <= std_logic_vector(to_unsigned((CGN_DATE_YEAR mod 1000)/100, 4));
      BUILD_YEAR_O( 7 downto  4)   <= std_logic_vector(to_unsigned((CGN_DATE_YEAR mod 100) /10 , 4));
      BUILD_YEAR_O( 3 downto  0)   <= std_logic_vector(to_unsigned((CGN_DATE_YEAR mod 10)      , 4));
      BUILD_MONTH_O(7 downto  4)   <= std_logic_vector(to_unsigned( CGN_DATE_MONTH/10,           4));
      BUILD_MONTH_O(3 downto  0)   <= std_logic_vector(to_unsigned( CGN_DATE_MONTH mod 10,       4));
      BUILD_DAY_O(7 downto 4)      <= std_logic_vector(to_unsigned( CGN_DATE_DAY/10,             4));
      BUILD_DAY_O(3 downto 0)      <= std_logic_vector(to_unsigned( CGN_DATE_DAY mod 10,         4));
      BUILD_HOUR_O(7 downto 4)     <= std_logic_vector(to_unsigned( CGN_TIME_HOUR/10,            4));
      BUILD_HOUR_O(3 downto 0)     <= std_logic_vector(to_unsigned( CGN_TIME_HOUR mod 10,        4));
      BUILD_MINUTE_O(7 downto 4)   <= std_logic_vector(to_unsigned( CGN_TIME_MINUTE/10,          4));
      BUILD_MINUTE_O(3 downto 0)   <= std_logic_vector(to_unsigned( CGN_TIME_MINUTE mod 10,      4));
      BUILD_SECOND_O(7 downto 4)   <= std_logic_vector(to_unsigned( CGN_TIME_SECOND/10,          4));
      BUILD_SECOND_O(3 downto 0)   <= std_logic_vector(to_unsigned( CGN_TIME_SECOND mod 10,      4));
    end generate BCD_FORMAT;

    DECIMAL_FORMAT : if not CGN_BCD_FORMAT generate
    begin
      BUILD_YEAR_O   <= std_logic_vector(to_unsigned(CGN_DATE_YEAR,   16));
      BUILD_MONTH_O  <= std_logic_vector(to_unsigned(CGN_DATE_MONTH,   8));
      BUILD_DAY_O    <= std_logic_vector(to_unsigned(CGN_DATE_DAY,     8));
      BUILD_HOUR_O   <= std_logic_vector(to_unsigned(CGN_TIME_HOUR,    8));
      BUILD_MINUTE_O <= std_logic_vector(to_unsigned(CGN_TIME_MINUTE,  8));
      BUILD_SECOND_O <= std_logic_vector(to_unsigned(CGN_TIME_SECOND,  8));
    end generate DECIMAL_FORMAT;

    GIT_HASHTAG_O   <= std_logic_vector(to_unsigned(CGN_GIT_HASHTAG, 32));
  end generate GENERIC_INFO;

  REG_INFO : if not CGN_USE_GENERICS generate
  begin
    BUILD_YEAR_O   <= year;
    BUILD_MONTH_O  <= month;
    BUILD_DAY_O    <= day;
    BUILD_HOUR_O   <= hour;
    BUILD_MINUTE_O <= minute;
    BUILD_SECOND_O <= second;
    GIT_HASHTAG_O  <= git_hash;
  end generate REG_INFO;

end structural;
