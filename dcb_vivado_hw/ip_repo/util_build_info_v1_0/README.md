# General Information

## Maintainer
Elmar Schmid [elmar.schmid@psi.ch]

## License
TBD

# Description
This IP core stores the following information about the firmware build:
* Build Date
* Build Time
* GIT hash the build is based on

Note that the GIT hash reflects the hash of the preceeding commit i.e. not the commit that contains the actual build. This is due to the fact that we cannot predict the hash of a future commit. In case the build was done outside a GIT repository, the hash value will be set to 0xDEADBEEF.

# Dependencies

The procedure info_generate of the TCL script util_build_info.tcl has to be executed in order to apply the build information to the core. The procedure requires one parameter that defines wether date and time are encoded as binary (bin) or as binray coded decimal (bcd).

In Vivado, calling the procedure can be done by adding a pre_optimization script in the implementation settings. The following code lines have to be added to that script (bcd encoding selected):
```
source ./../../../ip_repo/util_build_info_v1_0/util_build_info.tcl
info_generate bcd
```

# Original Code

This core was developed based on the FPGA Base core in the PSI opensource VHDL library (https://github.com/paulscherrerinstitute/vivadoIP_fpga_base/).

# Simulations and Testbenches

This core is only used for implementation.
There are no simulation sources available.
