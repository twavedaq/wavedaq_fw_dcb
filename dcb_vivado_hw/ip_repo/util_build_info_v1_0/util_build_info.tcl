##############################################################################
#  Copyright (c) 2022 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Elmar Schmid (based on Goran Marinkovics fpga_base.tcl)
##############################################################################

#
# ELABORATE_PROC
#
###############################################################################
# Note:
# Remember to source this script form the pre optimization script in your
# project e.g.:
#source ./../../../ip_repo/util_build_info_v1_0/util_build_info.tcl
#info_generate bcd
###############################################################################

###############################################################################
# Conversion decimal to binary
###############################################################################
proc dec2bin {i {width {}} enc} {
   #returns the binary representation of $i
   # width determines the length of the returned string (left truncated or added left 0)
   # use of width allows concatenation of bits sub-fields
   # enc can be set to "bin" or "bcd" for binary or binary encoded decimal respectively

   set res {}
   if {$i<0} {
      set sign -
      set i [expr {abs($i)}]
   } else {
      set sign {}
   }

   if {$enc eq "bcd"} {
      while {$i>0} {
         set nibble {}
         set digit [expr {$i%10}]
         for {set e 3} {$e >= 0} {incr e -1} {
            set bit [expr {$digit/(2**$e)}]
            if {$bit eq 1} {
               set digit [expr {$digit-(2**$e)}]
            }
            set nibble $nibble$bit
         }
         set i [expr {$i/10}]
         set res $nibble$res
      }
   } else {
      while {$i>0} {
         set res [expr {$i%2}]$res
         set i [expr {$i/2}]
      }
   }

   if {$res eq {}} {set res 0}

   if {$width ne {}} {
      append d [string repeat 0 $width] $res
      set res [string range $d [string length $res] end]
   }

   return $sign$res
}

###############################################################################
# Write date and time to generics.
###############################################################################
proc info_generate {enc} {
# enc: encoding of date and time information
#      bin = standard binary
#      bcd = binary coded decimal

   if {$enc ne "bcd"} {
      set enc "bin"
   }

   puts "Date and time will be encoded in: $enc"

   puts "Compilation date and time is set to:"

   set date_raw     [clock seconds]

   # Date
   set c_date [clock format $date_raw -format %Y]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_DATE_YEAR   : $c_date_int"
   set binYear [dec2bin $c_date_int 16 $enc]
   puts "$binYear"

   set x 15
   set y 0
   while {$x>=0} {
      set val [string index $binYear $y]
      set reg */util_build_info_0/U0/gen_year[$x].year_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }


   set c_date [clock format $date_raw -format %m]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_DATE_MONTH  : $c_date_int"
   set binMonth [dec2bin $c_date_int 8 $enc]
   puts "$binMonth"

   set x 7
   set y 0
   while {$x>=0} {
      set val [string index $binMonth $y]
      set reg */util_build_info_0/U0/gen_month[$x].month_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   set c_date [clock format $date_raw -format %e]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_DATE_DAY    : $c_date_int"
   set binDay [dec2bin $c_date_int 8 $enc]
   puts "$binDay"

   set x 7
   set y 0
   while {$x>=0} {
      set val [string index $binDay $y]
      set reg */util_build_info_0/U0/gen_day[$x].day_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   # Time
   set c_date [clock format $date_raw -format %k]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_TIME_HOUR   : $c_date_int"
   set binHour [dec2bin $c_date_int 8 $enc]
   puts "$binHour"

   set x 7
   set y 0
   while {$x>=0} {
      set val [string index $binHour $y]
      set reg */util_build_info_0/U0/gen_hour[$x].hour_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   set c_date [clock format $date_raw -format %M]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_TIME_MINUTE : $c_date_int"
   set binMinute [dec2bin $c_date_int 8 $enc]
   puts "$binMinute"

   set x 7
   set y 0
   while {$x>=0} {
      set val [string index $binMinute $y]
      set reg */util_build_info_0/U0/gen_minute[$x].minute_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   set c_date [clock format $date_raw -format %S]
   scan $c_date "%d" c_date_int
   set c_date_int [expr {$c_date_int}]
   puts "CGN_TIME_SECOND : $c_date_int"
   set binSecond [dec2bin $c_date_int 8 $enc]
   puts "$binSecond"

   set x 7
   set y 0
   while {$x>=0} {
      set val [string index $binSecond $y]
      set reg */util_build_info_0/U0/gen_second[$x].second_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   # GIT Hash
   if {[catch {set git_root [exec git rev-parse --show-toplevel]} errmsg]} {
      puts "Project is NOT in a git work folder..."
      set c_git_hash "0xdeadbeef"
   } else {
      puts "Project is in a git work folder..."
      # Get the git hashtag for this project
      set c_git_hash [exec git log -1 --pretty=%h]
   }
   # Convert the hex number string to an integer
   scan $c_git_hash "%x" c_git_hash
#   scan $c_git_hash "%d" c_git_hash
   # Show this in the log
   puts "GIT hashtag is set to: "
   puts [format "CGN_GIT_HASHTAG : 0x%08X" $c_git_hash ]
   set binGitHash [dec2bin $c_git_hash 32 bin]
   puts "$binGitHash"

   set x 31
   set y 0
   while {$x>=0} {
      set val [string index $binGitHash $y]
      set reg */util_build_info_0/U0/gen_git_hash[$x].git_hash_dfpe_inst
      puts "reg is $reg: x is $x : val is $val"
      set_property -verbose INIT $val [get_cells $reg]
      set x [expr {$x - 1}]
      set y [expr {$y + 1}]
   }

   return
}

###############################################################################
# Execute tcl script tcl.pre in optimize design.
###############################################################################

###############################################################################
# End of File
###############################################################################
