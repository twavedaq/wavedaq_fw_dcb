---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Clock Buffer
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2019.1 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  30.10.2020 08:31:34
--
--  Description :  Instantiation of all Xilinx series 7 clock buffers in Vivado.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity clock_buffer is
  generic (
    BUFFER_TYPE : string  := "BUFG";     -- Values: "BUFG, BUFGCE, BUFGCE_1, BUFH, BUFHCE, BUFIO, BUFR, BUFMR, BUFMRCE"
    -- BUFHCE and BUFMRCE
    CE_TYPE     : string  := "SYNC";     -- "SYNC" (glitchless switching) or "ASYNC" (immediate switch)
    INIT_OUT    : integer range 0 to 1 := 0;          -- Initial output value (0-1)
    -- BUFR
    BUFR_DIVIDE : string  := "BYPASS";   -- Values: "BYPASS, 1, 2, 3, 4, 5, 6, 7, 8"
    SIM_DEVICE  : string  := "7SERIES"   -- Must be set to "7SERIES"
  );
  port (
    O   : out std_logic;  -- 1-bit output: Clock output
    I   : in  std_logic;  -- 1-bit input: Clock input
    -- BUFGCE, BUFGCE_1, BUFHCE, BUFR, BUFMRCE
    CE  : in  std_logic; -- 1-bit input: Clock enable input for I0
    -- BUFR
    CLR : in  std_logic; -- 1-bit input: Active high, asynchronous clear (Divided modes only)
    -- BUFGMUX, BUFGMUX_1, BUFGMUX_CTRL
    I0  : in  std_logic; -- 1-bit input: Clock input (S=0)
    I1  : in  std_logic; -- 1-bit input: Clock input (S=1)
    S   : in  std_logic  -- 1-bit input: Clock select
  );
end clock_buffer;

architecture behavioral of clock_buffer is

  -- Add MUX Buffers

begin

  BUFG_GEN : if BUFFER_TYPE = "BUFG" generate
    BUFG_inst : BUFG
    port map (
      O => O, -- 1-bit output: Clock output
      I => I  -- 1-bit input: Clock input
    );
  end generate;

  BUFGCE_GEN : if BUFFER_TYPE = "BUFGCE" generate
    BUFGCE_inst : BUFGCE
    port map (
      O  => O,  -- 1-bit output: Clock output
      CE => CE, -- 1-bit input: Clock enable input for I0
      I  => I   -- 1-bit input: Primary clock
    );
  end generate;

  BUFGCE_1_GEN : if BUFFER_TYPE = "BUFGCE_1" generate
    BUFGCE_1_inst : BUFGCE_1
    port map (
      O  => O,  -- 1-bit output: Clock output
      CE => CE, -- 1-bit input: Clock enable input for I0
      I  => I   -- 1-bit input: Primary clock
    );
  end generate;

  BUFH_GEN : if BUFFER_TYPE = "BUFH" generate
    BUFH_inst : BUFH
    port map (
      O => O, -- 1-bit output: Clock output
      I => I  -- 1-bit input: Clock input
    );
  end generate;

  BUFHCE_GEN : if BUFFER_TYPE = "BUFHCE" generate
    BUFHCE_inst : BUFHCE
    generic map (
      CE_TYPE => "SYNC", -- "SYNC" (glitchless switching) or "ASYNC" (immediate switch)
      INIT_OUT => 0      -- Initial output value (0-1)
    )
    port map (
      O => O,   -- 1-bit output: Clock output
      CE => CE, -- 1-bit input: Active high enable
      I => I    -- 1-bit input: Clock input
    );
  end generate;

  BUFIO_GEN : if BUFFER_TYPE = "BUFIO" generate
    BUFIO_inst : BUFIO
    port map (
      O => O, -- 1-bit output: Clock output (connect to I/O clock loads).
      I => I  -- 1-bit input: Clock input (connect to an IBUF or BUFMR).
    );
  end generate;

  BUFR_GEN : if BUFFER_TYPE = "BUFR" generate
    BUFR_inst : BUFR
    generic map (
      BUFR_DIVIDE => "BYPASS",   -- Values: "BYPASS, 1, 2, 3, 4, 5, 6, 7, 8"
      SIM_DEVICE => "7SERIES"  -- Must be set to "7SERIES"
    )
    port map (
      O => O,     -- 1-bit output: Clock output port
      CE => CE,   -- 1-bit input: Active high, clock enable (Divided modes only)
      CLR => CLR, -- 1-bit input: Active high, asynchronous clear (Divided modes only)
      I => I      -- 1-bit input: Clock buffer input driven by an IBUF, MMCM or local interconnect
    );
  end generate;

  BUFMR_GEN : if BUFFER_TYPE = "BUFMR" generate
    BUFMR_inst : BUFMR
    port map (
      O => O, -- 1-bit output: Clock output (connect to BUFIOs/BUFRs)
      I => I  -- 1-bit input: Clock input (Connect to IBUF)
    );
  end generate;

  BUFMRCE_GEN : if BUFFER_TYPE = "BUFMRCE" generate
    BUFMRCE_inst : BUFMRCE
    generic map (
      CE_TYPE => "SYNC", -- SYNC, ASYNC
      INIT_OUT => 0      -- Initial output and stopped polarity, (0-1)
    )
    port map (
      O => O,   -- 1-bit output: Clock output (connect to BUFIOs/BUFRs)
      CE => CE, -- 1-bit input: Active high buffer enable
      I => I    -- 1-bit input: Clock input (Connect to IBUF)
    );
  end generate;

  BUFGMUX_GEN : if BUFFER_TYPE = "BUFGMUX" generate
    BUFGMUX_inst : BUFGMUX
    port map (
      O  => O,  -- 1-bit output: Clock output
      I0 => I0, -- 1-bit input: Clock input (S=0)
      I1 => I1, -- 1-bit input: Clock input (S=1)
      S  => S   -- 1-bit input: Clock select
    );
  end generate;

  BUFGMUX_1_GEN : if BUFFER_TYPE = "BUFGMUX_1" generate
    BUFGMUX_1_inst : BUFGMUX_1
    port map (
      O  => O,  -- 1-bit output: Clock output
      I0 => I0, -- 1-bit input: Clock input (S=0)
      I1 => I1, -- 1-bit input: Clock input (S=1)
      S  => S   -- 1-bit input: Clock select
    );
  end generate;

  BUFGMUX_CTRL_GEN : if BUFFER_TYPE = "BUFGMUX_CTRL" generate
    BUFGMUX_CTRL_inst : BUFGMUX_CTRL
    port map (
      O  => O,  -- 1-bit output: Clock output
      I0 => I0, -- 1-bit input: Clock input (S=0)
      I1 => I1, -- 1-bit input: Clock input (S=1)
      S  => S   -- 1-bit input: Clock select
    );
  end generate;

end architecture behavioral;
