##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Elmar Schmid
##############################################################################

###############################################################
# Include PSI packaging commands
###############################################################
source ../../../../library/TCL/PsiIpPackage/PsiIpPackage.tcl
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME clock_buffer
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY psi_3205
set IP_DESCIRPTION "Xilinx clock buffers"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
set_vendor_short PSI
#set_logo_relative "../doc/psi_logo_150.gif"
#set_datasheet_relative "../../../VHDL/psi_multi_stream_daq/doc/psi_multi_stream_daq.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/clock_buffer.vhd \
}

set_top_entity clock_buffer

#Relative Library Files
#add_lib_relative \
#  "../../../../library/" \
#  { \
#    VHDL/psi_common/hdl/psi_common_array_pkg.vhd \
#  }

###############################################################
# GUI Parameters
###############################################################

#General Configuration
gui_add_page "Buffer Configuration"

gui_create_parameter "BUFFER_TYPE" "Type of clock buffer"
gui_parameter_set_widget_dropdown {"BUFG" "BUFGCE" "BUFGCE_1" "BUFH" "BUFHCE" "BUFIO" "BUFR" "BUFMR" "BUFMRCE" "BUFGMUX" "BUFGMUX_1" "BUFGMUX_CTRL"}
gui_add_parameter

gui_create_parameter "CE_TYPE" "SYNC = glitchless switching, ASYNC = immediate switching (BUFHCE and BUFMCE only)"
gui_parameter_set_widget_dropdown {"SYNC" "ASYNC"}
gui_parameter_set_enablement { { $BUFFER_TYPE == "BUFHCE" || $BUFFER_TYPE == "BUFMRCE" } } true
gui_add_parameter

gui_create_parameter "INIT_OUT" "Initial output value (0, 1) (BUFHCE and BUFMCE only)"
gui_parameter_set_range 0 1
gui_parameter_set_enablement { { $BUFFER_TYPE == "BUFHCE" || $BUFFER_TYPE == "BUFMRCE" } } true
gui_add_parameter

gui_create_parameter "BUFR_DIVIDE" "BUFR clock divider (BUFR only)"
gui_parameter_set_widget_dropdown {"BYPASS" "1" "2" "3" "4" "5" "6" "7" "8"}
gui_parameter_set_enablement { { $BUFFER_TYPE == "BUFR" } } true
gui_add_parameter

gui_create_parameter "SIM_DEVICE" "Device selection for correct simulation (BUFR only)"
gui_parameter_set_widget_dropdown {"7SERIES"}
gui_parameter_set_enablement { { $BUFFER_TYPE == "BUFR" } } true
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

add_port_enablement_condition "I"   "\$BUFFER_TYPE == \"BUFG\"  || \$BUFFER_TYPE == \"BUFGCE\"  || \$BUFFER_TYPE == \"BUFGCE_1\"  || \$BUFFER_TYPE == \"BUFH\"  || \$BUFFER_TYPE == \"BUFHCE\"  || \$BUFFER_TYPE == \"BUFIO\"  || \$BUFFER_TYPE == \"BUFR\"  || \$BUFFER_TYPE == \"BUFMR\"  || \$BUFFER_TYPE == \"BUFMRCE\""
add_port_enablement_condition "CLR" "\$BUFFER_TYPE == \"BUFR\""
add_port_enablement_condition "CE"  "\$BUFFER_TYPE == \"BUFGCE\" || \$BUFFER_TYPE == \"BUFGCE_1\" || \$BUFFER_TYPE == \"BUFHCE\" || \$BUFFER_TYPE == \"BUFR\" || \$BUFFER_TYPE == \"BUFMRCE\""
add_port_enablement_condition "I0"  "\$BUFFER_TYPE == \"BUFGMUX\" || \$BUFFER_TYPE == \"BUFGMUX_1\" || \$BUFFER_TYPE == \"BUFGMUX_CTRL\""
add_port_enablement_condition "I1"  "\$BUFFER_TYPE == \"BUFGMUX\" || \$BUFFER_TYPE == \"BUFGMUX_1\" || \$BUFFER_TYPE == \"BUFGMUX_CTRL\""
add_port_enablement_condition "S"   "\$BUFFER_TYPE == \"BUFGMUX\" || \$BUFFER_TYPE == \"BUFGMUX_1\" || \$BUFFER_TYPE == \"BUFGMUX_CTRL\""

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#                                 Edit   Synth Part
package_ip $TargetDir             false  true  xc7z030fbg676-1
