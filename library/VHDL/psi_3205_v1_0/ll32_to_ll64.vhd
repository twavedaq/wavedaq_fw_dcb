--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll32_to_ll64.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  local link width conversion
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll32_to_ll64 is
  generic
  (
    CGN_LL_I_BUFF       : integer := 1;
    CGN_LL_O_BUFF       : integer := 1
  );
  port
  (
    LL_CLK_I            : in  std_logic;
    RESET_I             : in  std_logic;

    LL32_I_DATA_I       : in  std_logic_vector(31 downto 0);
    LL32_I_SOF_N_I      : in  std_logic;
    LL32_I_EOF_N_I      : in  std_logic;
    LL32_I_REM_I        : in  std_logic_vector(1 downto 0);
    LL32_I_SRC_RDY_N_I  : in  std_logic;
    LL32_I_DST_RDY_N_O  : out std_logic;

    LL64_O_DATA_O       : out std_logic_vector(63 downto 0);
    LL64_O_SOF_N_O      : out std_logic;
    LL64_O_EOF_N_O      : out std_logic;
    LL64_O_REM_O        : out std_logic_vector(2 downto 0);
    LL64_O_SRC_RDY_N_O  : out std_logic;
    LL64_O_DST_RDY_N_I  : in  std_logic
  );
end ll32_to_ll64;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll32_to_ll64 is
  signal word_sel       : std_logic;
  signal ll64_rem       : std_logic_vector (2 downto 0);
  signal ll64_sof       : std_logic;
  signal ll64_eof       : std_logic;
  signal reg_valid      : std_logic;

  signal word_0         : std_logic_vector (31 downto 0);
  signal word_1         : std_logic_vector (31 downto 0);

  signal word_en_0      : std_logic;
  signal word_en_1      : std_logic;

  signal enable_gate    : std_logic;

  ------------------------------------------------------------------------------
  signal ll32_us_data       : std_logic_vector (31 downto 0);
  signal ll32_us_sof_n      : std_logic;
  signal ll32_us_eof_n      : std_logic;
  signal ll32_us_rem        : std_logic_vector (1 downto 0);
  signal ll32_us_src_rdy_n  : std_logic;
  signal ll32_us_dst_rdy_n  : std_logic;

  signal ll64_ds_data       : std_logic_vector (63 downto 0);
  signal ll64_ds_sof_n      : std_logic;
  signal ll64_ds_eof_n      : std_logic;
  signal ll64_ds_rem        : std_logic_vector (2 downto 0);
  signal ll64_ds_src_rdy_n  : std_logic;
  signal ll64_ds_dst_rdy_n  : std_logic;

begin

  use_buff_us: if CGN_LL_I_BUFF > 0 generate
  begin
    dbuff_us : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 32,
     CGN_REM_WIDTH  => 2
    )
    port map
    (
      CLK_I            => LL_CLK_I,
      RESET_I          => RESET_I,

      LL_I_DATA_I      => LL32_I_DATA_I,
      LL_I_SOF_N_I     => LL32_I_SOF_N_I,
      LL_I_EOF_N_I     => LL32_I_EOF_N_I,
      LL_I_REM_I       => LL32_I_REM_I,
      LL_I_SRC_RDY_N_I => LL32_I_SRC_RDY_N_I,
      LL_I_DST_RDY_N_O => LL32_I_DST_RDY_N_O,

      LL_O_DATA_O      => ll32_us_data,
      LL_O_SOF_N_O     => ll32_us_sof_n,
      LL_O_EOF_N_O     => ll32_us_eof_n,
      LL_O_REM_O       => ll32_us_rem,
      LL_O_SRC_RDY_N_O => ll32_us_src_rdy_n,
      LL_O_DST_RDY_N_I => ll32_us_dst_rdy_n
    );
  end generate use_buff_us;

  no_buff_us: if CGN_LL_I_BUFF = 0 generate
  begin
    ll32_us_data         <= LL32_I_DATA_I;
    ll32_us_sof_n        <= LL32_I_SOF_N_I;
    ll32_us_eof_n        <= LL32_I_EOF_N_I;
    ll32_us_rem          <= LL32_I_REM_I;
    ll32_us_src_rdy_n    <= LL32_I_SRC_RDY_N_I;
    LL32_I_DST_RDY_N_O   <= ll32_us_dst_rdy_n;
  end generate no_buff_us;

  ------------------------------------------------------------------------------

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        word_0 <= x"00000000";
      elsif (word_en_0 = '1') then
        word_0 <= ll32_us_data;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (word_en_0 = '1') then
        word_1 <= x"00000000";
      elsif (word_en_1 = '1') then
        word_1 <= ll32_us_data;
      end if;
    end if;
  end process;


  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll64_rem <= "111";
      elsif (enable_gate = '1') then
        ll64_rem(1 downto 0) <= ll32_us_rem(1 downto 0);
        ll64_rem(2) <= word_sel;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll64_sof <= '1';
      elsif ((enable_gate = '1') and (ll32_us_sof_n = '0')) then
        ll64_sof <= '0';
      elsif (word_en_0 = '1') then
        ll64_sof <= '1';
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll64_eof <= '1';
      elsif ((enable_gate = '1') and (ll32_us_eof_n = '0')) then
        ll64_eof <= '0';
      elsif (word_en_0 = '1') then
        ll64_eof <= '1';
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        reg_valid <= '0';
      elsif (word_en_1 = '1') or ((enable_gate = '1') and (ll32_us_eof_n = '0')) then
        reg_valid <= '1';
      elsif (ll64_ds_dst_rdy_n = '0') then
        reg_valid <= '0';
      end if;
    end if;
  end process;

  word_sel <= '0' when (ll32_us_sof_n = '0') else
                  not ll64_rem(2);

  enable_gate <= (not ll32_us_src_rdy_n) and (not reg_valid or not ll64_ds_dst_rdy_n);

  word_en_0 <= '1' when (enable_gate = '1') and ( word_sel = '0') else '0';
  word_en_1 <= '1' when (enable_gate = '1') and ( word_sel = '1') else '0';

  ll32_us_dst_rdy_n  <= reg_valid and ll64_ds_dst_rdy_n;

  ll64_ds_src_rdy_n <= not reg_valid;
  ll64_ds_data      <= word_0 & word_1;
  ll64_ds_sof_n     <= ll64_sof;
  ll64_ds_eof_n     <= ll64_eof;
  ll64_ds_rem       <= ll64_rem;

  ------------------------------------------------------------------------------

  use_buff_ds: if CGN_LL_O_BUFF > 0 generate
  begin
    dbuff_ds : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 64,
     CGN_REM_WIDTH  => 3
    )
    port map
    (
      CLK_I             => LL_CLK_I,
      RESET_I           => RESET_I,

      LL_I_DATA_I      => ll64_ds_data,
      LL_I_SOF_N_I     => ll64_ds_sof_n,
      LL_I_EOF_N_I     => ll64_ds_eof_n,
      LL_I_REM_I       => ll64_ds_rem,
      LL_I_SRC_RDY_N_I => ll64_ds_src_rdy_n,
      LL_I_DST_RDY_N_O => ll64_ds_dst_rdy_n,

      LL_O_DATA_O      => LL64_O_DATA_O,
      LL_O_SOF_N_O     => LL64_O_SOF_N_O,
      LL_O_EOF_N_O     => LL64_O_EOF_N_O,
      LL_O_REM_O       => LL64_O_REM_O,
      LL_O_SRC_RDY_N_O => LL64_O_SRC_RDY_N_O,
      LL_O_DST_RDY_N_I => LL64_O_DST_RDY_N_I
    );
  end generate use_buff_ds;

  no_buff_ds: if CGN_LL_O_BUFF = 0 generate
  begin
    LL64_O_DATA_O      <= ll64_ds_data;
    LL64_O_SOF_N_O     <= ll64_ds_sof_n;
    LL64_O_EOF_N_O     <= ll64_ds_eof_n;
    LL64_O_REM_O       <= ll64_ds_rem;
    LL64_O_SRC_RDY_N_O <= ll64_ds_src_rdy_n;
    ll64_ds_dst_rdy_n   <= LL64_O_DST_RDY_N_I;
  end generate no_buff_ds;

end behavioral;
