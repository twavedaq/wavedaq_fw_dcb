--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll32_to_ll16.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  local link width conversion
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll32_to_ll16 is
  generic
  (
    CGN_LL_I_BUFF       : integer := 1;
    CGN_LL_O_BUFF       : integer := 1
  );
  port
  (
    LL_CLK_I            : in  std_logic;
    RESET_I             : in  std_logic;

    LL32_I_DATA_I       : in  std_logic_vector(31 downto 0);
    LL32_I_SOF_N_I      : in  std_logic;
    LL32_I_EOF_N_I      : in  std_logic;
    LL32_I_REM_I        : in  std_logic_vector(1 downto 0);
    LL32_I_SRC_RDY_N_I  : in  std_logic;
    LL32_I_DST_RDY_N_O  : out std_logic;

    LL16_O_DATA_O       : out std_logic_vector(15 downto 0);
    LL16_O_SOF_N_O      : out std_logic;
    LL16_O_EOF_N_O      : out std_logic;
    LL16_O_REM_O        : out std_logic_vector(0 downto 0);
    LL16_O_SRC_RDY_N_O  : out std_logic;
    LL16_O_DST_RDY_N_I  : in  std_logic
  );
end ll32_to_ll16;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll32_to_ll16 is

  signal sword_sel          : std_logic := '0';
  signal ll32_rem_int       : std_logic_vector (1 downto 0);

  signal ll32_us_data       : std_logic_vector (31 downto 0);
  signal ll32_us_sof_n      : std_logic;
  signal ll32_us_eof_n      : std_logic;
  signal ll32_us_rem        : std_logic_vector (1 downto 0);
  signal ll32_us_src_rdy_n  : std_logic;
  signal ll32_us_dst_rdy_n  : std_logic;

  signal ll16_ds_data       : std_logic_vector (15 downto 0);
  signal ll16_ds_sof_n      : std_logic;
  signal ll16_ds_eof_n      : std_logic;
  signal ll16_ds_rem        : std_logic_vector (0 downto 0);
  signal ll16_ds_src_rdy_n  : std_logic;
  signal ll16_ds_dst_rdy_n  : std_logic;

begin

  use_buff_us: if CGN_LL_I_BUFF > 0 generate
  begin
    dbuff_us : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 32,
     CGN_REM_WIDTH  => 2
    )
    port map
    (
      CLK_I             => LL_CLK_I,
      RESET_I           => RESET_I,

      LL_I_DATA_I      => LL32_I_DATA_I,
      LL_I_SOF_N_I     => LL32_I_SOF_N_I,
      LL_I_EOF_N_I     => LL32_I_EOF_N_I,
      LL_I_REM_I       => LL32_I_REM_I,
      LL_I_SRC_RDY_N_I => LL32_I_SRC_RDY_N_I,
      LL_I_DST_RDY_N_O => LL32_I_DST_RDY_N_O,

      LL_O_DATA_O      => ll32_us_data,
      LL_O_SOF_N_O     => ll32_us_sof_n,
      LL_O_EOF_N_O     => ll32_us_eof_n,
      LL_O_REM_O       => ll32_us_rem,
      LL_O_SRC_RDY_N_O => ll32_us_src_rdy_n,
      LL_O_DST_RDY_N_I => ll32_us_dst_rdy_n
    );
  end generate use_buff_us;

  no_buff_us: if CGN_LL_I_BUFF = 0 generate
  begin
    ll32_us_data         <= LL32_I_DATA_I;
    ll32_us_sof_n        <= LL32_I_SOF_N_I;
    ll32_us_eof_n        <= LL32_I_EOF_N_I;
    ll32_us_rem          <= LL32_I_REM_I;
    ll32_us_src_rdy_n    <= LL32_I_SRC_RDY_N_I;
    LL32_I_DST_RDY_N_O  <= ll32_us_dst_rdy_n;
  end generate no_buff_us;

  ------------------------------------------------------------------------------
  -- valid bytes may be <4 only allowed if EOF is true
  ll32_rem_int  <=  ll32_us_rem when ll32_us_eof_n ='0' else "11";

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (ll32_us_src_rdy_n ='1') or ((ll16_ds_dst_rdy_n = '0') and (sword_sel = ll32_rem_int(1)))  then
        sword_sel <= '0';
      elsif (ll16_ds_dst_rdy_n = '0') then
        sword_sel <= not sword_sel;
      end if;
    end if;
  end process;

  ll32_us_dst_rdy_n <= '1' when RESET_I = '1' else
                       '0' when (sword_sel = ll32_rem_int(1)) and (ll16_ds_dst_rdy_n = '0') else
                       '0' when (ll32_us_src_rdy_n ='1') else
                       '1';

  ll16_ds_src_rdy_n <= '0' when sword_sel = '1' else ll32_us_src_rdy_n;
  ll16_ds_data      <= ll32_us_data(31 downto 16) when (sword_sel = '0') else ll32_us_data(15 downto  0);
  ll16_ds_sof_n     <= '0' when (ll32_us_sof_n = '0') and (sword_sel = '0') else '1';
  ll16_ds_eof_n     <= '0' when (ll32_us_eof_n = '0') and (sword_sel = ll32_rem_int(1) ) else '1';
  ll16_ds_rem(0)    <= ll32_rem_int(0) when (ll32_us_eof_n = '0') and (sword_sel = ll32_rem_int(1) ) else '1';

  ------------------------------------------------------------------------------

  use_buff_ds: if CGN_LL_O_BUFF > 0 generate
  begin
    dbuff_ds : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 16,
     CGN_REM_WIDTH  => 1
    )
    port map
    (
      CLK_I            => LL_CLK_I,
      RESET_I          => RESET_I,

      LL_I_DATA_I      => ll16_ds_data,
      LL_I_SOF_N_I     => ll16_ds_sof_n,
      LL_I_EOF_N_I     => ll16_ds_eof_n,
      LL_I_REM_I       => ll16_ds_rem,
      LL_I_SRC_RDY_N_I => ll16_ds_src_rdy_n,
      LL_I_DST_RDY_N_O => ll16_ds_dst_rdy_n,

      LL_O_DATA_O      => LL16_O_DATA_O,
      LL_O_SOF_N_O     => LL16_O_SOF_N_O,
      LL_O_EOF_N_O     => LL16_O_EOF_N_O,
      LL_O_REM_O       => LL16_O_REM_O,
      LL_O_SRC_RDY_N_O => LL16_O_SRC_RDY_N_O,
      LL_O_DST_RDY_N_I => LL16_O_DST_RDY_N_I
    );
  end generate use_buff_ds;

  no_buff_ds: if CGN_LL_O_BUFF = 0 generate
  begin
    LL16_O_DATA_O       <= ll16_ds_data;
    LL16_O_SOF_N_O      <= ll16_ds_sof_n;
    LL16_O_EOF_N_O      <= ll16_ds_eof_n;
    LL16_O_REM_O        <= ll16_ds_rem;
    LL16_O_SRC_RDY_N_O  <= ll16_ds_src_rdy_n;
    ll16_ds_dst_rdy_n   <= LL16_O_DST_RDY_N_I;
  end generate no_buff_ds;

--------------------------------------------------------------------------------


end behavioral;

