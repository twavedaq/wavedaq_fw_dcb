/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  14.05.2014 12:47:20
 *
 *  Description :  Module for controling the onboard led.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "system.h"
#include "sc_io.h"
#include "sw_state.h"
#include "xfs_printf.h"
#include "utilities.h"
/*#include "xtime.h"*/
#include "dbg.h"
#include "cmd_processor.h"
#include "sleep.h"
#include <stdlib.h>

/************************************************************/

  int led_test(int argc, char **argv)
  {
    unsigned int delay = 4e6;

    CMD_HELP("",
             "set RGB LED colors one after another",
             "Applies all possible sw status settings to the onboard LED."
            );

    emio_reset_sw_state(SYSPTR(gpio_mio));
    xfs_printf("LED status \"SW Update\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_SW_UPDATE);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_SW_UPDATE);
    xfs_printf("LED status \"FW Update\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_FW_UPDATE);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_FW_UPDATE);
    xfs_printf("LED status \"Load SW\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_BL_LOAD);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_BL_LOAD);
    xfs_printf("LED status \"Bootloader Failure\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_BL_FAIL);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_BL_FAIL);
    xfs_printf("LED status \"Mark Board\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_MARKER);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_MARKER);
    xfs_printf("LED status \"DHCP Request\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_DHCP_REQ);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_DHCP_REQ);
    xfs_printf("LED status \"SW Error\"\r\n");
    emio_set_sw_state(SYSPTR(gpio_mio), SW_STATUS_ERROR);
    usleep(delay);
    emio_clr_sw_state(SYSPTR(gpio_mio), SW_STATUS_ERROR);
    xfs_printf("LED status \"Normal Operation\"\r\n");
    emio_reset_sw_state(SYSPTR(gpio_mio));

    return 0;
  }

/************************************************************/

  int module_led_help(int argc, char **argv)
  {
    CMD_HELP("",
             "LED Control Module",
             "Can be used to set the RGB LED"
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type led_cmd_table[] =
  {
    {0, "led", module_led_help},
    {0, "ledtest", led_test},
    {0, NULL, NULL}
  };

/************************************************************/
