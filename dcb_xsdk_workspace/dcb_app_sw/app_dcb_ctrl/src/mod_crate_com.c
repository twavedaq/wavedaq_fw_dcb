/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  26.08.2014 09:10:02
 *
 *  Description :  Module for accessing WDBs and TCB via the backplane SPI link.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "spi_bpl.h"
#include "dbg.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "system.h"
#include "cmd_processor.h"

/************************************************************/

  int wdb_ascii_com(int argc, char **argv)
  {
    unsigned int wdb_slot;

    CMD_HELP("<wdb slot> <data>",
             "communicate to WDB via backplane SPI ASCII command.\r\n",
             "  <wdb slot> : WDB slot (0..15)\r\n"
             "  <cmd>      : command to send to WDB\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    wdb_slot = strtoul(argv[1], NULL, 0);

    wdb_spi_ascii_cmd(argv[2], wdb_slot);

    return 0;
  }

/************************************************************/

  int wdb_flash_com(int argc, char **argv)
  {
    unsigned int wdb_slot;

    CMD_HELP("<wdb slot> <cmd>",
             "communicate to WDB via backplane SPI ASCII command.\r\n",
             "  <wdb slot> : WDB slot (0..15)\r\n"
             "  <cmd>      : command to send to WDB flash\r\n"
             "               e.g. id\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    wdb_slot = strtoul(argv[1], NULL, 0);

    if(fstrcmp(argv[2],"id"))
    {
      wdb_spi_flash_id_cmd(wdb_slot);
    }

    return 0;
  }

/************************************************************/

  int module_crate_com_help(int argc, char **argv)
  {
    CMD_HELP("",
             "Register Bank Module",
             "Can be used to read and write registers of the register bank"
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type crate_com_cmd_table[] =
  {
    {0, "crt_com", module_crate_com_help},
    {3, "wd", wdb_ascii_com},
    {0, "wdf", wdb_flash_com},
    {0, NULL, NULL}
  };

/************************************************************/
