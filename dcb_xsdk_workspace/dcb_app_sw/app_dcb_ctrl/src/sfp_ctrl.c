/******************************************************************************/
/*                                                                            */
/*  file: sfp_ctrl.cpp                                                        */
/*                                                                            */
/*  (c) 2010 PSI tg32/se32                                                    */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "sfp_ctrl.h"
#include "sc_io.h"
#include "sleep.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"

/******************************************************************************/

#define SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT   200 /* us */

/******************************************************************************/

xfs_u32 sfp_ctrl_wait_until_not_busy(sfp_ctrl_type *self, unsigned int timeout_us)
{
  unsigned int timer = timeout_us;

  /* Move checks to the beginning of the functions (calling this routine) */
  while( timer && XIicPs_BusIsBusy(self->iic_dev_ptr) )
  {
    usleep(1);
    timer--;
  }

  if(!timer)
  {
    if (DBG_ERR) xfs_local_printf("Error: SFP IIC timeout\r\n");
    return -1;
  }

  return 1;
}

/******************************************************************************/

xfs_u32 sfp_ctrl_init(sfp_ctrl_type *self, XIicPs *init_iic_dev_ptr, XGpioPs *init_emio_dev_ptr,
                      xfs_u32 init_rate_sel1_pin,
                      xfs_u32 init_rate_sel0_pin,
                      xfs_u32 init_tx_disable_pin,
                      xfs_u32 init_tx_fault_pin,
                      xfs_u32 init_mod_abs_pin,
                      xfs_u32 init_los_pin)
{
  self->iic_dev_ptr  = init_iic_dev_ptr;
  self->emio_dev_ptr = init_emio_dev_ptr;

  self->sfp_rate_sel1_pin  = init_rate_sel1_pin;
  self->sfp_rate_sel0_pin  = init_rate_sel0_pin;
  self->sfp_tx_disable_pin = init_tx_disable_pin;
  self->sfp_tx_fault_pin   = init_tx_fault_pin;
  self->sfp_mod_def0_pin   = init_mod_abs_pin;
  self->sfp_los_pin        = init_los_pin;

  /* Initialize output values */
  emio_set_pin(self->emio_dev_ptr, init_rate_sel1_pin, 0);
  emio_set_pin(self->emio_dev_ptr, init_rate_sel0_pin, 0);
  emio_set_pin(self->emio_dev_ptr, init_tx_disable_pin, 0);

  self->sfp_insert_delay = 0;

  if(sfp_module_present(self))
  {
    self->sfp_inserted = 1;

    if(is_marvell_phy(self))
    {
      self->marvell_phy = 1;
      sfp_marvell_phy_init(self);
    }
    else
    {
      self->marvell_phy = 0;
    }
  }
  else
  {
    self->sfp_inserted = 0;
  }

  return 1;
}

/******************************************************************************/

void gpio_sfp_read(sfp_ctrl_type *self)
{
  xfs_printf("\r\n\r\n");
  xfs_printf("SFP RS1        = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_rate_sel1_pin));
  xfs_printf("SFP RS0        = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_rate_sel0_pin));
  xfs_printf("SFP TX Disable = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_tx_disable_pin));
  xfs_printf("SFP TX Fault   = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_tx_fault_pin));
  xfs_printf("SFP MOD Def0   = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_mod_def0_pin));
  xfs_printf("SFP LOS        = %d\r\n", emio_get_pin(self->emio_dev_ptr, self->sfp_los_pin));
}

/******************************************************************************/

xfs_u16 sfp_marvell_reg_read(sfp_ctrl_type *self, unsigned int reg_num)
{
  xfs_u8  data_bytes[2];

  /* write phy-register address */
  data_bytes[0] = reg_num & 0x01f;
  XIicPs_MasterSendPolled(self->iic_dev_ptr, data_bytes, 1, IIC_ADDRESS(SFP_MARVELL_PHY_ADR));
  sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
  /* read 2-byte phy-register */
  XIicPs_MasterRecvPolled(self->iic_dev_ptr, data_bytes, 2, IIC_ADDRESS(SFP_MARVELL_PHY_ADR));
  sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
  return ( (data_bytes[0] << 8) | data_bytes[1] );
}

/******************************************************************************/

void sfp_marvell_reg_write(sfp_ctrl_type *self, unsigned int reg_num, xfs_u16 data)
{
  xfs_u8 data_bytes[3];

  data_bytes[0] = reg_num & 0x01f;
  data_bytes[1] = (data>>8);
  data_bytes[2] = data;

  XIicPs_MasterSendPolled(self->iic_dev_ptr, data_bytes, 3, IIC_ADDRESS(SFP_MARVELL_PHY_ADR));
  sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
}


/******************************************************************************/

void marvell_reg_dump(sfp_ctrl_type *self)
{
  xfs_u16 reg;
  unsigned int i,j;
  unsigned char reg30_pages[] = { 0, 3, 6, 7, 10, 12, 16, 18 };
  unsigned char page;
  xfs_u16 regs[64];

  xfs_printf("\n\r\n\r");
  xfs_printf("Marvel Rgisters:\n\r");

  reg = sfp_marvell_reg_read(self, 22);
  reg &= 0xff00;
  sfp_marvell_reg_write(self, 22, reg);  /* select page 0 */

  for(i=0; i<=28; i++)
  {
    regs[i] = sfp_marvell_reg_read(self, i);
  }

  sfp_marvell_reg_write(self, 22, reg | 0X01); /* select page 1 */
  for(i=0; i<=28; i++)
  {
    regs[i+32] = sfp_marvell_reg_read(self, i);
  }
  sfp_marvell_reg_write(self, 22, reg);  /* switch back to page 0 */

  xfs_printf("           Page 0 (Copper)   Page 1 (Fiber)\r\n");
  for(i=0; i<=28; i++)
  {
    xfs_printf("Reg(%2d):   0x%04X            0x%04X\r\n", i, regs[i], regs[i+32]);
  }


  reg = sfp_marvell_reg_read(self, 29);
  reg &= 0xffe0;

  for(j=0; j<sizeof(reg30_pages); j++)
  {
    page = reg30_pages[j];
    sfp_marvell_reg_write(self, 29, reg | page);
    reg = sfp_marvell_reg_read(self, 30);
    xfs_printf("Reg(%2d):   (Page %2d)         0x%04X\r\n", 30, page, regs[i]);
  }
  sfp_marvell_reg_write(self, 29, reg ); /* switch back to reg 30 page 0 */

  i=31;
  reg = sfp_marvell_reg_read(self, i);
  xfs_printf("Reg(%2d):   0x%04X\r\n", i, reg);

}

/******************************************************************************/


void sfp_module_info(sfp_ctrl_type *self)
{
  unsigned char vendor_name[17];
  unsigned char vendor_part_num[17];
  unsigned char vendor_rev[5];
  unsigned char vendor_sn[17];
  unsigned char date_code[9];
  unsigned char eeprom_addr;

  if (sfp_module_present(self))
  {
/* Note: the IIC transceiver core only has a depth of 16 bytes !!! */
/*       ==> With the standard driver there should be no transfers */
/*       of more than 16 Bytes !!!                                 */

#if 0
    {
      int j;
      unsigned char buff[128];

      print("--------------------\r\n");
      print("SFP EEPROM Contents \r\n");
      print("--------------------\r\n");

      iic_ptr->iic_read(SFP_EEPROM_ADDR, 0, buff, 128);
      print_frame(buff, 128);

      for( j=0;j<128;j++)
      {
          char c;
          if (j>0 && j%20 == 0) xfs_printf("\n\r");
          c = ((buff[j]>=32) && ( buff[j]<128)) ? buff[j] : ' ';
          xfs_printf("%c",c);
      }
      print("\n\r--------------------\r\n");
    }
#endif

    /* sfp eeprom infos */

    /* write phy-register address */
    eeprom_addr = 20;
    XIicPs_MasterSendPolled(self->iic_dev_ptr, &eeprom_addr, 1, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    XIicPs_MasterRecvPolled(self->iic_dev_ptr, vendor_name, 16, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    vendor_name[16] = 0;

    eeprom_addr = 40;
    XIicPs_MasterSendPolled(self->iic_dev_ptr, &eeprom_addr, 1, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    XIicPs_MasterRecvPolled(self->iic_dev_ptr, vendor_part_num, 16, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    vendor_part_num[16] = 0;

    eeprom_addr = 56;
    XIicPs_MasterSendPolled(self->iic_dev_ptr, &eeprom_addr, 1, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    XIicPs_MasterRecvPolled(self->iic_dev_ptr, vendor_rev, 4, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    vendor_rev[4] = 0;

    eeprom_addr = 68;
    XIicPs_MasterSendPolled(self->iic_dev_ptr, &eeprom_addr, 1, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    XIicPs_MasterRecvPolled(self->iic_dev_ptr, vendor_sn, 16, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    vendor_sn[16] = 0;

    eeprom_addr = 84;
    XIicPs_MasterSendPolled(self->iic_dev_ptr, &eeprom_addr, 1, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    XIicPs_MasterRecvPolled(self->iic_dev_ptr, date_code, 8, IIC_ADDRESS(SFP_EEPROM_ADDR));
    sfp_ctrl_wait_until_not_busy(self, SFP_CTRL_IIC_WAIT_NOT_BUSY_TIMEOUT);
    date_code[8] = 0;

    xfs_printf("SFP module: %s %s %s %s %s\n\r",vendor_name, vendor_part_num, vendor_rev, vendor_sn, date_code);
  }
  else
  {
    xfs_printf("SFP module: not present\n\r");
  }
}

/******************************************************************************/

void sfp_marvell_phy_init(sfp_ctrl_type *self)
{
  xfs_u16 reg;

  usleep(5000); /* wait_loop about 5 ms for phy ready ! */

/*
After hardware reset, write the following registers:
Reg29 = 0x0006
Reg30: bit 15:14 = ‘01’ (other bits in this register should not be changed)
Reg29 = 0x000a
Reg30: bit 5 =’0’ (other bits in this register should not be changed)
Apply software reset (0.15=’1’)
*/

/* Errata for Marvell 88E1111 B0: */
  sfp_marvell_reg_write (self, 29, 0x0006);
  reg = sfp_marvell_reg_read(self, 30);
  reg &= 0x3fff;
  reg |= 0x4000;
  sfp_marvell_reg_write (self, 30, reg);

  sfp_marvell_reg_write (self, 29, 0x000A);
  reg = sfp_marvell_reg_read(self, 30);
  reg &= 0xffdf;
  sfp_marvell_reg_write (self, 30, reg);
  sfp_marvell_reg_write (self, 0x00, 0x8140);  /* software reset */
  usleep(5000);

  /* enable 1000 Base-X auto negotiation */
  sfp_marvell_reg_write (self, 0x16, 0x0001);  /* 27 */
  sfp_marvell_reg_write (self, 0x00, 0x9140);  /*  reg 9 */
  sfp_marvell_reg_write (self, 0x16, 0x0000);  /* 0 */
  usleep(5000);

  /* set sgmii to copper with auto negociation */
  sfp_marvell_reg_write (self, 0x1b, 0x9084);  /* 27 */
  sfp_marvell_reg_write (self, 0x09, 0x0F00);  /*  reg 9 */
  sfp_marvell_reg_write (self, 0x00, 0x8140);  /* 0 */
  usleep(5000);

  sfp_marvell_reg_write (self, 0x04, 0x0DE1);  /* reg 4 */
  sfp_marvell_reg_write (self, 0x00, 0x9140);  /* 0 */
  usleep(5000);
}

/******************************************************************************/

int is_marvell_phy(sfp_ctrl_type *self)
{
  usleep(15000);
  /* marvell_reg_dump(); */
  if (sfp_marvell_reg_read(self, 2) != 0x0141) return 0;
  if ((sfp_marvell_reg_read(self, 3) & 0xfff0) != 0x0CC0) return 0;
  return 1;
}

/******************************************************************************/

xfs_u32 sfp_module_present(sfp_ctrl_type *self)
{
  return XGpioPs_ReadPin(self->emio_dev_ptr, self->sfp_mod_def0_pin);
}

/******************************************************************************/

void sfp_check_connection(sfp_ctrl_type *self)
{
  XTime tic_now;

  if (self->sfp_insert_delay==0) XTime_GetTime(&self->sfp_insert_delay);

  if (sfp_module_present(self))
  {
    /* sfp inserted */
    if (!self->sfp_inserted)
    {
      XTime_GetTime(&tic_now);
      if( (tic_now-self->sfp_insert_delay) > 100*(COUNTS_PER_SECOND/1000) ) /* wait_loop 500 ms */
      {
         sfp_module_info(self);
         self->sfp_inserted = 1;

         if (is_marvell_phy(self))
         {
           sfp_marvell_phy_init(self);
           self->marvell_phy = 1;
           /*xilinx_an_enable(); */
         }
         else
         {
           xfs_printf("---- non marvell phy ----\n\r");
           /*xilinx_an_disable(); */
           self->marvell_phy = 0;
         }

      }
    }
  }
  else
  {
    /* no sfp inserted */
    if (self->sfp_inserted) xfs_printf("---- sfp removed ----\n\r");

    self->sfp_inserted = 0;
    XTime_GetTime(&self->sfp_insert_delay);
  }

/*  if (self->sfp_inserted)
 *  {
 *    if (++an_check > 10)
 *    {
 *      check_an_interrupt();
 *      an_check = 0;
 *    }
 *  }
 */
}

/******************************************************************************/

/* LED functions. Might be added later. */

/*  void gpio_link_up(sfp_ctrl_type *self, unsigned int state)
 *  {
 *    if (state)
 *    {
 *      gpio_ptr->set(gpio_reg_nr,  sfp_link_up_bit);
 *    }
 *    else
 *    {
 *      gpio_ptr->clr(gpio_reg_nr,  sfp_link_up_bit);
 *    }
 *  }
 */

/******************************************************************************/

/* void gpio_sfp_error(sfp_ctrl_type *self, unsigned int state)
 * {
 *   if (state)
 *   {
 *     gpio_ptr->set(gpio_reg_nr,  sfp_error_bit);
 *   }
 *   else
 *   {
 *     gpio_ptr->clr(gpio_reg_nr,  sfp_error_bit);
 *   }
 * }
 */

/******************************************************************************/
