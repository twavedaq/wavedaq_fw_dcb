/******************************************************************************
*
* Copyright (C) 2013 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/
/*****************************************************************************/
/**
* @file si5324.c
*
* This file programs si5324 chip which generates clock for the peripherals.
*
* Please refer to Si5324 Datasheet for more information
* http://www.silabs.com/Support%20Documents/TechnicalDocs/Si5324.pdf
*
* Tested on Zynq ZC706 platform
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who  Date   Changes
* ----- ---- -------- ---------------------------------------------------------
* 1.0   srt  10/19/13 Initial Version
*
* </pre>
*
******************************************************************************/

/***************************** Include Files *********************************/
#include "xparameters.h"
#if defined (__arm__) || defined(__aarch64__)
#if XPAR_GIGE_PCS_PMA_SGMII_CORE_PRESENT == 1 || \
  XPAR_GIGE_PCS_PMA_1000BASEX_CORE_PRESENT == 1
#include "xfs_printf.h"
#include "xspips.h"
#include "sleep.h"
#include "xscugic.h"
/************************** Constant Definitions *****************************/
#define SPI_SLAVE_NR_SI3524   0

#define SI5324_NR_OF_CTRL_REGS   43
#define SI5324_NR_OF_STAT_REGS    5
#define SI5324_NR_OF_REGS          (SI5324_NR_OF_CTRL_REGS+SI5324_NR_OF_STAT_REGS)

#define SI5324_CMD_SET_ADDR    0x00
#define SI5324_CMD_WRITE       0x40
#define SI5324_CMD_WRITE_INC   0x60
#define SI5324_CMD_READ        0x80
#define SI5324_CMD_READ_INC    0x90
/**************************** Type Definitions *******************************/
typedef struct
{
  XSpiPs        *spi_if_ptr;
  unsigned char slave_nr;
} si5324_ctrl_type;

typedef struct
{
  unsigned char address;	/* Register address */
  unsigned char data;		  /* Register value */
} si5324_reg_type;

/************************** Function Prototypes *****************************/


/************************* Global Definitions *****************************/
/*
 * These configuration values generates 125MHz clock
 * For more information please refer to Si5324 Datasheet.
 */
si5324_reg_type si5324_reg_init[] = {
  {  0, 0x54},  /* Register 0 */
  {  1, 0xE4},  /* Register 1 */
  {  2, 0x32},  /* Register 2 */
  {  3, 0x15},  /* Register 3 */
  {  4, 0x92},  /* Register 4 */
  {  5, 0xED},  /* Register 5 */
  {  6, 0x2D},  /* Register 6 */
  {  7, 0x2A},  /* Register 7 */
  {  8, 0x00},  /* Register 8 */
  {  9, 0xC0},  /* Register 9 */
  { 10, 0x08},  /* Register 10 */
  { 11, 0x40},  /* Register 11 */
  { 19, 0x29},  /* Register 19 */
  { 20, 0x3E},  /* Register 20 */
  { 21, 0xFF},  /* Register 21 */
  { 22, 0xDF},  /* Register 22 */
  { 23, 0x1F},  /* Register 23 */
  { 24, 0x3F},  /* Register 24 */
  { 25, 0x60},  /* Register 25 */
  { 31, 0x00},  /* Register 31 */
  { 32, 0x00},  /* Register 32 */
  { 33, 0x05},  /* Register 33 */
  { 34, 0x00},  /* Register 34 */
  { 35, 0x00},  /* Register 35 */
  { 36, 0x05},  /* Register 36 */
  { 40, 0xC2},  /* Register 40 */
  { 41, 0x22},  /* Register 41 */
  { 42, 0xDF},  /* Register 42 */
  { 43, 0x00},  /* Register 43 */
  { 44, 0x77},  /* Register 44 */
  { 45, 0x0B},  /* Register 45 */
  { 46, 0x00},  /* Register 46 */
  { 47, 0x77},  /* Register 47 */
  { 48, 0x0B},  /* Register 48 */
  { 55, 0x00},  /* Register 55 */
  {131, 0x1F},  /* Register 131 */
  {132, 0x02},  /* Register 132 */
  {136, 0x00},  /* Register 136 */
  {137, 0x01},  /* Register 137 */
  {138, 0x0F},  /* Register 138 */
  {139, 0xFF},  /* Register 139 */
  {142, 0x00},  /* Register 142 */
  {143, 0x00}   /* Register 143 */
};

/************************** Function Definitions *****************************/
void si5324_write(si5324_ctrl_type *self, unsigned char addr, unsigned char tx_data)
{
  unsigned char tx_buffer[2];

  tx_buffer[0] = SI5324_CMD_SET_ADDR;
  tx_buffer[1] = addr;

  XSpiPs_SetSlaveSelect(self->spi_if_ptr, self->slave_nr);
  XSpiPs_PolledTransfer(self->spi_if_ptr, tx_buffer, NULL, 2);

  tx_buffer[0] = SI5324_CMD_WRITE;
  tx_buffer[1] = tx_data;

  XSpiPs_PolledTransfer(self->spi_if_ptr, tx_buffer, NULL, 2);
}

int ProgramSi5324(void)
{
  XSpiPs             spi_eclk_lmk_adc;
  XSpiPs_Config      *spi_cfg;
  si5324_ctrl_type   si5324_ctrl;
  int Status;

  si5324_ctrl.spi_if_ptr = &spi_eclk_lmk_adc;
  si5324_ctrl.slave_nr = SPI_SLAVE_NR_SI3524;

  spi_cfg = XSpiPs_LookupConfig(XPAR_PS7_SPI_1_DEVICE_ID);
  if (NULL == spi_cfg)
  {
    xfs_printf("FSBL> SPI ECLK-LMK-ADC Error: configuration lookup failed\r\n");
    return XST_FAILURE;
  }

  Status = XSpiPs_CfgInitialize(si5324_ctrl.spi_if_ptr, spi_cfg, spi_cfg->BaseAddress);
  if (Status != XST_SUCCESS)
  {
    xfs_printf("FSBL> SPI ECLK-LMK-ADC Error: initialization failed\r\n");
    return XST_FAILURE;
  }

  XSpiPs_SetOptions(si5324_ctrl.spi_if_ptr, XSPIPS_MASTER_OPTION | XSPIPS_FORCE_SSELECT_OPTION | XSPIPS_CLK_ACTIVE_LOW_OPTION | XSPIPS_CLK_PHASE_1_OPTION);
  XSpiPs_SetClkPrescaler(si5324_ctrl.spi_if_ptr, XSPIPS_CLK_PRESCALE_16);

  for(unsigned int i=0;i<SI5324_NR_OF_CTRL_REGS;i++)
  {
    si5324_write(&si5324_ctrl, si5324_reg_init[i].address, si5324_reg_init[i].data);
  }

  si5324_write(&si5324_ctrl, 136, 0x40);

  return XST_SUCCESS;
}
#endif
#endif