/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEG - DCB
 *
 *  Author  :  schmid_e
 *  Created :  21.09.2018 09:29:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/* system init uart */
/* system init cache */
/* see hello world app */

#include "xparameters.h"
#include "system.h"
#include "xstatus.h"
#include "xfs_printf.h"

/******************************************************************************/

#ifdef STDOUT_IS_16550
 #include "xuartns550_l.h"

 #define UART_BAUD 9600
#endif

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/
system_type system_hw;

/******************************************************************************/

void init_uart()
{
#ifdef STDOUT_IS_16550
    XUartNs550_SetBaud(STDOUT_BASEADDR, XPAR_XUARTNS550_CLOCK_HZ, UART_BAUD);
    XUartNs550_SetLineControlReg(STDOUT_BASEADDR, XUN_LCR_8_DATA_BITS);
#endif
    /* Bootrom/BSP configures PS7/PSU UART to 115200 bps */
}

/******************************************************************************/

int init_system()
{
  XGpioPs_Config *ConfigPtr;
  int Status;

  /*
   * If you want to run this example outside of SDK,
   * uncomment one of the following two lines and also #include "ps7_init.h"
   * or #include "ps7_init.h" at the top, depending on the target.
   * Make sure that the ps7/psu_init.c and ps7/psu_init.h files are included
   * along with this example source files for compilation.
   */
  /* ps7_init();*/
  /* psu_init();*/
  /*enable_caches();*/
  init_uart();

  ConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);
  Status = XGpioPs_CfgInitialize(SYSPTR(gpio_mio), ConfigPtr, ConfigPtr->BaseAddr);
  if (Status != XST_SUCCESS) {
    return XST_FAILURE;
  }

  return 0;
}

/******************************************************************************/
