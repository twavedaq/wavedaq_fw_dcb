#! /usr/bin/env python3
if __name__ == "__main__":
  import sys, os
  #import re
  import pandas as pd
  import numpy as np
  from datetime import datetime
  from shutil import copyfile

  ### Usage:
  ###
  ### csv2regconst.py <control xlsx path>
  ###
  ### <control csv path> is the path to the control register .csv file to be processed.
  ###
  ### <status csv path> is the path to the status register .csv file to be processed.
  ###
  ### <csv path> is the path to save the .vhd file.

  ###############################################################
  ###
  ### This script generates a VHDL file
  ### containing constans with register indices
  ### for a register bank based on a csv from
  ### Excel.
  ###
  ### 13.12.2022:
  ### Migration to python 3.6.8
  ###
  ###############################################################


###################################################################################################
# File and Path Checks/Settings
###################################################################################################

  ### Checking input file parameters
  if os.path.isabs(sys.argv[1]):
    path = sys.argv[1]
  else:
    path = os.path.abspath('.')+os.sep+sys.argv[1]

  indir = os.path.dirname(path)
  infile = os.path.basename(path)

  outdir = indir + os.sep + "script_output"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  sheetname = "DCB Registers"

  vhd_pkg_outfile = "ipif_user_cfg.vhd"
  vhd_outfile     = "axi_dcb_register_bank_v1_0.vhd"
  c_outfile       = "register_map_dcb.c"
  h_outfile       = "register_map_dcb.h"
  py_outfile      = "dcb_regs.py"
  class_outfile   = "DCBReg.h"
  css_outfile     = "dcb_register_map.css"
  html_outfile    = "dcb_register_map.html"

  # Paths relative to GIT root directory
  ip_dir      = "dcb_vivado_hw/ip_repo/axi_dcb_register_bank_1.0"
  ip_dir      = ip_dir.replace('/', os.sep)
  ip_vhdl_dir = ip_dir + os.sep + "hdl"
  ip_doc_dir  = ip_dir + os.sep + "doc"

  c_h_dir_arm = "dcb_xsdk_workspace/dcb_app_sw/app_dcb_ctrl/src"
  c_h_dir_arm = c_h_dir_arm.replace('/', os.sep)

  # Paths relative to GIT parent directory (only applicable in main repository)
  c_h_dir_lin = "sw/dcb/app/src"
  c_h_dir_lin = c_h_dir_lin.replace('/', os.sep)
  c_h_dir_wds = "sw/include"
  c_h_dir_wds = c_h_dir_wds.replace('/', os.sep)

  py_dir = "sw/scripts"
  py_dir = py_dir.replace('/', os.sep)

  class_dir = "sw/include"
  class_dir = class_dir.replace('/', os.sep)

  sw_doc_dir = "sw/doc"
  sw_doc_dir = sw_doc_dir.replace('/', os.sep)

  doc_dir = "doc/fw/dcb"
  doc_dir = doc_dir.replace('/', os.sep)

###################################################################################################
# Generic constants
###################################################################################################

  bits_per_reg  = 32

  extra_name_len = 12

  extra_assign_target_name_len = extra_name_len + 27

  date_str = datetime.now().strftime('%d.%m.%Y %H:%M:%S')

###################################################################################################
# Helper Functions
###################################################################################################

  def hex_prefix(cval):
    """Convert value to hex-string with '0x' prefix"""
    if cval:
        return "0x" + cval
    return cval

  def x_to_bool(cval):
    """Convert value to boolean """
    if cval:
        return True
    return False

  def binary_to_int(cval):
    """Convert hex-value to integer"""
    return int(cval, 0)

  def upper_case(cval):
    """Convert string to upper case"""
    return cval.upper()

  def range_to_vec_def(row):
    """The function range_to_vec_info(row) checks the vector range of a vhd port and corrects
      its direction if necessary. It also defines to type as scalar or vector."""
    if pd.notna(row['Range_Hi']):
      row['Range_Hi'] = int(row['Range_Hi'])
      if pd.notna(row['Range_Lo']):
        row['Range_Lo'] = int(row['Range_Lo'])
        row['D_Type'] = 'std_logic_vector'
        if row['Range_Hi'] < row['Range_Lo']:
          row['Range_Hi'], row['Range_Lo'] = row['Range_Lo'], row['Range_Hi']
        row['Bitmask'] = (2**(row['Range_Hi']-row['Range_Lo']+1)-1) << row['Range_Lo']
      else:
        row['Range_Lo'] = row['Range_Hi']
        row['D_Type'] = 'std_logic'
        row['Bitmask'] = 1 << row['Range_Hi']
      #row['Bitmask'] = f"0x{row['bitmask']:08X}"
    else:
      row['D_Type'] = np.nan

  def underscore_to_camelcase(name):
    return ''.join(w.capitalize() for w in name.split('_'))

  max_reg_name_len  = 0
  max_port_name_len = 0
  max_func_name_len = 0

  def reg_designator(reg_name):
    return f"DCB_REG_{reg_name}"
  def calc_reg_des_max_len():
    return max_reg_name_len + 8

  def bit_reg_designator(port_name):
    return f"DCB_{port_name}_REG"
  def calc_bit_reg_des_max_len():
    return max_port_name_len + 8

  def bit_msk_designator(port_name):
    return f"DCB_{port_name}_MASK"
  def calc_bit_msk_des_max_len():
    return max_port_name_len + 9

  def bit_ofs_designator(port_name):
    return f"DCB_{port_name}_OFS"
  def calc_bit_ofs_des_max_len():
    return max_port_name_len + 8

  def bit_const_designator(port_name):
    return f"DCB_{port_name}_CONST"
  def calc_bit_const_des_max_len():
    return max_port_name_len + 10

  def find_vcs_root(test):
      import subprocess
      import os
      cwd = os.getcwd()
      os.chdir(test)
      result = subprocess.check_output(["git", "rev-parse", "--show-toplevel"])
      os.chdir(cwd)
      result = result.rsplit('\n',1)[0]
      if os.path.isdir(result):
        return result
      else:
        return None
#  def find_vcs_root(test, dirs=(".git",), default=None):
#      import os
#      prev, test = None, os.path.abspath(test)
#      while prev != test:
#          if any(os.path.isdir(os.path.join(test, d)) for d in dirs):
#              return test
#          prev, test = test, os.path.abspath(os.path.join(test, os.pardir))
#      return default

  def copy_file_to_target(src, dst):
    src_dir  = os.path.dirname(src)
    src_file = os.path.basename(src)
    dst_dir  = os.path.dirname(dst)
    dst_file = os.path.basename(dst)
    if not os.path.exists(dst_dir):
      create_folder = input("Destination path does not exist. Shall I creat it? ([y]/n): ")
      if (not create_folder) or (create_folder == 'y') or (create_folder == "yes"):
        os.makedirs(dst_dir)
      else:
        return
    copyfile(src, dst)
    print(f"From {src}\nto   {dst}")

###################################################################################################
# Headers, Comments and Constant Code
###################################################################################################

  vhd_pkg_header_start = """---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  ipif_user_cfg.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)\n"""

  vhd_pkg_header_date = "--  Created :  " + date_str

  vhd_pkg_header_end = """\n--
--  Description :  Mapping of the register content of WaveDream2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------\n"""

  vhd_pkg_header = vhd_pkg_header_start + vhd_pkg_header_date + vhd_pkg_header_end

  vhd_pkg_content_start = """library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package ipif_user_cfg is

  ---------------------------------------------------------------------------
  -- General Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_WIDTH         : integer := 32;
  constant  C_SLV_AWIDTH        : integer := 32;
  constant  C_SLV_DWIDTH        : integer := 32;

  ---------------------------------------------------------------------------
  -- Register Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_UNDEFINED     : std_logic_vector(C_REG_WIDTH-1 downto 0) := X"DEADBEEF";
  constant  C_DEFAULT_ZERO      : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_BIT_ALL        : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '1');
  constant  C_WR_BIT_NONE       : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_PULSE_NONE     : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');

  constant  C_WR_EXT_NO         : integer := 0;
  constant  C_WR_EXT_YES        : integer := 1;

  constant  C_REG_SELF          : integer := -1;
  constant  C_REGISTER_REG_READ : boolean := false;


  ---------------------------------------------------------------------------
  -- register description
  ---------------------------------------------------------------------------

  -- Define special register types, default is C_REG_RW, possible types:
  -- C_REG_RW  : Read / Write register
  -- C_REG_R_W : Seperate Read (from Input) and Write (to output)
  -- C_REG_RO  : Read Only register
  -- C_REG_WR  : WRite register,               reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_SET : SET bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_CLR : CLR bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- set as record (<reg_type>, <reg_num>, <default>, <writable mask>),
  --   e.g. C_REG_CTRL_SET => (C_BIT_SET, C_REG_CTRL)
  --   <reg_num> only used for C_BIT_SET and C_BIT_CLR, otherwise set to C_REG_SELF

  type  reg_type is (C_REG_RW, C_REG_R_W, C_REG_RO, C_REG_WR, C_BIT_SET, C_BIT_CLR);

  type  reg_descr_record is
        record
          typ     : reg_type;
          reg     : integer;
          def     : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_msk  : std_logic_vector(C_REG_WIDTH-1 downto 0);
          pls_msk : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_ext  : integer;
        end record;

  type  reg_descr_type is array (natural range <>) of reg_descr_record;

  constant  C_REG_DESCR_DEFAULT : reg_descr_type(0 to 1) :=
  (        --  type         set/clr num       default           writable mask    write pulse only   external overwrite
    others => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,   C_WR_EXT_NO)
  );

  ---------------------------------------------------------------------------
  -- User register definition
  ---------------------------------------------------------------------------

"""

  vhd_pkg_content_end = """
  ---------------------------------------------------------------------------
  -- register user interface signals
  ---------------------------------------------------------------------------

  type  reg_array is array (natural range <>) of std_logic_vector(C_REG_WIDTH-1 downto 0);

  type  user_to_reg_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_ext           :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  type  reg_to_user_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
          rd_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  -------------------------------------------------------------------------------------------
  -- User memory definition
  -------------------------------------------------------------------------------------------

  constant  C_MEM_DEPTH                  : integer              := 9;
  constant  C_NUM_MEM                    : integer              := 0;


  type  user_to_mem_type is
        record
          enable  : std_logic;
          wr_en   : std_logic;
          addr    : std_logic_vector(8 downto 0);
          data    : std_logic_vector(31 downto 0);
        end record;


  type  mem_to_user_type is
        record
          data    : std_logic_vector(31 downto 0);
        end record;

  ---------------------------------------------------------------------------
  -- User interface signals to top level
  ---------------------------------------------------------------------------

  constant  C_NUM_CS : integer  := (1 + C_NUM_MEM);

  type  user_to_ipif_type is
        record
          reg    : user_to_reg_type;
          mem    : user_to_mem_type;
        end record;


  type  ipif_to_user_type is
        record
          rst    : std_logic;
          reg    : reg_to_user_type;
          mem    : mem_to_user_type;
        end record;


end;


--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body ipif_user_cfg is

end package body;
"""

  vhd_header_start = """---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  axi_dcb_register_bank_v1_0.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)\n"""

  vhd_header_date = "--  Created :  " + date_str

  vhd_header_end = """\n--
--  Description :  Toplevel of the MEGII DCB register bank.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------\n"""

  vhd_header = vhd_header_start + vhd_header_date + vhd_header_end

  vhd_entity_start = """library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;
use axi_dcb_reg_bank_v1_0.ipif_axi;

entity axi_dcb_register_bank_v1_0 is
  generic (
    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH  : integer           := 32;
    C_S00_AXI_ADDR_WIDTH  : integer           := 32;
    C_S00_AXI_BASEADDR    : std_logic_vector := X"FFFFFFFF";
    C_S00_AXI_HIGHADDR    : std_logic_vector := X"00000000";
    C_S00_AXI_MIN_SIZE    : std_logic_vector := X"FFFFFFFF";
    -- Users parameters
    C_USE_WSTRB           : integer          := 0;
    C_DPHASE_TIMEOUT      : integer          := 8;
    C_FAMILY              : string           := "virtex6"
    --C_MEM0_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM0_HIGHADDR       : std_logic_vector := X"00000000";
    --C_MEM1_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM1_HIGHADDR       : std_logic_vector := X"00000000"
  );
  port (
    -- -------------------------------------------
    -- User Ports                               --
    -- -------------------------------------------"""

  vhd_entity_end = """\n    -- -------------------------------------------
    -- Ports of Axi Slave Bus Interface S00_AXI --
    -- -------------------------------------------
    s00_axi_aclk     : in std_logic;
    s00_axi_aresetn  : in std_logic;
    s00_axi_awaddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot   : in std_logic_vector(2 downto 0);
    s00_axi_awvalid  : in std_logic;
    s00_axi_awready  : out std_logic;
    s00_axi_wdata    : in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb    : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid   : in std_logic;
    s00_axi_wready   : out std_logic;
    s00_axi_bresp    : out std_logic_vector(1 downto 0);
    s00_axi_bvalid   : out std_logic;
    s00_axi_bready   : in std_logic;
    s00_axi_araddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot   : in std_logic_vector(2 downto 0);
    s00_axi_arvalid  : in std_logic;
    s00_axi_arready  : out std_logic;
    s00_axi_rdata    : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp    : out std_logic_vector(1 downto 0);
    s00_axi_rvalid   : out std_logic;
    s00_axi_rready   : in std_logic
  );
end axi_dcb_register_bank_v1_0;\n\n"""

  vhd_architecture_start = """architecture arch_imp of axi_dcb_register_bank_v1_0 is

  ---------------------------------------------------------------------------
  -- interface signal definitions
  ---------------------------------------------------------------------------
  signal user_to_ipif     : user_to_ipif_type;
  signal ipif_to_user     : ipif_to_user_type;

begin

  ------------------------------------------------------
  -- register definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  -- Register Bit <-> I/O Mapping\n"""

  vhd_architecture_end = """\n--  ------------------------------------------------------
--  -- memory definition is done in ipif_user_cfg.vhdl
--  ------------------------------------------------------
--
--  user_to_ipif.mem.addr   <= core_mem_address;
--  user_to_ipif.mem.enable <= '1';
--  user_to_ipif.mem.wr_en  <= '0';
--  user_to_ipif.mem.data   <= (others => '0');

  ------------------------------------------------------------------------------
  -- instantiate AXI IP Interface
  ------------------------------------------------------------------------------

  ipif_axi_inst : entity axi_dcb_reg_bank_v1_0.ipif_axi
  generic map
  (
    -- Bus protocol parameters, do not add to or delete
    C_S_AXI_DATA_WIDTH  => C_S00_AXI_DATA_WIDTH,
    C_S_AXI_ADDR_WIDTH  => C_S00_AXI_ADDR_WIDTH,
    C_S_AXI_MIN_SIZE    => C_S00_AXI_MIN_SIZE,
    C_USE_WSTRB         => C_USE_WSTRB,
    C_BASEADDR          => C_S00_AXI_BASEADDR,
    C_HIGHADDR          => C_S00_AXI_HIGHADDR,
    C_SLV_AWIDTH        => C_S00_AXI_ADDR_WIDTH,
    C_SLV_DWIDTH        => C_S00_AXI_DATA_WIDTH,
    C_DPHASE_TIMEOUT    => C_DPHASE_TIMEOUT,
    C_FAMILY            => C_FAMILY
    --C_MEM0_BASEADDR     => C_MEM0_BASEADDR,
    --C_MEM0_HIGHADDR     => C_MEM0_HIGHADDR,
    --C_MEM1_BASEADDR     => C_MEM1_BASEADDR,
    --C_MEM1_HIGHADDR     => C_MEM1_HIGHADDR
  )
  port map
  (
    -- user ports for ip interface
    USER_TO_IPIF_I        => user_to_ipif,
    IPIF_TO_USER_O        => ipif_to_user,
    -- Bus protocol ports, do not add to or delete
    S_AXI_ACLK            => s00_axi_aclk,
    S_AXI_ARESETN         => s00_axi_aresetn,
    S_AXI_AWADDR          => s00_axi_awaddr,
    S_AXI_AWVALID         => s00_axi_awvalid,
    S_AXI_WDATA           => s00_axi_wdata,
    S_AXI_WSTRB           => s00_axi_wstrb,
    S_AXI_WVALID          => s00_axi_wvalid,
    S_AXI_BREADY          => s00_axi_bready,
    S_AXI_ARADDR          => s00_axi_araddr,
    S_AXI_ARVALID         => s00_axi_arvalid,
    S_AXI_RREADY          => s00_axi_rready,
    S_AXI_ARREADY         => s00_axi_arready,
    S_AXI_RDATA           => s00_axi_rdata,
    S_AXI_RRESP           => s00_axi_rresp,
    S_AXI_RVALID          => s00_axi_rvalid,
    S_AXI_WREADY          => s00_axi_wready,
    S_AXI_BRESP           => s00_axi_bresp,
    S_AXI_BVALID          => s00_axi_bvalid,
    S_AXI_AWREADY         => s00_axi_awready
  );

  -- Check if necessary:
  -- S_AXI_AWPROT  => s00_axi_awprot,
  -- S_AXI_ARPROT  => s00_axi_arprot,

end arch_imp;\n"""

  c_h_header_start = """/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEGII - DCB
 *
 *  Author  :  schmid_e (Author of generation script)\n"""
  c_h_header_date = " *  Created :  " + date_str
  c_h_header_end = """\n *
 *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */\n\n"""

  h_header_def_incl = """#ifndef __REGISTER_MAP_DCB_H__
#define __REGISTER_MAP_DCB_H__

#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS
#include "drv_axi_dcb_reg_bank.h"
#endif

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/\n\n"""

  c_header_incl = """#include "register_map_dcb.h"\n\n"""

  h_header = c_h_header_start + c_h_header_date + c_h_header_end + h_header_def_incl
  c_header = c_h_header_start + c_h_header_date + c_h_header_end + c_header_incl

  c_restore = """\n/******************************************************************************/
/* Register Restore                                                          */
/******************************************************************************/\n\n"""

  c_default = """\n/******************************************************************************/
/* Register Defaults                                                          */
/******************************************************************************/\n\n"""

  h_end = """\n/******************************************************************************/

#endif /* __REGISTER_MAP_DCB_H__ */

/******************************************************************************/\n"""

  c_reg_mapping = """\n/******************************************************************************/
/* Register Mapping                                                           */
/******************************************************************************/\n\n"""

  c_bit_mapping = """\n/******************************************************************************/
/* Bit Mapping                                                                */
/******************************************************************************/\n\n"""

  c_end = """\n/******************************************************************************/\n"""

  py_header_start = """# #####################################################################################
#  Paul Scherrer Institut
# #####################################################################################
#
#  Project :  MEGII - DCB
#
#  Author  :  schmid_e (Author of generation script)\n"""
  py_header_date = "#  Created :  " + date_str
  py_header_end = """\n#
#  Description :  Register map definitions.
#
# #####################################################################################
# #####################################################################################

# ###############################################################################
# # definitions                                                                ##
# ###############################################################################\n\n"""

  py_header = py_header_start + py_header_date + py_header_end

  py_end = """\n# #############################################################################
# #############################################################################\n"""

  class_header_start = """//
//  DCBReg.h
//
//  MEGII - DCB Register Access Class
//
//  This file is generated automatically, please do not edit!
//\n"""
  class_header_date = "// Created :  " + date_str
  class_header_end = """\n//\n

#ifndef __DCBREG_H__
#define __DCBREG_H__

#include "register_map_dcb.h"

//--------------------------------------------------------------------

class DCBREG {

private:
   // virtual bit functions, must be implemented in derived class
   virtual unsigned int BitExtract(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual void SetRegMask(unsigned int reg, unsigned int mask, unsigned int ofs, unsigned int v) = 0;

public:

   // constructor
   DCBREG(){};\n\n"""

  class_header = class_header_start + class_header_date + class_header_end

  class_end = """\n};\n\n//--------------------------------------------------------------------

#endif /* defined(__DCBREG_H__) */\n"""

  html_header = f"""<!DOCTYPE html>
<!-- Created :   {date_str} -->
<html>

<head>
  <link href="dcb_register_map.css" rel="stylesheet" type="text/css">
  <title>DCB Register Map</title>
</head>

<body>
  <h1>DCB Register Map</h1>
  Generated : {date_str}<br><br>
"""

  html_end = """
</body>

</html>
"""

  css_content = f"""/* Created :   {date_str} */
html *
{{
   font-family: Arial, Helvetica, sans-serif !important;
}}

table {{
  border-collapse: collapse;
  border: none;
}}

th, td {{
  padding-top: 3px;
  padding-bottom: 3px;
  padding-left: 10px;
  padding-right: 10px;
  border: 1px solid black;
}}

tr.reg_header {{
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}}

tr.reg_name {{
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}}

td.reg_table_title, tr.reg_table_title {{
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
}}

td.back_to_overview, tr.back_to_overview {{
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
  border-top: 3px solid black;
}}

tbody tr.reg_list:nth-child(even) {{
  background: rgb(185,205,230);
  border-left:  3px solid black;
  border-right: 3px solid black;
}}

tbody tr.reg_list:nth-child(odd) {{
  background: rgb(150,180,215);
  border-left:  3px solid black;
  border-right: 3px solid black;
}}

tbody tr.reg_content:nth-child(even) {{
  background: rgb(170,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}}

tbody tr.reg_content:nth-child(odd) {{
  background: rgb(220,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}}

a:link {{
  color: rgb(0,0,0);
  text-decoration: none;
}}

a:visited {{
  color: rgb(20,20,20);
}}

a:hover {{
  color: rgb(235,235,235);
}}

a:active {{
  color: rgb(255,255,0);
}}
"""

  ip_name = "axi_dcb_register_bank_v1_0"

  #print vhd_pkg_header + vhd_entity_start + "\n\n" + vhd_entity_end + "\n\n" + vhd_architecture_end

  ### Reading Register Information
  print(f"Reading {indir}{os.sep}{infile}")

  with pd.ExcelFile(indir+os.sep+infile) as xl:
    reg_map_df = xl.parse(sheetname, header=1, skipfooter=6, converters={'#':int, 'Offset':hex_prefix, 'Pulse':x_to_bool, 'SPI WR':x_to_bool, 'Restore':x_to_bool, 'Default':binary_to_int, 'Name':upper_case, 'SW Parameter':int})

  print("Generating register list\n")

  reg_map_df = reg_map_df.drop(columns=['Offset_dec', 'Unnamed: 14'])   # Drup unused columns
  reg_map_df.rename(columns = {'#':'Reg_Nr', 'HW/SW':'HW_SW', 'R/W':'R_W', 'SPI WR':'SPI_WR', 'SW Function':'SW_Function', 'SW Parameter':'SW_Parameter'}, inplace=True)   # Rename columns to simplify processing

  ### Create dataframe with registers
  registers_df = reg_map_df[reg_map_df['Offset'].notnull()]   # Drop bit definitions
  registers_df = registers_df.drop(columns=['R_W', 'Pulse', 'Bit', 'Remark'])   # Drup unused columns
  registers_df.reset_index(inplace=True)
  reg_count = len(registers_df.index)

  # Find maximum register name length
  max_reg_name_len = registers_df.Name.map(len).max()
  print(f'max_reg_name_len = {max_reg_name_len}')

  # Find maximum port name length
  max_port_name_len = reg_map_df[reg_map_df['Offset'].isnull()].Name.map(len).max()
  print(f'max_port_name_len = {max_port_name_len}')

  # Find maximum function name length
  max_func_name_len = registers_df.SW_Function.map(len).max()
  print(f'max_func_name_len = {max_func_name_len}')

  ### Create dataframe with ports
  reg_map_df.Reg_Nr.fillna(method='ffill', inplace=True)   # Fill empty register number with preceeding register number
  ports_df = reg_map_df[reg_map_df['Offset'].isnull()]    # Drop registe definition rows

  # Create vector parameters from 'Bit' column and append it to the dataframe
  vec_def_df = ports_df['Bit'].astype('str').str.split(':', n=1, expand=True)   # Extract 'Bit' column and split at ':'
  test = ports_df.copy()
  vec_def_df.rename(columns = {0:'Range_Hi', 1:'Range_Lo'}, inplace=True)   # Name the two new columns given by the split
  vec_def_df['D_Type'] = ''
  vec_def_df['Bitmask'] = ''
  vec_def_df.apply(range_to_vec_def, axis=1)   # Process each row with range_to_vec_def function
  vec_def_df['Bitmask'] = vec_def_df['Bitmask'].astype('uint')   # Convert column to unsigned int
  ports_df = pd.concat([ports_df, vec_def_df], axis=1)   # Append the new columns to the existing dataframe
  #ports_df['Range_Hi'] = ports_df['Range_Hi'].astype('int')   # Convert column to int
  #ports_df['Range_Lo'] = ports_df['Range_Lo'].astype('int')   # Convert column to int

  # Add columns for port postfix and direction according to read/write spec
  ports_df['Postfix'] = '_I'
  ports_df.loc[ports_df['R_W'].str.contains('W|w'),'Postfix'] = '_O'
  ports_df['Dir'] = 'in '
  ports_df.loc[ports_df['R_W'].str.contains('W|w'),'Dir'] = 'out'

  ports_df = ports_df.drop(columns=['Offset', 'HW_SW', 'Bit', 'SW_Function', 'SW_Parameter', 'Remark'])   # Drup obsolete columns

  # Group the ports list by register number
  ports_group = ports_df.groupby('Reg_Nr')
  # For each 'Reg_Nr' in the 'ports_df' sum the 'bitmask' entries if 'R_W' is 'W' or 'RW' and write the resulting sum into 'regsiters_df'
  registers_df['Write_Mask'] = ports_group.apply(lambda x: x.loc[ (x['R_W'].str.upper() == 'W') | (x['R_W'].str.upper() == 'RW') ]['Bitmask'].sum())
#  registers_df['Write_Mask'] = ports_group.apply(lambda x: x.loc[ x['R_W'].str.contains('W|w') ]['Bitmask'].sum())
#  registers_df['Write_Mask'] = registers_df['Write_Mask'].fillna(0)   # Replace NaN with 0
#  registers_df['Write_Mask'] = registers_df['Write_Mask'].astype('uint')   # Convert column to unsigned int
  # For each 'Reg_Nr' in the 'ports_df' sum the 'bitmask' entries if 'Pulse' is True and write the resulting sum into 'regsiters_df'
  registers_df['Pulse_Mask'] = ports_group.apply(lambda x: x.loc[ x['Pulse'] ]['Bitmask'].sum())
#  registers_df['Pulse_Mask'] = registers_df['Pulse_Mask'].fillna(0)   # Replace NaN with 0
#  registers_df['Pulse_Mask'] = registers_df['Pulse_Mask'].astype('uint')   # Convert column to unsigned int

  default_reg_df = registers_df[registers_df.Name != "CRC32_REG_BANK"]

  # Check for duplicated register names
  register_duplicates = registers_df['Name'].duplicated()
  if register_duplicates.any():
    print("\n!!! ERROR !!!   Duplicate register names found:\n")
    print(registers_df[register_duplicates].Name)
    print("\nQUITTING EXECUTION\n\n")
    quit()

  # Check for duplicated port names ignoring "reserved"
  valid_ports_df = ports_df[ ports_df['Name'].str.lower() != 'reserved' ]
  port_duplicates = valid_ports_df['Name'].duplicated()
  if port_duplicates.any():
    print("\n!!! ERROR !!!   Duplicate port names found:\n")
    print(valid_ports_df[port_duplicates].Name)
    print("\nQUITTING EXECUTION\n\n")
    quit()

  # calculate maximum lengths
  reg_des_max_len       = calc_reg_des_max_len()
  bit_reg_des_max_len   = calc_bit_reg_des_max_len()
  bit_msk_des_max_len   = calc_bit_msk_des_max_len()
  bit_ofs_des_max_len   = calc_bit_ofs_des_max_len()
  bit_const_des_max_len = calc_bit_const_des_max_len()

#  ### create ip directory structure
#  def gen_ip_dirs():
#    print "Creating ip_dir structure..."
#    if not os.path.exists(ip_dir):
#      os.makedirs(ip_dir)
#    if not os.path.exists(ip_vhdl_dir):
#      os.makedirs(ip_vhdl_dir)
#    if not os.path.exists(ip_data_dir):
#      os.makedirs(ip_data_dir)


  # ###### vhd pkg file functions ###########################################################################################

  def wrlines_vhd_pkg_reg_numbers(reg, of):
    reg_name = f'{reg.Name.upper():<{max_reg_name_len}}'
    outline = f"  constant C_REG_{reg_name} : integer := {reg.Reg_Nr:3d};\n"
    of.write(outline)

  def write_vhd_pkg_reg_numbers(out_file):
    outline = "\n  -- register numbers\n"
    out_file.write(outline)
    registers_df.apply(wrlines_vhd_pkg_reg_numbers, of=out_file, axis=1)
    const_name = f'{"NUM_REG":<{max_reg_name_len+4}}'
    outline = f"\n  constant C_{const_name} : integer := {reg_count:3d};\n"
    out_file.write(outline)

    #-- not yet used
    #  constant  C_REG_CTRL_PULSE_MASK   : std_logic_vector(C_REG_WIDTH-1 downto 0) := x"000000FF";

  def wrlines_vhd_pkg_reg_config(reg, of):
    reg_name = f'{reg.Name.upper():<{max_reg_name_len}}'
    outline = f"    C_REG_{reg_name} => ("

    if reg.Write_Mask and not("SW" in reg.HW_SW):
      if reg_name.startswith( 'SET_' ):
        outline += "C_BIT_SET,    "
      elif reg_name.startswith( 'CLR_' ):
        outline += "C_BIT_CLR,    "
      else:
        outline += "C_REG_RW,     "
    else:
        outline += "C_REG_RO,     "

    if reg_name.startswith( 'SET_' ):
      ref_reg = reg.Name.upper()[4:]
      ref_reg = f'{f"{ref_reg},":<{max_reg_name_len+1}}'
    elif reg_name.startswith( 'CLR_' ):
      ref_reg = reg.Name.upper()[4:]
      ref_reg = f'{f"{ref_reg},":<{max_reg_name_len+1}}'
    else:
      ref_reg = f'{"SELF,":<{max_reg_name_len+1}}'
    outline += f"C_REG_{ref_reg}"
    if "SW" in reg.HW_SW:
      outline += "X\"DEADBEEF\",      "
    else:
      outline += f"X\"{reg.Default:08X}\",      "
    if "SW" in reg.HW_SW or np.isnan(reg.Write_Mask):
      outline += "X\"00000000\",     "
    else:
      outline += f"X\"{reg.Write_Mask:08X}\",     "
    outline += f"X\"{reg.Pulse_Mask:08X}\",       "
    if reg.SPI_WR:
      outline += "C_WR_EXT_YES"
    else:
      outline += "C_WR_EXT_NO "
    outline += "),\n"
    of.write(outline)

  def write_vhd_pkg_reg_config(out_file):
    outline = "\n  -- register description\n"
    out_file.write(outline)
    outline = "  constant  C_REG_DESCR     : reg_descr_type(0 to C_NUM_REG-1) :=\n"
    out_file.write(outline)
    outline  = "  ( "
    outline += f'{" ":<{max_reg_name_len+7}}'
    outline += "--  type          "
    outline += f'{"set/clr num":<{max_reg_name_len+7}}'
    outline += "default           writable mask    write pulse only   external write\n"
    out_file.write(outline)
    registers_df.apply(wrlines_vhd_pkg_reg_config, of=out_file, axis=1)

    # "others" line
    outline  = "    "
    outline += f'{"others":<{max_reg_name_len+6}}'
    outline += " => ("
    outline += "C_REG_RW,     "
    outline += f'{"C_REG_SELF,":<{max_reg_name_len+7}}'
    outline += "X\"00000000\",      "
    outline += "X\"FFFFFFFF\",     "
    outline += "X\"00000000\",       "
    outline += "C_WR_EXT_NO )\n"
    out_file.write(outline)
    # terminate configuration table
    outline = "  );\n"
    out_file.write(outline)

  # #########################################################################################################################

  # ###### vhd file functions ###############################################################################################

  # vhd entity
  def wrlines_reg_ports_vhd_entity(port, of, reg_data):
    if "RESERVED" not in port.Name.upper():
      # Omit registers for reserved bits
      if ("HW" in reg_data.HW_SW) and not( not(np.isnan(port.Default)) and port.R_W == "R"):
        # Only generate IOs for hardware registers
        # Omit inputs if there is a "Default" value for read-only registers (constant register value defined in register table)
        port_name = f'{port.Name.upper()+port.Postfix:<{max_port_name_len+extra_name_len}}'
        outline = f"    {port_name} : {port.Dir} "
        port_vec = f"{port.D_Type}"
        if port.Range_Hi != port.Range_Lo:
          port_vec = port_vec + f"({port.Range_Hi-port.Range_Lo} downto 0)"
        port_vec = f'{f"{port_vec};":<}'
        outline += port_vec + "\n"
        of.write(outline)

  def wrlines_regs_vhd_entity(reg, of):
    current_reg = reg.Reg_Nr.iloc[0]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = f"\n    -- Register {current_reg} [{reg_data.Offset}]: {reg_data.Name.upper()}\n"
    of.write(outline)
    if "HW" in reg_data.HW_SW:
      # Generate one read/write strobe port pair for each register
      reg_name = f'{f"{reg_data.Name.upper()}_REG_RD_STROBE_O":<{max_port_name_len+extra_name_len}}'
      outline = f"    {reg_name} : out std_logic;\n"
      of.write(outline)
      reg_name = f'{f"{reg_data.Name.upper()}_REG_WR_STROBE_O":<{max_port_name_len+extra_name_len}}'
      outline = f"    {reg_name} : out std_logic;\n"
      of.write(outline)
    else:
      outline = "    -- '--> Pure software register => no ports available\n"
      of.write(outline)
    reg.apply(wrlines_reg_ports_vhd_entity, of=of, reg_data=reg_data, axis=1)

  def write_vhd_entity(out_file):
    # write all ports with comments per register
    out_file.write(vhd_entity_start)
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_vhd_entity, of=out_file)
    out_file.write(vhd_entity_end)

  # vhd signal mappings
  def wrlines_reg_ports_vhd_signal_mappings(port, of, reg_data):
    if "RESERVED" not in port.Name:
      if port.Range_Hi == port.Range_Lo:
        port_range = f"({port.Range_Hi})"
      else:
        if (port.Range_Hi == bits_per_reg-1) and (port.Range_Lo == 0):
          port_range = ""
        else:
          port_range = f"({port.Range_Hi} downto {port.Range_Lo})"
      if "W" in port.R_W:
        # Case control (RW) port: assign register content to output
        port_name = f'{port.Name+port.Postfix:<{max_port_name_len+extra_assign_target_name_len}}'
        outline = f"  {port_name} <= ipif_to_user.reg.data(C_REG_{reg_data.Name}){port_range};\n"
        of.write(outline)
      else:
        # Case status (R) port: assign input or constant to register content
        # generate constant
        if not(np.isnan(port.Default)):
          port_initial = port.Default
          nr_of_bits = port.Range_Hi-port.Range_Lo+1
          if nr_of_bits == 1:
            port_initial = f"'{port_initial}'"
          elif nr_of_bits % 4 == 0:
            port_initial = f'x\"{port_initial:0{int(nr_of_bits/4)}X}\"'
          else:
            port_initial = f'\"{port_initial:0{nr_of_bits}b}\"'
        if "in" in port.Dir:
          # Actual assignment (R)
          signal_name = f"user_to_ipif.reg.data(C_REG_{reg_data.Name}){port_range}"
          signal_name = f'{signal_name:<{max_port_name_len+extra_assign_target_name_len}}'
          outline = f"  {signal_name}"
          if not(np.isnan(port.Default)):
            # Assign constant
            outline += f" <= {port_initial};\n"
          else:
            # Assign port
            outline += f" <= {port.Name+port.Postfix};\n"
        of.write(outline)

  def wrlines_regs_vhd_signal_mappings(reg, of):
    current_reg = reg.Reg_Nr.iloc[0]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = f"\n  -- Register {current_reg} [{reg_data.Offset}]: {reg_data.Name}\n"
    of.write(outline)
    if "HW" in reg_data.HW_SW:
      port_name = f'{f"{reg_data.Name}_REG_RD_STROBE_O":<{max_port_name_len+extra_assign_target_name_len}}'
      outline = f"  {port_name} <= ipif_to_user.reg.rd_strobe(C_REG_{reg_data.Name});\n"
      of.write(outline)
      port_name = f'{f"{reg_data.Name}_REG_WR_STROBE_O":<{max_port_name_len+extra_assign_target_name_len}}'
      outline = f"  {port_name} <= ipif_to_user.reg.wr_strobe(C_REG_{reg_data.Name});\n"
      of.write(outline)
      reg.apply(wrlines_reg_ports_vhd_signal_mappings, of=of, reg_data=reg_data, axis=1)
    else:
      outline = "  -- '--> Pure software register => no ports assigned\n"
      of.write(outline)

  def write_vhd_signal_mappings(out_file):
    out_file.write(vhd_architecture_start)
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_vhd_signal_mappings, of=out_file)
    out_file.write(vhd_architecture_end)

  # #########################################################################################################################

  # ###### h file functions #################################################################################################

#  def bit_mask(port):
#    bit_mask_pattern = ""
#    for i in range(31,-1,-1):
#      if i in range(port[i_port_range_lo],port[i_port_range_hi]+1):
#        bit_mask_pattern = bit_mask_pattern + '1'
#      else:
#        bit_mask_pattern = bit_mask_pattern + '0'
#    bit_mask_pattern = f"0x{int(bit_mask_pattern, 2):0>8X}"
#    return bit_mask_pattern

  def wrlines_h_reg_offsets(reg, of):
#    last_reg = int(reg.Reg_Nr)
    reg_name = f'{reg_designator(reg.Name):<{reg_des_max_len+extra_name_len}}'
    outline = f"#define {reg_name}   {reg.Offset}\n"
    of.write(outline)

  def write_h_reg_offsets(out_file):
    outline = "/*\n * Register Offsets\n */\n\n"
    out_file.write(outline)
#    last_reg = 0
    registers_df.apply(wrlines_h_reg_offsets, of=out_file, axis=1)

  def wrlines_h_bit_parameters(port, of, reg_data):
    if ("RESERVED" not in port.Name):
      outline = f"/* {port.Name} - {port.Comment} */\n"
      of.write(outline)
      port_name = f'{bit_reg_designator(port.Name):<{bit_reg_des_max_len+1}}'
      port_reg  = f'{reg_designator(reg_data.Name):>{reg_des_max_len}}'
      outline = f"#define {port_name}   {port_reg}\n"
      of.write(outline)
      port_name = f'{bit_msk_designator(port.Name):<{bit_msk_des_max_len}}'
#      bit_pattern = bit_mask(port)
#      bit_pattern = f'{bit_pattern:>{reg_des_max_len}}'
      bit_pattern = f'0x{port.Bitmask:08X}'
      outline = f"#define {port_name}   {bit_pattern:>{reg_des_max_len}}\n"
      of.write(outline)
      port_name = f'{bit_ofs_designator(port.Name):<{bit_ofs_des_max_len+1}}'
      port_ofs  = f'{port.Range_Lo:>{reg_des_max_len}}'
      outline = f"#define {port_name}   {port_ofs}\n"
      of.write(outline)
      if ( not(np.isnan(port.Default)) and ("W" not in port.R_W) ):
        hex_len = int((port.Range_Hi-port.Range_Lo+3)/4)
        port_name = f'{bit_const_designator(port.Name):<{bit_const_des_max_len-1}}'
        port_const = f"0x{port.Default:0{hex_len}X}"
        port_const = f'{port_const:>{reg_des_max_len}}'
        outline = f"#define {port_name}   {port_const}\n"
        of.write(outline)
      outline = "\n"
      of.write(outline)

  def wrlines_regs_h_bit_parameters(reg, of):
    current_reg = reg.Reg_Nr.iloc[-1]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = f"\n\n/* ****** Register {current_reg} [{reg_data.Offset}]: {reg_data.Name} - {reg_data.Comment} (Default: 0x{reg_data.Default:08X}) ****** */\n\n"
    of.write(outline)
    reg.apply(wrlines_h_bit_parameters, of=of, reg_data=reg_data, axis=1)

  def write_h_bit_parameters(out_file):
    outline = "\n/*\n * Bit Positions\n */"
    out_file.write(outline)
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_h_bit_parameters, of=out_file)

  def write_h_total_nr_of_regs(out_file):
    outline = "\n/*\n * Number of Registers\n */\n\n"
    out_file.write(outline)
    outline = f"#define NR_OF_REGS          {reg_count:d}\n\n"
    out_file.write(outline)

  def write_h_register_access_declarations(out_file):
    outline  = "typedef unsigned int (*dcb_reg_func)(unsigned int, unsigned int, unsigned int, unsigned int);\n\n"
    outline += "typedef struct\n"
    outline += "{\n"
    outline += "  dcb_reg_func  func;\n"
    outline += "  unsigned int par;\n"
    outline += "} dcb_reg_func_type;\n\n"
    outline += "extern const dcb_reg_func_type  dcb_reg_func_list[];\n\n"
    out_file.write(outline)

  def wrlines_c_register_access_functions(reg, of):
    outline = f'{f"  {{ {reg.SW_Function}":<{max_func_name_len+4}}'
    outline += f" , {reg.SW_Parameter} }}"
    if reg.Reg_Nr != reg_count-1:
      outline += ','
    else:
      outline += ' '
    outline += f"   /* register {reg.Reg_Nr:d} [{reg.Offset}]: {reg.Name} */\n"
    of.write(outline)

  def write_c_register_access_functions(out_file):
    outline = "\nconst dcb_reg_func_type  dcb_reg_func_list[] = {\n"
    out_file.write(outline)
    registers_df.apply(wrlines_c_register_access_functions, of=out_file, axis=1)
    outline = "};\n\n"
    out_file.write(outline)

  def write_h_register_mapping_declarations(out_file):
    outline = "\n"
    outline += "typedef struct {\n"
    outline += "  const char * name;\n"
    outline += "  const unsigned int reg;\n"
    outline += "  const unsigned int read_only;\n"
    outline += "} dcb_reg_entry_type;\n\n\n"
    outline += "typedef struct {\n"
    outline += "  const char * name;\n"
    outline += "  const unsigned int reg;\n"
    outline += "  const unsigned int mask;\n"
    outline += "  const unsigned int ofs;\n"
    outline += "} dcb_bit_group_entry_type;\n\n"
    out_file.write(outline)
    outline  = "extern const dcb_reg_entry_type  dcb_reg_list[];\n"
    outline += "extern const dcb_bit_group_entry_type  dcb_bit_group_list[];\n"
    outline += "extern const unsigned char reg_restore[];\n"
    outline += "extern const unsigned int reg_default[];\n\n"
    out_file.write(outline)
    outline  = "#define DCB_WRITABLE_REG     0\n"
    outline += "#define DCB_READONLY_REG     1\n\n"
    outline += "#define DCB_DONT_TOUCH_REG   0\n"
    outline += "#define DCB_RESTORE_REG      1\n\n"
    out_file.write(outline)

  def wrlines_c_mapping_lists(reg, of):
    reg_name = f'  {{ "{reg.Name}"'
    outline = f'{reg_name:<{max_reg_name_len+6}}'
    outline += f'{f" , {reg_designator(reg.Name)}":<{reg_des_max_len+3}}'
    if reg.Write_Mask:
      outline += ", DCB_WRITABLE_REG"
    else:
      outline += ", DCB_READONLY_REG"
    outline += " },\n"
    of.write(outline)

  def wrlines_c_port_mapping_lists(port, of):
    if "RESERVED" not in port.Name:
      port_name = f'  {{ "{port.Name}"'
      outline =  f'{port_name:<{max_port_name_len+6}}'
      outline += f'{f" , {bit_reg_designator(port.Name)}":<{bit_reg_des_max_len+3}}'
      outline += f'{f" , {bit_msk_designator(port.Name)}":<{bit_msk_des_max_len+3}}'
      outline += f'{f" , {bit_ofs_designator(port.Name)}":<{bit_ofs_des_max_len+3}}'
      outline += " },\n"
      of.write(outline)

  def write_c_mapping_lists(out_file):
    out_file.write(c_reg_mapping)
    outline = "const dcb_reg_entry_type  dcb_reg_list[] = {\n"
    out_file.write(outline)
    registers_df.apply(wrlines_c_mapping_lists, of=out_file, axis=1)
    outline = f'{"  { (const char*)0":<{max_reg_name_len+6}}'
    outline += f'{" , 0":<{reg_des_max_len+3}}'
    outline += ", 0               "
    outline += " }\n};\n"
    out_file.write(outline)

    # list with bit mappings
    out_file.write(c_bit_mapping)
    outline = "const dcb_bit_group_entry_type  dcb_bit_group_list[] = {\n"
    out_file.write(outline)
    ports_df.apply(wrlines_c_port_mapping_lists, of=out_file, axis=1)
    outline =  f'{"  { (const char*)0":<{max_port_name_len+6}}'
    outline += f'{" , 0":<{bit_reg_des_max_len+3}}'
    outline += f'{" , 0":<{bit_msk_des_max_len+3}}'
    outline += f'{" , 0":<{bit_ofs_des_max_len+3}}'
    outline += " }\n};\n"
    out_file.write(outline)

  def wrlines_c_register_restore(reg, of, last_reg):
    if reg.Restore:
      outline = "  DCB_RESTORE_REG   "
    else:
      outline = "  DCB_DONT_TOUCH_REG"
    if reg.Reg_Nr == last_reg:
      outline += " "
    else:
      outline += ","
    outline += f"   /* Offset {reg.Offset} */\n"
    of.write(outline)

  def write_c_register_restore(out_file):
    out_file.write(c_restore)
    outline = "const unsigned char reg_restore[] = {\n"
    out_file.write(outline)
    registers_df.apply(wrlines_c_register_restore, of=out_file, last_reg=registers_df.Reg_Nr.iloc[-1], axis=1)
    outline = "};\n\n"
    out_file.write(outline)

  def wrlines_c_register_defaults(reg, of, last_reg):
    outline = f"  0x{reg.Default:08X}"
    if reg.Reg_Nr != last_reg:
      outline += ","
    else:
      outline += " "
    outline += f"   /* Offset {reg.Offset} */\n"
    of.write(outline)

  def write_c_register_defaults(out_file):
    out_file.write(c_default)
    outline = "const unsigned int reg_default[] = {\n"
    out_file.write(outline)
    default_reg_df.apply(wrlines_c_register_defaults, of=out_file, last_reg=default_reg_df.Reg_Nr.iloc[-1], axis=1)
    outline = "};\n\n"
    out_file.write(outline)

  # #########################################################################################################################

  # ###### py file functions ################################################################################################

  def wrlines_py_reg_offsets(reg, of):
      reg_name = f'{reg_designator(reg.Name):<{reg_des_max_len+extra_name_len}}'
      outline = f"{reg_name}   = {reg.Offset}\n"
      of.write(outline)

  def write_py_reg_offsets(out_file):
    outline = "#\n# Register Offsets\n#\n\n"
    out_file.write(outline)
    registers_df.apply(wrlines_py_reg_offsets, of=out_file, axis=1)

  def wrlines_py_bit_parameters(port, of, reg_data):
    if ("RESERVED" not in port.Name):
      port_comment = port.Comment.replace("\n","\n# ")
      outline = f'# {port.Name} - {port_comment}\n'
      of.write(outline)
      port_name = f'{bit_reg_designator(port.Name):<{bit_reg_des_max_len+3}}'
      port_reg  = f'{reg_designator(reg_data.Name):>{max(reg_des_max_len, 10)}}'
      outline = f"{port_name} = {port_reg}\n"
      of.write(outline)
      port_name = f'{bit_msk_designator(port.Name):<{bit_msk_des_max_len+2}}'
      bit_pattern = f'0x{port.Bitmask:08X}'
      bit_pattern = f'{bit_pattern:>{max(reg_des_max_len, 10)}}'
      outline = f"{port_name} = {bit_pattern}\n"
      of.write(outline)
      port_name = f'{bit_ofs_designator(port.Name):<{bit_ofs_des_max_len+3}}'
      port_ofs  = f'{port.Range_Lo:>{max(reg_des_max_len, 10)}}'
      outline = f"{port_name} = {port_ofs}\n"
      of.write(outline)
      if ( not(np.isnan(port.Default)) and ("W" not in port.R_W) ):
        hex_len = int((port.Range_Hi-port.Range_Lo+4)/4)
        port_name = f'{bit_const_designator(port.Name):<{bit_const_des_max_len+1}}'
        port_const = f"0x{port.Default:0{hex_len}X}"
        port_const = f'{port_const:>{max(reg_des_max_len, 10)}}'
        outline = f"{port_name} = {port_const}\n"
        of.write(outline)
      outline = "\n"
      of.write(outline)

  def wrlines_regs_py_bit_parameters(reg, of):
    current_reg = reg.Reg_Nr.iloc[-1]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = f"\n# ****** Register {reg_data.Reg_Nr} [{reg_data.Offset}]: {reg_data.Name} - {reg_data.Comment} (Default: 0x{reg_data.Default:08X}) ******\n\n"
    of.write(outline)
    reg.apply(wrlines_py_bit_parameters, of=of, reg_data=reg_data, axis=1)

  def write_py_bit_parameters(out_file):
    outline = "\n#\n# Bit Positions\n#"
    out_file.write(outline)
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_py_bit_parameters, of=out_file)

  def write_py_total_nr_of_regs(out_file):
    outline = "\n\n# Number of Registers\n\n"
    out_file.write(outline)
    outline = f"REG_NR_OF_REGS          = {reg_count:d}\n"
    out_file.write(outline)

  def optimize_comment_string(in_str):
    out_str = in_str.replace("\n"," / ")
    out_str = out_str.replace('"','\\"')
    out_str = out_str.replace("'","\\'")
    return out_str

  def wrlines_py_port_mapping_lists(port, of):
    if ("RESERVED" not in port.Name):
      port_name = f'  ( "{port.Name}"'
      port_rw = f', "{port.R_W}"'
      outline =  f'{port_name:<{max_port_name_len+6}}'
      outline += f'{port_rw:<{7}}'
      outline += f'{f", {bit_reg_designator(port.Name)}":<{bit_reg_des_max_len+3}}'
      outline += f'{f", {bit_msk_designator(port.Name)}":<{bit_msk_des_max_len+3}}'
      outline += f'{f", {bit_ofs_designator(port.Name)}":<{bit_ofs_des_max_len+3}}'
      outline += f", \"{optimize_comment_string(port.Comment)}\""
      outline += " ),\n"
      of.write(outline)

  def wrlines_py_reg_mapping_lists(reg, of):
    reg_name = f'  ( "{reg.Name}"'
    outline = f'{reg_name:<{max_reg_name_len+6}}'
    outline += f'{f" , {reg_designator(reg.Name)}":<{reg_des_max_len+3}}'
    if reg.Write_Mask:
      outline += ", DCB_WRITABLE_REG"
    else:
      outline += ", DCB_READONLY_REG"
    outline = outline + f", \"{optimize_comment_string(str(reg.Comment))}\""
    outline += " ),\n"
    of.write(outline)

  def write_py_mapping_lists(out_file):
    # write list with register name-offset mapping
    outline = "\n\n\n# List of register names, offsets and writable info\n"
    out_file.write(outline)
    outline  = "\nDCB_WRITABLE_REG = False\n"
    outline += "DCB_READONLY_REG = True\n"
    out_file.write(outline)
    outline = "\ndcb_reg_list = (\n"
    out_file.write(outline)
    registers_df.apply(wrlines_py_reg_mapping_lists, of=out_file, axis=1)
    outline = ")\n"
    out_file.write(outline)
    # write list with bit mappings
    outline = "\n\n\n# List of bit names and parameters\n"
    out_file.write(outline)
    outline = "\ndcb_bit_group_list = (\n"
    out_file.write(outline)
    ports_df.apply(wrlines_py_port_mapping_lists, of=out_file, axis=1)
    outline = ")\n"
    out_file.write(outline)

  def wrlines_py_register_restore(reg, of, last_reg):
    if reg.Restore:
      outline = "DCB_RESTORE_REG   "
    else:
      outline = "DCB_DONT_TOUCH_REG"
    if reg.Reg_Nr == last_reg:
        outline += f")   # Offset {reg.Offset}\n"
    else:
        outline += f",   # Offset {reg.Offset}\n               "
    of.write(outline)

  def write_py_register_restore(out_file):
    outline = "\n\n\n#\n# Register Restore\n#\n\n"
    out_file.write(outline)
    outline = "DCB_DONT_TOUCH_REG = False\n"
    out_file.write(outline)
    outline  = "DCB_RESTORE_REG    = True\n\n"
    outline += "reg_restore = ("
    out_file.write(outline)
    registers_df.apply(wrlines_py_register_restore, of=out_file, last_reg=registers_df.Reg_Nr.iloc[-1], axis=1)

  def wrlines_py_register_defaults(reg, of, last_reg):
    outline = f"0x{reg.Default:08X}"
    of.write(outline)
    if reg.Reg_Nr == last_reg:
      outline = f")   # Offset {reg.Offset}\n\n"
    else:
      outline = f",   # Offset {reg.Offset}\n                    "
    of.write(outline)

  def write_py_register_defaults(out_file):
    outline  = "\n\n\n#\n# Register Defaults\n#\n\n"
    outline +=    "ctrl_reg_default = ("
    out_file.write(outline)
    default_reg_df.apply(wrlines_py_register_defaults, of=out_file, last_reg=default_reg_df.Reg_Nr.iloc[-1], axis=1)

  # #########################################################################################################################

  # ###### class file functions #############################################################################################

  def gen_class_get_function(port_name):
    name = underscore_to_camelcase(port_name)
    func_str =  f"   unsigned int Get{name}() {{ return BitExtract("
    func_str += f"{bit_reg_designator(port_name)}, "
    func_str += f"{bit_msk_designator(port_name)}, "
    func_str += f"{bit_ofs_designator(port_name)});"
    func_str += " };\n"
    return func_str

  def gen_class_set_function(port_name):
    name = underscore_to_camelcase(port_name)
    func_str =  f"   void         Set{name}(unsigned int value) {{ SetRegMask("
    func_str += f"{bit_reg_designator(port_name)}, "
    func_str += f"{bit_msk_designator(port_name)}, "
    func_str += f"{bit_ofs_designator(port_name)}, value);"
    func_str += " };\n"
    return func_str

  def wrlines_class_functions(port, of, reg_data):
    if "RESERVED" not in port.Name:
      port_comment = port.Comment.replace("\n","\n   // ")
      outline = f'   // 0x{port.Bitmask:08X}: {port.Name} - {port_comment}\n'
      of.write(outline)
      outline = gen_class_get_function(port.Name)
      of.write(outline)
      if "W" in port.R_W:
        outline = gen_class_set_function(port.Name) + "\n"
        of.write(outline)

  def wrlines_regs_class_functions(reg, of):
    current_reg = reg.Reg_Nr.iloc[-1]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = f"\n\n   ////// ------ Register {reg_data.Reg_Nr} [{reg_data.Offset}]: {reg_data.Name} - {reg_data.Comment} (Default: 0x{reg_data.Default:08X}) ------ //////\n\n"
    of.write(outline)
    reg.apply(wrlines_class_functions, of=of, reg_data=reg_data, axis=1)

  def write_class_functions(out_file):
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_class_functions, of=out_file)

  # #########################################################################################################################

  # ###### html file ########################################################################################################
  def bool_to_X(tag):
    if tag:
      return "X"
    return ""

  def wrlines_html_reg_list(reg, of):
      outline = "      <tr class=\"reg_list\">\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:right\" ><a href=\"#register_{reg.Reg_Nr:d}\" id=\"reg_list_{reg.Reg_Nr:d}\">{reg.Reg_Nr:d}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:right\" ><a href=\"#register_{reg.Reg_Nr:d}\">{reg.Offset}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:center\"><a href=\"#register_{reg.Reg_Nr:d}\">{reg.HW_SW}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:center\"><a href=\"#register_{reg.Reg_Nr:d}\">{bool_to_X(reg.SPI_WR)}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:center\"><a href=\"#register_{reg.Reg_Nr:d}\">{bool_to_X(reg.Restore)}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:right\" ><a href=\"#register_{reg.Reg_Nr:d}\">0x{reg.Default:08X}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:left\"  ><a href=\"#register_{reg.Reg_Nr:d}\">{reg.Name}</a></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:left\"  ><a href=\"#register_{reg.Reg_Nr:d}\">{reg.Comment}</a></td>\n"
      of.write(outline)
      outline = "      </tr>\n"
      of.write(outline)

  def write_html_reg_list(out_file):
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>SPI WR</th>\n        <th>Restore</th>\n        <th>Default</th>\n        <th>Name</th>\n        <th>Comment</th>\n      </tr>\n"
    out_file.write(outline)
    registers_df.apply(wrlines_html_reg_list, of=out_file, axis=1)
    outline = "      <tr class=\"back_to_overview\">"
    out_file.write(outline)
    outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#register_overview\">Registers Top</a></a></td>"
    out_file.write(outline)
    outline = "      </tr>\n    </tbody>\n  </table>"
    out_file.write(outline)

  def wrlines_html_reg_contents(port, of, reg):
      outline = "      <tr class=\"reg_content\">\n"
      of.write(outline)
      outline = "        <td style=\"text-align:center\"></td>\n"
      of.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      of.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      of.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      of.write(outline)
      outline = "        <td style=\"text-align:center\"></td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:center\"  >{port.R_W}</td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:center\"  >{bool_to_X(port.Pulse)}</td>\n"
      of.write(outline)
      if port.Range_Hi == port.Range_Lo:
        outline = f"        <td style=\"text-align:center\">{port.Range_Hi:d}</td>\n"
      else:
        outline = f"        <td style=\"text-align:center\">{port.Range_Hi:d}:{port.Range_Lo:d}</td>\n"
      of.write(outline)
      nr_of_bits = port.Range_Hi-port.Range_Lo+1
#      if "RESERVED" in port.Name or np.isnan(port.Default):
      if np.isnan(port.Default):
        port_initial = 0
      else:
        port_initial = port.Default
      if nr_of_bits % 4 != 0:
        port_initial = f'0b{int(port_initial):0{nr_of_bits}b}'
      else:
        port_initial = f'0x{int(port_initial):0{int(nr_of_bits/4)}X}'
      outline = f"        <td style=\"text-align:right\" >{port_initial}</td>\n"
      of.write(outline)
      outline = f"        <td style=\"text-align:left\"  >{port.Name}</td>\n"
      of.write(outline)
      if "RESERVED" in port.Name:
        outline = "        <td style=\"text-align:left\"  ></td>\n"
      else:
        port_comment = "<br>".join(port.Comment.split("\n"))
        outline = f'        <td style="text-align:left"  >{port_comment}</td>\n'
      of.write(outline)
      outline = "      </tr>\n"
      of.write(outline)

  def wrlines_regs_html_reg_contents(reg, of):
    current_reg = reg.Reg_Nr.iloc[-1]
    reg_data = registers_df.loc[registers_df.Reg_Nr == current_reg].iloc[0]
    outline = "      <tr class=\"reg_table_title\">\n"
    of.write(outline)
    outline = "        <td class=\"reg_table_title\" colspan=\"7\">"
    of.write(outline)
    outline = f"<a id=\"register_{reg_data.Reg_Nr}\">"
    of.write(outline)
    outline = f"<h3>Register {reg_data.Reg_Nr:d}: {reg_data.Name}</h3>"
    of.write(outline)
    outline = "</a></td>\n      </tr>\n"
    of.write(outline)
    outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>SPI WR</th>\n        <th>Restore</th>\n        <th>R/W</th>\n        <th>Pulse</th>\n        <th>Bit</th>\n        <th>Initial</th>\n        <th>Name</th>\n        <th>Comment</th>\n      </tr>\n"
    of.write(outline)
    outline = "      <tr class=\"reg_name\">\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:center\">{reg_data.Reg_Nr}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:right\" >{reg_data.Offset}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:center\">{reg_data.HW_SW}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:center\">{bool_to_X(reg_data.SPI_WR)}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:center\">{bool_to_X(reg_data.Restore)}</td>\n"
    of.write(outline)
    outline = "        <td style=\"text-align:center\"></td>\n"
    of.write(outline)
    outline = "        <td style=\"text-align:center\"></td>\n"
    of.write(outline)
    outline = "        <td style=\"text-align:center\"></td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:right\" >0x{reg_data.Default:08X}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:left\"  >{reg_data.Name}</td>\n"
    of.write(outline)
    outline = f"        <td style=\"text-align:left\"  >{reg_data.Comment}</td>\n"
    of.write(outline)
    outline = "      </tr>\n"
    of.write(outline)
    reg.apply(wrlines_html_reg_contents, of=of, reg=reg_data, axis=1)
    outline = "      <tr class=\"back_to_overview\">\n"
    of.write(outline)
    outline = f"        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#reg_list_{reg_data.Reg_Nr:d}\">Register Overview</a></td>\n      </tr>\n"
    of.write(outline)

  def write_html_reg_contents(out_file):
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    ports_group = ports_df.groupby('Reg_Nr')
    ports_group.apply(wrlines_regs_html_reg_contents, of=out_file)
    outline = "\n    </tbody>\n"
    out_file.write(outline)
    outline = "\n  </table>\n"
    out_file.write(outline)

  # #########################################################################################################################

  def write_vhd_pkg():
    outfile = vhd_pkg_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(vhd_pkg_header)
        out_file.write(vhd_pkg_content_start)

        write_vhd_pkg_reg_numbers(out_file)
        write_vhd_pkg_reg_config(out_file)

        out_file.write(vhd_pkg_content_end)

  def write_vhd():
    outfile = vhd_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(vhd_header)

        write_vhd_entity(out_file)

        write_vhd_signal_mappings(out_file)

  def write_c():
    outfile = c_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(c_header)

        outline = "#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS\n\n"
        out_file.write(outline)

        write_c_register_access_functions(out_file)

        outline = "#endif /* DCB_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
        out_file.write(outline)

        outline = "#ifndef DCB_DONT_INCLUDE_VARS\n"
        out_file.write(outline)

        write_c_mapping_lists(out_file)
        write_c_register_restore(out_file)
        write_c_register_defaults(out_file)

        outline = "#endif /* DCB_DONT_INCLUDE_VARS */\n\n"
        out_file.write(outline)

        out_file.write(c_end)

  def write_h():
    outfile = h_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(h_header)

        write_h_reg_offsets(out_file)
        write_h_bit_parameters(out_file)
        write_h_total_nr_of_regs(out_file)

        outline = "#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS\n\n"
        out_file.write(outline)

        write_h_register_access_declarations(out_file)

        outline = "#endif /* DCB_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
        out_file.write(outline)

        outline = "#ifndef DCB_DONT_INCLUDE_VARS\n"
        out_file.write(outline)

        write_h_register_mapping_declarations(out_file)

        outline = "#endif /* DCB_DONT_INCLUDE_VARS */\n\n"
        out_file.write(outline)

        out_file.write(h_end)

  def write_py():
    outfile = py_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(py_header)

        write_py_reg_offsets(out_file)
        write_py_bit_parameters(out_file)
        write_py_total_nr_of_regs(out_file)
        write_py_mapping_lists(out_file)
        write_py_register_restore(out_file)
        write_py_register_defaults(out_file)

        out_file.write(py_end)

  def write_class():
    outfile = class_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(class_header)

        write_class_functions(out_file)

        out_file.write(class_end)

  def write_html():
    outfile = html_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(html_header)

        write_html_reg_list(out_file)

        outline = "  <br>\n  <br>\n  <hr>\n  <hr>\n  <h2>Register Contents</h2>\n"
        out_file.write(outline)

        write_html_reg_contents(out_file)

        outline = f"\n  <br>\n  Generated :   {date_str}\n\n</body>\n\n</html>\n"
        out_file.write(outline)

  def write_css():
    outfile = css_outfile
    with open(outdir+os.sep+outfile,'w') as out_file:
        out_file.write(css_content)

  # #########################################################################################################################

  print("Writing temporary output files:")
  print(f"Writing {outdir}{os.sep}{vhd_pkg_outfile}...")
  write_vhd_pkg()
  print(f"Writing {outdir}{os.sep}{vhd_outfile}..")
  write_vhd()
  print(f"Writing {outdir}{os.sep}{css_outfile}...")
  write_css()
  print(f"Writing {outdir}{os.sep}{html_outfile}...")
  write_html()
  print(f"Writing {outdir}{os.sep}{c_outfile}...")
  write_c()
  print(f"Writing {outdir}{os.sep}{py_outfile}...")
  write_h()
  print(f"Writing {outdir}{os.sep}{py_outfile}...")
  write_py()
  print(f"Writing {outdir}{os.sep}{class_outfile}...")
  write_class()

  # Copy files to target directories?
  do_file_copy = input("\nCopy files to target directories? ([y]/n): ")
  if (not do_file_copy) or (do_file_copy == 'y') or (do_file_copy == "yes"):
    do_file_copy = 'y'
  else:
    do_file_copy = 'n'

  # Copy files to target directories!
  if do_file_copy == 'y':
    print("\n")
    #git_root = find_vcs_root(os.getcwd()) # to find git root with current working directory
    git_root = find_vcs_root(indir)
    if git_root == None:
      sys.exit("Error: GIT root not found.\n       Make sure .xlsx source is in a DCB GIT repository directory")
    else:
      print(f"Performing filecopy to target directories (GIT root = {git_root}):")
      print("Copy IP files ...")
      print(f"---\n{vhd_pkg_outfile} :")
      copy_file_to_target(outdir + os.sep + vhd_pkg_outfile,  git_root + os.sep + ip_vhdl_dir + os.sep + vhd_pkg_outfile)
      print(f"---\n{vhd_outfile} :")
      copy_file_to_target(outdir + os.sep + vhd_outfile,  git_root + os.sep + ip_vhdl_dir + os.sep + vhd_outfile)
      print(f"---\n{css_outfile} :")
      copy_file_to_target(outdir + os.sep + css_outfile,  git_root + os.sep + ip_doc_dir + os.sep + css_outfile)
      print(f"---\n{html_outfile} :")
      copy_file_to_target(outdir + os.sep + html_outfile, git_root + os.sep + ip_doc_dir + os.sep + html_outfile)
      print(f"---\n{c_outfile} :")
      copy_file_to_target(outdir + os.sep + c_outfile, git_root + os.sep + c_h_dir_arm + os.sep + c_outfile)
      print(f"---\n{h_outfile} :")
      copy_file_to_target(outdir + os.sep + h_outfile, git_root + os.sep + c_h_dir_arm + os.sep + h_outfile)
      #print("\nCopy baremetal ARM softwre files ...")
      print("\n--> Remember to update the IP in the block design <--\n")
    parent_git_root = find_vcs_root(f"{git_root}/..")
    if parent_git_root == None:
      sys.exit("Error: Parent GIT root not found.\n       Make sure .xlsx source is in a wavedaq_main GIT repository directory")
    else:
      print( "\nCopy Linux softwre files ...")
      print(f"---\n{c_outfile} :")
      copy_file_to_target(outdir + os.sep + c_outfile, parent_git_root + os.sep + c_h_dir_lin + os.sep + c_outfile)
      print(f"---\n{h_outfile} :")
      copy_file_to_target(outdir + os.sep + h_outfile, parent_git_root + os.sep + c_h_dir_lin + os.sep + h_outfile)
      print("\nCopy external host softwre files ...")
      print(f"---\n{c_outfile} :")
      copy_file_to_target(outdir + os.sep + c_outfile, parent_git_root + os.sep + c_h_dir_wds + os.sep + c_outfile)
      print(f"---\n{h_outfile} :")
      copy_file_to_target(outdir + os.sep + h_outfile, parent_git_root + os.sep + c_h_dir_wds + os.sep + h_outfile)
      print(f"---\n{class_outfile} :")
      copy_file_to_target(outdir + os.sep + class_outfile, parent_git_root + os.sep + class_dir + os.sep + class_outfile)
      print("\nCopy python script files ...")
      print(f"---\n{py_outfile} :")
      copy_file_to_target(outdir + os.sep + py_outfile, parent_git_root + os.sep + py_dir + os.sep + py_outfile)
      print("\nCopy software doc files ...")
      print(f"---\n{css_outfile} :")
      copy_file_to_target(outdir + os.sep + css_outfile, parent_git_root + os.sep + sw_doc_dir + os.sep + css_outfile)
      print(f"---\n{html_outfile} :")
      copy_file_to_target(outdir + os.sep + html_outfile, parent_git_root + os.sep + sw_doc_dir + os.sep + html_outfile)
      print("\nCopy documentation files ...")
      print(f"---\n{css_outfile} :")
      copy_file_to_target(outdir + os.sep + css_outfile, parent_git_root + os.sep + doc_dir + os.sep + css_outfile)
      print(f"---\n{html_outfile} :")
      copy_file_to_target(outdir + os.sep + html_outfile, parent_git_root + os.sep + doc_dir + os.sep + html_outfile)
      print("\nFilecopy completed !!!\n")
  else:
    print("Target files not updated")
